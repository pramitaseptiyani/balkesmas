<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="csrf_token" content="{{ csrf_token() }}" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/bootstrap/css/bootstrap.min.css') }}"/>    
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material/material.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/material_style.css') }}"/>
    <!-- sweet alert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/sweet-alert/sweetalert.min.css') }}"/>
    <!-- Theme Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/theme_style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/plugins.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/responsive.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/theme-color.css') }}"/>
    <!-- favicon -->
    <link rel="stylesheet" type="image/x-icon" href="{{ asset('assets/template/img/favicon.ico') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/jquery-tags-input/jquery-tags-input.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main/doctor/autocomplete.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2-bootstrap.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/> 

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom/styles.css') }}"/>

    <!-- Form -->
    <link href="{{ asset('assets/template/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/formlayout.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/theme-color.css') }}" rel="stylesheet" type="text/css" />
    <!-- -->

    <link href="{{ asset('assets/css/custom/styles.css') }}" rel="stylesheet" type="text/css" />

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <script>
      var base_path = "{{url('/')}}";
      var base_url = "{{url('/')}}";

      <?php 
          $domain = 'http://'.$_SERVER['HTTP_HOST']; 
      ?>
      var APIurl = "{{url('/')}}/";
  </script>
</head>
<body>
    <div class="page-content-wrapper">
      <div class="page-content">
          <div class="page-bar">
              <div class="page-title-breadcrumb">
                  <div class=" pull-left">
                      <div class="page-title">Tambah Pasien Keluarga Pegawai</div>
                  </div>
                  <ol class="breadcrumb page-breadcrumb pull-right">
                      <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                      </li>
                      <li class="active">Tambah Pasien Keluarga Pegawai</li>
                  </ol>
              </div>
          </div>

          <div id="overlay">
              <div class="cv-spinner">
                  <span class="spinner"></span>
              </div>
          </div>

              <div class="row">
                  <div class="col-sm-12">
                      <div class="card-box">
                          <div class="card-head">
                              <header>Informasi Pasien</header>
                              <button id = "panel-button" 
                                 class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                                 data-upgraded = ",MaterialButton">
                                 <i class = "material-icons">more_vert</i>
                              </button>
                              <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                 data-mdl-for = "panel-button">
                                 <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                 <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                 <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                              </ul>
                          </div>
                          <!-- <form method="POST" id="form-pasien" action="{{url('/receptionist/patient/tambah')}}"> -->
                          <div class="card-body row">
                              {{csrf_field()}}
                              <input type="hidden" name="tipe" value="family">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <label>Reference NIP
                                    <span class="required"></span>
                                  </label>
                                  <select id="search-nip" class="form-control">
                                    <option></option>
                                  </select>
                                </div>
                              </div>

                              <div class="row col-lg-12">
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="float-label">NIP
                                            <span class="required"></span>
                                          </label>
                                          <input id="nip" name="nip" type="text" class="form-control" placeholder="" readonly="">
                                      </div>
                                  </div>

                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="float-label">Satuan Kerja
                                            <span class="required"></span>
                                          </label>
                                          <input id="unit" name="unit" type="text" class="form-control" placeholder="" readonly="">
                                          <input id="satkerid" name="satkerid" type="hidden" class="form-control" placeholder="" readonly="">
                                      </div>
                                  </div>  
                              </div>

                              <br/>
                              <div class="col-lg-12"> 
                                  <hr class="new1">
                              </div>

                                      
                              <div class="row col-lg-12">
                                      <div class="col-lg-12 col-sm-12">
                                          <label class="float-label">Pilih Anggota Keluarga
                                            <span class="required"></span>
                                          </label>
                                      </div>
                                      <div class="col-lg-12 col-sm-12">
                                          <select id="select2-anggota" class="select2-anggota form-control select2" style="width: 100%"> 
                                          </select>
                                      </div>
                              </div>

                              <br/><br/><br/>
                              <div class="col-lg-12"> 
                                  <hr class="new1">
                              </div>

                              <div class="row col-lg-12">
                                  <div class="col-lg-6">
                                      <div class="col-lg-12 p-t-20">
                                        <label class="float-label">Hubungan dengan Pegawai
                                          <span class="required"></span>
                                        </label>
                                        <div class="radio p-0">
                                            <input type="radio" name="optionRelation" id="optionRelationPasangan" value="pasangan" checked>
                                            <label for="optionsRadios1">
                                                Suami/Istri
                                            </label>
                                        </div>
                                        <div class="radio p-0">
                                            <input type="radio" name="optionRelation" id="optionRelationAnak1" value="3">
                                            <label for="optionsRadios2">
                                                Anak Pertama
                                            </label>
                                        </div>
                                        <div class="radio p-0">
                                            <input type="radio" name="optionRelation" id="optionRelationAnak2" value="4">
                                            <label for="optionsRadios3">
                                                Anak Kedua
                                            </label>
                                        </div>
                                        <div class="radio p-0">
                                            <input type="radio" name="optionRelation" id="optionRelationAnak3" value="5">
                                            <label for="optionsRadios4">
                                                Anak Ketiga
                                            </label>
                                        </div>
                                      </div>

                                      <div class="col-lg-12 p-t-20"> 
                                        <div class="form-group">
                                          <label class="float-label">Nama Lengkap
                                            <span class="required"></span>
                                          </label>
                                          <input id="nama" name="nama" type="text" class="form-control" disabled>
                                        </div>
                                      </div>
                                      <div class="col-lg-12 p-t-20"> 
                                        <label class="float-label">Tanggal Lahir
                                          <span class="required"></span>
                                        </label>
                                        <input name="ttl" type="text" id="date" class="floating-label mdl-textfield__input"  placeholder="Tanggal Lahir" disabled>
                                      </div>
                                      <div class="col-lg-12 p-t-20">
                                        <label class="float-label">Jenis Kelamin
                                          <span class="required"></span>
                                        </label>
                                        <div class="radio p-0">
                                            <input type="radio" name="optionGender" id="optionGender" value="L" checked>
                                            <label for="optionsRadios1">
                                                Laki-laki
                                            </label>
                                        </div>
                                        <div class="radio p-0">
                                            <input type="radio" name="optionGender" id="optionGender" value="P">
                                            <label for="optionsRadios2">
                                                Perempuan
                                            </label>
                                        </div>
                                      </div>
                                      <div class="col-lg-12 p-t-20">
                                          <div class="form-group">
                                              <label class="float-label">No HP
                                                  <span class="required"></span>
                                              </label>
                                              <input id="nohp" name="nohp" type = "text" class="form-control"></input>
                                          </div> 
                                      </div>
                                      <div class="col-lg-12 p-t-20"> 
                                          <label class="float-label">Email
                                              <span class="required"></span>
                                          </label>
                                          <input name="email" class = "form-control" type = "email" id = "email">
                                          <span class = "mdl-textfield__error">Enter Valid Email Address!</span>
                                      </div>
                                      <div class="col-lg-12 p-t-20"> 
                                        <div class="form-group">
                                            <label>No MR lama
                                            </label>
                                            <input id="mr_lama" name="mr_lama" type = "mr_lama" class="form-control" placeholder="Not Set"></input>
                                        </div>
                                      </div>

                                      <div class="col-lg-12 p-t-20"> 
                                          <div class="form-group">
                                              <label class="float-label">BPJS</label>
                                              <input id="bpjs" name="bpjs" type = "text" class="form-control"></input>
                                          </div>
                                      </div>

                                      <div class="col-lg-12 p-t-20"> 
                                      <div class="form-group">
                                          <label>Alamat
                                            <span class="required"></span>
                                          </label>
                                          <textarea id="alamat" name="alamat" type="text" class="form-control" rows="3" placeholder="" readonly=""></textarea>
                                      </div>
                                  </div>
                                      
                                  </div>
                                  <div class="col-md-6">

                                      
                                      
                                      

                                      <div class="col-lg-12 p-t-20"> 
                                        <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                           <label class = "float-label" for = "riwayat_penyakit">Penyakit yang Pernah Diderita</label>
                                           <textarea name="riwayat_penyakit" class = "mdl-textfield__input float-input" rows =  "4" 
                                              id = "riwayat_penyakit" ></textarea>
                                           
                                        </div>
                                      </div>
                                      <div class="col-lg-12 p-t-20"> 
                                        <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                           <label class = " float-label" for = "riwayat_turunan">Penyakit Keluarga/Turunan</label>
                                           <textarea name="penyakit_turunan" class = "mdl-textfield__input float-input" rows =  "4" 
                                              id = "riwayat_turunan" ></textarea>
                                           
                                        </div>
                                      </div>
                                      <div class="col-lg-12 p-t-20"> 
                                        <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                           <label class = "float-label" for = "riwayat_alergi">Riwayat Alergi</label>
                                           <textarea name="riwayat_alergi" class = "mdl-textfield__input float-input" rows =  "4" 
                                              id = "riwayat_alergi" ></textarea>
                                           
                                        </div>
                                      </div>

                                      <div class="col-lg-12 p-t-20"> 
                                        <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                           <label class = " float-label" for = "riwayat_vaksinasi">Riwayat Vaksinasi</label>
                                           <textarea name="riwayat_vaksinasi" class = "mdl-textfield__input float-input" rows =  "4" 
                                              id = "riwayat_vaksinasi" ></textarea>
                                           
                                        </div>
                                      </div>



                                      <div class="col-lg-12 p-t-20">
                                          <span class="col-md-12">
                                              <label>Prioritas</label>
                                          </span>
                                          <span class="col-md-12">
                                              <input id="is-vip" type="checkbox" data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                         </span>
                                      </div>

                                      <br/>

                                  </div>
                              </div>
                              
                              
                              <div class="col-lg-12 p-t-20 text-center"> 
                                  <!-- <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" data-toggle="modal" data-target="#printCard">Simpan</button> -->
                                  <button id="simpan" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button>
                                  <a href="{{url ('receptionist/patient')}}" type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                              </div>
                          </div>

                          <div class="alert-form"></div>

                        <!-- </form> -->
                      </div>
                  </div>
              </div> 


              <!-- Print ID Card Modal -->
              <div class="modal fade" id="printCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLongTitle">Berhasil Menambah Pasien Baru</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div class="col-xl-12 col-md-12 col-lg-12">
                          <div class="info-box bg-success">
                            <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                            <div class="info-box-content">
                              <span class="info-box-text">No MR : SET90328392.02</span>
                              <span class="info-box-number">
                                Nama :
                                <span>Shaka Nagano</span>
                              </span>
                              <span class="progress-description">
                                <span></span>
                              </span>
                              
                            </div>
                            <!-- /.info-box-content -->
                          </div>
                          <!-- /.info-box -->
                        </div>
                        <div class="modal-footer">
                          <div class="profile-userbuttons" style="margin:auto">
                              <a href="#" class="btn btn-circle blue-bgcolor btn-md">Print Kartu</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>    
              </div>
              <!-- End of Print ID Card Modal -->
          </div>
      </div>

    <script src="{{ asset('assets/template/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/template/popper/popper.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.blockui.min.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/template/moment.min.js') }}"></script>
    <script src="{{ asset('assets/template/counterup/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/template/counterup/jquery.counterup.min.js') }}"></script>
    
    <!-- bootstrap -->
    <script src="{{ asset('assets/template/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/template/summernote/summernote.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-inputmask/bootstrap-inputmask.min.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-colorpicker/js/bootstrap-colorpicker-init.js') }}"></script>

    
    <!-- Common js-->
    <script src="{{ asset('assets/template/app.js') }}"></script>
    <script src="{{ asset('assets/template/layout.js') }}"></script>
    <script src="{{ asset('assets/template/theme-color.js') }}"></script>
    <script src="{{ asset('assets/template/profile.js') }}"></script>
    <!-- material -->
    <script src="{{ asset('assets/template/material/material.min.js') }}"></script>
    
    <!-- Sweet Alert -->
    <script src="{{ asset('assets/template/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/template/sweet-alert/sweet-alert-data.js') }}"></script>
    <!-- end Sweet Alert -->

    <!-- Javascript Helper -->
    <script src="{{ asset('assets/js/helper/helper.js') }}"></script>
    <!-- End of Javascript Helper -->

     <!-- Datetime Picker -->
    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js') }}"></script>

    <!-- End of Datetime Picker -->

    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

     <!--tags input-->
    <script src="{{ asset('assets/template/jquery-tags-input/jquery-tags-input.js') }}"></script>
    <script src="{{ asset('assets/template/jquery-tags-input/jquery-tags-input-init.js') }}"></script>

    <!--select2-->
    <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/template/select2/js/select2-init.js') }}"></script>

    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
        $('#search-nip').select2({
            placeholder: 'Ketikan NIP',
            theme: "classic",
            minimumInputLength: 3,
            ajax: {
                dataType: 'json',
                url: base_path+'/api_public/simpeg/get_pegawai',
                delay: 800,
                data: function(params) {
                    return {
                        nip: params.term
                    }
                },
                processResults: function (data, page) {
                    var list = [];
                    var result = data;
                    $.each(result, function(index,item){
                        list.push({
                            id      : item.nip,
                            text    : item.nip +" - "+ item.nama_pegawai,
                            nama    : item.nama_pegawai,
                            satker  : item.nama_satker,
                            satkerid  : item.kode_satker,
                            hp      : item.no_hp,
                            email   : item.email_dinas,
                            alamat  : item.alamat,
                            gender  : item.jenis_kelamin,
                            bpjs  : item.askes,
                        })
                    });
                    return {
                        results: list
                    };
                },
            }
        });

        $('#search-nip').on('select2:select', function (e) {
            var data = e.params.data;
            var satker = data.satker
            var unit = satker.substring(0, satker.indexOf("-"));         

            $("#nip").val(data.id);
            $("#unit").val(unit);
            $("#satkerid").val(data.satkerid);
            $("#hp").val(data.hp);
            $("#email").val(data.email);

            if(data.bpjs != null) $("#bpjs").html(data.bpjs);

            select2Anggota(data.id)
            onChangeAnggota();
        });

        $("#simpan").click(function(){

              var unit = $('#unit').val()
              var satkerid = $('#satkerid').val()
              var nip = $('#nip').val()
              var nama = $('#nama').val()
              var ttl = $('#date').val()
              var hp = $('#nohp').val()
              var email = $('#email').val()

              if(nama =='' || nip =='' || ttl =='' 
                  || hp =='' || email == ''){

                var alert_html = 
                          '<div class="add-alert alert alert-danger">'+
                            'Harap isi Required Field (bertanda *)'+  
                          '</div>';

                $('.alert-form').html(alert_html);
              }
              else{
                var tipe = $('input[type=radio][name=optionRelation]:checked').val()
                var jenis_kelamin = $('input[type=radio][name=optionGender]:checked').val()

                if(tipe == "pasangan"){
                  if(jenis_kelamin = "P"){
                    var id_detail_keluarga = 2
                  }
                  else var id_detail_keluarga = 1
                } 
                else var id_detail_keluarga = tipe

                var formData = {
                  "_token": "{{ csrf_token() }}",
                  'id_detail_keluarga'    : id_detail_keluarga,
                  'kategori': 'keluarga pegawai',
                  'nama'    : nama,
                  'id_card' : nip,
                  'ttl'     : ttl,
                  'hp'      : hp,
                  'email'   : email,
                  'unit'    : unit,
                  'bpjs'    : $('#bpjs').val(),
                  'alamat'  : $('#alamat').val(),
                  'jenis_kelamin' : jenis_kelamin,
                  'riwayat_penyakit'  : $('#riwayat_penyakit').val(),
                  'riwayat_turunan'   : $('#riwayat_turunan').val(),
                  'riwayat_alergi'    : $('#riwayat_alergi').val(),
                  'riwayat_vaksinasi' : $('#riwayat_vaksinasi').val(),
                  'satkerid' : $('#satkerid').val(),
                  'mr_lama' : $('#mr_lama').val(),
                  'is_vip' : $('#is-vip').is(':checked')
                }

                $("#overlay").fadeIn(300);　
                send(base_url+"/api_public/patient/tambah", formData, false, function(responseAdd){
                    $("#overlay").fadeOut(300);

                    sweetalert(
                      'success',
                      'Reservasi Berhasil!',
                      'Harap Simpan Nomor MR Anda : '+responseAdd.data
                    )


                  // window.location = base_url+'/receptionist/patient';
                })
              }

        });
    });

    function select2Anggota(nip) {
        $('#select2-anggota').select2({
              ajax: {
                dataType: 'json',
                url: base_path+'/api_public/family/active/select2',
                data: function(params){
                    return {
                      nip: $('#nip').val(),
                      search: params.term
                    }
                },
                processResults: function (data, page) {
                    var list = [];
                    var result = data;
                    $.each(result.data, function(index,item){
                      list.push({
                        id: index+1,
                        text: item.nama,
                        nama    : item.nama,
                        tgllahir  : item.tgllhr,
                        alamat  : item.alamat,
                        kdkeluarga : item.kdkeluarga,
                        nip : item.nip,
                      })
                    });
                    return {
                      results: list
                    };
                   },
            }
        });
    }

    function onChangeAnggota() {
        $('#select2-anggota').on('select2:select', function (e) {
            var data = e.params.data;        

            $("#nama").val(data.nama);
            $("#date").val(data.tgllahir);
            $("#alamat").val(data.alamat);

            if(parseInt(data.kdkeluarga) == 1 || parseInt(data.kdkeluarga) == 2){
                $('#optionRelationPasangan').prop('checked',true);
            }
            else{
                getDetailChildren(data.nip, data.tgllahir)
            }
        });
    }

    function getDetailChildren(nip, tgllahir) {
        let childrenData = {
            _token : $('meta[name="csrf_token"]').attr('content'),
            nip : nip,
            tgllahir : tgllahir,
        }

        send(base_url+"/api_public/family/category", childrenData, false, function(response){
            responsedata = response.data;

            if(parseInt(responsedata) == 3){
                $('#optionRelationAnak1').prop('checked',true);
            }
            else if(parseInt(responsedata) == 4){
                $('#optionRelationAnak2').prop('checked',true);
            }
            else $('#optionRelationAnak3').prop('checked',true);

            console.log(responsedata)
            
        })   
    }

  </script>

</body>
</html>