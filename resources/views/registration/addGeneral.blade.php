<!DOCTYPE html>
<html lang="en">
<head>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/bootstrap/css/bootstrap.min.css') }}"/>    
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material/material.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/material_style.css') }}"/>
    <!-- sweet alert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/sweet-alert/sweetalert.min.css') }}"/>
    <!-- Theme Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/theme_style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/plugins.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/responsive.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/theme-color.css') }}"/>
    <!-- favicon -->
    <link rel="stylesheet" type="image/x-icon" href="{{ asset('assets/template/img/favicon.ico') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/jquery-tags-input/jquery-tags-input.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main/doctor/autocomplete.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2-bootstrap.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/> 

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom/styles.css') }}"/>

    <!-- Form -->
    <link href="{{ asset('assets/template/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/formlayout.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/theme-color.css') }}" rel="stylesheet" type="text/css" />
    <!-- -->

    <link href="{{ asset('assets/css/custom/styles.css') }}" rel="stylesheet" type="text/css" />

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <script>
      var base_path = "{{url('/')}}";
      var base_url = "{{url('/')}}";

      <?php 
          $domain = 'http://'.$_SERVER['HTTP_HOST']; 
      ?>
      var APIurl = "{{url('/')}}/";
  </script>
</head>
<body>

    <div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Tambah Pasien Umum</div>
                </div>
            </div>
        </div>

        <div id="overlay">
            <div class="cv-spinner">
                <span class="spinner"></span>
            </div>
        </div>
        
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="card-head">
                            <header>Informasi Pasien</header>
                            <button id = "panel-button" 
                               class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                               data-upgraded = ",MaterialButton">
                               <i class = "material-icons">more_vert</i>
                            </button>
                            <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                               data-mdl-for = "panel-button">
                               <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                               <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                               <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                            </ul>
                        </div>
                        <!-- <form method="POST" id="form-pasien" action="{{url('/receptionist/patient/tambah')}}"> -->
                        <div class="card-body row">
                            {{csrf_field()}}
                            <input type="hidden" name="tipe" value="general">

                            <div class="row col-lg-12">
                                <div class="col-md-6">

                                    <div class="col-lg-12 p-t-20"> 
                                      <div class="form-group">
                                        <label class="float-label">Nama Lengkap
                                          <span class="required"></span>
                                        </label>
                                        <input id="nama" name="nama" type="text" class="form-control" placeholder="">
                                    </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20"> 
                                      <label class="float-label">NIK
                                          <span class="required"></span>
                                        </label>
                                        <input id="nik" name="nik" type="text" class="form-control" placeholder="">
                                    </div>
                                    <div class="col-lg-12 p-t-20"> 
                                      <label class="float-label">Tanggal Lahir
                                        <span class="required"></span>
                                      </label>
                                      <input name="ttl" type="text" id="date" class="floating-label mdl-textfield__input"  placeholder="Tanggal Lahir">
                                    </div>
                                    <div class="col-lg-12 p-t-20">
                                      <label class="float-label">Jenis Kelamin
                                        <span class="required"></span>
                                      </label>
                                      <div class="radio p-0">
                                          <input type="radio" name="optionGender" id="optionGender" value="L" checked>
                                          <label for="optionsRadios1">
                                              Laki-laki
                                          </label>
                                      </div>
                                      <div class="radio p-0">
                                          <input type="radio" name="optionGender" id="optionGender" value="P">
                                          <label for="optionsRadios2">
                                              Perempuan
                                          </label>
                                      </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20">
                                       <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                         <input name="nohp" class = "mdl-textfield__input float-input" type = "text"
                                            pattern = "-?[0-9]*(\.[0-9]+)?" id = "hp">
                                         <label class = "mdl-textfield__label float-label" for = "text5">No HP
                                            <span class="required"></span>
                                         </label>
                                         <span class = "mdl-textfield__error">Number required!</span>
                                      </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20"> 
                                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                         <input name="email" class = "mdl-textfield__input float-input" type = "email" id = "email">
                                         <label class = "mdl-textfield__label float-label" for = "txtemail">Email
                                            <span class="required"></span>
                                         </label>
                                          <span class = "mdl-textfield__error">Enter Valid Email Address!</span>
                                      </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20"> 
                                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                         <input name="bpjs" class = "mdl-textfield__input float-input" type = "text" id = "bpjs">
                                         <label class = "mdl-textfield__label float-label" for = "age">No BPJS</label>
                                      </div>
                                    </div>

                                    <div class="col-lg-12 p-t-20"> 
                                      <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                        <label class = "float-label" for = "alamat">Alamat</label>
                                         <textarea name="alamat" class = "mdl-textfield__input float-input" rows =  "4" 
                                            id = "alamat" ></textarea>
                                         
                                      </div>
                                    </div>
                                    
                                </div>

                                <div class="col-md-6">
                                    <div class="col-lg-12 p-t-20"> 
                                      <div class="form-group">
                                          <label>No MR lama
                                          </label>
                                          <input id="mr_lama" name="mr_lama" type = "mr_lama" class="form-control" placeholder="Not Set"></input>
                                      </div>
                                    </div>
                                   
                                    <div class="col-lg-12 p-t-20"> 
                                      <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                         <label class = "float-label" for = "riwayat_penyakit">Penyakit yang Pernah Diderita</label>
                                         <textarea name="riwayat_penyakit" class = "mdl-textfield__input float-input" rows =  "4" 
                                            id = "riwayat_penyakit" ></textarea>
                                         
                                      </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20"> 
                                      <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                         <label class = " float-label" for = "riwayat_turunan">Penyakit Keluarga/Turunan</label>
                                         <textarea name="penyakit_turunan" class = "mdl-textfield__input float-input" rows =  "4" 
                                            id = "riwayat_turunan" ></textarea>
                                         
                                      </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20"> 
                                      <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                         <label class = "float-label" for = "riwayat_alergi">Riwayat Alergi</label>
                                         <textarea name="riwayat_alergi" class = "mdl-textfield__input float-input" rows =  "4" 
                                            id = "riwayat_alergi" ></textarea>
                                         
                                      </div>
                                    </div>

                                    <div class="col-lg-12 p-t-20"> 
                                      <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                         <label class = " float-label" for = "riwayat_vaksinasi">Riwayat Vaksinasi</label>
                                         <textarea name="riwayat_vaksinasi" class = "mdl-textfield__input float-input" rows =  "4" 
                                            id = "riwayat_vaksinasi" ></textarea>
                                         
                                      </div>
                                    </div>
                                    
                                    <!-- <div class="col-lg-12 p-t-20">
                                        <span class="col-md-12">
                                            <label>Prioritas</label>
                                        </span>
                                        <span class="col-md-12">
                                            <input id="is-vip" type="checkbox" data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                       </span>
                                    </div> -->

                                </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="col-lg-12 p-t-20 text-center"> 
                                <!-- <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" data-toggle="modal" data-target="#printCard">Simpan</button> -->
                                <button id="simpan" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button>
                                <a href="{{url ('receptionist/patient')}}" type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                            </div>
                        </div>

                        <div class="alert-form"></div>

                      <!-- </form> -->
                    </div>
                </div>
            </div> 


            <!-- Print ID Card Modal -->
            <div class="modal fade" id="printCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="exampleModalLongTitle">Berhasil Menambah Pasien Baru</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="col-xl-12 col-md-12 col-lg-12">
                        <div class="info-box bg-success">
                          <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                          <div class="info-box-content">
                            <span class="info-box-text">No MR : SET90328392.02</span>
                            <span class="info-box-number">
                              Nama :
                              <span>Shaka Nagano</span>
                            </span>
                            <span class="progress-description">
                              <span></span>
                            </span>
                            
                          </div>
                          <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                      </div>
                      <div class="modal-footer">
                        <div class="profile-userbuttons" style="margin:auto">
                            <a href="#" class="btn btn-circle blue-bgcolor btn-md">Print Kartu</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>    
            </div>
            <!-- End of Print ID Card Modal -->
        </div>
    </div>

    <script src="{{ asset('assets/template/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/template/popper/popper.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.blockui.min.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/template/moment.min.js') }}"></script>
    <script src="{{ asset('assets/template/counterup/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/template/counterup/jquery.counterup.min.js') }}"></script>
    
    <!-- bootstrap -->
    <script src="{{ asset('assets/template/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/template/summernote/summernote.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-inputmask/bootstrap-inputmask.min.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-colorpicker/js/bootstrap-colorpicker-init.js') }}"></script>

    
    <!-- Common js-->
    <script src="{{ asset('assets/template/app.js') }}"></script>
    <script src="{{ asset('assets/template/layout.js') }}"></script>
    <script src="{{ asset('assets/template/theme-color.js') }}"></script>
    <script src="{{ asset('assets/template/profile.js') }}"></script>
    <!-- material -->
    <script src="{{ asset('assets/template/material/material.min.js') }}"></script>
    
    <!-- Sweet Alert -->
    <script src="{{ asset('assets/template/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/template/sweet-alert/sweet-alert-data.js') }}"></script>
    <!-- end Sweet Alert -->

    <!-- Javascript Helper -->
    <script src="{{ asset('assets/js/helper/helper.js') }}"></script>
    <!-- End of Javascript Helper -->

     <!-- Datetime Picker -->
    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js') }}"></script>

    <!-- End of Datetime Picker -->

    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

     <!--tags input-->
    <script src="{{ asset('assets/template/jquery-tags-input/jquery-tags-input.js') }}"></script>
    <script src="{{ asset('assets/template/jquery-tags-input/jquery-tags-input-init.js') }}"></script>

    <!--select2-->
    <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/template/select2/js/select2-init.js') }}"></script>

    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>


  <script type="text/javascript">
    $(document).ready(function(){
        $("#simpan").click(function(){

            var nama = $('#nama').val()
            var nik = $('#nik').val()
            var ttl = $('#date').val()
            var hp = $('#hp').val()
            var email = $('#email').val()

            console.log(nama)

            if(nama =='' || nik=='' || ttl =='' 
                || hp =='' || email == ''){

              var alert_html = 
                        '<div class="add-alert alert alert-danger">'+
                          'Harap isi Required Field (bertanda *)'+  
                        '</div>';

              $('.alert-form').html(alert_html);
            }
            else{
              var nik = $('#nik').val();
              console.log(nik)

              var formData = {
                "_token": "{{ csrf_token() }}",
                'tipe'    : 'umum',
                'kategori': 'umum',
                'nama'    : nama,
                'id_card' : nik,
                'ttl'     : ttl,
                'hp'      : hp,
                'email'   : email,
                'unit'    : 'UMUM',
                'bpjs'    : $('#bpjs').val(),
                'alamat'  : $('#alamat').val(),
                'jenis_kelamin' : $('input[type=radio][name=optionGender]:checked').val(),
                'riwayat_penyakit'  : $('#riwayat_penyakit').val(),
                'riwayat_turunan'   : $('#riwayat_turunan').val(),
                'riwayat_alergi'    : $('#riwayat_alergi').val(),
                'riwayat_vaksinasi' : $('#riwayat_vaksinasi').val(),
                'mr_lama' : $('#mr_lama').val(),
                'is_vip' : 0
              }

              $("#overlay").fadeIn(300);　
              send(base_url+"/api_public/patient/tambah", formData, true, function(responseAdd){
                $("#overlay").fadeOut(300);

                sweetalert(
                  'success',
                  'Reservasi Berhasil!',
                  'Harap Simpan Nomor MR Anda : '+responseAdd.data
                )
              })

              // var HttpTimeout=setTimeout(ajaxTimeout, 60000); //10s

            }

        });
    });
  </script>
</body>
</html>