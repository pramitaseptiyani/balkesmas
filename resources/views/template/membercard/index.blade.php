<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
        body {
          /*background: rgb(204,204,204); */
        }
        .bg-img {
            background-size: cover;
        }
        page {
          background: white;
          display: block;
          margin: 0 auto;
          /*margin-bottom: 0.5cm;*/
          /*box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
          box-shadow: 0 0 0.2cm rgba(0,0,0,0.5);
        }
        page[size="A4"] {  
          width: 21cm;
          height: 29.7cm; 
        }
        page[size="A4"][layout="portrait"] {
          width: 29.7cm;
          height: 21cm;  
        }
        page[size="A3"] {
          width: 29.7cm;
          height: 42cm;
        }
        page[size="A3"][layout="portrait"] {
          width: 42cm;
          height: 29.7cm;  
        }
        page[size="A5"] {
          width: 14.8cm;
          height: 21cm;
        }
        page[size="A5"][layout="portrait"] {
          width: 21cm;
          height: 14.8cm;  
        }

        page[size="custom"][layout="portrait"] {
          width: 32.5cm;
          height: 35cm;  
        }
        @media print {
          body, page {
            margin: 0;
            box-shadow: 0;
          }
        }
        page table {
            width:100%; 
            line-height:inherit; 
            text-align: center;
        }
        page table td{
            vertical-align:top; 
            position: relative;
        } 
        page p {
            font-family: Helvetica;
            line-height: 20px;
            font-size: 1.1em;
        }
        .primary-color {
            color: #C8AA4E;
        }
        .top h1 {
            /*padding: 5.5% 0 0;*/
            padding: 6.5% 0 0;
            /*font-size: 2.7em;*/
            font-size: 1.9em;
        }
        .name h1 {
            margin: 0;
            font-size: 3em;
            /*font-size: 3.1em;*/
        }
        .title h1 {
            margin: 0;
            font-size: 2.4em;
            /*font-size: 2.5em;*/
        }
        .signature hr {
            border-color: #000;
        }
        .signature .box {
            width: 7cm;
            position: absolute;
            top: 3cm;
        }
        .signature .box h3 {
            margin-bottom: 0;
        }
        .signature .box h4 {
            margin-top: 6px;
        }
        .signature .box.left {
            left: 4cm;
        }
        .signature .box.right {
            right: 4cm;
        }

        body{
            height : 100vh;
        }
    </style>
</head>
<body>
    <!-- <page size="A4"></page>
    <page size="A4"></page>
    <page size="A5"></page>
    <page size="A5" layout="portrait"></page>
    <page size="A3"></page>
    <page size="A3" layout="portrait"></page> -->

    <!-- <page class="bg-img" size="A4" layout="portrait"> -->
    <page class="bg-img" size="custom" layout="portrait">
        <table cellpadding="0" cellspacing="0">
            <tbody>
                <tr class="top">
                    <td colspan="3">
                        <h1>KARTU ANGGOTA</h1>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <h1>Balkesmas</h1>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <img width="200px" height="200px" src="https://balkesmas.kemenkumham.go.id/assets/img/avatar/avatar.png">
                    </td>
                </tr>
                <tr class=""><td><br></td></tr>
                <tr class="">
                    <td colspan="3">
                        <p></p>
                    </td>
                </tr>
                <tr class="name primary-color">
                    
                    <!-- <img src="{{ url('/') }}/assets/img/a"> -->
                    <td colspan="3">
                        <h1>Shaka Nagano</h1>
                    </td>
                </tr>
                <tr class="">
                    <td colspan="3">
                        <p>
                        </p>
                    </td>
                </tr>
                <tr class="title">
                </tr>
                <tr class="">
                    <td colspan="3">
                        <p>
                            Alamat_user
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <!-- <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={No MR : MR_number, link : http://balkesmas.kemenkumham.go.id/mr/content?MR=MR_number}" title="Your secret code" /> -->
                        <img src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=http://balkesmas.kemenkumham.go.id/mr/content?MR=MR_number" title="Your secret code" />
                    </td>
                </tr>
                <tr class="signature">
                    <td colspan="3">
                        <div class="box">
                            <h3 class="primary-color">No MR</h3>
                            <h4>MR_number</h4>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </page>
</body>
</html>