       <!-- start footer -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy; Kementerian Hukum dan Hak Asasi Manusia
            <a href="mailto:standarisasi16@gmail.com" target="_top" class="makerCss">Pusat Data dan Teknologi Informasi</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- end footer -->
    </div>
