<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
        body {
          /*background: rgb(204,204,204); */
        }
        .bg-img {
            background-size: cover;
            /*background-image: url('https://catatansikaswo.files.wordpress.com/2014/07/tampak-depan-balkesmas.jpg');
            background-opacity: 0.1;*/
        }
        page {
          background: white;
          display: block;
          margin: 0 auto;
          /*margin-bottom: 0.5cm;*/
          /*box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
          box-shadow: 0 0 0.2cm rgba(0,0,0,0.5);
        }
        page[size="A4"] {  
          width: 21cm;
          height: 29.7cm; 
        }
        page[size="A4"][layout="portrait"] {
          width: 29.7cm;
          height: 21cm;  
        }
        page[size="A3"] {
          width: 29.7cm;
          height: 42cm;
        }
        page[size="A3"][layout="portrait"] {
          width: 42cm;
          height: 29.7cm;  
        }
        page[size="A5"] {
          width: 14.8cm;
          height: 21cm;
        }
        page[size="A5"][layout="portrait"] {
          width: 21cm;
          height: 14.8cm;  
        }

        page[size="custom"][layout="portrait"] {
          width: 30cm;
          height: 15cm;  
        }
        @media print {
          body, page {
            margin: 0;
            box-shadow: 0;
          }
        }
        page table {
            width:100%; 
            line-height:inherit; 
            /*text-align: center;*/
        }
        page table td{
            vertical-align:top; 
            position: relative;
        } 
        page p {
            font-family: Helvetica;
            line-height: 20px;
            font-size: 1.1em;
        }
        .primary-color {
            color: #C8AA4E;
        }
        .top h1 {
            /*padding: 5.5% 0 0;*/
            padding: 6.5% 0 0;
            /*font-size: 2.7em;*/
            font-size: 1.9em;
        }
        .name h1 {
            margin: 0;
            font-size: 3em;
            /*font-size: 3.1em;*/
        }
        .title h1 {
            margin: 0;
            font-size: 2.4em;
            /*font-size: 2.5em;*/
        }
        .signature hr {
            border-color: #000;
        }
        .signature .box {
            width: 7cm;
            position: absolute;
            top: 3cm;
        }
        .signature .box h3 {
            margin-bottom: 0;
        }
        .signature .box h4 {
            margin-top: 6px;
        }
        .signature .box.left {
            left: 4cm;
        }
        .signature .box.right {
            right: 4cm;
        }

        body{
            height : 100vh;
        }
    </style>
</head>
<body>
    <!-- <page size="A4"></page>
    <page size="A4"></page>
    <page size="A5"></page>
    <page size="A5" layout="portrait"></page>
    <page size="A3"></page>
    <page size="A3" layout="portrait"></page> -->

    <!-- <page class="bg-img" size="A4" layout="portrait"> -->
    <page class="bg-img" size="custom" layout="portrait">
        <table cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <tr class="top">
                    <td colspan="3">
                        <h1 style="text-align: center;">REKAM MEDIS</h1>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <h1 style="text-align: center;">replace_nama</h1>
                    </td>
                </tr>
                <tr></tr>
                <tr>
                    <td>No MR</td>
                    <td>:</td>
                    <td>replace_mr</td>
                </tr>
                <tr>
                    <td>NIP</td>
                    <td>:</td>
                    <td>replace_nip</td>
                </tr>
                <tr>
                    <td>Unit</td>
                    <td>:</td>
                    <td>replace_unit</td>
                </tr>
                <tr>
                    <td>No BPJS</td>
                    <td>:</td>
                    <td>replace_bpjs</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>replace_email</td>
                </tr>
                <tr>
                    <td>No HP</td>
                    <td>:</td>
                    <td>replace_phone</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>replace_address</td>
                </tr>
                <tr>
                    <td>Penyakit Yang Pernah Diderita</td>
                    <td>:</td>
                    <td>replace_penyakit_pernah_diderita</td>
                </tr>
                <tr>
                    <td>Penyakit Keluarga/Turunan</td>
                    <td>:</td>
                    <td>replace_penyakit_turunan</td>
                </tr>
                <tr>
                    <td>Riwayat Alergi</td>
                    <td>:</td>
                    <td>replace_alergi</td>
                </tr>
                <tr>
                    <td>Riwayat Vaksinasi</td>
                    <td>:</td>
                    <td>replace_vaksinasi</td>
                </tr>
                <td colspan="3">
                    <h1 style="text-align: center;"></h1>
                </td>
                <tr>
                    <td colspan="3" style="text-align: center">
                        Jl Rasuna Said Kav 6-7, Jakarta Selatan,
                        Jakarta
                    </td>
                </tr>
            </tbody>
        </table>
    </page>
</body>
</html>