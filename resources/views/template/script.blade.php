<!-- start js include path -->
    <script src="{{ asset('assets/template/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/template/popper/popper.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.blockui.min.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/template/moment.min.js') }}"></script>
    <script src="{{ asset('assets/template/counterup/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/template/counterup/jquery.counterup.min.js') }}"></script>
    
    <!-- bootstrap -->
    <script src="{{ asset('assets/template/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/template/summernote/summernote.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-inputmask/bootstrap-inputmask.min.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-colorpicker/js/bootstrap-colorpicker-init.js') }}"></script>

    
    <!-- Common js-->
    <script src="{{ asset('assets/template/app.js') }}"></script>
    <script src="{{ asset('assets/template/layout.js') }}"></script>
    <script src="{{ asset('assets/template/theme-color.js') }}"></script>
    <script src="{{ asset('assets/template/profile.js') }}"></script>
    <!-- material -->
    <script src="{{ asset('assets/template/material/material.min.js') }}"></script>
    
    <!-- Sweet Alert -->
    <script src="{{ asset('assets/template/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/template/sweet-alert/sweet-alert-data.js') }}"></script>
    <!-- end Sweet Alert -->

    <!-- Javascript Helper -->
    <script src="{{ asset('assets/js/helper/helper.js') }}"></script>
    <!-- End of Javascript Helper -->

     <!-- Datetime Picker -->
    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js') }}"></script>

    <!-- End of Datetime Picker -->

    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

     <!--tags input-->
    <script src="{{ asset('assets/template/jquery-tags-input/jquery-tags-input.js') }}"></script>
    <script src="{{ asset('assets/template/jquery-tags-input/jquery-tags-input-init.js') }}"></script>

    <!--select2-->
    <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/template/select2/js/select2-init.js') }}"></script>

    <!-- adminlte script -->
   


@yield('js')
@yield('menu-js')