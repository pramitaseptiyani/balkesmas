<!DOCTYPE html>
<html lang="en">
<head>
  @include('template.header')
  <title>@yield('title')</title>
</head>
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-indigo">
  @include('template.navbar')
      @include('template.left_menu')
      @yield('content')
  @include('template.footer')
@include('template.script')
</body>
</html>
