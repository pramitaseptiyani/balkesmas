
@section('menu-js')
  <script type="text/javascript">
    $(document).ready(function(){

      /* dummy data for receptionist/administrator */
      var allmenu = [
        {
          "id":1,
          "nama":"PASIEN",
          "type":"menu",
          "icon":"accessible",
          "url":"",
          "order_id":1,
          "parent_id":0,
          "created_at":null,
          "updated_at":null,
          "deleted_at":null
        },
        {
          "id":2,
          "nama":"Daftar Pasien",
          "type":"submenu",
          "icon":"fa-dashboard",
          "url":"receptionist/patient",
          "order_id":1,
          "parent_id":1,
          "created_at":null,
          "updated_at":null,
          "deleted_at":null
        },
        {
          "id":3,
          "nama":"Tambah Pasien",
          "type":"submenu",
          "icon":"fa-dashboard",
          "url":"receptionist/patient/addMenu",
          "order_id":1,
          "parent_id":1,
          "created_at":null,
          "updated_at":null,
          "deleted_at":null
        },
        // {
        //   "id":4,
        //   "nama":"Tambah Pasien Umum",
        //   "type":"submenu",
        //   "icon":"fa-dashboard",
        //   "url":"receptionist/patient/add_general",
        //   "order_id":1,
        //   "parent_id":1,
        //   "created_at":null,
        //   "updated_at":null,
        //   "deleted_at":null
        // },
        {
          "id":5,
          "nama":"POLI",
          "type":"menu",
          "icon":"assignment",
          "url":"",
          "order_id":2,
          "parent_id":0,
          "created_at":null,
          "updated_at":null,
          "deleted_at":null
        },
        {
          "id":6,
          "nama":"Semua Poli",
          "type":"submenu",
          "icon":"dashboard",
          "url":"receptionist/poli",
          "order_id":2,
          "parent_id":5,
          "created_at":null,
          "updated_at":null,
          "deleted_at":null
        },
        {
          "id":7,
          "nama":"Pendaftaran Poli",
          "type":"submenu",
          "icon":"dashboard",
          "url":"receptionist/poli/add",
          "order_id":2,
          "parent_id":5,
          "created_at":null,
          "updated_at":null,
          "deleted_at":null
        },
        {
          "id":8,
          "nama":"DOKTER",
          "type":"menu",
          "icon":"person",
          "url":"",
          "order_id":3,
          "parent_id":0,
          "created_at":null,
          "updated_at":null,
          "deleted_at":null
        },
        {
          "id":9,
          "nama":"List Pasien",
          "type":"submenu",
          "icon":"person",
          "url":"doctor/listpasien",
          "order_id":3,
          "parent_id":8,
          "created_at":null,
          "updated_at":null,
          "deleted_at":null
        },
        {
          "id":10,
          "nama":"LABORATORIUM",
          "type":"menu",
          "icon":"person",
          "url":"lab/listlab",
          "order_id":4,
          "parent_id":0,
          "created_at":null,
          "updated_at":null,
          "deleted_at":null
        }
      ];
      /* End of dummy data */

      /* dummy data for nurse */
      // var allmenu = [
      //   {
      //     "id":1,
      //     "nama":"PASIEN",
      //     "type":"menu",
      //     "icon":"assignment",
      //     "url":"",
      //     "order_id":1,
      //     "parent_id":0,
      //     "created_at":null,
      //     "updated_at":null,
      //     "deleted_at":null
      //   },
      //   {
      //     "id":2,
      //     "nama":"Pasien Belum Dilayani",
      //     "type":"submenu",
      //     "icon":"dashboard",
      //     "url":"nurse/patient/queue",
      //     "order_id":2,
      //     "parent_id":1,
      //     "created_at":null,
      //     "updated_at":null,
      //     "deleted_at":null
      //   }
      // ];
      /* End of dummy data */

      var user_id = '{{(Auth::user()->id)}}';

      get(APIurl+"permission/menu/"+user_id, false, function(response){
        allmenu = response.data;

        if(allmenu.length != 0){
          allmenu.forEach(function(menu){

            if(menu.type == 'singlemenu'){
              link = base_path+"/"+menu.url;


              singlemenu_html =
              '<li class="nav-item start">'+
                '<a href="'+link+'" class="nav-link nav-toggle">'+
                  '<i class="material-icons">'+menu.icon+'</i>'+
                  '<span class="title">'+menu.name+'</span>'+
                  '<span class="selected"></span>'+
                '</a>'+
              '</li>';

              $(".sidemenu").append(singlemenu_html);
            }
            else if(menu.type == 'menu'){
              menu_html =
              '<li class="nav-item parent-'+menu.id+' ">'+
                '<a href="#" class="nav-link nav-toggle">'+
                  '<i class="material-icons">'+menu.icon+'</i>'+
                    '<span class="title">'+menu.name+'</span>'+
                    '<span class="arrow"></span></a>'+
                '<ul class="sub-menu children-menu-'+menu.id+'">'+
                '</ul>'+
              '</li>';

              $(".sidemenu").append(menu_html);
            }
            else{
              link = base_path+"/"+menu.url;

              child_html =
              '<li class="nav-item  ">'+
                '<a href="'+link+'" class="nav-link ">'+
                  '<span class="title">'+menu.name+'</span>'+
                '</a>'+
              '</li>';

              $(".children-menu-"+menu.parent_id).append(child_html);
            }
          })
        }

      });

    });
  </script>


@endsection
