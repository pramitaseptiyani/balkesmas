@extends('template.main')

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/>

  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom/styles.css') }}"/>
@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
            <div class="page-title">Pemeriksaan oleh Perawat</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Pemeriksaan oleh Perawat</li>
        </ol>
      </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="card-head">
                    <header><i>Medical Record</i></header>
                    <button id = "panel-button" 
                       class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                       data-upgraded = ",MaterialButton">
                       <i class = "material-icons">more_vert</i>
                    </button>
                </div>
                <div class="card-body row">
                    <input type="hidden" id="id_periksa" value="{{$id_periksa}}">
                    <div class="col-lg-12 p-t-20"> 
                      <div class="form-group">
                        <label>No Antrian</label>
                        <input type="text" class="form-control" placeholder="{{$no_urut_perawat}}" disabled>
                      </div>
                    </div>
                    <div class="col-lg-12 p-t-20"> 
                      <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input type="text" class="form-control" placeholder="{{$patient_nm}}" disabled>
                      </div>
                    </div>
                    <div class="col-lg-12 p-t-20"> 
                      <div class ="form-group">
                         <label>Tensi
                            <span class="required"></span>
                         </label>
                         <input id="tensi" name="tensi" type="text" class="form-control" placeholder="">
                      </div>
                    </div>
                    <div class="col-lg-12 p-t-20"> 
                      <div class = "form-group">
                         <label>Berat Badan
                            <span class="required"></span>
                         </label>
                         <input id="bb" name="bb" type="text" class="form-control" placeholder="">
                      </div>
                    </div>
                    <div class="col-lg-12 p-t-20"> 
                      <div class = "form-group">
                         <label>Tinggi Badan
                            <span class="required"></span>
                         </label>
                         <input id="tb" name="bb" type="text" class="form-control" placeholder="">
                      </div>
                    </div>
                    <div class="col-lg-12 p-t-20"> 
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                         <input class = "mdl-textfield__input" type = "text" id = "nadi">
                         <label class = "mdl-textfield__label" for = "age">Nadi</label>
                      </div>
                    </div>
                    <div class="col-lg-12 p-t-20"> 
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                         <input class = "mdl-textfield__input" type = "text" id = "suhu">
                         <label class = "mdl-textfield__label" for = "age">Suhu</label>
                      </div>
                    </div>
                    <div class="col-lg-12 p-t-20"> 
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                         <input class = "mdl-textfield__input" type = "text" id = "rr">
                         <label class = "mdl-textfield__label" for = "age">RR</label>
                      </div>
                    </div>
                    <div class="col-lg-12 p-t-20"> 
                      <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                         <textarea class = "mdl-textfield__input" rows =  "4" id = "keluhan" ></textarea>
                         <label class = "mdl-textfield__label" for = "text7">Keluhan</label>
                      </div>
                    </div>
                    
                    <div class="col-lg-12 p-t-20 text-center"> 
                        <button type="button" id="simpan" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button>
                        <a href="{{url ('nurse/patient/queue')}}" type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                    </div>
                </div>
                <div class="alert-form"></div>
            </div>
        </div>
    </div>

    <!-- Patient's Queue Number Modal -->
    <!-- System show patient's queue number for certain poli -->
        <div class="modal fade" id="queuePoliModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="exampleModalLongTitle">Berhasil Menyimpan Data</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="col-xl-12 col-md-12 col-lg-12">
                    <div class="info-box bg-success">
                      <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>

                      <div class="">Nomor Antrian Poli :</div>
                      <div class="poli-html info-box-content">
                        
                      </div>
                      <!-- <div class="info-box-content">
                        <span class="info-box-text"></span>
                        <span class="info-box-number">
                          No Antrian : 3
                        </span>
                        <span class="progress-description">
                          <span>Nama Poli (Ke Poli Tujuan)</span>
                        </span>
                        
                      </div> -->
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                </div>
              </div>
            </div>    
        </div>
  </div>
</div>
<!-- end page content -->
@endsection


@section('js')
  <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
  <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>

  <script type="text/javascript">
    $(document).ready(function() {
        $("#simpan").click(function(){

          var tensi = $('#tensi').val()
          var bb = $('#bb').val()
          var tb = $('#tb').val()

          if(tensi=='' || bb =='' || tb == ''){
            var alert_html = 
                    '<div class="add-alert alert alert-danger">'+
                      'Harap isi Required Field (bertanda *)'+  
                    '</div>';

            $('.alert-form').html(alert_html);
          }
          else{
            var formData = {
                "_token"    : "{{ csrf_token() }}",
                'id_periksa': $('#id_periksa').val(),
                'bb'        : $('#bb').val(),
                'tensimeter': $('#tensi').val(),
                'id_user_perawat' : '{{$id_user}}',
                'tinggi_badan'    : $('#tb').val(),
                'nadi'      : $('#nadi').val(),
                'suhu'      : $('#suhu').val(),
                'rr'        : $('#rr').val(),
                'keluhan'   : $('#keluhan').val()
              }

              send(base_url+"/nurse/api/exam/add", formData, true, function(response){

                  $("#queuePoliModal").modal()

                  var result = response.data
                  console.log(result)

                  var poli_html = '';
                  result.forEach(function(poli) {
                      console.log(poli);

                      add_html = '<span class="info-box-text"></span>'+
                        '<span class="info-box-number">'+
                        '</span>'+
                        '<span class="progress-description">'+
                          '<span>'+poli.no_urut_poli+'</span>'+
                        '</span>';

                      poli_html = poli_html+add_html
                  });

                  $(".poli-html").html(poli_html)
                  console.log(response)

                  // setTimeout(function(){ 
                  //   window.location = base_url+'/nurse/patient';
                  // }, 1000);

              });
          }
        })

    });
  </script>
@endsection