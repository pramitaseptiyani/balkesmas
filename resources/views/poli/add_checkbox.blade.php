@extends('template.main')


@section('css') 
  <link href="{{asset('assets/css/formlayout.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Pendaftaran Poli</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Pendaftaran Poli</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="card-head">
                        <header>Informasi Poli</header>
                        <button id = "panel-button" 
                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                           data-upgraded = ",MaterialButton">
                           <i class = "material-icons">more_vert</i>
                        </button>
                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                           data-mdl-for = "panel-button">
                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                        </ul>
                    </div>
                    <div class="card-body row">
                        {{csrf_field()}}

                        <input type="hidden" id="token" name="_token" value="{{csrf_token()}}"/>

                        <input type="hidden" id="id_pasien" name="id_pasien" value="{{$detail->id}}">
                        <div class="form-group col-lg-12 p-t-20">
                          <label>No MR</label>
                          <input type="text" name="mr_number" id="mr_number" class="form-control" value="{{$detail->patient_mr_number}}" disabled>
                        </div>
                        <div class="form-group col-lg-12 p-t-20">
                          <label>Nama</label>
                          <input type="text" name="name" id="name" class="form-control" value="{{$detail->patient_nm}}" disabled>
                        </div>
                        <div class="col-lg-12 p-t-20"> 
                          <label>Pilih Poli 
                            <small>
                              (Yang dipilih, yang dilayani dahulu)
                            </small>
                          </label>
                          <div class="row">
                            <div class="col-md-3 col-sm-3">
                            </div>
                            <div class="col-md-3 col-sm-3">
                              <div class="checkbox checkbox-icon-black">
                                  <input id="checkboxUmum" type="checkbox" value="1">
                                  <label for="checkboxUmum">
                                    Poli Umum
                                  </label>
                                  <span id="badgecheckboxUmum" class="badge badge-info"></span>
                              </div>
                              <div class="checkbox checkbox-icon-black">
                                  <input id="checkboxGigi" type="checkbox" value="2">
                                  <label for="checkboxGigi">
                                    Poli Gigi
                                  </label>
                                  <span id="badgecheckboxGigi" class="badge badge-info"></span>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                              <div class="checkbox checkbox-icon-black">
                                  <input id="checkboxMata" type="checkbox" value="3">
                                  <label for="checkboxMata">
                                    Poli Mata
                                  </label>
                                  <span id="badgecheckboxMata" class="badge badge-info"></span>
                              </div>
                              <div class="checkbox checkbox-icon-yellow">
                                  <input id="checkboxBKIA" type="checkbox" value="4">
                                  <label for="checkboxBKIA">
                                    BKIA
                                  </label>
                                  <span id="badgecheckboxBKIA" class="badge badge-info"></span>
                              </div>
                              <!-- <div class="checkbox checkbox-icon-aqua">
                                  <input id="checkboxUSG" type="checkbox" value="USG">
                                  <label for="checkboxUSG">
                                    USG
                                  </label>
                                  <span id="badgecheckboxUSG" class="badge badge-info"></span>
                              </div> -->
                            </div>
                            <div class="col-md-3 col-sm-3">
                            </div>
                            <!-- <div class="col-md-4 col-sm-4">
                              <div class="checkbox checkbox-icon-red">
                                  <input id="checkboxLab" type="checkbox" value="Laboratorium">
                                  <label for="checkboxLab">
                                    Laboratorium
                                  </label>
                                  <span id="badgecheckboxLab" class="badge badge-info"></span>
                              </div>
                              <div class="checkbox checkbox-icon-red">
                                  <input id="checkboxFisio" type="checkbox" value="Fisioterapi">
                                  <label for="checkboxFisio">
                                    Fisioterapi
                                  </label>
                                  <span id="badgecheckboxFisio" class="badge badge-info"></span>
                              </div>
                            </div> -->

                          </div>
                        </div>
                        
                        
                        <div class="col-lg-12 p-t-20 text-center"> 
                            <button id="simpan" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button>
                            <a href="{{url ('receptionist/patient')}}" type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        
        <!-- Patient's Queue Number Modal -->
        <div class="modal fade" id="queueNumberModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="exampleModalLongTitle">Berhasil Mendaftar Poli</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="col-xl-12 col-md-12 col-lg-12">
                    <div class="info-box bg-success">
                      <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                      <div class="info-box-content">
                        <!-- <span class="info-box-text">No MR : SET90328392.02</span> -->
                        <span class="info-box-text">No Antrian Anda adalah :</span>
                        <span class="info-box-number">
                          <span id="no_queue_nurse"></span> 
                          <span>(Perawat)</span>
                        </span>
                        <!-- <span class="progress-description">
                          No Antrian : 3 
                          <span>(Poli Pertama)</span>
                        </span> -->
                        
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                </div>
              </div>
            </div>    
        </div>
        <!-- End of Patient Detail Selection Modal --> 
    </div>
</div>
<!-- end page content -->
@endsection

@section('js')
  <script src="{{ asset('assets/js/main/poli/add.js') }}">
    </script>
@endsection