@extends('template.main')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>
@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class="pull-left">
                    <div class="page-title">Daftar Data Poli</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Daftar Data Poli</li>
                </ol>
            </div>
        </div>

         <!-- chart start -->
        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Daftar Data Poli </header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body no-padding height-9">
                        
                    <!-- end widget -->
                    </div>
                    <div class="card-body">
                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width" id="patientDatatable"">
                                <thead>
                                    <tr>
                                        <th width="20%"> No MR </th>
                                        <th> Nama </th>
                                        <th> Jenis Poli </th>
                                        <th> Tanggal </th>
                                        <th> No Antrian Perawat</th>
                                        <th> No Antrian Poli</th>
                                        <th> Status </th>
                                        <th> Aksi </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <!-- Chart end -->

         <!-- Patient Detail Selection Modal -->
        <div class="modal fade" id="poliDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Detail Poli</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>No MR</label>
                                    <input type="text" class="form-control" placeholder="SET90328392.02" disabled>
                                </div>
                                
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" placeholder="Nama" disabled>
                                </div>
                                <div class="form-group">
                                    <label>NIP/NIK</label>
                                    <input type="text" class="form-control" placeholder="NIP/NIK" disabled>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Jenis Poli</label>
                                    <input type="text" class="form-control" placeholder="NIP/NIK" disabled>
                                </div>
                                <div class="form-group">
                                    <label>No Antrian</label>
                                    <input type="text" class="form-control" placeholder="No Antrian" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal</label>
                                    <input type="text" class="form-control"  placeholder="Tanggal" disabled>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Patient Detail Selection Modal -->


        

    </div>
</div>
<!-- end page content -->
@endsection

@section('js')
    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- End of Datatable JS -->

    <script src="{{ asset('assets/js/main/poli/index.js') }}">
    </script>
@endsection