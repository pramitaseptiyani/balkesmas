@extends('template.main')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>

    <!-- Sweet alert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/sweet-alert/sweetalert.min.css') }}"/>
    <!-- -->

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <!-- <div class="pull-left">
                    <div class="page-title">Daftar Semua Pasien</div>
                </div> -->
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    Uji Lab</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Daftar Uji Lab</li>
                </ol>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="borderBox light bordered">
                <div class="borderBox-title tabbable-line">
                    <div class="caption">
                        <span class="caption-subject font-dark bold uppercase">Daftar Uji Lab</span>
                    </div>
                </div>
                <div class="borderBox-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-md-10">
                                <a href="{{ url('/admin/lab2/') }}/{{$id_parent}}">
                                    <button type="button" class="back-to-level2 btn btn-success"><i class="fa fa-arrow-left"></i>  </button>
                                    <button type="button" class="btn blue-bgcolor btn-outline m-b-10 btn-circle">{{$parent1}} 
                                        &nbsp;
                                        <i class="fa fa-angle-right"></i>
                                        {{$parent2}}
                                    </button>
                                </a>
                            </div>
                            <div class="pull-right">
                                <button type="button" class="add-lab btn btn-success"><i class="fa fa-plus"></i> Tambah Uji Lab </button>
                            </div>
                        </div>

                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width" id="activeLabLevel3Datatable">
                                <thead>
                                    <tr>
                                        <th> Nama </th>
                                        <th> Nama Parent </th>
                                        <th style="min-width:200px;"> Aksi </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <!-- Add Lab Modal -->
        <div class="modal fade" id="labAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Uji Lab</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="hidden" id="add-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="id" id="add-id">
                                <div class="form-group">
                                    <label>Nama Parent</label>

                                    <div class="col-lg-12 col-md-12">
                                        <select id="select2-lab-add" class="select2-lab-add form-control select2" style="width: 100%">

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Nama</label>
                                        <input type="text" id="add-nama" class="form-control" placeholder="1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-12">
                                        <label>Aktif</label>
                                    </span>
                                    <span class="col-md-12">
                                        <input id="add-aktif" type="checkbox" checked data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                   </span>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-edit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Add Lab Modal -->


        <!-- Edit Lab Modal -->
        <div class="modal fade" id="labEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Uji Lab</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="hidden" id="edit-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="id" id="edit-id">
                                <div class="form-group">
                                    <label>Nama Parent</label>

                                    <div class="col-lg-12 col-md-12">
                                        <select id="select2-lab-edit" class="select2-lab-edit form-control select2" style="width: 100%">

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Nama</label>
                                        <input type="text" id="edit-nama" class="form-control" placeholder="Tuliskan Nama Lab">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-12">
                                        <label>Aktif</label>
                                    </span>
                                    <span class="col-md-12">
                                        <input id="edit-aktif" type="checkbox" checked data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                   </span>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-edit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Edit Lab Modal -->

        
        

    </div>
</div>
<!-- end page content -->
@endsection

@section('js')
    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

    <!-- Datetime Picker -->
    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <!-- End of Datetime Picker -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- -->

    <!-- Sweet alert -->
    <script src="{{ asset('assets/template/sweet-alert/sweetalert.min.js') }}"></script>
    <!-- -->

    <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/js/helper/helper.js') }}"></script>

    <script src="{{ asset('assets/js/main/admin/lab_level3.js') }}">
    </script>
@endsection