@extends('template.main')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>

    <!-- Sweet alert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/sweet-alert/sweetalert.min.css') }}"/>
    <!-- -->

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <!-- Form -->
    <!-- <link href="{{ asset('assets/template/css/theme_style.css') }}" id="rt_style_components" rel="stylesheet" type="text/css" /> -->
    <link href="{{ asset('assets/template/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/formlayout.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/theme-color.css') }}" rel="stylesheet" type="text/css" />
    <!-- -->
@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <!-- <div class="pull-left">
                    <div class="page-title">Daftar Semua Pasien</div>
                </div> -->
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    Pasien</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Tambah Pasien Pegawai</li>
                </ol>
            </div>
        </div>

    <!-- Employee -->
        <!-- <div class="col-md-12 col-sm-12">
            <div class="borderBox light bordered">
                <div class="borderBox-title tabbable-line">
                    <div class="caption">
                        <span class="caption-subject font-dark bold uppercase">Daftar Pegawai</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a href="#borderBox_active" data-toggle="tab" class="active"> Pegawai Aktif </a>
                        </li>
                        <li class="nav-item">
                            <a href="#borderBox_archive" data-toggle="tab"> Pegawai Tidak Aktif </a>
                        </li>
                    </ul>
                </div>
                <div class="borderBox-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="borderBox_active">
                            <div class="pull-right">
                                <button type="button" class="add-employee btn btn-success"><i class="fa fa-plus"></i> Tambah Pegawai </button>
                            </div>
                            <div class="table-scrollable">
                                <table class="table table-hover table-checkable order-column full-width" id="activeEmployeeDatatable">
                                    <thead>
                                        <tr>
                                            <th style="min-width:200px;"> NIP </th>
                                            <th> Nama </th>
                                            <th> Unit </th>
                                            <th> Tanggal Lahir </th>
                                            <th style="min-width:200px;"> Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="borderBox_archive">
                            <div class="table-scrollable">
                                <table class="table table-hover table-checkable order-column full-width" id="archiveEmployeeDatatable">
                                    <thead>
                                        <tr>
                                            <th style="min-width:200px;"> NIP </th>
                                            <th> Nama </th>
                                            <th> Unit </th>
                                            <th> Tanggal Lahir </th>
                                            <th style="min-width:200px;"> Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

        <!-- Add Role Modal -->
        <!-- <div class="modal fade" id="roleAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Data Pegawai</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="hidden" id="edit-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="id" id="edit-id">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Role</label>
                                        <input type="text" id="edit-role" class="form-control" placeholder="Admin">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-12">
                                        <label>Aktif</label>
                                    </span>
                                    <span class="col-md-12">
                                        <input id="edit-aktif" type="checkbox" checked data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                   </span>
                                </div>
                                

                                <div class="form-group">
                                    <label>Menu</label>

                                    <div class="col-lg-12 col-md-12">
                                        <select id="multiple-menu-add" class="multiple-menu form-control select2-multiple" style="width: 100%" multiple>

                                        </select>
                                    </div>
                                </div>
                                
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-edit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- End of Edit Patient Selection Modal -->

        <!-- See Employee Modal -->
        <!-- <div class="modal fade" id="employeeSeeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Lihat Data Pegawai</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="hidden" id="edit-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="id" id="see-id">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>NIP</label>
                                            <input type="text" id="see-nip" class="form-control" placeholder="199503132019012001" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Nama</label>
                                            <input type="text" id="see-nama" class="form-control" placeholder="Shaka Nagano" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Tanggal Lahir</label>
                                            <input type="text" id="see-tgl-lahir" class="form-control" placeholder="" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Unit</label>
                                            <input type="text" id="see-unit" class="form-control" placeholder="Sekretariat Jenderal" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Email</label>
                                            <input type="text" id="see-email" class="form-control" placeholder="your@email.com" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>No HP</label>
                                            <input type="text" id="see-hp" class="form-control" placeholder="" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Alamat</label>
                                        <textarea id="see-alamat" class="form-control" rows="3" placeholder="Tuliskan alamat" disabled>
                                        </textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Aktif</label>
                                        </div>
                                        <div>
                                            <input id="edit-aktif" type="checkbox" checked data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <label>Jenis Kelamin</label>
                                        </div>

                                        <div>
                                            <span class="radio radio-aqua">
                                                <input id="optionGenderL" name="optionGender" type="radio" value="L" checked disabled>
                                                <label for="optionGenderL">
                                                    Laki-Laki
                                                </label>
                                            </span>
                                            <span class="radio radio-red">
                                                <input id="optionGenderP" name="optionGender" type="radio" value="P"disabled >
                                                <label for="optionGenderP">
                                                    Perempuan
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                </div>                         
                                
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- End of See Employee Modal -->

        <!-- Add Employee Modal -->
        <div class="modal fade" id="employeeAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="add-title">Tambah Data Pegawai</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="hidden" id="edit-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="id" id="add-id">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>NIP
                                                <span class="required"></span>
                                            </label>
                                            <input type="text" id="add-nip" class="form-control" placeholder="199503132019012001">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Nama
                                                <span class="required"></span>
                                            </label>
                                            <input type="text" id="add-nama" class="form-control" placeholder="Shaka Nagano">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <div>
                                            <label>Unit
                                                <span class="required"></span>
                                            </label>
                                                <select id="select2-satker-add" class="select2-satker-add form-control select2" style="width: 100%">

                                                </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Email
                                                <span class="required"></span>
                                            </label>
                                            <input type="text" id="add-email" class="form-control" placeholder="your@email.com">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>No HP
                                                <span class="required"></span>
                                            </label>
                                            <input type="text" id="add-hp" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Alamat
                                            <span class="required"></span>
                                        </label>
                                        <textarea id="add-alamat" class="form-control" rows="3" placeholder="">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Aktif</label>
                                        </div>
                                        <div>
                                            <input id="add-aktif" type="checkbox" checked data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <label>Jenis Kelamin</label>
                                        </div>

                                        <div>
                                            <span class="radio radio-aqua">
                                                <input id="addOptionGenderL" name="optionGender" type="radio" value="L" checked>
                                                <label for="addOptionGenderL">
                                                    Laki-Laki
                                                </label>
                                            </span>
                                            <span class="radio radio-red">
                                                <input id="addOptionGenderP" name="optionGender" type="radio" value="P" >
                                                <label for="addOptionGenderP">
                                                    Perempuan
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                </div>                         
                                
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-add" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Add Employee Modal -->

        <!-- Edit Employee Modal -->
        <!-- <div class="modal fade" id="employeeEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="edit-title">Edit Data Pegawai</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="hidden" id="edit-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="id" id="see-id">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>NIP</label>
                                            <input type="text" id="see-nip" class="form-control" placeholder="199503132019012001" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Nama</label>
                                            <input type="text" id="see-nama" class="form-control" placeholder="Shaka Nagano" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Tanggal Lahir</label>
                                            <input type="text" id="see-tgl-lahir" class="form-control" placeholder="" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Unit</label>
                                            <input type="text" id="see-unit" class="form-control" placeholder="Sekretariat Jenderal">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Email</label>
                                            <input type="text" id="see-tgl-lahir" class="form-control" placeholder="your@email.com">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>No HP</label>
                                            <input type="text" id="see-unit" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Alamat</label>
                                        <textarea id="see-alamat" class="form-control" rows="3" placeholder="Tuliskan alamat">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Aktif</label>
                                        </div>
                                        <div>
                                            <input id="edit-aktif" type="checkbox" checked data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <label>Jenis Kelamin</label>
                                        </div>

                                        <div>
                                            <span class="radio radio-aqua">
                                                <input id="optionGenderL" name="optionGender" type="radio" value="L" checked>
                                                <label for="optionGenderL">
                                                    Laki-Laki
                                                </label>
                                            </span>
                                            <span class="radio radio-red">
                                                <input id="optionGenderP" name="optionGender" type="radio" value="P" >
                                                <label for="optionGenderP">
                                                    Perempuan
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                </div>                         
                                
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-edit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- End of Edit Employee Modal -->
    <!-- End Of Employee -->


    <!-- Patient -->
        <div class="col-md-12 col-sm-12">
            <div class="borderBox light bordered">
                <div class="borderBox-title tabbable-line">
                    <div class="caption">
                        <span class="caption-subject font-dark bold uppercase">Daftar Pasien</span>
                    </div>
                </div>
                <div class="borderBox-body">
                    <div class="tab-content">
                        <div class="pull-right">
                            <button type="button" class="add-employee btn btn-success"><i class="fa fa-plus"></i> Tambah Pasien Pegawai </button>
                        </div>
                        <div class="table-scrollable">
                            <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width" id="patientDatatable">
                                <thead>
                                    <tr>
                                        <th width="20%"> No MR </th>
                                        <th> NIP/NIK </th>
                                        <th> Nama </th>
                                        <th> Kategori </th>
                                        <th> Alamat </th>
                                        <th> Update Pada </th>
                                        <th> Aksi </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>


        <!-- Edit Patient Selection Modal -->
        <div class="modal fade" id="patientEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Data Pasien</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <input type="hidden" id="edit-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="id" id="edit-id">
                                <input type="hidden" name="id-kategori" id="edit-id-kategori">
                                <div class="form-group">
                                    <label>No MR</label>
                                    <input type="text" id="edit-MR" class="form-control" placeholder="SET90328392.02" disabled>
                                </div>
                                
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" id="edit-nama" class="form-control" placeholder="Nama" disabled>
                                </div>
                                <div class="form-group">
                                    <label>NIP/NIK</label>
                                    <input type="text" id="edit-nip" class="form-control" placeholder="NIP/NIK" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" id="edit-ttl" class="form-control"  placeholder="Tanggal Lahir" disabled>
                                </div>
                                <input type="hidden" id="edit-satker-id" name="satker_id"/>
                                <div class="form-group">
                                    <label>Satuan kerja</label>
                                    <input type="text" id="edit-unit" class="form-control"  placeholder="Umum" disabled>
                                </div>
                                <div class="form-group">
                                    <label>No BPJS</label>
                                    <input type="text" id="edit-bpjs" class="form-control" placeholder="BPJS">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" id="edit-email" class="form-control"  placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label>No HP</label>
                                    <input type="text" id="edit-hp" class="form-control"  placeholder="No HP">
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <div>
                                        <label>Jenis Kelamin</label>
                                    </div>

                                    <div>
                                        <span class="radio radio-aqua">
                                            <input id="optionGenderL" name="optionGender" type="radio" value="L" checked>
                                            <label for="optionGenderL">
                                                Laki-Laki
                                            </label>
                                        </span>
                                        <span class="radio radio-red">
                                            <input id="optionGenderP" name="optionGender" type="radio" value="P" >
                                            <label for="optionGenderP">
                                                Perempuan
                                            </label>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea id="edit-address" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Penyakit Yang Pernah Diderita</label>
                                    <textarea id="edit-riwayat-penyakit" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Penyakit Keluarga/Turunan</label>
                                    <textarea id="edit-penyakit-turunan" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Riwayat Alergi</label>
                                    <textarea id="edit-riwayat-alergi" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Riwayat Vaksinasi</label>
                                    <textarea id="edit-riwayat-vaksinasi" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-edit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Edit Patient Selection Modal -->
        
    <!-- End of Patient -->

    </div>
</div>
<!-- end page content -->
@endsection

@section('js')
    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

    <!-- Datetime Picker -->
    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <!-- End of Datetime Picker -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- -->

    <!-- Sweet alert -->
    <script src="{{ asset('assets/template/sweet-alert/sweetalert.min.js') }}"></script>
    <!-- -->

    <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/js/helper/helper.js') }}"></script>

    <script src="{{ asset('assets/js/main/admin/employee.js') }}">
    </script>
@endsection