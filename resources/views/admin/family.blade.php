@extends('template.main')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>

    <!-- Sweet alert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/sweet-alert/sweetalert.min.css') }}"/>
    <!-- -->

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <!-- Form -->
    <!-- <link href="{{ asset('assets/template/css/theme_style.css') }}" id="rt_style_components" rel="stylesheet" type="text/css" /> -->
    <link href="{{ asset('assets/template/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/formlayout.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/theme-color.css') }}" rel="stylesheet" type="text/css" />
    <!-- -->
@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <!-- <div class="pull-left">
                    <div class="page-title">Daftar Semua Pasien</div>
                </div> -->
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    Pegawai</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Data Keluarga Pegawai</li>
                </ol>
            </div>
        </div>

        <!-- Add Family Member Modal -->
        <div class="modal fade" id="familymemberAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="add-title">Tambah Data Keluarga Pegawai</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="hidden" id="edit-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="id" id="add-id">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <div>
                                            <label>NIP
                                                <span class="required"></span>
                                            </label>
                                            <input type="text" id="add-nip" class="form-control" placeholder="" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <div>
                                            <label>Satker ID
                                                <span class="required"></span>
                                            </label>
                                            <input type="text" id="add-satkerid" class="form-control" placeholder="" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <div>
                                            <label>Nama Satker
                                                <span class="required"></span>
                                            </label>
                                            <input type="text" id="add-satkername" class="form-control" placeholder="" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Nama Anggota Keluarga
                                                <span class="required"></span>
                                            </label>
                                            <input type="text" id="add-nama" class="form-control" placeholder="Input Nama Anggota Keluarga">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Tanggal Lahir
                                                <span class="required"></span>
                                            </label>
                                            <input type="date" id="add-tgllahir" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <div>
                                            <label>Status Hubungan Dalam Keluarga
                                                <span class="required"></span>
                                            </label>
                                            <select class="form-control select2" id="select-kdkeluarga" style="width: 100%">
                                                <option></option>
                                              </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="data-nikah" style="display: none">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Tanggal Menikah
                                                <span class="required"></span>
                                            </label>
                                            <input type="date" id="add-tglnikah" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>No Surat Nikah
                                                <span class="required"></span>
                                            </label>
                                            <input type="text" id="add-nosuratnikah" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="form-group col-md-12">
                                        <div>
                                            <label>Jenis Pekerjaan
                                                <span class="required"></span>
                                            </label>
                                            <select class="form-control select2" id="select-kdkerja" style="width: 100%">
                                                <option></option>
                                              </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Nama Ayah
                                                <span class="required"></span>
                                            </label>
                                            <input type="text" id="add-namaayah" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Nama Ibu
                                                <span class="required"></span>
                                            </label>
                                            <input type="text" id="add-namaibu" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Alamat
                                            <span class="required"></span>
                                        </label>
                                        <textarea id="add-alamat" class="form-control" rows="3" placeholder="">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Keterangan
                                            <span class="required"></span>
                                        </label>
                                        <textarea id="add-keterangan" class="form-control" rows="3" placeholder="">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div>
                                            <label>Mendapatkan Tunjangan</label>
                                        </div>
                                        <div>
                                            <input id="add-tunjangan" type="checkbox" checked data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                       </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-add" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Add Family Member Modal -->

    <!-- End Of Employee -->

    <!-- Edit Family Member Selection Modal -->
    <div class="modal fade" id="familymemberEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Edit Data Keluarga Pegawai</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <input type="hidden" id="edit-token" name="_token" value="{{csrf_token()}}"/>
                            <input type="hidden" name="id" id="edit-id">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div>
                                        <label>NIP
                                            <span class="required"></span>
                                        </label>
                                        <input type="text" id="edit-nip" class="form-control" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div>
                                        <label>Satker ID
                                            <span class="required"></span>
                                        </label>
                                        <input type="text" id="edit-satkerid" class="form-control" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div>
                                        <label>Nama Satker
                                            <span class="required"></span>
                                        </label>
                                        <input type="text" id="edit-satkername" class="form-control" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <div>
                                        <label>Nama Anggota Keluarga
                                            <span class="required"></span>
                                        </label>
                                        <input type="text" id="edit-nama" class="form-control" placeholder="Input Nama Anggota Keluarga">
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div>
                                        <label>Tanggal Lahir
                                            <span class="required"></span>
                                        </label>
                                        <input type="date" id="edit-tgllahir" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div>
                                        <label>Status Hubungan Dalam Keluarga
                                            <span class="required"></span>
                                        </label>
                                        <select class="form-control select2" id="select-editkdkeluarga" style="width: 100%">
                                            <option></option>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="editdata-nikah" style="display: none">
                                <div class="form-group col-md-6">
                                    <div>
                                        <label>Tanggal Menikah
                                            <span class="required"></span>
                                        </label>
                                        <input type="date" id="edit-tglnikah" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div>
                                        <label>No Surat Nikah
                                            <span class="required"></span>
                                        </label>
                                        <input type="text" id="edit-nosuratnikah" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="form-group col-md-12">
                                    <div>
                                        <label>Jenis Pekerjaan
                                            <span class="required"></span>
                                        </label>
                                        <select class="form-control select2" id="select-editkdkerja" style="width: 100%">
                                            <option></option>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <div>
                                        <label>Nama Ayah
                                            <span class="required"></span>
                                        </label>
                                        <input type="text" id="edit-namaayah" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div>
                                        <label>Nama Ibu
                                            <span class="required"></span>
                                        </label>
                                        <input type="text" id="edit-namaibu" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Alamat
                                        <span class="required"></span>
                                    </label>
                                    <textarea id="edit-alamat" class="form-control" rows="3" placeholder="">
                                    </textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>Keterangan
                                        <span class="required"></span>
                                    </label>
                                    <textarea id="edit-keterangan" class="form-control" rows="3" placeholder="">
                                    </textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <div>
                                        <label>Mendapatkan Tunjangan</label>
                                    </div>
                                    <div>
                                        <input id="edit-tunjangan" type="checkbox" data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" id="submit-edit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Edit Family Member Selection Modal -->

    <!-- Data Pegawai -->
        <div class="col-md-12 col-sm-12">
            <div class="borderBox light bordered">
                <div class="borderBox-title tabbable-line">
                    <div class="caption">
                        <span class="caption-subject font-dark bold uppercase">Data Keluarga Pegawai</span>
                    </div>
                </div>
                <div class="borderBox-body">
                    <div class="card-body row">
                        {{csrf_field()}}

                        <input type="hidden" id="csrf" name="_token" value="{{csrf_token()}}"/>

                        <input type="hidden" name="tipe" value="pegawai">
                        <div class="col-md-12">
                          <div class="form-group">
                            <select id="search-nip" class="form-control">
                              <option></option>
                            </select>
                          </div>
                        </div>

                        <br/>

                        {{-- Detail Data Pegawai --}}

                        <div class="row col-lg-12">
                            <div class="col-lg-12 p-t-20">
                                <div class="form-group">
                                    <label class="float-label">Nama Lengkap
                                    </label>
                                    <input id="nama" name="nama" type="text" class="form-control" placeholder="" readonly="">
                                </div>
                            </div>
                            <div class="col-lg-12 p-t-20">
                                <div class="form-group">
                                    <label class="float-label">NIP
                                    </label>
                                    <input id="nip" name="nip" type="text" class="form-control" placeholder="" readonly="">
                                </div>
                            </div>
                            <div class="col-lg-12 p-t-20">
                                <div class="form-group">
                                    <label class="float-label">Satuan Kerja
                                    </label>
                                    <input id="unit" name="unit" type="text" class="form-control" placeholder="" readonly="">
                                    <input id="satkerid" name="satkerid" type="hidden" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <hr class="new1">
                        </div>
                        <div class="pull-right">
                            <button type="button" id="btn-family-member" class="add-family-member btn btn-success" style="display: none"><i class="fa fa-plus"></i> Tambah Data Keluarga </button>
                        </div>
                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width" id="familyDatatable">
                                <thead>
                                    <tr>
                                        <th width="20%"> Kode Satker </th>
                                        <th> Nama Satker </th>
                                        <th> Nama </th>
                                        <th> Tanggal Lahir </th>
                                        <th> NIP </th>
                                        <th> Posisi Keluarga </th>
                                        <th> Aksi </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

    <!-- End of Data Pegawai -->

    </div>
</div>
<!-- end page content -->
@endsection

@section('js')
    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

    <!-- Datetime Picker -->
    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <!-- End of Datetime Picker -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- -->

    <!-- Sweet alert -->
    <script src="{{ asset('assets/template/sweet-alert/sweetalert.min.js') }}"></script>
    <!-- -->

    <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/js/helper/helper.js') }}"></script>

    <script src="{{ asset('assets/js/main/admin/family.js') }}">
    </script>
@endsection
