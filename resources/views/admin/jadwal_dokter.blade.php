@extends('template.main')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>

    <!-- Sweet alert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/sweet-alert/sweetalert.min.css') }}"/>
    <!-- -->

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <!-- <div class="pull-left">
                    <div class="page-title">Daftar Semua Pasien</div>
                </div> -->
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                        Dokter</a>&nbsp;<i class="fa fa-angle-right"></i>
                        </li>
                        <li class="active">Jadwal Dokter</li>
                </ol>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="borderBox light bordered">
                <div class="borderBox-title tabbable-line">
                    <div class="caption">
                        <span class="caption-subject font-dark bold uppercase">Daftar Jadwal Dokter</span>
                    </div>
                </div>
                <div class="borderBox-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="borderBox_active">
                            <div class="pull-right">
                                <button type="button" class="add-jadwal btn btn-success"><i class="fa fa-plus"></i> Tambah Jadwal Dokter </button>
                            </div>
                            <input type="hidden" name="id" id="id_dokter" value="{{$id}}">
                            <div class="table-scrollable">
                                <table class="table table-hover table-checkable order-column full-width" id="jadwalDokterDatatable">
                                    <thead>
                                        <tr>
                                            <th width="40%"> Nama </th>
                                            <th width="30%" style="text-align: center"> Hari </th>
                                            <th width="10%" style="text-align: center"> Dari </th>
                                            <th width="10%" style="text-align: center"> Sampai </th>
                                            <th style="min-width:150px;"> Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Add Role Modal -->
        <div class="modal fade" id="jadwalAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Jadwal Dokter</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="hidden" id="add-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="id" id="add-id" value="{{$id}}">
                                <div class="form-group">
                                    <label>Hari</label>
                                    <select class="form-control select2" id="select-hari" style="width: 100%">
                                      <option></option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Jam - Dari</label>
                                    <input type="time" id="add-jam_dari" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Jam - Sampai</label>
                                    <input type="time" id="add-jam_sampai" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-add" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Add Role Modal -->

        <!-- Edit Role Modal -->
        <div class="modal fade" id="jadwalEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Permission</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="hidden" id="edit-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="edit-id" id="edit-id">
                                <div class="form-group">
                                    <label>Hari</label>
                                    <input type="text" id="edit-hari" class="form-control" placeholder="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Jam - Dari</label>
                                    <input type="time" id="edit-jam_dari" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Jam - Sampai</label>
                                    <input type="time" id="edit-jam_sampai" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-edit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Edit Patient Modal -->




    </div>
</div>
<!-- end page content -->
@endsection

@section('js')
    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

    <!-- Datetime Picker -->
    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <!-- End of Datetime Picker -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- -->

    <!-- Sweet alert -->
    <script src="{{ asset('assets/template/sweet-alert/sweetalert.min.js') }}"></script>
    <!-- -->

    <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/js/helper/helper.js') }}"></script>


    <script src="{{ asset('assets/js/main/admin/jadwal_dokter.js') }}">

    // <script src="{{ asset('assets/js/main/admin/permission.js') }}">
    </script>
@endsection
