@extends('template.main')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>

    <!-- Sweet alert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/sweet-alert/sweetalert.min.css') }}"/>
    <!-- -->

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    Pasien</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Daftar User</li>
                </ol>
            </div>
        </div>

        <div class="col-md-12 col-sm-12">
            <div class="borderBox light bordered">
                <div class="borderBox-title tabbable-line">
                    <div class="caption">
                        <span class="caption-subject font-dark bold uppercase">Daftar User</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a href="#borderBox_active" data-toggle="tab" class="active"> User Aktif </a>
                        </li>
                        <li class="nav-item">
                            <a href="#borderBox_archive" data-toggle="tab"> User Tidak Aktif </a>
                        </li>
                    </ul>
                </div>

                

                <div class="borderBox-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="borderBox_active">
                
                            <div class="pull-right">
                                <button type="button" class="add-user btn btn-success"><i class="fa fa-plus"></i> Tambah User </button>
                            </div>

                            <div class="table-scrollable">
                                <table class="table table-hover table-checkable order-column full-width" id="activeUserDatatable">
                                    <thead>
                                        <tr>
                                            <th width="20%"> Nama </th>
                                            <th> NIP </th>
                                            <th> Username </th>
                                            <th> Email </th>
                                            <th> Role </th>
                                            <th> Update Pada </th>
                                            <th style="min-width:150px;"> Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="borderBox_archive">

                            <div class="table-scrollable">
                                <table class="table table-hover table-checkable order-column full-width" id="archiveUserDatatable">
                                    <thead>
                                        <tr>
                                            <th width="20%"> Nama </th>
                                            <th> NIP </th>
                                            <th> Username </th>
                                            <th> Email </th>
                                            <th> Role </th>
                                            <th> Update Pada </th>
                                            <th style="min-width:150px;"> Aksi </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Add User Modal -->
        <div class="modal fade" id="userAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="hidden" id="add-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="id" id="edit-id">

                                <div class="form-group">
                                    <select id="search-nip" class="form-control select2" style="width: 100%">
                                        <option></option>
                                    </select>
                                </div>

                                <hr> 

                                <div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control select2" id="add-role" style="width: 100%" >
                                      <option></option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" id="add-username" class="form-control" placeholder="shaka_nagano">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" id="add-password" class="form-control" placeholder="*****">
                                </div>
                                <div class="form-group">
                                    <span class="col-md-6">
                                        <label>Aktif</label>
                                    </span>
                                    <span class="col-md-6">
                                        <input id="add-aktif" type="checkbox" checked data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                   </span>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-add" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Add Patient Selection Modal -->


        <!-- Edit Patient Selection Modal -->
        <div class="modal fade" id="userEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Data User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="hidden" id="edit-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="id" id="edit-id">
                                
                                <div class="form-group">
                                    <select id="edit-nip" class="form-control select2" style="width: 100%">
                                        <option></option>
                                    </select>
                                </div>

                                <hr> 

                                <div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control select2" id="edit-role" style="width: 100%" >
                                      <option></option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" id="edit-username" class="form-control" placeholder="shaka_nagano">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" id="add-password" class="form-control" placeholder="*****">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" id="edit-email" class="form-control"  placeholder="Your_email@example.com">
                                </div>
                                <div class="form-group">
                                    <span class="col-md-6">
                                        <label>Aktif</label>
                                    </span>
                                    <span class="col-md-6">
                                        <input id="edit-aktif" type="checkbox" checked data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                   </span>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-edit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Edit Patient Selection Modal -->


        

    </div>
</div>
<!-- end page content -->
@endsection

@section('js')
    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

    <!-- Datetime Picker -->
    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <!-- End of Datetime Picker -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- -->

    <!-- Sweet alert -->
    <script src="{{ asset('assets/template/sweet-alert/sweetalert.min.js') }}"></script>
    <!-- -->

    <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/js/helper/helper.js') }}"></script>

    <script src="{{ asset('assets/js/main/admin/user.js') }}">
    </script>
@endsection