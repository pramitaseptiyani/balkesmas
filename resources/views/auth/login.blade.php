<!DOCTYPE html>
<html lang="en">
<head>
	<title>E-Balkesmas</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{ asset('assets/login/images/icons/favicon.ico') }}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/login/vendor/bootstrap/css/bootstrap.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/login/vendor/animate/animate.css') }}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/login/vendor/css-hamburgers/hamburgers.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/login/vendor/animsition/css/animsition.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/login/vendor/select2/select2.min.css') }}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/login/vendor/daterangepicker/daterangepicker.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/login/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/login/css/main.css') }}">
<!--===============================================================================================-->
</head>
<body style="background-color: #666666;">
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form validate-form">
					<form method="POST" action="{{ route('login') }}">
					
						@csrf
						<span class="login100-form-title p-b-43" style="color: #ad2444;font-family: Impact;">
							<h1>E-BALKESMAS</h1>
						</span>
						<div class="wrap-input100 validate-input" data-validate="Username is required">
							<input id="username" type="text" class="form-control input100" name="username" required autocomplete="email" autofocus>
							<span class="focus-input100"></span>
							<span class="label-input100">Username</span>
						</div>
						<div class="wrap-input100 validate-input" data-validate="Password is required">
							<input id="password" type="password" class="form-control input100" name="password" required autocomplete="current-password">
							<span class="focus-input100"></span>
							<span class="label-input100">Password</span>
						</div>
						<div class="container-login100-form-btn">
							<button type="submit" class="login100-form-btn" style="background : #ad2444;">
								Login
							</button>
						</div>
					</form>
				
					<br/><br/>
					<div class="row">
						<div class="col-md-6">
							<a href="{{url('/reservasi')}}">
								<button class="login100-form-btn" style="background : #2E96E3;">
									Reservasi
								</button>
							</a>
						</div>

						{{-- <div class="col-md-6">
							<a href="{{url('/survei')}}">
								<button class="login100-form-btn" style="background : #2E96E3;">
									Isi Survei
								</button>
							</a>
						</div> --}}
					</div>

				</div>
				



				<!-- <div class="validate-form">
					
					
				</div> -->

				<div class="login100-more" style=" background-image: url('{{asset('assets/login/images/login_background.jpg')}}');"> 
				</div>
			</div>
		</div>
	</div>
	
<!--===============================================================================================-->
	<script src="{{ asset('assets/login/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('assets/login/vendor/animsition/js/animsition.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('assets/login/vendor/bootstrap/js/popper.js') }}"></script>
	<script src="{{ asset('assets/login/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('assets/login/vendor/select2/select2.min.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('assets/login/vendor/daterangepicker/moment.min.js') }}"></script>
	<script src="{{ asset('assets/login/vendor/daterangepicker/daterangepicker.js') }}"></script>
<!--===============================================================================================-->
	<script src="{{ asset('assets/login/vendor/countdowntime/countdowntime.js') }}"></script>
<!--===============================================================================================-->
	<!-- <script src="{{ asset('assets/login/js/main.js') }}"></script> -->
	<script src="{{ asset('assets/login/js/login.js') }}">
	</script>
	<script src="{{ asset('assets/js/helper/helper.js') }}">
	</script>

</body>
</html>