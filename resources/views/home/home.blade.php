@extends('template.main')

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Dashboard</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Dashboard</li>
                            </ol>
                        </div>
                    </div>

                     <!-- chart start -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header>Dashboard </header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body no-padding height-9">
                                    
                                <!-- end widget -->
                                </div>
                                <div class="card-body no-padding height-6">
                                    <div class="row">
                                         <div class="col-md-8">
                                            <canvas id="home_admin"></canvas>
                                        </div>
                                        <div class="col-md-4">
                                            <canvas id="chartjs_doughnut_admin"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <!-- Chart end -->
                </div>
            </div>
            <!-- end page content -->
        </div>
        <!-- end page container -->
@endsection