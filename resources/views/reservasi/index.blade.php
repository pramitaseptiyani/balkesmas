<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <title>E-Balkesmas</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/bootstrap/css/bootstrap.min.css') }}"/>    
    <!-- Material Design Lite CSS -->
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material/material.min.css') }}"/> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/material_style.css') }}"/>
    <!-- sweet alert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/sweet-alert/sweetalert.min.css') }}"/>
    <!-- Theme Styles -->
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/theme_style.css') }}"/> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/plugins.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/responsive.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/theme-color.css') }}"/>
    <!-- favicon -->
    <link rel="stylesheet" type="image/x-icon" href="{{ asset('assets/template/img/favicon.ico') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>

    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/> -->

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/jquery-tags-input/jquery-tags-input.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main/doctor/autocomplete.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2-bootstrap.min.css') }}"/>

    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/>  -->
    
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/> 
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/> 

    <link href="{{ asset('assets/css/custom/step_form.css') }}" rel="stylesheet" type="text/css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>

    <link href="{{ asset('assets/css/custom/styles.css') }}" rel="stylesheet" type="text/css" />

</head>
<body>


    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="container-fluid" id="grad1">
            <div class="row justify-content-center mt-0">
                <div class="col-11 col-md-10 col-lg-8 col-sm-12 text-center p-0 mt-3 mb-2">
                    <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                        <h2><strong>Reservasi</strong></h2>
                        <p>Isi form di bawah ini untuk melakukan reservasi pemeriksaan di Balkesmas</p>
                        <div class="row">
                            <div class="col-md-12 mx-0">
                                <form id="msform">
                                    <!-- progressbar -->
                                    <ul id="progressbar">
                                        <li class="active fa fa-search"><strong>Cari MR</strong></li>
                                        <li id="pick"><strong>Pilih Poli</strong></li>
                                        <li id="handshake"><strong>Buat Janji</strong></li>
                                        <!-- <li id="confirm"><strong>Selesai</strong></li> -->
                                    </ul> <!-- fieldsets -->
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title">Cari MR</h2> 
                                            <div class="row">
                                                <div class="col-lg-2"></div>
                                                <div class="col-lg-6 col-sm-12">
                                                    <input type="text" id="no_mr" name="no_mr" class="form-control"  placeholder="No MR">
                                                </div>

                                                <div class="col-lg-2 col-sm-12">
                                                    <button id="search-data" type="button" class="btn btn-info">Cari</button>
                                                </div>
                                                <div class="col-lg-2"></div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4"></div>

                                                <div class="col-md-4">
                                                    <a href="#" id="show-registration">
                                                        Belum mempunyai MR? Klik Disini
                                                    </a>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>

                                            <div id="panel-registrasi-mr" style="display: none;">
                                                <div class="row">
                                                    <div class="col-lg-2"></div>
                                                    <div class="col-lg-8">
                                                        <p class="text-center">Anda belum Terdaftar. Pilih Salah Satu untuk Mendaftar.</p>

                                                        
                                                    </div>
                                                    <div class="col-lg-2"></div>  
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="card card-topline-aqua">
                                                            <div class="card-body no-padding ">
                                                                <div class="doctor-profile"> 
                                                                    <div class="profile-usertitle">
                                                                        <div class="doctor-name font-black"><strong>Pegawai </strong></div>
                                                                    </div>
                                                                        <p>Pasien dengan NIP.</p> 
                                                                    <div class="profile-userbuttons">
                                                                        <a href="{{url ('registration/patient/add')}}" type="button" class="btn btn-primary btn-rounded" style="color: white;">Pilih</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="card card-topline-red">
                                                            <div class="card-body no-padding ">
                                                                <div class="doctor-profile"> 
                                                                    <div class="profile-usertitle">
                                                                        <div class="doctor-name font-black"><strong>Keluarga </strong></div>
                                                                    </div>
                                                                        <p>Pasien dengan <i>reference </i> NIP.</p> 
                                                                    <div class="profile-userbuttons">
                                                                        <a href="{{url ('registration/patient/addfamily')}}" type="button" class="btn btn-primary btn-rounded" style="color: white;">Pilih</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="card card-topline-green">
                                                            <div class="card-body no-padding ">
                                                                <div class="doctor-profile"> 
                                                                    <div class="profile-usertitle">
                                                                        <div class="doctor-name font-black"><strong>Pasien Umum </strong></div>
                                                                    </div>
                                                                        <p>Pasien dengan NIK.</p> 
                                                                    <div class="profile-userbuttons">
                                                                        <a href="{{url ('registration/patient/addgeneral')}}" type="button" class="btn btn-primary btn-rounded" style="color: white;">Pilih</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="panel-mr-found" style="display: none;">
                                                <input id="id_pasien" name="id_pasien" type="hidden" class="form-control" placeholder="">


                                                <div class="row">
                                                    <div class="col-lg-4 col-sm-12">
                                                        <div>
                                                            <label><strong>Nama</strong></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-8 col-sm-12">
                                                        <input type="text" id="result_nama" name="result_nama" class="form-control" disabled>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-lg-4 col-sm-12">
                                                        <div>
                                                            <label><strong>Tanggal Lahir</strong></label> 
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-8 col-sm-12">
                                                        <input type="text" id="result_ttl" name="result_ttl" class="form-control" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <input type="button" id="next-step-2" name="next" class="next action-button" value="Selanjutnya" style="display: none;"  />
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title">Pilih Poli</h2> 

                                            <div class="row">
                                                <div class="col-lg-4 col-sm-12">
                                                    <div>
                                                        <label><strong>Poli</strong></label>      
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-sm-12">
                                                    <select id="select2-poli" class="select2-poli form-control select2" style="width: 100%"> 
                                                    </select>
                                                </div>
                                            </div>

                                            <br/>
                                            <div id="doctor-schedule" class="row" style="display: none">
                                                <div class="col-md-12 text-center font-black"><strong>Jadwal Poli</strong></div>

                                                <table class="table table-striped">
                                                  <thead>
                                                    <tr>
                                                      <th scope="col">Hari</th>
                                                      <th scope="col">Nama Dokter</th>
                                                      <th scope="col">Jam Kerja</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="table-doctor-schedule">
                                                  </tbody>
                                                </table>
                                            </div>
                                        </div> <input type="button" id="previous-step-1" name="previous" class="previous action-button-previous" value="Sebelumnya" /> 
                                        <input type="button" id="next-step-3" name="next" class="next action-button" value="Selanjutnya" style="display: none;"/>
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title">Buat Janji</h2>
                                            
                                            <div class="row">
                                                <div class="col-lg-4 col-sm-12">
                                                    <div>
                                                        <label><strong>Tanggal Periksa</strong></label>
                                                    </div>

                                                    <div class="tab-content" style="text-align: left;">
                                                        <input type="text"  name="date" id="datepicker" class="datepicker form-control" autocomplete="off">              
                                                    </div>
                                                </div>

                                                <div class="col-lg-2"></div>
                                                <!-- <div class="col-lg-6 col-sm-12" id="doctor-by-date">
                                                        <div>
                                                            <label><strong>Tenaga Kesehatan yang Bertugas</strong></label>
                                                        </div>
                                                        <div id="doctors-name" style="font-weight: normal;">
                                                        </div>
                                                </div> -->

                                                <div class="col-lg-6 col-sm-12">
                                                    <h6 style="color: red"> *) Tanggal tidak bisa dipilih pada hari libur atau kuota sudah penuh. </h6>
                                                    <h6 style="color: blue"> *) Maksimal rentang reservasi selama 2 minggu </h6>
                                                </div>
                                            </div>

                                            <div id="panel-patient-list" class="row">
                                                <div class="col-md-6">
                                                    <div>
                                                        <label><strong>Jumlah Pasien yang Terdaftar : </strong></label>
                                                    </div>
                                                    <div id="patients-count" style="font-weight: normal;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div> <input type="button" id="previous-step-2" name="previous" class="previous action-button-previous" value="Sebelumnya" /> <input type="button" id="confirmation" name="next-confirm" class="next-confirm action-button" value="Konfirmasi" />
                                    </fieldset>
                                    <fieldset>
                                        <div class="form-card">
                                            <h2 class="fs-title text-center">Berhasil!</h2> <br><br>
                                            <div class="row justify-content-center">
                                                <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                                            </div> <br><br>
                                            <div class="row justify-content-center">
                                                <div class="col-7 text-center">
                                                    <h5>Anda telah Berhasil Melakukan Reservasi</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <script src="{{ asset('assets/template/jquery.min.js') }}"></script> -->

    <!-- <script src="{{ asset('assets/template/jquery.min.js') }}"></script> -->
    <script src="{{ asset('assets/template/popper/popper.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.blockui.min.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/template/moment.min.js') }}"></script>
    <script src="{{ asset('assets/template/counterup/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/template/counterup/jquery.counterup.min.js') }}"></script>
    
    <!-- bootstrap -->
    <script src="{{ asset('assets/template/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/template/summernote/summernote.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-inputmask/bootstrap-inputmask.min.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-colorpicker/js/bootstrap-colorpicker-init.js') }}"></script>
    
    <!-- Common js-->
    <script src="{{ asset('assets/template/app.js') }}"></script>
    <script src="{{ asset('assets/template/layout.js') }}"></script>
    <script src="{{ asset('assets/template/theme-color.js') }}"></script>
    <script src="{{ asset('assets/template/profile.js') }}"></script>
    <!-- material -->
    <script src="{{ asset('assets/template/material/material.min.js') }}"></script>

    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

     <!--tags input-->
    <script src="{{ asset('assets/template/jquery-tags-input/jquery-tags-input.js') }}"></script>
    <script src="{{ asset('assets/template/jquery-tags-input/jquery-tags-input-init.js') }}"></script>

    <!--select2-->
    <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/template/select2/js/select2-init.js') }}"></script>
    
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

    <!-- Datetime Picker -->
    <!-- <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script> -->
    <!-- End of Datetime Picker -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- -->

    <!-- Sweet alert -->
    <script src="{{ asset('assets/template/sweet-alert/sweetalert.min.js') }}"></script>
    <!-- -->

    <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/js/helper/helper.js') }}"></script>
    

    <script type="text/javascript">
        var base_path = "{{url('/')}}";
        var base_url = "{{url('/')}}";

        $(document).ready(function() { 
            form_wizard();
            select2Poli();
            onChangePoli();

            $("#show-registration").click(function(){
                $('#panel-mr-found').css("display", "none");
                $('#panel-registrasi-mr').css("display", "block");

                $('#next-step-2').css("display", "none");
                searchNIP();
            });

            
            searchData()            
        });

        function form_wizard() {
            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;

            $(".next").click(function(){

                current_fs = $(this).parent();
                next_fs = $(this).parent().next();

                //Add Class Active
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function(now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        next_fs.css({'opacity': opacity});
                    },
                    duration: 600
                });
            });

            $(".previous").click(function(){

                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();

                //Remove class active
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the previous fieldset
                previous_fs.show();

                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function(now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({'opacity': opacity});
                    },
                    duration: 600
                });
            });

            $('.radio-group .radio').click(function(){
                $(this).parent().find('.radio').removeClass('selected');
                $(this).addClass('selected');
            });

            $(".submit").click(function(){
                return false;
            })
        }

        function select2Poli() {
            $('#select2-poli').select2({
                  ajax: {
                    dataType: 'json',
                    url: base_path+'/api/poli/select2',
                    data: function(params) {
                        return {
                          search: params.term,
                        }
                    },
                    processResults: function (data, page) {
                        var list = [];
                        var result = data;
                        $.each(result.data, function(index,item){
                          list.push({
                            id: item.id,
                            text: item.nama,
                          })
                        });
                        return {
                          results: list
                        };
                       },
                }
            });
        }

        function onChangePoli() {
            $( "#select2-poli" ).change(function() {

                $("#overlay").fadeIn(300);

                if($("#select2-poli").val() != null){
                    showDoctorSchedule();

                    showQuota();

                    if(parseInt($("#select2-poli").val()) == 2){
                        initGigiDatePicker()
                        onChangeDatepicker();
                    }
                    else {
                        initDatePicker()
                        onChangeDatepicker();
                    }

                    $('#next-step-3').removeAttr("style")
                }

                $("#overlay").fadeOut(300);
            });
        }

        function onChangeDatepicker() {
            $( "#datepicker" ).change(function() {
                // showJumlahReservasi();
                showPasien();
                $('#patient-list').css("display", "block")

                //showDokter();
                
                nexStep3()
            });
        }

        function showQuota() {
            _token = $('meta[name=csrf_token]').attr('content');
            currentDate = new Date();
            endDate = new Date(Date.now() + 12096e5); //2 weeks after
            id_poli = $("#select2-poli").val();

            /*current*/
            currentYear = currentDate.getFullYear();
            currentMonth = String(currentDate.getMonth() + 1).padStart(2, '0');
            currentDay = String(currentDate.getDate()).padStart(2, '0');

            now = currentYear+'-'+currentMonth+'-'+currentDay;

            /**/

            /*2 weeks later*/
            endYear = endDate.getFullYear();
            endMonth = String(endDate.getMonth() + 1).padStart(2, '0');
            endDay = String(endDate.getDate()).padStart(2, '0');

            end = endDate+'-'+endDate+'-'+endDate;

            /**/

            if(parseInt(id_poli) == 2){

            }
        }

        // function libraryDateBawaan() {
        //     var disabledDays = ['2022-01-28', '2022-11-29'];
        //     var disabledDays = [];

        //    $('.datepicker').datepicker({
        //         format: 'mm/dd/yyyy',
        //         minDate: 0,
        //         beforeShowDay: function(date){
        //           var dayNr = date.getDay();
        //             if (dayNr==0  ||  dayNr==6){
        //                 if (enableDays.indexOf(formatDate(date)) >= 0) {
        //                     return true;
        //                 }
        //                 return false;
        //             }
        //             if (disabledDays.indexOf(formatDate(date)) >= 0) {
        //                return false;
        //             }
        //             return true; // bawaan library
        //         }
        //    });
        // }

        function initDatePicker() {
            $("#overlay").fadeIn(300);

            let initDay = moment().add('1', 'days')
            tempDay = initDay

            let enableDays = [];
            for (let i = 1; i <= 14; i++) {
                enable = tempDay.format('YYYY-MM-DD');
                enableDays.push(enable)

                tempDay = initDay.add('1', 'days')
            }            

            let disabledDays = [];

            restrictDatepicker(enableDays, disabledDays)

            $("#overlay").fadeOut(300);
        }

        function initGigiDatePicker() {
            $("#overlay").fadeIn(300);

            let initDay = moment().add('1', 'days')
            tempDay = initDay

            let disabledDays = [];
            let enableDays = [];

            let dates = []
            for (let i = 1; i <= 14; i++) {
                enable = tempDay.format('YYYY-MM-DD');
                dates.push(enable)

                tempDay = initDay.add('1', 'days')
            }     

            /* check avalability */
            let input = {
                "_token" : $('meta[name=csrf_token]').attr('content'),
                "dates" : dates
            }
            /* end of check availability */

            send(base_url+"/reservasi/api/date_availability", input, false, function(response){
                enableDays = response.data
                restrictDatepicker(enableDays, disabledDays)
                
            });                   

            $("#overlay").fadeOut(300);
        }

        function restrictDatepicker(enableDays, disabledDays) {
            $('.datepicker').datepicker('destroy').datepicker({
                format: 'yyyy-mm-dd',
                minDate: 0,
                beforeShowDay: function(date){
                  var dayNr = date.getDay();
                    if (dayNr==0  ||  dayNr==6){
                        return false;
                    }
                    else {
                        if (enableDays.indexOf(formatDate(date)) >= 0) {
                            return true;
                        }
                        return false;
                    }

                    if (disabledDays.indexOf(formatDate(date)) >= 0) {
                       return false;
                    }
                    return false;
                }
           });
        }

        function formatDate(d) {
            var day = String(d.getDate())
         //add leading zero if day is is single digit

            if (day.length == 1)
                day = '0' + day
            var month = String((d.getMonth()+1))
            //add leading zero if month is is single digit
            if (month.length == 1)
                month = '0' + month
            return d.getFullYear() + '-' + month + "-" + day;
        }

        function showDoctorSchedule() {
            let input = {
                _token : $('meta[name=csrf_token]').attr('content'),
                id_poli : $("#select2-poli").val()
            }

            send(base_url+"/reservasi/api/doktor_schedule", input, false, function(response){
                responsedata = response.data

                let html_schedule = '';
                temp_hari = 0;

                $.each( responsedata, function( key, value ) {
                    if(temp_hari == value.id_hari){
                        html_schedule += `<tr>
                                        <td></td>
                                        <td>${value.has_user.name}</td>
                                        <td>${value.jam_dari} - ${value.jam_sampai}</td>
                                    </tr>`;
                    }

                    else{
                        html_schedule += `<tr>
                                        <td>${value.has_hari.hari}</td>
                                        <td>${value.has_user.name}</td>
                                        <td>${value.jam_dari} - ${value.jam_sampai}</td>
                                    </tr>`;
                    }
                    
                    temp_hari = value.id_hari;
                });

                $('#doctor-schedule').css("display", "block")
                $('#table-doctor-schedule').html(html_schedule)

            })
        }

        function nexStep3() {
            $("#btn-to-step-3").click(function(e){
                e.preventDefault();

                $('#step-3').css("display", "block")

                setTimeout(function(){
                    searchData()
                }, 500);

            });
        }

        function searchData() {
            $("#search-data").click(function(e){
                $("#overlay").fadeIn(300);

                e.preventDefault();

                let search = {
                    _token : $('meta[name=csrf_token]').attr('content'),
                    no_mr : $( "#no_mr" ).val(),
                }

                send(base_url+"/reservasi/api/patient_by_MR", search, true, function(response){
                    responsedata = response.data

                    if(responsedata != null){
                        $('#panel-mr-found').css("display", "block");    
                        $('#panel-registrasi-mr').css("display", "none"); 
 
                        $('#next-step-2').removeAttr("style");

                        $('#result_nama').val(responsedata.patient_nm)
                        $('#result_ttl').val(responsedata.patient_ttl)   
                        $('#id_pasien').val(responsedata.id)


                        confirm()   
                    }
                    else {
                        $('#panel-mr-found').css("display", "none");
                        $('#panel-registrasi-mr').css("display", "block");

                        $('#next-step-2').css("display", "none");

                        searchNIP();
                    }
                    
                })

                $("#overlay").fadeOut(300);
            });
        }

        function searchNIP() {
            $('#search-nip').select2({
                placeholder: 'Ketikan NIP',
                theme: "classic",
                minimumInputLength: 3,
                ajax: {
                    dataType: 'json',
                    url: base_path+'/reservasi/api/get_pegawai',
                    delay: 800,
                    data: function(params) {
                        return {
                            nip: params.term
                        }
                    },
                    processResults: function (data, page) {
                        var list = [];
                        var result = data;
                        $.each(result, function(index,item){
                            list.push({
                                id      : item.nip,
                                text    : item.nip +" - "+ item.nama_pegawai,
                                nama    : item.nama_pegawai,
                                satker  : item.nama_satker,
                                satkerid  : item.kode_satker,
                                hp      : item.no_hp,
                                email   : item.email_dinas,
                                alamat  : item.alamat,
                                gender  : item.jenis_kelamin,
                                bpjs  : item.askes,
                            })
                        });
                        return {
                            results: list
                        };
                    },
                }
            });

            $('#search-nip').on('select2:select', function (e) {
                var data = e.params.data;
                var satker = data.satker
                var unit = satker.substring(0, satker.indexOf("-")); 
     

                $("#nama").val(data.nama);
                $("#nip").val(data.id);
                $("#unit").val(unit);
                $("#satkerid").val(data.satkerid);
                $("#no_hp").val(data.hp);
                $("#email").val(data.email);
                $("#alamat").val(data.alamat);
                $("#bpjs").val(data.bpjs);
            });
        }

        function showJumlahReservasi(){
            let inputJumlah = {
                _token : $('meta[name=csrf_token]').attr('content'),
                tanggal : $( "#datepicker" ).val(),
                userid_dokter : $("#select2-dokter").val()
            }

            send(base_url+"/reservasi/api/jumlah", inputJumlah, false, function(response){
                if(response.data[0].jumlah == 0){
                    $('#btn-next').css("display", "block")
                    $('#patient-list').html('Belum ada reservasi')
                }
                else if(response.data[0].jumlah <= 5){
                    $('#btn-next').css("display", "block")
                }
            })
        }

        function showPasien() {
            let inputPasien = {
                _token : $('meta[name=csrf_token]').attr('content'),
                tanggal : $( "#datepicker" ).val(),
                id_poli : $("#select2-poli").val()
            }


            send(base_url+"/reservasi/api/patient_by_poli", inputPasien, false, function(response){
                responsedata = response.data


                let html_patient_list = '';
                // $('#patients-count').html(html_patient_list);
                $('#patients-count').html(responsedata.length);

                // $.each( responsedata, function( key, value ) {
                //     html_patient_list += `<p>${value.has_pasien.patient_nm}</p>`;
                // });

                // $('#patients-count').html(html_patient_list)
            })
        }

        // function showDokter() {
        //     let input = {
        //         _token : $('meta[name=csrf_token]').attr('content'),
        //         tanggal : $( "#datepicker" ).val(),
        //         id_poli : $("#select2-poli").val()
        //     }

        //     console.log(input)

        //     send(base_url+"/reservasi/api/doktor_working_day", input, false, function(response){
        //         responsedata = response.data

                
        //         // $('#patients-count').html(responsedata.length);
        //     })
        // }

        function confirm() {
            $("#confirmation").click(function(e){
                e.preventDefault();

                let id_pasien = $("#id_pasien").val();

                var formData = {
                    "_token"        : $('meta[name=csrf_token]').attr('content'),
                    'id_pasien'     : id_pasien,
                    'list_poli'     : parseInt($("#select2-poli").val()),
                    'tanggal'          : $( "#datepicker" ).val()
                }

                send(base_url+"/reservasi/api/isExist_reservasi", formData, true, function(response){
                    if(response.isSuccess){
                        var sendData = {
                            _token : $('meta[name=csrf_token]').attr('content'),
                            tanggal : $( "#datepicker" ).val(),
                            id_patient : id_pasien,
                            id_poli : $("#select2-poli").val(),
                            nama : null,
                            no_hp : null,
                            id_card : null,
                            id_kategori : null,
                            satkerid : null
                        }

                        send(base_url+"/reservasi/api/confirm", sendData, true, function(response){
                            setTimeout(function(){
                                window.location = base_url; 
                            }, 1000);

                        }) 
                    }
                });

                
                
            });
        }
    </script>
</body>
</html>