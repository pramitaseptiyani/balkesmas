@extends('template.main')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/>

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <!-- Form -->
    <link href="{{ asset('assets/template/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/formlayout.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/theme-color.css') }}" rel="stylesheet" type="text/css" />
    <!-- -->

@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class="pull-left">
                    <div class="page-title">Daftar Semua Reservasi</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    Reservasi</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Daftar Reservasi</li>
                </ol>
            </div>
        </div>

         <!-- chart start -->
        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Daftar Semua Reservasi </header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body no-padding height-9">
                        
                    <!-- end widget -->
                    </div>
                    <div class="card-body">
                        <!-- <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <button type="button"
                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-circle btn-primary"
                                data-toggle="modal" data-target="#addPatientModal">
                                    Tambah Pasien
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div> -->
                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width" id="patientDatatable">
                                <thead>
                                    <tr>
                                        <th> No MR </th>
                                        <th> Nama </th>
                                        <th> Poli </th>
                                        <th> Nama Dokter </th>
                                        <th> Tanggal </th>
                                        <th> Aksi </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <!-- Chart end -->

        

    </div>
</div>
<!-- end page content -->
@endsection

@section('js')
    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

    <!-- Datetime Picker -->
    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <!-- End of Datetime Picker -->

    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#patientDatatable').DataTable({
                    
                ajax: {
                    type: "GET",
                    dataType: "json",
                    url: APIurl+'receptionist/reservasi/list',
                },
                columns: [        
                    { data: 'patient_mr_number', name: 'patient_mr_number' },
                    { data: 'patient_nm', name: 'patient_nm' },
                    { data: 'poli', name: 'poli' },
                    { data: 'nama_dokter', name: 'nama_dokter' },
                    { data: 'tanggal', name: 'tanggal' },
                    { data: 'id', name: 'id', className: 'text-center', 
                        render: function(data, type, row, meta){
                            var nextButton = '<button type="button" title="Lanjut ke Perawat" id="nextButton" data-id="'+data+'" data-id_pasien="'+row['id_pasien']+'" data-id_poli="'+row['id_poli']+'" class="label label-warning menu-next-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-stethoscope"></i></button>';

                            button = nextButton;

                            return button;
                        },
                        searchable: false,
                        sortable: false
                    }
                ],
                order: [[ 5, "desc" ]]
            });

            //Click See detail of patient
            $('#patientDatatable').on( 'click', '.menu-next-button', function () {

                var id = $(this).data('id');

                let sendData = {
                    _token : $('meta[name=csrf_token]').attr('content'),
                    id_pasien : $(this).data('id_pasien'),
                    list_poli : $(this).data('id_poli')
                }
                send(base_url+"/receptionist/poli/register", sendData, true, function(response){

                });
            });

        });
    </script>
@endsection