<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="csrf_token" content="{{ csrf_token() }}" />
	<title>E-Balkesmas</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/bootstrap/css/bootstrap.min.css') }}"/>    
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material/material.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/material_style.css') }}"/>
    <!-- sweet alert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/sweet-alert/sweetalert.min.css') }}"/>
    <!-- Theme Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/theme_style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/plugins.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/responsive.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/css/theme-color.css') }}"/>
    <!-- favicon -->
    <link rel="stylesheet" type="image/x-icon" href="{{ asset('assets/template/img/favicon.ico') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/jquery-tags-input/jquery-tags-input.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main/doctor/autocomplete.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2-bootstrap.min.css') }}"/>

    <!-- Test Collapsible -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <!-- End of Test Collapsible -->

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/> 


	<link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>

    <!-- Sweet alert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/sweet-alert/sweetalert.min.css') }}"/>
    <!-- -->

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <!-- Form -->
    <!-- <link href="{{ asset('assets/template/css/theme_style.css') }}" id="rt_style_components" rel="stylesheet" type="text/css" /> -->
    <link href="{{ asset('assets/template/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/formlayout.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/theme-color.css') }}" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .panel-heading a:after {
            font-family:'Glyphicons Halflings';
            content:"\e114";
            float: right;
            color: grey;
        }
        .panel-heading a.collapsed:after {
            content:"\e080";
        }
    </style>

</head>
<body>
	
	<div class="limiter">
		<div class="row"  id="step-1" style="padding-top: 3vh;">
			<div class="col-lg-2 col-sm-12"></div>
			<div class="col-lg-8 col-sm-12" style="text-align: center;">
	            <div class="borderBox light bordered">
	                <div class=" tabbable-line">
	                    <div class="caption">
	                        <span class="caption-subject font-dark bold uppercase" style="text-align: center;"><strong>Reservasi Pelayanan Balkesmas<strong>
	                        </span>
	                        <br/><br/>
	                    </div>
	                </div>

	                <div class="borderBox-body">
	                    <div class="tab-content" style="text-align: left;">
	                    	<div class="row">
		                        <div class="col-lg-4 col-sm-12">
					                <div>
					                    <label><strong>Poli</strong></label>      
					                </div>
					            </div>
					            <div class="col-lg-8 col-sm-12">
					            	<select id="select2-poli" class="select2-poli form-control select2" style="width: 100%"> 
					                </select>
					            </div>
					        </div>
					        <br/>

					        <!-- <div class="row">
		                        <div class="col-lg-4 col-sm-12">
					                <div>
					                    <label><strong>Dokter</strong></label>
					                        
					                </div>
					            </div>
					            <div class="col-lg-8 col-sm-12">
					            	<select id="select2-dokter" class="select2-dokter form-control select2" placeholder="Pilih Dokter" style="width: 100%"> 
					                </select>
					            </div>
					        </div> -->

                            <!-- <div class="row" style="display: none;"> -->
                            <div class="row"></div>
                                <div class="row">
                                    <div class="col-md-5"></div>
                                    <div class="col-md-2">
                                        <p><strong>Jadwal Dokter</strong></p>
                                        
                                    </div>
                                    <div class="col-md-5"></div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8">
                                        <table style="border:1px solid black; margin-left:auto;margin-right:auto;">
                                            <thead>
                                                <td width="20%">Hari</td>
                                                <td width="20%">Tanggal</td>
                                                <td width="40%">Nama</td>
                                                <td width="20%">Jam Kerja</td>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Senin</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Selasa</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-2"></div>

                                </div>
                            </div>

                            
	                       
	                    </div>
	                </div>
	            </div>
			</div>
			<div class="col-lg-2 col-sm-12"></div>
		</div>

		<div class="row" style="padding-top: 3vh;">
			<div class="col-lg-2 col-sm-12"></div>
			<div class="col-lg-8 col-sm-12" id="step-2" style="display: none;">
	            <div class="borderBox light bordered">
	                <div class=" tabbable-line">
	                </div>

	                <div class="borderBox-body">
                        <div class="row">
                            <div class="col-lg-4 col-sm-12">
                                <div>
                                    <label><strong>Tanggal Periksa</strong></label>
                                </div>

                                <div class="tab-content" style="text-align: left;">
                                    <input type="text" name="ttl" id="date" class="form-control"  placeholder="YYYY-MM-DD">
                                   
                                </div>
                            </div>

                            <div class="col-lg-2"></div>
                            <div class="col-lg-6 col-sm-12" id="patient-list" style="display: none;">
                                <div>
                                    <label><strong>Daftar Pasien Terdaftar</strong></label>
                                </div>
                                <div id="patients-name" style="font-weight: normal;">
                                </div>
                            </div>
                        </div>

                        <br/>
                        <div class="text-center" id="btn-next" style="display: none;">
                            <button id="btn-to-step-3" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Reservasi</button>
                        </div>

                        <div class="text-center" id="btn-full" style="display: none;">
                            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-default" disabled>Kuota Penuh</button>
                        </div>
	                </div>
	            </div>
			</div>
			<div class="col-lg-2 col-sm-12"></div>
		</div>

        <div class="row" style="padding-top: 3vh;">
            <div class="col-lg-2 col-sm-12"></div>
            <div class="col-lg-8 col-sm-12" id="step-3"  style= "display: none;">
                <div class="borderBox light bordered">
                    <div class=" tabbable-line">
                    </div>

                    <div class="borderBox-body">
                       
                        <div class="row">
                            <div class="col-lg-4 col-sm-12">
                                <div>
                                    <label><strong>Nomor MR</strong></label>   
                                </div>
                            </div>
                            <div class="col-lg-8 col-sm-12">
                                <input type="text" id="no_mr" name="no_mr" class="form-control"  placeholder="No MR">
                            </div>
                        </div>
                        <br/>

                        <div class="text-center" id="btn-search">
                            <button id="search-data" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Cari</button>
                        </div>

                        <div id="search-result" style="display: none;">
                            <input id="id_pasien" name="id_pasien" type="hidden" class="form-control" placeholder="">

                            <div class="row">
                                <div class="col-lg-4 col-sm-12">
                                    <div>
                                        <label><strong>Nama</strong></label>
                                            
                                    </div>
                                </div>
                                <div class="col-lg-8 col-sm-12">
                                    <input type="text" id="result_nama" name="result_nama" class="form-control" disabled>
                                </div>
                            </div>

                            <br/>
                            <div class="row">
                                <div class="col-lg-4 col-sm-12">
                                    <div>
                                        <label><strong>Tanggal Lahir</strong></label>
                                            
                                    </div>
                                </div>
                                <div class="col-lg-8 col-sm-12">
                                    <input type="text" id="result_ttl" name="result_ttl" class="form-control" disabled>
                                </div>
                            </div>

                            <br/>
                            <div class="col-lg-12 p-t-20"> 
                                <div class="form-group">
                                    <label>Alamat
                                      <span class="required"></span>
                                    </label>
                                    <textarea id="alamat" name="alamat" type="text" class="form-control" rows="3" placeholder="" readonly=""></textarea>
                                </div>
                            </div>

                            <div class="col-lg-12 p-t-20"> 
                                <div class="form-group">
                                    <label>Email
                                      <span class="required"></span>
                                    </label>
                                    <input id="email" name="email" type = "email" class="form-control" placeholder="Not Set"></input>
                                </div>
                            </div>
                        </div>

                        <br/>

                        <div id="input-manual" style="display: none">

                            <div id="kategori">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-12">
                                        <div>
                                            <label><strong>Kategori</strong></label>
                                                
                                        </div>
                                    </div>
                                    <div>
                                        <span class="radio radio-aqua">
                                            <input id="optionKategori1" name="optionKategori" type="radio" value="1" checked>
                                            <label for="optionKategori1">
                                                Pegawai Kumham
                                            </label>
                                        </span>
                                        <span class="radio radio-red">
                                            <input id="optionKategori3" name="optionKategori" type="radio" value="3">
                                            <label for="optionKategori3">
                                                Umum
                                            </label>
                                        </span>
                                    </div>
                                </div>
                                <br/>
                            </div>

                            <hr/>

                            <div id="input-manual-umum">
                                <div class="row col-lg-12" id="row-nip">
                                    <div class="col-lg-4 col-sm-12">
                                        <div>
                                            <label><strong>NIP</strong></label>      
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-sm-12">
                                      <div class="form-group">
                                        <select id="search-nip" class="form-control">
                                          <option></option>
                                        </select>
                                      </div>
                                    </div>
                                    <br/>
                                </div>

                                <div class="row col-lg-12" id="row-satker">
                                    <div class="col-lg-4 col-sm-12">
                                        <div>
                                            <label><strong>Unit Kerja</strong></label>
                                                
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-sm-12">
                                        <input type="text" id="unit" name="unit" class="form-control"  placeholder="" disabled>
                                        <input id="satkerid" name="satkerid" type="hidden" class="form-control" placeholder="">
                                    </div>
                                    <br/>
                                </div>
                                <br/>

                                <div class="row col-lg-12" id="row-nik" style="display: none">
                                    <div class="col-lg-4 col-sm-12">
                                        <div>
                                            <label><strong>NIK</strong></label>
                                                
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-sm-12">
                                        <input type="text" id="nik" name="nik" class="form-control"  placeholder="">
                                    </div>
                                </div>
                                <br/>
                                <br/>

                                <div class="row col-lg-12">
                                    <div class="col-lg-4 col-sm-12">
                                        <div>
                                            <label><strong>Nama</strong></label>
                                                
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-sm-12">
                                        <input type="text" id="nama" name="nama" class="form-control"  placeholder="Shaka Nagano">
                                    </div>
                                </div>
                                <br/>


                                <div style="margin-bottom: 15vh"></div>

                                <hr/>

                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                      <h4 class="panel-title">
                                        <a class="colpsible-panel" data-toggle="collapse" data-parent="#accordion" href="#collapsePribadi">
                                          Data Pribadi
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapsePribadi" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        
                                        <div class="row">
                                            <div class="col-lg-4 col-sm-12">
                                                <div>
                                                    <label>
                                                        <strong>Tanggal Lahir</strong>
                                                        <span class="required"></span>
                                                    </label>
                                                        
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-sm-12">
                                                <input type="text" id="date" class="form-control"  placeholder="YYYY-MM-DD">
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="row">
                                            <div class="col-lg-4 col-sm-12">
                                                <div>
                                                    <label>
                                                        <strong>No HP</strong>
                                                        <span class="required"></span>
                                                    </label>
                                                        
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-sm-12">
                                                <input type="text" id="no_hp" name="no_hp" class="form-control"  placeholder="">
                                            </div>
                                        </div>
                                        <br/>

                                        <br/>
                                        <div class="row">
                                            <div class="col-lg-4 col-sm-12">
                                                <div>
                                                    <label><strong>Alamat</strong></label>
                                                        
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-sm-12">
                                                <textarea id="alamat" name="alamat" type="text" class="form-control" rows="3" placeholder=""></textarea>
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="row">
                                            <div class="col-lg-4 col-sm-12">
                                                <div>
                                                    <label>
                                                        <strong>Email</strong>
                                                        <span class="required"></span>
                                                    </label>
                                                        
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-sm-12">
                                                <input type="email" id="email" name="email" class="form-control"  placeholder="">
                                            </div>
                                        </div>

                                        <br/>
                                        <div class="row">
                                            <div class="col-lg-4 col-sm-12">
                                                <div>
                                                    <label><strong>No MR Lama</strong></label>
                                                        
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-sm-12">
                                                <input type="text" id="mr_lama" name="mr_lama" class="form-control"  placeholder="">
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="row">
                                            <div class="col-lg-4 col-sm-12">
                                                <div>
                                                    <label><strong>BPJS</strong></label>
                                                        
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-sm-12">
                                                <input type="text" id="bpjs" name="bpjs" class="form-control"  placeholder="">
                                            </div>
                                        </div>
                                        <br/>

                                      </div>
                                    </div>
                                </div>

                                
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                      <h4 class="panel-title">
                                        <a class="colpsible-panel" data-toggle="collapse" data-parent="#accordion" href="#collapsePenyakit">
                                          Data Penyakit
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapsePenyakit" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        
                                        <div class="row">
                                            <div class="col-lg-4 col-sm-12">
                                                <div>
                                                    <label><strong>Penyakit yang Pernah Diderita</strong></label>
                                                        
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-sm-12">
                                                <textarea id="riwayat_penyakit" name="riwayat_penyakit" type="text" class="form-control" rows="3" placeholder=""></textarea>
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="row">
                                            <div class="col-lg-4 col-sm-12">
                                                <div>
                                                    <label><strong>Penyakit Keluarga/Turunan</strong></label>
                                                        
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-sm-12">
                                                <textarea id="penyakit_turunan" name="penyakit_turunan" type="text" class="form-control" rows="3" placeholder=""></textarea>
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="row">
                                            <div class="col-lg-4 col-sm-12">
                                                <div>
                                                    <label><strong>Riwayat Alergi</strong></label>
                                                        
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-sm-12">
                                                <textarea id="riwayat_alergi" name="riwayat_alergi" type="text" class="form-control" rows="3" placeholder=""></textarea>
                                            </div>
                                        </div>
                                        <br/>

                                        <div class="row">
                                            <div class="col-lg-4 col-sm-12">
                                                <div>
                                                    <label><strong>Riwayat Vaksinasi</strong></label>
                                                        
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-sm-12">
                                                <textarea id="riwayat_vaksinasi" name="riwayat_vaksinasi" type="text" class="form-control" rows="3" placeholder=""></textarea>
                                            </div>
                                        </div>
                                        <br/>
                                    </div>
                                        

                                        

                                      </div>
                                    </div>
                                </div>
                                

                               
                            </div>
                        </div>


                        <br/>
                        <div class="text-center" id="row-confirm" style="display: none;">
                            <button id="confirm" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">Konfirmasi Reservasi</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-12"></div>
        </div>
	</div>

	<script src="{{ asset('assets/template/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/template/popper/popper.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.blockui.min.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('assets/template/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/template/moment.min.js') }}"></script>
    <script src="{{ asset('assets/template/counterup/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/template/counterup/jquery.counterup.min.js') }}"></script>
    
    <!-- bootstrap -->
    <script src="{{ asset('assets/template/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/template/summernote/summernote.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-inputmask/bootstrap-inputmask.min.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script src="{{ asset('assets/template/bootstrap-colorpicker/js/bootstrap-colorpicker-init.js') }}"></script>
    
    <!-- Common js-->
    <script src="{{ asset('assets/template/app.js') }}"></script>
    <script src="{{ asset('assets/template/layout.js') }}"></script>
    <script src="{{ asset('assets/template/theme-color.js') }}"></script>
    <script src="{{ asset('assets/template/profile.js') }}"></script>
    <!-- material -->
    <script src="{{ asset('assets/template/material/material.min.js') }}"></script>

    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

     <!--tags input-->
    <script src="{{ asset('assets/template/jquery-tags-input/jquery-tags-input.js') }}"></script>
    <script src="{{ asset('assets/template/jquery-tags-input/jquery-tags-input-init.js') }}"></script>

    <!--select2-->
    <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/template/select2/js/select2-init.js') }}"></script>
	
	<script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

    <!-- Datetime Picker -->
    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <!-- End of Datetime Picker -->

    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- -->

    <!-- Sweet alert -->
    <script src="{{ asset('assets/template/sweet-alert/sweetalert.min.js') }}"></script>
    <!-- -->

    <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>
    <script src="{{ asset('assets/js/helper/helper.js') }}"></script>

    <script type="text/javascript">
    	var base_path = "{{url('/')}}";
    	var base_url = "{{url('/')}}";

    	$(document).ready(function() { 

    		select2Poli();
    		onChangePoli();
    		onChangeDokterpoli();
            onChangeDatepicker();

            $('#step-3').css("display", "block")
            searchData()
    	});

    	function select2Poli() {
    		$('#select2-poli').select2({
		          ajax: {
		            dataType: 'json',
		            url: base_path+'/api/poli/select2',
		            data: function(params) {
		                return {
		                  search: params.term,
		                }
		            },
		            processResults: function (data, page) {
		                var list = [];
		                var result = data;
		                $.each(result.data, function(index,item){
		                  list.push({
		                    id: item.id,
		                    text: item.nama,
		                  })
		                });
		                return {
		                  results: list
		                };
			           },
		        }
		    });
    	}

    	function select2Dokter() {
    		$('#select2-dokter').select2({
	            allowClear: true,
	            ajax: {
	                dataType: 'json',
	                url: base_path+'/api/dokterpoli/select2',
	                delay: 200,
	                type: 'POST',
	                data: function(params) {
	                     return {
	                     	_token: $('meta[name=csrf_token]').attr('content'),
	                        search: params.term,
	                        poli_id: $("#select2-poli").val()
	                    }
	                },
	                processResults: function (data, page) {
	                    var list = [];
	                    var result = data.data;
	                    $.each(result, function(index,item){
	                        list.push({
	                            id: item.user.id,
	                            text: item.user.name
	                        })
	                    });
	                    return {
	                        results: list
	                    };
	                },
	            }
	        });
    	}

    	function onChangePoli() {
    		$( "#select2-poli" ).change(function() {
    			// $("#select2-dokter").val('').trigger('change')
				// select2Dokter();

                if($("#select2-poli").val() != null){
                    $('#step-2').css("display", "block")

                    $('#btn-next').css("display", "block")
                    $('#patient-list').html('Belum ada reservasi')
                }
			});
    	}

    	function onChangeDokterpoli() {
    		$( "#select2-dokter" ).change(function() {
                if($("#select2-dokter").val() != null){
                    $('#step-2').css("display", "block")
                }
			});
    	}

        function onChangeKategori() {
            $('input[type=radio][name="optionKategori"]').change(function () {   
                let kategoriSelected = $('input[type=radio][name=optionKategori]:checked').val();

                if(kategoriSelected == '1'){
                    $('#row-nip').css("display", "block")
                    $('#row-satker').css("display", "block")

                    $('#row-nik').css("display", "none") 
                }
                else{
                    $('#row-nik').css("display", "block")

                    $('#row-nip').css("display", "none")
                    $('#row-satker').css("display", "none")
                }
            });
        }

        function onChangeDatepicker() {
            $( "#date" ).change(function() {
                showJumlahReservasi();
                showPasien();
                $('#patient-list').css("display", "block")
                
                nexStep3()
            });
        }

        function nexStep3() {
            $("#btn-to-step-3").click(function(e){
                e.preventDefault();

                $('#step-3').css("display", "block")

                setTimeout(function(){
                    searchData()
                }, 500);

            });
        }

        function searchData() {
            $("#search-data").click(function(e){
                e.preventDefault();

                let search = {
                    _token : $('meta[name=csrf_token]').attr('content'),
                    no_mr : $( "#no_mr" ).val(),
                }

                send(base_url+"/reservasi/api/patient_by_MR", search, true, function(response){
                    responsedata = response.data

                    if(responsedata != null){
                        $('#search-result').css("display", "block");    
                        $('#input-manual').css("display", "none"); 

                        $('#row-confirm').css("display", "block"); 

                        $('#result_nama').val(responsedata.patient_nm)
                        $('#result_ttl').val(responsedata.patient_ttl)   
                        $('#id_pasien').val(responsedata.id)


                        confirm()   
                    }
                    else {
                        $('#search-result').css("display", "none");
                        $('#input-manual').css("display", "block");


                        $('#row-confirm').css("display", "block"); 
                        confirm();

                        onChangeKategori()
                        searchNIP();
                    }
                    
                })
            });
        }

        function searchNIP() {
            $('#search-nip').select2({
                placeholder: 'Ketikan NIP',
                theme: "classic",
                minimumInputLength: 3,
                ajax: {
                    dataType: 'json',
                    url: base_path+'/reservasi/api/get_pegawai',
                    delay: 800,
                    data: function(params) {
                        return {
                            nip: params.term
                        }
                    },
                    processResults: function (data, page) {
                        var list = [];
                        var result = data;
                        $.each(result, function(index,item){
                            list.push({
                                id      : item.nip,
                                text    : item.nip +" - "+ item.nama_pegawai,
                                nama    : item.nama_pegawai,
                                satker  : item.nama_satker,
                                satkerid  : item.kode_satker,
                                hp      : item.no_hp,
                                email   : item.email_dinas,
                                alamat  : item.alamat,
                                gender  : item.jenis_kelamin,
                                bpjs  : item.askes,
                            })
                        });
                        return {
                            results: list
                        };
                    },
                }
            });

            $('#search-nip').on('select2:select', function (e) {
                var data = e.params.data;
                var satker = data.satker
                var unit = satker.substring(0, satker.indexOf("-")); 

                console.log(data)        

                $("#nama").val(data.nama);
                $("#nip").val(data.id);
                $("#unit").val(unit);
                $("#satkerid").val(data.satkerid);
                $("#no_hp").val(data.hp);
                $("#email").val(data.email);
                $("#alamat").val(data.alamat);
                $("#bpjs").val(data.bpjs);
            });
        }

        function showJumlahReservasi(){
            let inputJumlah = {
                _token : $('meta[name=csrf_token]').attr('content'),
                tanggal : $( "#date" ).val(),
                userid_dokter : $("#select2-dokter").val()
            }

            send(base_url+"/reservasi/api/jumlah", inputJumlah, false, function(response){
                if(response.data[0].jumlah == 0){
                    $('#btn-next').css("display", "block")
                    $('#patient-list').html('Belum ada reservasi')
                }
                else if(response.data[0].jumlah <= 5){
                    $('#btn-next').css("display", "block")
                }
            })
        }

        function showPasien() {
            let inputPasien = {
                _token : $('meta[name=csrf_token]').attr('content'),
                tanggal : $( "#date" ).val(),
                userid_dokter : $("#select2-dokter").val(),
                id_poli : $("#select2-poli").val()
            }

            send(base_url+"/reservasi/api/patient_by_poli", inputPasien, false, function(response){
                responsedata = response.data

                let html_patient_list = ''
                $.each( responsedata, function( key, value ) {
                    html_patient_list += `<p>${value.patient_nm}</p>`;
                });

                $('#patients-name').html(html_patient_list)
            })
        }

        function confirm() {
            $("#confirm").click(function(e){
                e.preventDefault();

                let id_pasien = $("#id_pasien").val();

                if(id_pasien != ''){
                    var sendData = {
                        _token : $('meta[name=csrf_token]').attr('content'),
                        tanggal : $( "#date" ).val(),
                        userid_dokter : $("#select2-dokter").val(),
                        id_patient : id_pasien,
                        id_poli : $("#select2-poli").val(),
                        nama : null,
                        no_hp : null,
                        id_card : null,
                        id_kategori : null,
                        satkerid : null
                    }
                }
                else{
                    var kategoriSelected = $('input[type=radio][name=optionKategori]:checked').val();
                    if(kategoriSelected == 1) {
                        id_card = $("#search-nip").val()
                        satkerid = $("#satkerid").val()
                    }
                    else{
                        id_card = $("#nik").val()
                        satkerid = null  
                    } 

                    var sendData = {
                        _token : $('meta[name=csrf_token]').attr('content'),
                        tanggal : $( "#date" ).val(),
                        userid_dokter : $("#select2-dokter").val(),
                        id_patient : id_pasien,
                        id_poli : $("#select2-poli").val(),
                        nama : $("#nama").val(),
                        no_hp : $("#no_hp").val(),
                        id_card : id_card,
                        id_kategori : kategoriSelected,
                        satkerid : satkerid
                    }
                }

                send(base_url+"/reservasi/api/confirm", sendData, true, function(response){
                    setTimeout(function(){
                        window.location = base_url; 
                    }, 1000);

                })
                
            });
        }
    </script>
</body>
</html>