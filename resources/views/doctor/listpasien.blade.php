@extends('template.main')

@section('css')
    <!-- adminlte css -->
    <!-- Google Font: Source Sans Pro -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{ asset('assets/template/fontawesome-free/css/all.min.css') }}"/>
      <!-- daterange picker -->
      <link rel="stylesheet" href="{{ asset('assets/template/daterangepicker/daterangepicker.css') }}"/>
      <!-- iCheck for checkboxes and radio inputs -->
      <link rel="stylesheet" href="{{ asset('assets/template/icheck-bootstrap/icheck-bootstrap.min.css') }}"/>
      <!-- Bootstrap Color Picker -->
      <link rel="stylesheet" href="{{ asset('assets/template/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}"/>
      <!-- Tempusdominus Bootstrap 4 -->
      <link rel="stylesheet" href="{{ asset('assets/template/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}"/>
      <!-- Select2 -->
      <link rel="stylesheet" href="{{ asset('assets/template/select2/css/select2.min.css') }}"/>
      <link rel="stylesheet" href="{{ asset('assets/template/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}"/>
      <!-- Bootstrap4 Duallistbox -->
      <link rel="stylesheet" href="{{ asset('assets/template/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}"/>
      <!-- BS Stepper -->
      <link rel="stylesheet" href="{{ asset('assets/template/bs-stepper/css/bs-stepper.min.css') }}"/>
      <!-- dropzonejs -->
      <link rel="stylesheet" href="{{ asset('assets/template/dropzone/min/dropzone.min.css') }}"/>
      <!-- Theme style -->
      <link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}"/>
    <!-- -->
    <style type="text/css">
        .modal {
        padding: 0 !important;
        }
        .modal .modal-dialog-editpatient {
            width: 100% !important;
            max-width: none !important;
            height: 100% !important;
            margin: 0 !important;
        }
        .modal .modal-content-editpatient {
            height: 100% !important;
            border: 0 !important;
            border-radius: 0 !important;
        }
        .modal .modal-body-editpatient {
            overflow-y: auto !important;
        }
    
    </style>
@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class="pull-left">
                    <div class="page-title">DATA PEMERIKSAAN POLI</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    Dokter</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Daftar Dokter</li>
                </ol>
            </div>
        </div>

         <!-- chart start -->
        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Daftar Pasien Hari Ini </header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body no-padding height-9">
                        
                    <!-- end widget -->
                    </div>
                    <div class="card-body">
                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width antriandoctor" id="patientDoctorDatatable">
                                <thead>
                                    <tr>
                                        <th width="13%"> Antrian </th>
                                        <th> Nama Poli </th>
                                        <th> NIP/NIK </th>
                                        <th> Nama </th>
                                        <th> Tanggal Periksa </th>
                                        <th> Status </th>
                                        <th width="15%"> Aksi </th>
                                        <th width="7%"> Done </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <!-- Chart end -->

         <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Daftar Pasien Rujukan Poli Lain</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body no-padding height-9">
                        
                    <!-- end widget -->
                    </div>
                    <div class="card-body">
                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width antriandoctor" id="patientRujukanDatatable">
                                <thead>
                                    <tr>
                                        <th width="13%"> Antrian </th>
                                        <th> Asal Poli </th>
                                        <th> NIP/NIK </th>
                                        <th> Nama </th>
                                        <th> Tanggal Rujukan </th>
                                        <th> Status </th>
                                        <th width="15%"> Aksi </th>
                                        <th width="7%"> Done </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <!-- Chart end -->

         <!-- Modal Confirm Done -->

            <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="modal-title" id="myModalLabel">Apakah Anda Yakin Pasien Selesai Diperiksa?</h5>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="modal-btn-ya">Ya</button>
                    <button type="button" class="btn btn-danger" id="modal-btn-no">Tidak</button>
                  </div>
                </div>
              </div>
            </div>

            <div class="alert" role="alert" id="result"></div>
         <!-- -->

        <!-- Patient Detail Selection Modal -->
        <div class="modal fade" id="patientDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Detail Pasien</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                {{csrf_field()}}

                                <input type="hidden" id="token" name="_token" value="{{csrf_token()}}"/>
                                <div class="form-group">
                                    <label>No MR</label>
                                    <input type="text" id="detail-MR" class="form-control" placeholder="SET90328392.02" disabled>
                                </div>
                                <div class="form-group">
                                    <label>NIP/NIK</label>
                                    <input type="text" id="detail-nip" class="form-control" placeholder="NIP/NIK" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" id="detail-nama" class="form-control" placeholder="Nama" disabled>
                                </div>
                                <div class="form-group">
                                    <label>No BPJS</label>
                                    <input type="text" id="detail-bpjs" class="form-control" placeholder="BPJS" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" id="detail-ttl" class="form-control"  placeholder="Tanggal Lahir" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Unit</label>
                                    <input type="text" id="detail-unit" class="form-control"  placeholder="Unit" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" id="detail-email" class="form-control"  placeholder="Email" disabled>
                                </div>
                                <div class="form-group">
                                    <label>No HP</label>
                                    <input type="text" id="detail-hp" class="form-control"  placeholder="No HP" disabled>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6">
                                 <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea id="detail-address" class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Penyakit Yang Pernah Diderita</label>
                                    <textarea id="detail-riwayat-penyakit" class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Penyakit Keluarga/Turunan</label>
                                    <textarea id="detail-penyakit-turunan" class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Riwayat Alergi</label>
                                    <textarea id="detail-riwayat-alergi" class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Riwayat Vaksinasi</label>
                                    <textarea id="detail-riwayat-vaksinasi" class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div> 
                            </div>
                        </div> 
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Patient Detail Selection Modal -->

        <!-- Edit Patient Selection Modal -->
        <div class="modal fade" id="patientEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-lg modal-dialog-editpatient" role="document">
                <div class="modal-content modal-content-editpatient">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Data Pasien</h5>
                    </div>
                    <div class="modal-body modal-body-editpatient">
                        <div class="row" style="width: 100% !important;">
                            <div class="col-md-12 col-sm-12" style="padding-left: 0px; padding-right: 0px; margin-left: 2px !important; width: 100% !important;">
                                <div class="white-box" style="float: left !important; padding-top: 0px !important;width: 100% !important;">
                                    <!-- Nav tabs -->
                                    <div class="p-rl-20">
                                        <ul class="nav customtab nav-tabs" role="tablist" style="padding-bottom: 10px;">
                                            <li class="nav-item"><a href="#tab1" class="nav-link active"  data-toggle="tab" >Input Diagnosa</a></li>
                                            <li class="nav-item"><a href="#tab2" class="nav-link" data-toggle="tab">Biodata</a></li>
                                            <li class="nav-item"><a href="#tab3" class="nav-link" data-toggle="tab">Riwayat Pemeriksaan</a></li>
                                            <li class="nav-item"><a href="#tab4" class="nav-link" data-toggle="tab">Riwayat Resep</a></li>
                                            <li class="nav-item"><a href="#tab5" class="nav-link" data-toggle="tab">Riwayat Lab</a></li>
                                            <li class="nav-item"><a href="#tab6" class="nav-link" data-toggle="tab">Riwayat Fisioterapi</a></li>
                                        </ul>
                                    </div>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active fontawesome-demo" id="tab1">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label>No Pemeriksaan Dokter</label>
                                                        <input type="text" class="form-control" id="id_periksa_dokter" name="id_periksa_dokter">
                                                        <input type="hidden" class="form-control" id="id_pasien_antri" name="id_pasien_antri">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>S</label>
                                                        <textarea class="form-control" rows="3" placeholder="Enter ..." id="sanamnesa" name="sanamnesa"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>O</label>
                                                        <textarea class="form-control" rows="3" placeholder="Enter ..." id="oanamnesa" name="oanamnesa"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>A</label>
                                                        <textarea class="form-control" rows="3" placeholder="Enter ..." id="aanamnesa" name="aanamnesa"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>P</label>
                                                        <textarea class="form-control" rows="3" placeholder="Enter ..." id="panamnesa" name="panamnesa"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab2">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <label>No MR</label>
                                                    <input type="text" id="edit-MR" class="form-control" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nama</label>
                                                    <input type="text" id="edit-nama" class="form-control" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label>NIP/NIK</label>
                                                    <input type="text" id="edit-nip" class="form-control" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label>No BPJS</label>
                                                    <input type="text" id="edit-bpjs" class="form-control" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label>Tanggal Lahir</label>
                                                    <input type="text" id="edit-ttl" class="form-control"  disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label>Unit</label>
                                                    <input type="text" id="edit-unit" class="form-control"  disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" id="edit-email" class="form-control" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label>No HP</label>
                                                    <input type="text" id="edit-hp" class="form-control" disabled>
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat</label>
                                                    <input type="text" id="edit-address" class="form-control"  disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab3">
                                             <!-- chart start -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-scrollable">
                                                        <table class="table table-hover table-checkable order-column full-width" id="riwayatperiksadatatable">
                                                            <thead>
                                                                <tr>
                                                                    <th width="15%"> Tgl Periksa </th>
                                                                    <th> Poli </th>
                                                                    <th> Perawat </th>
                                                                    <th> Keluhan </th>
                                                                    <th> Dokter </th>
                                                                    <th> S-Anamnesa </th>
                                                                    <th width="15%"> Aksi </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Chart end -->
                                        </div>
                                        <div class="tab-pane" id="tab4">
                                            <!-- chart start -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="table-scrollable">
                                                        <table class="table table-hover table-checkable order-column full-width" id="riwayatresepdatatable">
                                                            <thead>
                                                                <tr>
                                                                    <th width="15%"> Tgl Resep </th>
                                                                    <th> Poli </th>
                                                                    <th> Nama Obat </th>
                                                                    <th> Aturan Pakai </th>
                                                                    <th> Jml </th>
                                                                    <th> Satuan </th>
                                                                    <th width="15%"> Aksi </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Chart end -->
                                        </div>
                                        <div class="tab-pane" id="tab5">
                                            <!-- chart start -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-scrollable">
                                                        <table class="table table-hover table-checkable order-column full-width" id="riwayatlabdatatable">
                                                            <thead>
                                                                <tr>
                                                                    <th > ID Periksa</th>
                                                                    <th> tgl periksa </th>
                                                                    <th> no_lab </th>
                                                                    <th> dokter </th>
                                                                    <th> lab </th>
                                                                    <th> hasil </th>
                                                                    <th> nilai </th>
                                                                    <th > Aksi </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Chart end -->
                                        </div>
                                        <div class="tab-pane" id="tab6">
                                            <!-- chart start -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-scrollable">
                                                        <table class="table table-hover table-checkable order-column full-width" id="riwayatfisiodatatable">
                                                            <thead>
                                                                <tr>
                                                                    <th > ID Periksa </th>
                                                                    <th> tgl_periksa </th>
                                                                    <th> no_lab </th>
                                                                    <th> dokter </th>
                                                                    <th> fisio </th>
                                                                    <th> hasil </th>
                                                                    <th> nilai </th>
                                                                    <th > Aksi </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Chart end -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-9 col-sm-9" style="float: left !important; margin-left: -200px !important;">
                            <button type="button" id="btnresep" class="btn btn-warning">Resep</button>
                            <button type="button" id="btnlanjutan" class="btn btn-info">Periksa Lanjutan</button>
                        </div>
                        <div class="col-md-2 col-sm-2" style="float: right !important;">
                            <button type="button" id="simpan" class="btn btn-success">Simpan</button>
                            <button type="button" id="btndoctorclose" class="btn btn-danger">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Edit Patient Selection Modal -->

         <!-- Input Resep Selection Modal -->
         <div class="modal fade" id="resepModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Input Resep</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                 {{csrf_field()}}

                                <input type="hidden" id="token" name="_token" value="{{csrf_token()}}"/>
                                <div class="form-group">
                                    <label>No Pemeriksaan Dokter</label>
                                    <input type="text" class="form-control" id="idperiksadokter_resep" name="idperiksadokter_resep">
                                </div>
                                <div class="form-group">
                                    <label>Petugas Farmasi</label>
                                    <input type="text" class="form-control" id="petugas_farmasi" name="petugas_farmasi">
                                </div>
                                <div class="form-group" >
                                    <label>Tanggal Resep</label>
                                    <div class= "input-append date form_datetime">
                                        <table>
                                            <tr>
                                                <td> <input type="text" class="form-control" id="tglresep" style="padding-left:10px; width: 350px; height: 40px;" readonly></td>
                                                <td style="padding-left:10px;"><span class="add-on"><i class="fa fa-calendar icon-th"></i></span></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <label>List Obat</label>
                                <br>
                                <!-- Table -->
                                <div style="min-height:230;max-height: 230;overflow-x:auto;overflow-y:auto">
                                <div class="autocomplete">
                                  <table id="mainTable" class="listobat" style="overflow-x:auto;text-align:left; width: 100%;">
                                      <tbody>
                                          <tr align="center" style="padding: 5px; height: 40px;text-align: center; background-color: #98d9eb;width: 100%;min-width: 600;">
                                            <!-- <th width="5">No</th> -->
                                            <th style="width: 30%;">Nama / Komposisi</th>
                                            <th style="width: 5%;">ID</th>
                                            <th style="width: 13%;">Satuan</th>
                                            <th style="width: 10%;">Stock</th>
                                            <th style="width: 6%;">Jumlah</th>
                                            <th style="width: 30%;">Aturan Pakai</th>
                                          </tr>
                                        <berulang>
                                          <tr style="vertical-align:top;" id="tta">
                                              <td id="noangka" rowspan="3" colspan="5" style="width: 150%;">
                                                    <input style="width: 150%;height: 50px;" type="text" name="obnama[]" id="obnama" class="obnama" data-obnama="1" value="Klik Button Tambah Obat" style="color: red;text-decoration: italic;" readonly />
                                              </td>
                                          </tr>
                                        </berulang>
                                      </tbody>
                                  </table>
                                </div>
                                </div>
                                <!-- Table -->

                                <input type="hidden" id="counter" value="0">
                                <input type="hidden" id="statopen" value="0">
                                <br>
                                <input type="button" class="btn btn-sm btn-block btn-warning" id="addobat" value="Tambah Obat" align="center" onClick="tambahdata()" style="text-align:center; color:black; font-weight:bold; width: 30%; " />
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <!-- <a href="/doctor/cetak_resep" class="btn btn-primary" target="_blank">CETAK RESEP</a> -->
                        <a id="cetakresep" class="btn btn-primary" target="_blank">CETAK RESEP</a>
                        <button type="button" id="simpanresep" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" id="closemdlresep">Close</button>
                    </div>
                </div>
            </div>
        </div>
         <!-- End of Input Resep Selection Modal -->


         <!-- Input Laboratorium Selection Modal -->
         <div class="modal fade" id="labModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-editpatient" role="document">
                <div class="modal-content modal-content-editpatient">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Input Laboratorium</h5>
                    </div>
                    <div class="modal-body modal-body-editpatient">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="card card-box">
                                    <div class="card-body " id="bar-parent2">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                {{csrf_field()}}
                                                <input type="hidden" id="token" name="_token" value="{{csrf_token()}}"/>
                                                <input type="hidden" id="idtr_lab" name="idtr_lab">
                                                <div class="form-group">
                                                    <label>Nama</label>
                                                    <input type="text" class="form-control" id="nmpasien_lab" readonly>
                                                </div>
                                                 <div class="form-group">
                                                    <label>Alamat / Telp</label>
                                                    <input type="text" class="form-control" id="almtpasien_lab" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>E-mail</label>
                                                    <input type="text" class="form-control" id="email_lab">
                                                </div>
                                                <div class="form-group">
                                                    <label>Umur</label>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <input type="text" class="form-control" id="umur_lab" style="width: 400px;">
                                                            </td>
                                                            <td style="padding-left: 20px;">
                                                                <input type="radio" name="gender" id="g_l" value="L"> L
                                                                <input type="radio" name="gender" id="g_p" value="P" > P
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <label>Dokter</label>
                                                    <input type="text" class="form-control" id="dokter_lab" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat</label>
                                                    <input type="text" class="form-control" id="almt_dokter_lab" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Telp</label>
                                                    <input type="text" class="form-control" id="telp_lab">
                                                </div>
                                                <div class="form-group">
                                                    <label>Tgl</label>
                                                    <div class= "input-append date form_datetime">
                                                        <input type="text" id="tgllab" style="padding-left:10px; width: 350px; height: 40px;">
                                                        <span class="add-on"><i class="fa fa-calendar icon-th"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Diagnosis / Keterangan Poli</label>
                                                    <textarea class="form-control" rows="3" id="anamnesa_lab"></textarea>
                                                </div>
                                            </div>
                                            <!-- -->
                                            <?php if ($doctorid == 4) { ?> <!-- jika user dokternya gigi --> 
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Dental XRAY</label>
                                                    <table border="1">
                                                        <tr>
                                                            <td>
                                                                <?php for ($i=8;$i>0;$i--) {
                                                                echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$i.'</button>';
                                                                } ?>
                                                                
                                                            </td>
                                                            <td>
                                                                <?php for ($i=1;$i<=8;$i++) {
                                                                 echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$i.'</button>';
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <?php for ($i=8;$i>0;$i--) {
                                                                 echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$i.'</button>';
                                                                } ?>
                                                                
                                                            </td>
                                                            <td>
                                                                <?php for ($i=1;$i<=8;$i++) {
                                                                 echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$i.'</button>';
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                    </table><br> 

                                                    <table border="1">
                                                        <tr>
                                                            <td>
                                                                <?php for ($i=5;$i>0;$i--) {
                                                                    if ($i==1) {$x="I";} 
                                                                    else if ($i==2) {$x="II";} 
                                                                    else if ($i==3) {$x="III";} 
                                                                    else if ($i==4) {$x="IV";} 
                                                                    else if ($i==5) {$x="V";} 
                                                                echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$x.'</button>';
                                                                } ?>
                                                                
                                                            </td>
                                                            <td>
                                                                <?php for ($i=1;$i<=5;$i++) {
                                                                    if ($i==1) {$x="I";} 
                                                                    else if ($i==2) {$x="II";} 
                                                                    else if ($i==3) {$x="III";} 
                                                                    else if ($i==4) {$x="IV";} 
                                                                    else if ($i==5) {$x="V";}
                                                                 echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$x.'</button>';
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <?php for ($i=5;$i>0;$i--) {
                                                                    if ($i==1) {$x="I";} 
                                                                    else if ($i==2) {$x="II";} 
                                                                    else if ($i==3) {$x="III";} 
                                                                    else if ($i==4) {$x="IV";} 
                                                                    else if ($i==5) {$x="V";}
                                                                 echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$x.'</button>';
                                                                } ?>
                                                                
                                                            </td>
                                                            <td>
                                                                <?php for ($i=1;$i<=5;$i++) {
                                                                    if ($i==1) {$x="I";} 
                                                                    else if ($i==2) {$x="II";} 
                                                                    else if ($i==3) {$x="III";} 
                                                                    else if ($i==4) {$x="IV";} 
                                                                    else if ($i==5) {$x="V";}
                                                                 echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$x.'</button>';
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <!-- -->
                                        </div>
                                       
                                        <?php $j=0; ?>
                                        @foreach ($level1 as $a)
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="row .lab-level-{{$j}}" style="background-color: #ededed;color: black;padding: 5px;font-weight: bold;" onclick="hideshowlab({{$j}})">
                                                    {{$level1[$j]['nama']}}
                                                </div>
                                                
                                                <div class="row" id="lab-card-{{$j}}" style="padding: 5px; display: none;" >
                                                    
                                                    <!-- opsi laboratorium -->

                                                    <div class="form-group">
                                                      <select class="select2" name="cblab" multiple="multiple" data-placeholder="Pilih" style="width: 100%;">
                                                        <?php $k=0; ?>
                                                        @foreach ($level3[$j] as $b)
                                                            <option title="{{$level3[$j][$k]['id']}}">{{$level3[$j][$k]['nama']}}</option>
                                                        <?php $k++;?>
                                                        @endforeach
                                                      </select>
                                                    </div>

                                                    <!-- end opsi laboratorium -->
                                                </div>
                                            </div>
                                        </div> <!-- end row level 1-->
                                        <?php $j++;?>
                                        @endforeach
                                        <!-- <div class="row" style="border-top: 1px solid black;padding: 5px;padding-top: 10px !important;">
                                            
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <a id="cetaklab" class="btn btn-primary" target="_blank">CETAK LAB</a>
                        <button type="button" id="simpanlab" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" id="closemdllab">Close</button>
                    </div>
                </div>
            </div>
        </div>
         <!-- End of Input Laboratorium Selection Modal -->
         <!-- start pilih periksa lanjutan Modal -->
         <div class="modal fade" id="lanjutanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLongTitle"><center><b>PILIH JENIS PEMERIKSAAN LANJUTAN</b></center></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4 col-sm-4" id="btlab">
                                <div class="card card-box">
                                    <div class="card-body " id="bar-parent2">
                                        <img src="{{asset('assets/img/lab/laboratorium.jpg')}}" alt="Snow" style="width:100%;height: 120px;">
                                        <p style="width:100%;border-style: solid;border-color: #f5f6f7;text-align: center;font-weight: bold;background-color: #f7f78d;height: 60px; padding-top: 15px;">LABORATORIUM</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4" id="btfisio">
                                <div class="card card-box">
                                    <div class="card-body " id="bar-parent2">
                                        <img src="{{asset('assets/img/lab/physio.jpg')}}" alt="Snow" style="width:100%;height: 120px;">
                                        <p style="width:100%;border-style: solid;border-color: #f5f6f7;text-align: center;font-weight: bold; background-color: #f090de;height: 60px;padding-top: 15px;">FISIOTERAPI</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4" id="btrujukanpoli">
                                <div class="card card-box">
                                    <div class="card-body " id="bar-parent2">
                                        <img src="{{asset('assets/img/lab/pindah.jpg')}}" alt="Snow" style="width:100%;height: 120px;">
                                        <p style="width:100%;border-style: solid;border-color: #61e1e8;text-align: center;font-weight: bold; background-color: #61e1e8;height: 60px;padding-top: 15px;">RUJUK KE POLI</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="closemdllanj">Close</button>
                    </div>
                </div>
            </div>
        </div>
         <!-- End of pilih periksa lanjutan Modal -->
         <!-- Input fisioterapi Selection Modal -->
         <div class="modal fade" id="fisioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Input Fisioterapi</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="card card-box">
                                    <div class="card-body " id="bar-parent2">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                {{csrf_field()}}
                                                <input type="hidden" id="token" name="_token" value="{{csrf_token()}}"/>
                                                <input type="hidden" id="idtr_fisio" name="idtr_fisio">
                                                <div class="form-group">
                                                    <label>Nama</label>
                                                    <input type="text" class="form-control" id="nmpasien_fisio" readonly>
                                                </div>
                                                 <div class="form-group">
                                                    <label>Alamat / Telp</label>
                                                    <input type="text" class="form-control" id="almtpasien_fisio" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>E-mail</label>
                                                    <input type="text" class="form-control" id="email_fisio">
                                                </div>
                                                <div class="form-group">
                                                    <label>Umur</label>
                                                    <table>
                                                        <tr>
                                                            <td width="70%">
                                                                <input type="text" class="form-control" id="umur_fisio">
                                                            </td>
                                                            <td style="padding-left: 10px;" width="30%">
                                                                <input type="radio" name="gender" id="fg_l" value="L"> L
                                                                <input type="radio" name="gender" id="fg_p" value="P" > P
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <label>Dokter</label>
                                                    <input type="text" class="form-control" id="dokter_fisio" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat</label>
                                                    <input type="text" class="form-control" id="almt_dokter_fisio" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Telp</label>
                                                    <input type="text" class="form-control" id="telp_fisio">
                                                </div>
                                                <div class="form-group">
                                                    <label>Tgl</label>
                                                    <div class= "input-append date form_datetime">
                                                        <table>
                                                            <tr>
                                                                <td width="100%"><input type="text" class="form-control" id="tglfisio"></td>
                                                                <td width="20%"><span class="add-on"><i class="fa fa-calendar icon-th"></i></span></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Diagnosis / Keterangan Poli</label>
                                                    <textarea class="form-control" rows="3" id="anamnesa_fisio"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="row" style="background-color: #ab726c;color: white;padding: 5px;font-weight: bold;">
                                                    JENIS TINDAKAN FISIOTERAPI
                                                </div>
                                                
                                                <div class="row" style="padding: 5px;">
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        <div class="checkbox checkbox-icon-red p-0">
                                                            <table >
                                                                <tr>
                                                                    <td style=" vertical-align: top !important;"><input id="checkbox1" type="checkbox" name="cbfisio[]" value=""></td>
                                                                    <td style="padding-left: 2px !important;">
                                                                      <label for="checkbox1">
                                                                        MWD
                                                                      </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="checkbox checkbox-icon-red p-0">
                                                            <table >
                                                                <tr>
                                                                    <td style=" vertical-align: top !important;"><input id="checkbox1" type="checkbox" name="cbfisio[]" value=""></td>
                                                                    <td style="padding-left: 2px !important;">
                                                                      <label for="checkbox1">
                                                                        ES
                                                                      </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="checkbox checkbox-icon-red p-0">
                                                            <table >
                                                                <tr>
                                                                    <td style=" vertical-align: top !important;"><input id="checkbox1" type="checkbox" name="cbfisio[]" value=""></td>
                                                                    <td style="padding-left: 2px !important;">
                                                                      <label for="checkbox1">
                                                                        ULTRASOUND
                                                                      </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="checkbox checkbox-icon-red p-0">
                                                            <table >
                                                                <tr>
                                                                    <td style=" vertical-align: top !important;"><input id="checkbox1" type="checkbox" name="cbfisio[]" value=""></td>
                                                                    <td style="padding-left: 2px !important;">
                                                                      <label for="checkbox1">
                                                                        NEBULIZER
                                                                      </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="checkbox checkbox-icon-red p-0">
                                                            <table >
                                                                <tr>
                                                                    <td style=" vertical-align: top !important;"><input id="checkbox1" type="checkbox" name="cbfisio[]" value=""></td>
                                                                    <td style="padding-left: 2px !important;">
                                                                      <label for="checkbox1">
                                                                        LASER
                                                                      </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="checkbox checkbox-icon-red p-0">
                                                            <table >
                                                                <tr>
                                                                    <td style=" vertical-align: top !important;"><input id="checkbox1" type="checkbox" name="cbfisio[]" value=""></td>
                                                                    <td style="padding-left: 2px !important;">
                                                                      <label for="checkbox1">
                                                                        MASSAGE
                                                                      </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="checkbox checkbox-icon-red p-0">
                                                            <table >
                                                                <tr>
                                                                    <td style=" vertical-align: top !important;"><input id="checkbox1" type="checkbox" name="cbfisio[]" value=""></td>
                                                                    <td style="padding-left: 2px !important;">
                                                                      <label for="checkbox1">
                                                                        EXCERCISE
                                                                      </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="checkbox checkbox-icon-red p-0">
                                                            <table >
                                                                <tr>
                                                                    <td style=" vertical-align: top !important;"><input id="checkbox1" type="checkbox" name="cbfisio[]" value=""></td>
                                                                    <td style="padding-left: 2px !important;">
                                                                      <label for="checkbox1">
                                                                        TAPPING
                                                                      </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <a id="cetaklab" class="btn btn-primary" target="_blank">CETAK FISIO</a>
                        <button type="button" id="simpanfisio" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" id="closemdlfisio">Close</button>
                    </div>
                </div>
            </div>
        </div>
         <!-- End of Input fisioterapi Selection Modal -->
         <!-- Input Rujukan Selection Modal -->
         <div class="modal fade" id="rujukanpoliModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Input Poli Rujukan</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="card card-box">
                                    <div class="card-body " id="bar-parent2">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                {{csrf_field()}}
                                                <input type="hidden" id="token" name="_token" value="{{csrf_token()}}"/>
                                                <input type="hidden" id="idtr_rujukan" name="idtr_rujukan">
                                                <div class="form-group">
                                                    <label>Nama</label>
                                                    <input type="text" class="form-control" id="nmpasien_rujukan" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Tgl Rujukan</label>
                                                    <div class= "input-append date form_datetime">
                                                        <table>
                                                            <tr>
                                                                <td width="100%"><input type="text" class="form-control" id="tglrujukan"></td>
                                                                <td width="20%"><span class="add-on"><i class="fa fa-calendar icon-th"></i></span></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <label>Dokter Perujuk</label>
                                                    <input type="text" class="form-control" id="dokter_rujukan" readonly value="{{$name}}">
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>S</label>
                                                    <textarea class="form-control" rows="3" id="sanamnesa_rujukan" readonly></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>O</label>
                                                    <textarea class="form-control" rows="3" id="oanamnesa_rujukan" readonly></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>A</label>
                                                    <textarea class="form-control" rows="3" id="aanamnesa_rujukan" readonly></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>P</label>
                                                    <textarea class="form-control" rows="3" id="panamnesa_rujukan" readonly></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="row" style="background-color: #ab726c;color: white;padding: 5px;font-weight: bold;">
                                                    Pilih Poli Rujukan
                                                </div>
                                                
                                                <div class="row" style="padding: 5px;">
                                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                                        <div class="checkbox checkbox-icon-red p-0">
                                                            <table >
                                                                <tr>
                                                                    <td style=" vertical-align: top !important;"><input id="checkbox1" type="checkbox" name="cbrujukan[]" value="1"></td>
                                                                    <td style="padding-left: 2px !important;">
                                                                      <label for="checkbox1">
                                                                        Poli Umum
                                                                      </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="checkbox checkbox-icon-red p-0">
                                                            <table >
                                                                <tr>
                                                                    <td style=" vertical-align: top !important;"><input id="checkbox1" type="checkbox" name="cbrujukan[]" value="2"></td>
                                                                    <td style="padding-left: 2px !important;">
                                                                      <label for="checkbox1">
                                                                        Poli Gigi
                                                                      </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="checkbox checkbox-icon-red p-0">
                                                            <table >
                                                                <tr>
                                                                    <td style=" vertical-align: top !important;"><input id="checkbox1" type="checkbox" name="cbrujukan[]" value="4"></td>
                                                                    <td style="padding-left: 2px !important;">
                                                                      <label for="checkbox1">
                                                                        Poli BKIA
                                                                      </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="checkbox checkbox-icon-red p-0">
                                                            <table >
                                                                <tr>
                                                                    <td style=" vertical-align: top !important;"><input id="checkbox1" type="checkbox" name="cbrujukan[]" value="3"></td>
                                                                    <td style="padding-left: 2px !important;">
                                                                      <label for="checkbox1">
                                                                        Poli Mata
                                                                      </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <a id="cetaklab" class="btn btn-primary" target="_blank">CETAK RUJUKAN</a>
                        <button type="button" id="simpanrujukan" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" id="closemdlrujukan">Close</button>
                    </div>
                </div>
            </div>
        </div>
         <!-- End of Input Rujukan Selection Modal -->
    </div>
</div>
<!-- end page content -->


@endsection

@section('js')
    
    <script src="{{ asset('assets/js/main/doctor/index.js') }}"></script>
    <script src="{{ asset('assets/js/main/doctor/simpan_periksa.js') }}"></script>
    <script src="{{ asset('assets/js/main/doctor/resep.js') }}"></script>
    <script src="{{ asset('assets/js/main/doctor/periksalanjutan.js') }}"></script>

    <script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>
    <script>
          $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
              theme: 'bootstrap4'
            }) 
        })
    </script>

@endsection