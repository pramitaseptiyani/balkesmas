@extends('template.main')

@section('css')
<!-- Theme style -->
      <link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}"/>
@endsection

@section('content')
<!-- Input Laboratorium Selection Modal -->
         <!-- <div class="modal fade" id="labModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-editpatient" role="document">
                <div class="modal-content modal-content-editpatient">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Input Laboratorium</h5>
                    </div>
                    <div class="modal-body modal-body-editpatient"> -->
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="card card-box">
                                    <div class="card-body " id="bar-parent2">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                {{csrf_field()}}
                                                <input type="hidden" id="token" name="_token" value="{{csrf_token()}}"/>
                                                <input type="hidden" id="idtr_lab" name="idtr_lab">
                                                <div class="form-group">
                                                    <label>Nama</label>
                                                    <input type="text" class="form-control" id="nmpasien_lab" readonly>
                                                </div>
                                                 <div class="form-group">
                                                    <label>Alamat / Telp</label>
                                                    <input type="text" class="form-control" id="almtpasien_lab" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>E-mail</label>
                                                    <input type="text" class="form-control" id="email_lab">
                                                </div>
                                                <div class="form-group">
                                                    <label>Umur</label>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <input type="text" class="form-control" id="umur_lab" style="width: 400px;">
                                                            </td>
                                                            <td style="padding-left: 20px;">
                                                                <input type="radio" name="gender" id="g_l" value="L"> L
                                                                <input type="radio" name="gender" id="g_p" value="P" > P
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <label>Dokter</label>
                                                    <input type="text" class="form-control" id="dokter_lab" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat</label>
                                                    <input type="text" class="form-control" id="almt_dokter_lab" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Telp</label>
                                                    <input type="text" class="form-control" id="telp_lab">
                                                </div>
                                                <div class="form-group">
                                                    <label>Tgl</label>
                                                    <div class= "input-append date form_datetime">
                                                        <input type="text" id="tgllab" style="padding-left:10px; width: 350px; height: 40px;">
                                                        <span class="add-on"><i class="fa fa-calendar icon-th"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Diagnosis / Keterangan Poli</label>
                                                    <textarea class="form-control" rows="3" id="anamnesa_lab"></textarea>
                                                </div>
                                            </div>
                                            <!-- -->
                                            <?php if ($doctorid == 4) { ?> <!-- jika user dokternya gigi --> 
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Dental XRAY</label>
                                                    <table border="1">
                                                        <tr>
                                                            <td>
                                                                <?php for ($i=8;$i>0;$i--) {
                                                                echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$i.'</button>';
                                                                } ?>
                                                                
                                                            </td>
                                                            <td>
                                                                <?php for ($i=1;$i<=8;$i++) {
                                                                 echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$i.'</button>';
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <?php for ($i=8;$i>0;$i--) {
                                                                 echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$i.'</button>';
                                                                } ?>
                                                                
                                                            </td>
                                                            <td>
                                                                <?php for ($i=1;$i<=8;$i++) {
                                                                 echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$i.'</button>';
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                    </table><br> 

                                                    <table border="1">
                                                        <tr>
                                                            <td>
                                                                <?php for ($i=5;$i>0;$i--) {
                                                                    if ($i==1) {$x="I";} 
                                                                    else if ($i==2) {$x="II";} 
                                                                    else if ($i==3) {$x="III";} 
                                                                    else if ($i==4) {$x="IV";} 
                                                                    else if ($i==5) {$x="V";} 
                                                                echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$x.'</button>';
                                                                } ?>
                                                                
                                                            </td>
                                                            <td>
                                                                <?php for ($i=1;$i<=5;$i++) {
                                                                    if ($i==1) {$x="I";} 
                                                                    else if ($i==2) {$x="II";} 
                                                                    else if ($i==3) {$x="III";} 
                                                                    else if ($i==4) {$x="IV";} 
                                                                    else if ($i==5) {$x="V";}
                                                                 echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$x.'</button>';
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <?php for ($i=5;$i>0;$i--) {
                                                                    if ($i==1) {$x="I";} 
                                                                    else if ($i==2) {$x="II";} 
                                                                    else if ($i==3) {$x="III";} 
                                                                    else if ($i==4) {$x="IV";} 
                                                                    else if ($i==5) {$x="V";}
                                                                 echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$x.'</button>';
                                                                } ?>
                                                                
                                                            </td>
                                                            <td>
                                                                <?php for ($i=1;$i<=5;$i++) {
                                                                    if ($i==1) {$x="I";} 
                                                                    else if ($i==2) {$x="II";} 
                                                                    else if ($i==3) {$x="III";} 
                                                                    else if ($i==4) {$x="IV";} 
                                                                    else if ($i==5) {$x="V";}
                                                                 echo '<button type="button" title="Lihat" id="downloadButton" data-id="" class="btn btn-xs btn-light menu-see-button download-" style="margin-right:1vw;">'.$x.'</button>';
                                                                } ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <!-- -->
                                        </div>
                                       
                                        <?php $j=0; ?>
                                        @foreach ($level1 as $a)
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="row .lab-level-{{$j}}" style="background-color: #ededed;color: black;padding: 5px;font-weight: bold;" onclick="hideshowlab({{$j}})">
                                                    {{$level1[$j]['nama']}}
                                                </div>
                                                
                                                <div class="row" id="lab-card-{{$j}}" style="padding: 5px; display: none;" >
                                                    
                                                    <!-- opsi laboratorium -->

                                                    <div class="form-group">
                                                      <select class="select2" name="cblab" multiple="multiple" data-placeholder="Pilih" style="width: 100%;">
                                                        <?php $k=0; ?>
                                                        @foreach ($level3[$j] as $b)
                                                            <option title="{{$level3[$j][$k]['id']}}">{{$level3[$j][$k]['nama']}}</option>
                                                        <?php $k++;?>
                                                        @endforeach
                                                      </select>
                                                    </div>

                                                    <!-- end opsi laboratorium -->
                                                </div>
                                            </div>
                                        </div> <!-- end row level 1-->
                                        <?php $j++;?>
                                        @endforeach
                                        <!-- <div class="row" style="border-top: 1px solid black;padding: 5px;padding-top: 10px !important;">
                                            
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="modal-footer">
                        <a id="cetaklab" class="btn btn-primary" target="_blank">CETAK LAB</a>
                        <button type="button" id="simpanlab" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" id="closemdllab">Close</button>
                    </div>
                </div>
            </div>
        </div> -->
         <!-- End of Input Laboratorium Selection Modal -->
@endsection

@section('js')
<script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>
    <script>
          $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
              theme: 'bootstrap4'
            }) 
        })
    </script>

@endsection