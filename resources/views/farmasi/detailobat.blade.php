@extends('template.main')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}" />

<link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}" />
@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class="pull-left">
                    <div class="page-title">Daftar Semua Farmasi</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                        Farmasi</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Pengambilan Obat</li>
                </ol>
            </div>
        </div>

        <!-- chart start -->
        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Daftar Obat Pasien </header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body no-padding height-9">

                        <!-- end widget -->
                    </div>

                    <div class="card-body">
                    <div class='p-2 mb-2 bg-success text-white' style="display: none;" id="text-obat-sudah-diambil">Obat Sudah Diambil</div>
                        <table>
                            <tbody>
                                <tr>
                                    <td style="width: 20%;">Nama Pasien</td>
                                    <td style="width: 5%; text-align: right;">:</td>
                                    <td style="width: 75%; " id="nama-pasien"></td>
                                </tr>
                                <tr>
                                    <td>No MR</td>
                                    <td style="width: 5%; text-align: right;">:</td>
                                    <td id="nomor-mr"></td>
                                </tr>
                                <tr>
                                    <td>Alergi</td>
                                    <td style="width: 5%; text-align: right;">:</td>
                                    <td id="alergi-pasien"></td>
                                </tr>
                                <tr>
                                    <td>Vaksinasi</td>
                                    <td style="width: 5%; text-align: right;">:</td>
                                    <td id="vaksin-pasien"></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="table-scrollable">

                            <table class="table table-hover table-checkable order-column full-width" id="patientDatatable">
                                <thead>
                                    <tr>
                                        <th> Nomor </th>
                                        <th> Nama Obat </th>
                                        <th> Jumlah Obat </th>
                                        <th> Aturan </th>
                                        <!-- <th width="25%"> Aksi </th> -->
                                    </tr>
                                </thead>
                                <tbody id="body-table">

                                </tbody>
                            </table>
                        </div>
                        <div style="float: right;">
                            <input type="hidden" id='input-idtrpoli' value="{{ $id_tr_poli }}"></input>
                            <input type="hidden" id='csrf_token' value="{{ csrf_token() }}"></input>

                            <button id="btn-obat-diambil" style="display: none;" class="btn btn-success">Penerimaan Obat</button>
                            <button id="btn-obat-siap" style="display: none;" class="btn btn-primary">Tandai Obat Siap</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Chart end -->

        <!-- Edit Patient Selection Modal -->
        <div class="modal fade" id="ambilObatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Pengambilan Obat</h5>
                        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button> -->
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <input type="hidden" id="edit-token" name="_token" value="{{csrf_token()}}" />
                                <input type="hidden" name="id" id="edit-id">
                                <div class="form-group">
                                    <label>No MR</label>
                                    <input type="text" id="edit-MR" class="form-control" placeholder="SET90328392.02" disabled>
                                </div>

                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" id="edit-nama" class="form-control" placeholder="Nama" disabled>
                                </div>
                                <div class="form-group">
                                    <label>NIP/NIK</label>
                                    <input type="text" id="edit-nip" class="form-control" placeholder="NIP/NIK" disabled>
                                </div>

                            </div>
                            <div class="col-md6 col-sm-6">
                                <input type="hidden" name="ttd" id='signatureJSON' rows="3" column="10" required />
                                Tanda Tangan Pasien
                                <span style="color: red;">*</span>
                                <div class="row justify-content-left">
                                    <div>
                                        <div class="js-signature"></div>
                                    </div>
                                </div>
                                <br>
                                <button class="btn" id="btn-clear-ttd">reset ttd</button>
                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-ambil" class="btn btn-success" disabled>Ambil Obat</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Edit Patient Selection Modal -->
    </div>
</div>
<!-- end page content -->
@endsection

@section('js')

<script src="{{ asset('assets/js/main/farmasi/jq-signature.min.js') }}"></script>
<script src="{{ asset('assets/js/main/farmasi/obatpasien.js') }}"></script>

@endsection