<!DOCTYPE html>
<html lang="en">
<head>
  @include('template.header')
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/signature-pad/signature-pad.css') }}"/>
  <title>Balkesmas - Finish Signature</title>

</head>
<body>
  <!-- Navbar -->
    <div class="page-wrapper">
        <!-- start header -->
        <div class="page-header navbar navbar-fixed-top">
            <div class="page-header-inner ">
                <!-- logo start -->
                <div class="page-logo" style="background-color: #b2112b !important;">
                    <a href="index.html">
                    <span class="logo-icon fa fa-rotate-45">
                        <img width="30px" src="">
                    </span>
                    <span class="logo-default" >e-Balkesmas</span> </a>
                </div>
            </div>
        </div>

  <!-- End of Navbar -->


  <!-- <div class="page-container"> -->
      <div class="page-content-wrapper">
        <div class="page-content" style="padding-top:12vh; margin-left: 0;">
            <div class="row">
              <div class="col-md-8" style="margin: auto">
                  <div class="white-box">
                      <h3><b>CONFIDENTIALITY AGREEMENT</b> <span class="pull-right"></span></h3>
                      <hr>
                      <div class="row">
                          <div class="col-md-12">
                            <div class="pull-left">
                              <address>
                                <img src="{{asset('/assets/img/logo/kumham.png')}}" alt="logo" class="logo-default" />
                                <p class="text-muted m-l-5">
                                  D 103, RedStar Hospital, <br> Opp. Town Hall, <br>
                                  Sardar Patel Road, <br> Ahmedabad - 380015
                                </p>
                              </address>
                            </div>
                            <div class="pull-right text-right">
                              <address>
                                <p class="addr-font-h3">To,</p>
                                <p class="font-bold addr-font-h4">Shaka Nagano</p>
                                <p class="text-muted m-l-30">
                                  207, Prem Sagar Appt., <br> Near Income Tax Office, <br>
                                  Ashram Road, <br> Ahmedabad - 380057
                                </p>
                              </address>
                            </div>
                          </div>
                          <div style="margin:2vh"></div>
                          <div class="col-md-12">
                            I, _______________________________, agree with the following statements:



                            I have read and understood [name of agency]’s Privacy Policy.


                            I understand that I may come in contact with confidential information during my time at [name of agency]. As part of the condition of my work with [name of agency] I hereby undertake to keep in strict confidence any information regarding any client, employee or business of [name of agency] or any other organization that comes to my attention while at [name of agency]. I will do this in accordance with the [name of agency]’s privacy policy and applicable laws, including those that require mandatory reporting.

                            I also agree to never remove any confidential material of any kind from the premises of [name of agency] unless authorized as part of my duties, or with the express permission or direction to do so from [name of agency]. 

                          </div>
                          <div class="col-md-12">
                              <div class="col-md-8">
                              </div>
                              <div id="whole_signature" class="col-md-4 pull-right">

                                <div id="date">
                                  Jakarta, 30 Oktober 2019
                                </div>

                                <div id="signature-pad" class="signature-pad">
                                  <div class="signature-pad--body">
                                    <canvas></canvas>
                                  </div>
                                  <div class="signature-pad--footer">
                                    <div class="description">Silahkan Tanda Tangan di Atas</div>

                                    <!-- <div class="signature-pad--actions"> -->
                                      <div>  
                                        <div>
                                          <button type="button" class="btn btn-danger btn-xs m-b-10 clear" data-action="clear">Clear</button>
                                          <button type="button" class="btn btn-primary btn-xs m-b-10" data-action="undo">Undo</button>
                                        </div>
                                        <div>
                                          <a href="{{url('/patient/finish/signature')}}">Full Screen</a>
                                        </div>
                                      </div>
                                  </div>
                                </div>

                                <div id="receiver">
                                  Shaka Nagano
                                </div>

                              </div>
                          </div>

                          <div class="col-md-12">
                            <div style="width: 100%;
                              display: flex;
                              align-items: center;
                              justify-content: center;">
                              <button type="button" class="btn btn-lg btn-circle btn-success">Selesai</button>
                            </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>



        

        </div>  
      </div>
    </div>
  <!-- </div> -->

  @include('template.footer')
  <script src="{{ asset('assets/js/plugins/signature-pad/signature-pad.umd.js') }}"></script>
  <script src="{{ asset('assets/js/plugins/signature-pad/app.js') }}"></script>
</body>
</html>