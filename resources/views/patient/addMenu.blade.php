@extends('template.main')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/datatables/export/buttons.dataTables.min.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/> -->
@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class="pull-left">
                    <div class="page-title">Tambah Pasien</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="/">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    Pasien
                    </a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Tambah Pasien</li>
                </ol>
            </div>
        </div>

         <!-- chart start -->
        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Tambah Pasien Baru</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body no-padding height-9">
                        
                    <!-- end widget -->
                    </div>
                    <div class="card-body">
                        <!-- <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <button type="button"
                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-circle btn-primary"
                                data-toggle="modal" data-target="#addPatientModal">
                                    Tambah Pasien
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div> -->

                        <div class="row">
                            <div class="col-md-4">
                                <div class="card card-topline-aqua">
                                    <div class="card-body no-padding ">
                                        <div class="doctor-profile"> 
                                            <div class="profile-usertitle">
                                                <div class="doctor-name">Pegawai </div>
                                            </div>
                                                <p>Pasien dengan NIP.</p> 
                                            <div class="profile-userbuttons">
                                                <a href="{{url ('receptionist/patient/add')}}" class="btn btn-circle blue-bgcolor btn-sm">Pilih</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="card card-topline-red">
                                    <div class="card-body no-padding ">
                                        <div class="doctor-profile"> 
                                            <div class="profile-usertitle">
                                                <div class="doctor-name">Keluarga </div>
                                            </div>
                                                <p>Pasien dengan <i>reference </i> NIP.</p> 
                                            <div class="profile-userbuttons">
                                                <a href="{{url ('receptionist/patient/add_family')}}" class="btn btn-circle blue-bgcolor btn-sm">Pilih</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-topline-green">
                                    <div class="card-body no-padding ">
                                        <div class="doctor-profile"> 
                                            <div class="profile-usertitle">
                                                <div class="doctor-name">Pasien Umum </div>
                                            </div>
                                                <p>Pasien dengan NIK.</p> 
                                            <div class="profile-userbuttons">
                                                <a href="{{url ('receptionist/patient/add_general')}}" class="btn btn-circle blue-bgcolor btn-sm">Pilih</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
         <!-- Chart end -->

        <!-- Patient Detail Selection Modal -->
        <div class="modal fade" id="patientDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Detail Pasien</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>No MR</label>
                                    <input type="text" class="form-control" placeholder="SET90328392.02" disabled>
                                </div>
                                
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" placeholder="Nama" disabled>
                                </div>
                                <div class="form-group">
                                    <label>NIP/NIK</label>
                                    <input type="text" class="form-control" placeholder="NIP/NIK" disabled>
                                </div>
                                <div class="form-group">
                                    <label>No BPJS</label>
                                    <input type="text" class="form-control" placeholder="NIP/NIK" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" class="form-control"  placeholder="Tanggal Lahir" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Unit</label>
                                    <input type="text" class="form-control"  placeholder="Unit" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control"  placeholder="Email" disabled>
                                </div>
                                <div class="form-group">
                                    <label>No HP</label>
                                    <input type="text" class="form-control"  placeholder="No HP" disabled>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Penyakit Yang Pernah Diderita</label>
                                    <textarea class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Penyakit Keluarga/Turunan</label>
                                    <textarea class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Riwayat Alergi</label>
                                    <textarea class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Riwayat Vaksinasi</label>
                                    <textarea class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Patient Detail Selection Modal -->

        <!-- Add Patient Selection Modal -->
        <div class="modal fade" id="addPatientModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLongTitle">Tambah Pasien</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="card card-topline-aqua">
                                    <div class="card-body no-padding ">
                                        <div class="doctor-profile"> 
                                            <div class="profile-usertitle">
                                                <div class="doctor-name">Pegawai </div>
                                            </div>
                                                <p>Pasien dengan NIP</p> 
                                            <div class="profile-userbuttons">
                                                <a href="{{url ('receptionist/patient/add')}}" class="btn btn-circle blue-bgcolor btn-sm">Pilih</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="card card-topline-red">
                                    <div class="card-body no-padding ">
                                        <div class="doctor-profile"> 
                                            <div class="profile-usertitle">
                                                <div class="doctor-name">Keluarga </div>
                                            </div>
                                                <p>Pasien dengan <i>reference </i> NIP</p> 
                                            <div class="profile-userbuttons">
                                                <a href="{{url ('receptionist/patient/add_general')}}" class="btn btn-circle blue-bgcolor btn-sm">Pilih</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card card-topline-green">
                                    <div class="card-body no-padding ">
                                        <div class="doctor-profile"> 
                                            <div class="profile-usertitle">
                                                <div class="doctor-name">Pasien Umum </div>
                                            </div>
                                                <p>Pasien dengan NIK</p> 
                                            <div class="profile-userbuttons">
                                                <a href="{{url ('receptionist/patient/add_general')}}" class="btn btn-circle blue-bgcolor btn-sm">Pilih</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Add Patient Selection Modal -->


        

    </div>
</div>
<!-- end page content -->
@endsection

@section('js')
    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

    <!-- Datetime Picker -->
    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <!-- End of Datetime Picker -->
@endsection