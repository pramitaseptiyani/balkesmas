@extends('template.main')

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>

  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom/styles.css') }}"/>

  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

@endsection   

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Tambah Pasien Keluarga Pegawai</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Tambah Pasien Keluarga Pegawai</li>
                </ol>
            </div>
        </div>

        <div id="overlay">
            <div class="cv-spinner">
                <span class="spinner"></span>
            </div>
        </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="card-head">
                            <header>Informasi Pasien</header>
                            <button id = "panel-button" 
                               class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                               data-upgraded = ",MaterialButton">
                               <i class = "material-icons">more_vert</i>
                            </button>
                            <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                               data-mdl-for = "panel-button">
                               <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                               <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                               <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                            </ul>
                        </div>
                        <!-- <form method="POST" id="form-pasien" action="{{url('/receptionist/patient/tambah')}}"> -->
                        <div class="card-body row">
                            {{csrf_field()}}
                            <input type="hidden" name="tipe" value="family">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>Reference NIP
                                  <span class="required"></span>
                                </label>
                                <select id="search-nip" class="form-control">
                                  <option></option>
                                </select>
                              </div>
                            </div>

                            <div class="row col-lg-12">
                                <div class="col-md-6">

                                    <div class="col-lg-12 p-t-20"> 
                                        <div class="form-group">
                                            <label class="float-label">NIP
                                              <span class="required"></span>
                                            </label>
                                            <input id="nip" name="nip" type="text" class="form-control" placeholder="" readonly="">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20"> 
                                        <div class="form-group">
                                            <label class="float-label">Satuan Kerja
                                              <span class="required"></span>
                                            </label>
                                            <input id="unit" name="unit" type="text" class="form-control" placeholder="" readonly="">
                                            <input id="satkerid" name="satkerid" type="hidden" class="form-control" placeholder="" readonly="">
                                        </div>
                                    </div>

                                    <br/>
                                    <div class="col-lg-12"> 
                                        <hr class="new1">
                                    </div>

                                    <div class="col-lg-12 p-t-20">
                                      <label class="float-label">Hubungan dengan Pegawai
                                        <span class="required"></span>
                                      </label>
                                      <div class="radio p-0">
                                          <input type="radio" name="optionRelation" id="optionRelation" value="pasangan" checked>
                                          <label for="optionsRadios1">
                                              Suami/Istri
                                          </label>
                                      </div>
                                      <div class="radio p-0">
                                          <input type="radio" name="optionRelation" id="optionRelation" value="3">
                                          <label for="optionsRadios2">
                                              Anak Pertama
                                          </label>
                                      </div>
                                      <div class="radio p-0">
                                          <input type="radio" name="optionRelation" id="optionRelation" value="4">
                                          <label for="optionsRadios3">
                                              Anak Kedua
                                          </label>
                                      </div>
                                      <div class="radio p-0">
                                          <input type="radio" name="optionRelation" id="optionRelation" value="5">
                                          <label for="optionsRadios4">
                                              Anak Ketiga
                                          </label>
                                      </div>
                                    </div>

                                    <div class="col-lg-12 p-t-20"> 
                                      <div class="form-group">
                                        <label class="float-label">Nama Lengkap
                                          <span class="required"></span>
                                        </label>
                                        <input id="nama" name="nama" type="text" class="form-control" placeholder="">
                                      </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20"> 
                                      <label class="float-label">Tanggal Lahir
                                        <span class="required"></span>
                                      </label>
                                      <input name="ttl" type="text" id="date" class="floating-label mdl-textfield__input"  placeholder="Tanggal Lahir">
                                    </div>
                                    <div class="col-lg-12 p-t-20">
                                      <label class="float-label">Jenis Kelamin
                                        <span class="required"></span>
                                      </label>
                                      <div class="radio p-0">
                                          <input type="radio" name="optionGender" id="optionGender" value="L" checked>
                                          <label for="optionsRadios1">
                                              Laki-laki
                                          </label>
                                      </div>
                                      <div class="radio p-0">
                                          <input type="radio" name="optionGender" id="optionGender" value="P">
                                          <label for="optionsRadios2">
                                              Perempuan
                                          </label>
                                      </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20">
                                        <div class="form-group">
                                            <label class="float-label">No HP
                                                <span class="required"></span>
                                            </label>
                                            <input id="nohp" name="nohp" type = "text" class="form-control"></input>
                                        </div> 
                                    </div>
                                    <div class="col-lg-12 p-t-20"> 
                                        <label class="float-label">Email
                                            <span class="required"></span>
                                        </label>
                                        <input name="email" class = "form-control" type = "email" id = "email">
                                        <span class = "mdl-textfield__error">Enter Valid Email Address!</span>
                                    </div>
                                    <div class="col-lg-12 p-t-20"> 
                                      <div class="form-group">
                                          <label>No MR lama
                                          </label>
                                          <input id="mr_lama" name="mr_lama" type = "mr_lama" class="form-control" placeholder="Not Set"></input>
                                      </div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">

                                    <div class="col-lg-12 p-t-20"> 
                                        <div class="form-group">
                                            <label class="float-label">BPJS</label>
                                            <input id="bpjs" name="bpjs" type = "text" class="form-control"></input>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 p-t-20"> 
                                      <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                        <label class = "float-label" for = "alamat">Alamat</label>
                                         <textarea name="alamat" class = "mdl-textfield__input float-input" rows =  "4" 
                                            id = "alamat" ></textarea>
                                         
                                      </div>
                                    </div>
                                    
                                    

                                    <div class="col-lg-12 p-t-20"> 
                                      <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                         <label class = "float-label" for = "riwayat_penyakit">Penyakit yang Pernah Diderita</label>
                                         <textarea name="riwayat_penyakit" class = "mdl-textfield__input float-input" rows =  "4" 
                                            id = "riwayat_penyakit" ></textarea>
                                         
                                      </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20"> 
                                      <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                         <label class = " float-label" for = "riwayat_turunan">Penyakit Keluarga/Turunan</label>
                                         <textarea name="penyakit_turunan" class = "mdl-textfield__input float-input" rows =  "4" 
                                            id = "riwayat_turunan" ></textarea>
                                         
                                      </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20"> 
                                      <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                         <label class = "float-label" for = "riwayat_alergi">Riwayat Alergi</label>
                                         <textarea name="riwayat_alergi" class = "mdl-textfield__input float-input" rows =  "4" 
                                            id = "riwayat_alergi" ></textarea>
                                         
                                      </div>
                                    </div>

                                    <div class="col-lg-12 p-t-20"> 
                                      <div class = "mdl-textfield mdl-js-textfield txt-full-width">
                                         <label class = " float-label" for = "riwayat_vaksinasi">Riwayat Vaksinasi</label>
                                         <textarea name="riwayat_vaksinasi" class = "mdl-textfield__input float-input" rows =  "4" 
                                            id = "riwayat_vaksinasi" ></textarea>
                                         
                                      </div>
                                    </div>



                                    <div class="col-lg-12 p-t-20">
                                        <span class="col-md-12">
                                            <label>Prioritas</label>
                                        </span>
                                        <span class="col-md-12">
                                            <input id="is-vip" type="checkbox" data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                       </span>
                                    </div>

                                    <br/>

                                </div>
                            </div>
                            
                            
                            <div class="col-lg-12 p-t-20 text-center"> 
                                <!-- <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" data-toggle="modal" data-target="#printCard">Simpan</button> -->
                                <button id="simpan" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button>
                                <a href="{{url ('receptionist/patient')}}" type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                            </div>
                        </div>

                        <div class="alert-form"></div>

                      <!-- </form> -->
                    </div>
                </div>
            </div> 


            <!-- Print ID Card Modal -->
            <div class="modal fade" id="printCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="exampleModalLongTitle">Berhasil Menambah Pasien Baru</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="col-xl-12 col-md-12 col-lg-12">
                        <div class="info-box bg-success">
                          <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                          <div class="info-box-content">
                            <span class="info-box-text">No MR : SET90328392.02</span>
                            <span class="info-box-number">
                              Nama :
                              <span>Shaka Nagano</span>
                            </span>
                            <span class="progress-description">
                              <span></span>
                            </span>
                            
                          </div>
                          <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                      </div>
                      <div class="modal-footer">
                        <div class="profile-userbuttons" style="margin:auto">
                            <a href="#" class="btn btn-circle blue-bgcolor btn-md">Print Kartu</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>    
            </div>
            <!-- End of Print ID Card Modal -->
        </div>
    </div>
<!-- end page content -->
@endsection


@section('js')
  <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
  <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
  <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>

  <!-- Bootstrap Toggle -->
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <!-- -->

  <script type="text/javascript">
    $(document).ready(function(){

      // $('#search-nip').select2({
      //   placeholder: 'Ketikan NIP',
      //   theme: "classic",
      //   minimumInputLength: 3,
      //     ajax: {
      //       dataType: 'json',
      //       url: base_path+'/receptionist/patient/search',
      //       delay: 800,
      //       data: function(params) {
      //           return {
      //             nip: params.term,
      //           }
      //       },
      //       processResults: function (data, page) {
      //           var list = [];
      //           var result = data;
      //           $.each(result.data, function(index,item){
      //             list.push({
      //               id: item.nip,
      //               text: item.nip +" - "+ item.nama,
      //               nama: item.nama,
      //               satker: item.satker,
      //               satkerid: item.satkerid,
      //             })
      //           });
      //           return {
      //             results: list
      //           };
      //       },
      //     }
      // });

      $('#search-nip').select2({
        placeholder: 'Ketikan NIP',
        theme: "classic",
        minimumInputLength: 3,
        ajax: {
            dataType: 'json',
            url: base_path+'/simpeg/get_pegawai',
            delay: 800,
            data: function(params) {
                return {
                    nip: params.term
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result, function(index,item){
                    list.push({
                        id      : item.nip,
                        text    : item.nip +" - "+ item.nama_pegawai,
                        nama    : item.nama_pegawai,
                        satker  : item.nama_satker,
                        satkerid  : item.kode_satker,
                        hp      : item.no_hp,
                        email   : item.email_dinas,
                        alamat  : item.alamat,
                        gender  : item.jenis_kelamin,
                        bpjs  : item.askes,
                    })
                });
                return {
                    results: list
                };
            },
        }
    });

      $('#search-nip').on('select2:select', function (e) {
        var data = e.params.data;
        var satker = data.satker
        var unit = satker.substring(0, satker.indexOf("-"));         

        $("#nip").val(data.id);
        $("#unit").val(unit);
        $("#satkerid").val(data.satkerid);
        $("#hp").val(data.hp);
        $("#email").val(data.email);

        if(data.bpjs != null) $("#bpjs").html(data.bpjs);
      });

      $("#simpan").click(function(){

          var unit = $('#unit').val()
          var satkerid = $('#satkerid').val()
          var nip = $('#nip').val()
          var nama = $('#nama').val()
          var ttl = $('#date').val()
          var hp = $('#nohp').val()
          var email = $('#email').val()

          console.log(hp)

          if(nama =='' || nip =='' || ttl =='' 
              || hp =='' || email == ''){

            var alert_html = 
                      '<div class="add-alert alert alert-danger">'+
                        'Harap isi Required Field (bertanda *)'+  
                      '</div>';

            $('.alert-form').html(alert_html);
          }
          else{
            var tipe = $('input[type=radio][name=optionRelation]:checked').val()
            var jenis_kelamin = $('input[type=radio][name=optionGender]:checked').val()

            if(tipe == "pasangan"){
              if(jenis_kelamin = "P"){
                var id_detail_keluarga = 2
              }
              else var id_detail_keluarga = 1
            } 
            else var id_detail_keluarga = tipe

            var formData = {
              "_token": "{{ csrf_token() }}",
              'id_detail_keluarga'    : id_detail_keluarga,
              'kategori': 'keluarga pegawai',
              'nama'    : nama,
              'id_card' : nip,
              'ttl'     : ttl,
              'hp'      : hp,
              'email'   : email,
              'unit'    : unit,
              'bpjs'    : $('#bpjs').val(),
              'alamat'  : $('#alamat').val(),
              'jenis_kelamin' : jenis_kelamin,
              'riwayat_penyakit'  : $('#riwayat_penyakit').val(),
              'riwayat_turunan'   : $('#riwayat_turunan').val(),
              'riwayat_alergi'    : $('#riwayat_alergi').val(),
              'riwayat_vaksinasi' : $('#riwayat_vaksinasi').val(),
              'satkerid' : $('#satkerid').val(),
              'mr_lama' : $('#mr_lama').val(),
              'is_vip' : $('#is-vip').is(':checked')
            }

            $("#overlay").fadeIn(300);　
            send(base_url+"/receptionist/patient/tambah", formData, true, function(responseAdd){
              $("#overlay").fadeOut(300);

              // window.location = base_url+'/receptionist/patient';
            })

            // var HttpTimeout=setTimeout(ajaxTimeout, 60000); //10s

            // setTimeout(function(){ 
            //   window.location = base_url+'/receptionist/patient';
            // }, 1000);
          }

      });
    });
  </script>
@endsection