<!DOCTYPE html>
<html lang="en">
<head>
  @include('template.header')
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/signature-pad/signature-pad.css') }}"/>
  <title>Balkesmas - Finish Signature</title>

</head>
<body>
  <!-- Navbar -->
    <div class="page-wrapper">
        <!-- start header -->
        <div class="page-header navbar navbar-fixed-top">
            <div class="page-header-inner ">
                <!-- logo start -->
                <div class="page-logo" style="background-color: #b2112b !important;">
                    <a href="index.html">
                    <span class="logo-icon fa fa-rotate-45">
                        <img width="30px" src="">
                    </span>
                    <span class="logo-default" >e-Balkesmas</span> </a>
                </div>
            </div>
        </div>

  <!-- End of Navbar -->


    <!-- <div class="page-container"> -->
      <div class="page-content-wrapper">
        <div class="page-content" style="padding-top:12vh; margin-left: 0;">

          <div id="whole_signature" style="width: 50%; margin: 0 auto;">

            <div id="signature-pad" class="signature-pad-full">
              <div class="signature-pad--body">
                <canvas></canvas>
              </div>
              <div class="signature-pad--footer">
                <div class="description">Silahkan Tanda Tangan di Atas</div>

                <div class="signature-pad--actions">
                  <div>  
                    <div>
                      <button type="button" class="btn btn-danger btn-sm m-b-10 clear" data-action="clear">Clear</button>
                      <button type="button" class="btn btn-primary btn-sm m-b-10" data-action="undo">Undo</button>
                    </div>
                  </div>

                  <div>
                    <div>
                      <button type="button" class="btn btn-primary btn-sm m-b-10 save" data-action="save-png">Download</button>
                      <button type="button" class="btn btn-success btn-sm m-b-10">Selesai</button>
                    </div>
                  </div>
              </div>
            </div>

          </div>  
        </div>
    </div>
  <!-- </div> -->

  @include('template.footer')
  <script src="{{ asset('assets/js/plugins/signature-pad/signature-pad.umd.js') }}"></script>
  <script src="{{ asset('assets/js/plugins/signature-pad/app.js') }}"></script>
</body>
</html>