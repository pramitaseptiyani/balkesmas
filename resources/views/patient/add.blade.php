@extends('template.main')

@section('css')
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/template/select2/css/select2.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/custom/styles.css') }}"/>

    <!-- Form -->
    <link href="{{ asset('assets/template/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/formlayout.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/template/css/theme-color.css') }}" rel="stylesheet" type="text/css" />
    <!-- -->

    <link href="{{ asset('assets/css/custom/styles.css') }}" rel="stylesheet" type="text/css" />

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
          <div class="page-title-breadcrumb">
            <div class=" pull-left">
                <div class="page-title">Tambah Pasien Pegawai</div>
            </div>
            <ol class="breadcrumb page-breadcrumb pull-right">
                <li>
                    <i class="fa fa-home"></i>&nbsp;
                    <a class="parent-item" href="index.html">Home</a>&nbsp;
                    <i class="fa fa-angle-right"></i>
                </li>
                <li class="active">Tambah Pasien Pegawai</li>
            </ol>
          </div>
        </div>
        <div id="overlay">
            <div class="cv-spinner">
                <span class="spinner"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="card-head">
                        <header>Informasi Pasien</header>
                        <button id = "panel-button" 
                           class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                           data-upgraded = ",MaterialButton">
                           <i class = "material-icons">more_vert</i>
                        </button>
                        <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                           data-mdl-for = "panel-button">
                           <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                           <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                           <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                        </ul>
                    </div>
                    <div class="card-body row">
                        {{csrf_field()}}

                        <input type="hidden" id="csrf" name="_token" value="{{csrf_token()}}"/>

                        <input type="hidden" name="tipe" value="pegawai">
                        <div class="col-md-12">
                          <div class="form-group">
                            <select id="search-nip" class="form-control">
                              <option></option>
                            </select>
                          </div>
                        </div>

                        <br/>
                        <div class="col-lg-12"> 
                            <hr class="new1">
                        </div>

                        <input type="hidden" id="token" name="_token" value="{{csrf_token()}}"/>

                        <div class="row col-lg-12">
                            <div class="col-md-6">
                                <div class="col-lg-12 p-t-20"> 
                                    <div class="form-group">
                                        <label class="float-label">Nama Lengkap
                                          <span class="required"></span>
                                        </label>
                                        <input id="nama" name="nama" type="text" class="form-control" placeholder="" readonly="">
                                    </div>
                                </div>
                                <div class="col-lg-12 p-t-20"> 
                                    <div class="form-group">
                                        <label class="float-label">NIP
                                          <span class="required"></span>
                                        </label>
                                        <input id="nip" name="nip" type="text" class="form-control" placeholder="" readonly="">
                                    </div>
                                </div>
                                <div class="col-lg-12 p-t-20"> 
                                    <div class="form-group">
                                        <label class="float-label">Satuan Kerja
                                          <span class="required"></span>
                                        </label>
                                        <input id="unit" name="unit" type="text" class="form-control" placeholder="" readonly="">
                                        <input id="satkerid" name="satkerid" type="hidden" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-lg-12 p-t-20"> 
                                    <label class="float-label">Tanggal Lahir
                                      <span class="required"></span>
                                    </label>
                                    <input type="text" name="ttl" id="date" class="form-control"  placeholder="YYYY-MM-DD" readonly="">
                                </div>
                                <div class="col-lg-12 p-t-20">
                                    <label class="float-label">Jenis Kelamin
                                        <span class="required"></span>
                                    </label>
                                    <div>
                                        <span class="radio radio-aqua">
                                            <input id="optionGenderL" name="optionGender" type="radio" value="L" checked disabled="disabled">
                                            <label for="optionGenderL">
                                                Laki-Laki
                                            </label>
                                        </span>
                                        <span class="radio radio-red">
                                            <input id="optionGenderP" name="optionGender" type="radio" value="P"  disabled="disabled">
                                            <label for="optionGenderP">
                                                Perempuan
                                            </label>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-12 p-t-20"> 
                                    <div class="form-group">
                                        <label class="float-label">No HP
                                          <span class="required"></span>
                                        </label>
                                        <input id="hp" name="nohp" type="text" class="form-control" pattern = "-?[0-9]*(\.[0-9]+)?" placeholder="Not Set" readonly=""></input>
                                    </div>
                                </div>
                                
                                <div class="col-lg-12 p-t-20"> 
                                    <div class="form-group">
                                        <label>Alamat
                                          <span class="required"></span>
                                        </label>
                                        <textarea id="alamat" name="alamat" type="text" class="form-control" rows="3" placeholder="" readonly=""></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-12 p-t-20"> 
                                    <div class="form-group">
                                        <label>Email
                                          <span class="required"></span>
                                        </label>
                                        <input id="email" name="email" type = "email" class="form-control" placeholder="Not Set"></input>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col-md-6">
                                <div class="col-lg-12 p-t-20"> 
                                    <div class="form-group">
                                        <label>No MR lama
                                        </label>
                                        <input id="mr_lama" name="mr_lama" type = "mr_lama" class="form-control" placeholder="Not Set"></input>
                                    </div>
                                </div>

                                <div class="col-lg-12 p-t-20"> 
                                    {{-- <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <textarea name="bpjs" class = "mdl-textfield__input" rows =  "4" id = "bpjs"></textarea>
                                        <label class = "mdl-textfield__label float-label" for = "bpjs">No BPJS</label>
                                    </div> --}}

                                    <div class="form-group">
                                        <label>BPJS</label>
                                        <input id="bpjs" name="bpjs" type = "text" class="form-control"></input>
                                    </div>
                                </div>
                                <div class="col-lg-12 p-t-20"> 
                                    <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                       <textarea name="riwayat_penyakit" class = "mdl-textfield__input float-input" rows =  "4" 
                                          id = "riwayat_penyakit" ></textarea>
                                       <label class = "mdl-textfield__label float-label" for = "riwayat_penyakit">Penyakit yang Pernah Diderita</label>
                                    </div>
                                </div>
                                <div class="col-lg-12 p-t-20"> 
                                    <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                       <textarea name="penyakit_turunan" class = "mdl-textfield__input float-input" rows =  "4" 
                                          id = "riwayat_turunan" ></textarea>
                                       <label class = "mdl-textfield__label float-label" for = "riwayat_turunan">Penyakit Keluarga/Turunan</label>
                                    </div>
                                </div>
                                <div class="col-lg-12 p-t-20"> 
                                    <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                       <textarea name="riwayat_alergi" class = "mdl-textfield__input float-input" rows =  "4" 
                                          id = "riwayat_alergi" ></textarea>
                                       <label class = "mdl-textfield__label float-label" for = "riwayat_alergi">Riwayat Alergi</label>
                                    </div>
                                </div>

                                <div class="col-lg-12 p-t-20"> 
                                    <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                       <textarea name="riwayat_vaksinasi" class = "mdl-textfield__input float-input" rows =  "4" 
                                          id = "riwayat_vaksinasi" ></textarea>
                                       <label class = "mdl-textfield__label float-label" for = "riwayat_vaksinasi">Riwayat Vaksinasi</label>
                                    </div>
                                </div> 

                                <div class="col-lg-12 p-t-20">
                                    <span class="col-md-12">
                                        <label>Prioritas</label>
                                    </span>
                                    <span class="col-md-12">
                                        <input id="is-vip" type="checkbox" data-toggle="toggle"  data-size="small" data-onstyle="success" data-width="80" data-on="Ya" data-off="Tidak">
                                   </span>
                                </div>

                                <br/>
                                
                            </div>

                        </div>

                        <div class="col-lg-12">
                            <div class="col-lg-12 p-t-20 text-center"> 
                                <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" data-toggle="modal" data-target="#printCard" style="display: none;">Modal</button>
                                <button id="simpan" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Simpan</button>
                                <a href="{{url ('receptionist/patient')}}" type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</a>
                            </div>
                        </div>


                        

                        
                    </div>

                    <div class="alert-form"></div>
                    

                    <!-- </form> -->
                </div>
            </div>
        </div>


        <!-- Print ID Card Modal -->
        <div class="modal fade" id="printCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="exampleModalLongTitle">Berhasil Menambah Pasien Baru</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="col-xl-12 col-md-12 col-lg-12">
                    <div class="info-box bg-success">
                      <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">No MR : SET90328392.02</span>
                        <span class="info-box-number">
                          Nama :
                          <span>Shaka Nagano</span>
                        </span>
                        <span class="progress-description">
                          <span></span>
                        </span>
                        
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </div>
                  <div class="modal-footer">
                    <div class="profile-userbuttons" style="margin:auto">
                        <a href="#" class="btn btn-circle blue-bgcolor btn-md">Print Kartu</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>    
        </div>
        <!-- End of Print ID Card Modal --> 

    </div>
</div>
<!-- end page content -->
@endsection


@section('js')
  <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
  <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
  <script src="{{ asset('assets/template/select2/js/select2.js') }}"></script>

  <!-- Bootstrap Toggle -->
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <!-- -->

  <script src="{{ asset('assets/js/helper/helper.js') }}"></script>

  <script src="{{ asset('assets/js/main/patient/add.js') }}"></script>
@endsection