<!DOCTYPE html>
<html>
<head>
    <title>Resep Pasien ID Periksa {{$idperiksa}}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <style type="text/css">
        table tr td,
        table tr th{
            font-size: 9pt;
        }
    </style>
    <table style="width: 100%">
        <tr>
            <td style="width: 20%;border-bottom:1pt solid black;">
                <img src="assets/img/logo/logokumham.png" alt="logo" width="50" >
                <img src="assets/img/logo/logopasti.jpg" alt="logopasti" width="70">
            </td>
            <td style="width: 80%;border-bottom:1pt solid black;">
                <center>
                    <h3>BALKESMAS</h3><h5>KEMENTERIAN HUKUM DAN HAK ASASI MANUSIA</h5>
                    <h6><a target="_blank" href="https://balkesmas.kemenkumham.go.id/">balkesmas.kemenkumham.go.id</a></h6>
                </center>
            </td>
        </tr>
    </table>
    <br><br>
    <center><H3><b>RESEP OBAT</b></H3></center>

    <table class='table table-bordered'>
        <tbody>
            <tr><th>ID TRANSAKSI / TANGGAL</th><td>92 / 28 FEBRUARI 2020</td></tr>
            <tr><th>PASIEN / USIA</th><td>191 - PRAMITA SEPTIYANI / 28 TAHUN </td></tr>
            <tr><th>PERAWAT</th><td>201 - AVITA TRI UTAMI</td></tr>
            <tr><th>DOKTER</th><td>402 - AINATUL MAULIDA</td></tr>
        </tbody>
    </table>
 
    <table class='table table-bordered'>
        <thead>
            <tr><th>ID OBAT</th><th>NAMA OBAT</th><th>ATURAN PAKAI</th><th>JUMLAH</th></tr>
        </thead>
        <tbody>
            <tr><td>101</td><td>PARACETAMOL</td><td>2XSEHARI</td><td>2 TABLET</td></tr>
            <tr><td>101</td><td>VITAMIN</td><td>1XSEHARI</td><td>2 TABLET</td></tr>
            <tr><td>101</td><td>PANADOL</td><td>3XSEHARI</td><td>2 TABLET</td></tr>
            <tr><td>101</td><td>VIKS</td><td>1XSEHARI</td><td>2 ML</td></tr>
        </tbody>
    </table>
    
    <table style="position: absolute;bottom: 150;width: 100%;">
        <tr align="center">
            <td>
                <b>PASIEN,<br><br><br><br><br><br><br>
                PRAMITA SEPTIYANI</b>
            </td>
            <td>
                <b>APOTEKER,<br><br><br><br><br><br><br>
                SATRIO WIBISONO</b>
            </td>
            <td>
                <b>DOKTER,<br><br><br><br><br><br><br>
                AINATUL MAULIDA</b>
            </td>
        </tr>
    </table>
</body>
</html>