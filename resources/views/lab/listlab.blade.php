@extends('template.main')

@section('css')
    
@endsection

@section('title')
E-BALKESMAS
@endsection

@section('content')
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class="pull-left">
                    <div class="page-title">DATA PEMERIKSAAN LAB</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    Lab</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Daftar Lab</li>
                </ol>
            </div>
        </div>

         <!-- chart start -->
        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Daftar Semua Lab </header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body no-padding height-9">
                        
                    <!-- end widget -->
                    </div>
                    <div class="card-body">
                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width" id="patientDoctorDatatable">
                                <thead>
                                    <tr>
                                        <th width="13%"> Antrian </th>
                                        <th> ID Periksa </th>
                                        <th> Nama Poli </th>
                                        <th> No MR </th>
                                        <th> Nama </th>
                                        <th> Tanggal Periksa </th>
                                        <th width="15%"> Aksi </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>2001293</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button type="button" title="Lihat" id="seeButton" data-id="'+data+'" class="label label-primary menu-see-button download-" style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>
                                            <button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-" style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <!-- Chart end -->

         <!-- Modal Confirm Done -->

            <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Yakin Pasien Telah Selesai Diperiksa?</h4>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="modal-btn-ya">Ya</button>
                    <button type="button" class="btn btn-danger" id="modal-btn-no">Tidak</button>
                  </div>
                </div>
              </div>
            </div>

            <div class="alert" role="alert" id="result"></div>
         <!-- -->

        <!-- Patient Detail Selection Modal -->
        <div class="modal fade" id="patientDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Detail Pasien</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>No MR</label>
                                    <input type="text" id="detail-MR" class="form-control" placeholder="" disabled>
                                </div>
                                
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" id="detail-nama" class="form-control" placeholder="Nama" disabled>
                                </div>
                                <div class="form-group">
                                    <label>NIP/NIK</label>
                                    <input type="text" id="detail-nip" class="form-control" placeholder="NIP/NIK" disabled>
                                </div>
                                <div class="form-group">
                                    <label>No BPJS</label>
                                    <input type="text" id="detail-bpjs" class="form-control" placeholder="BPJS" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" id="detail-ttl" class="form-control"  placeholder="Tanggal Lahir" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Satuan Kerja</label>
                                    <input type="text" id="detail-unit" class="form-control"  placeholder="Umum" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" id="detail-email" class="form-control"  placeholder="Email" disabled>
                                </div>
                                <div class="form-group">
                                    <label>No HP</label>
                                    <input type="text" id="detail-hp" class="form-control"  placeholder="No HP" disabled>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <input type="text" id="detail-gender" class="form-control"  placeholder="Not Set" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea id="detail-address" class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Penyakit Yang Pernah Diderita</label>
                                    <textarea id="detail-riwayat-penyakit" class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Penyakit Keluarga/Turunan</label>
                                    <textarea id="detail-penyakit-turunan" class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Riwayat Alergi</label>
                                    <textarea id="detail-riwayat-alergi" class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Riwayat Vaksinasi</label>
                                    <textarea id="detail-riwayat-vaksinasi" class="form-control" rows="3" placeholder="Enter ..." disabled></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Patient Detail Selection Modal -->

        <!-- Edit Patient Selection Modal -->
        <div class="modal fade" id="patientEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Data Pasien</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <input type="hidden" id="edit-token" name="_token" value="{{csrf_token()}}"/>
                                <input type="hidden" name="id" id="edit-id">
                                <input type="hidden" name="id-kategori" id="edit-id-kategori">
                                <div class="form-group">
                                    <label>No MR</label>
                                    <input type="text" id="edit-MR" class="form-control" placeholder="" disabled>
                                </div>
                                
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" id="edit-nama" class="form-control" placeholder="Nama" disabled>
                                </div>
                                <div class="form-group">
                                    <label>NIP/NIK</label>
                                    <input type="text" id="edit-nip" class="form-control" placeholder="NIP/NIK" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" id="edit-ttl" class="form-control"  placeholder="Tanggal Lahir" disabled>
                                </div>
                                <input type="hidden" id="edit-satker-id" name="satker_id"/>
                                <div class="form-group">
                                    <label>Satuan kerja</label>
                                    <input type="text" id="edit-unit" class="form-control"  placeholder="Umum" disabled>
                                </div>
                                <div class="form-group">
                                    <label>No BPJS</label>
                                    <input type="text" id="edit-bpjs" class="form-control" placeholder="BPJS">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" id="edit-email" class="form-control"  placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label>No HP</label>
                                    <input type="text" id="edit-hp" class="form-control"  placeholder="No HP">
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <div>
                                        <label>Jenis Kelamin</label>
                                    </div>

                                    <div>
                                        <span class="radio radio-aqua">
                                            <input id="optionGenderL" name="optionGender" type="radio" value="L" checked>
                                            <label for="optionGenderL">
                                                Laki-Laki
                                            </label>
                                        </span>
                                        <span class="radio radio-red">
                                            <input id="optionGenderP" name="optionGender" type="radio" value="P" >
                                            <label for="optionGenderP">
                                                Perempuan
                                            </label>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea id="edit-address" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Penyakit Yang Pernah Diderita</label>
                                    <textarea id="edit-riwayat-penyakit" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Penyakit Keluarga/Turunan</label>
                                    <textarea id="edit-penyakit-turunan" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Riwayat Alergi</label>
                                    <textarea id="edit-riwayat-alergi" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Riwayat Vaksinasi</label>
                                    <textarea id="edit-riwayat-vaksinasi" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" id="submit-edit" class="btn btn-success">Simpan</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Edit Patient Selection Modal -->
    </div>
</div>
<!-- end page content -->


@endsection

@section('js')
    
    <!-- Datatable JS -->
    <script src="{{ asset('assets/template/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/template/table_data.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/template/datatables/export/buttons.print.min.js') }}"></script>
    <!-- Datatable JS -->

    <!-- Datetime Picker -->
    <script src="{{ asset('assets/template/material-datetimepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/template/material-datetimepicker/datetimepicker.js') }}"></script>
    <!-- End of Datetime Picker -->

    <script src="{{ asset('assets/js/main/lab/index.js') }}">
    </script>
@endsection