<?php

namespace App\Http\Controllers\Farmasi;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Resep;


class FarmasiController extends Controller
{

   public function index()
   {
      $data['name'] = Auth::user()->name;
      return view('farmasi/index', $data);
   }

   public function listpasien()
   {
      $result = DB::select('CALL sp_get_pasien_selesai_periksa');
      return $this->success($result, "Berhasil mengambil data");
   }

   public function obatpasien($id)
   {
      $data['name'] = Auth::user()->name;
      $data['id_tr_poli'] = $id;
      return view('farmasi/detailobat', $data);
   }

   public function getdataresep($id)
   {
      $result = DB::select('CALL sp_get_data_resepbyidperiksa(?)', array($id));
      return $this->success($result, "Berhasil mengambil data");
   }

   public function getdatapasien($id)
   {
      $result = DB::select('CALL sp_get_pasien_by_id_tr_poli(?)', array($id));
      return $this->success($result, "Berhasil mengambil data");
   }

   public function updateresepsiap(Request $request)
   {
      $id = $request->input('id');

      $result = DB::select('CALL sp_update_resep_selesai(?)', array($id));
      return $this->success($result, "Berhasil mengambil data");
   }

   public function updateresepdiambil(Request $request)
   {
      $id = $request->input('id');
      $ttd = $request->input('ttd');

      $result = DB::select('CALL sp_update_resep_diambil(?,?)', array($id, $ttd));
      return $this->success($result, "Berhasil mengambil data");
   }

   public function getresepstatus($id)
   {
      $status = Resep::where('id_tr_poli', $id)->first();
      return $status->id_status;
   }
}
