<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\Permission;
use App\Models\Userrole;
use Auth;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/receptionist/patient';

    protected function authenticated(Request $request, $user)
    {
        if (Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password'), 'is_aktif' => 1])) {

            $request->session()->put('iduser',$user->id);
            $result = Userrole::where('user_id', $user->id)
                        ->with(['hasRole.hasPermission.hasMenu'
                            => function($menu) {
                            $menu->select('*');
                            $menu->orderBy('parent_id', 'asc');
                        }
                        ])
                        ->get();
            $roles = $result->pluck('role_id');
            $permission= Permission::whereIn('role_id',$roles)
                    ->with('hasMenu')
                    ->select('menu_id')
                    ->groupBy('menu_id')
            ->get();


            if(!empty($permission)){
                $allPermissions = [];
                foreach ($permission as $key => $value) {
                    $allPermissions[] = $value->hasMenu;
                }    

                usort($allPermissions, array($this, "compare"));
            }

            if(!empty($allPermissions)) 
                return redirect($allPermissions[0]->url);
            else return redirect('/receptionist/patient');
        }
        else{
            Session::flush();
            Session::flash('message', 'User is inactive'); 
            return redirect()->route('login');
        }
    }

    function compare($a, $b)
    {
        return strcmp($b->parent_id, $a->parent_id);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
