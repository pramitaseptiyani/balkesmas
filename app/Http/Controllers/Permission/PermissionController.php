<?php

namespace App\Http\Controllers\Permission;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Users;

class PermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function menu($user_id)
    {
        // $menu = (new Permission())->getMenu($user_id);
        $menu = DB::select('CALL sp_get_akses_menu(?)', array($user_id));
    	return $this->success($menu, "Berhasil mengambil data!");
    }

}