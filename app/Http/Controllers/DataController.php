<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Masterpoli;
use App\Models\Pegawai;
use App\Models\Pasien;
use App\Models\Tr_periksa;
use App\Models\Tr_antrian_poli;
use App\Models\Kategori;
use App\Http\Libraries\Html2Pdf;
use App\Http\Libraries\GeneratePdf;
use App\Http\Libraries\SendMail;
use Illuminate\Support\Facades\View; 
use Mail;

/**
 * Class ReceptionistController
 */
class DataController extends Controller    
{
    public function list()
    {   
        $result = DB::select('CALL sp_get_detail_pasien_all()');
        return $this->success($result, "Berhasil mengambil data");
    }

    /**
     * add patient
     */
    public function addNew(Request $request){
        /* Call SP to save and generate MR number */
        if($request->input("kategori") == "pegawai"){
            $sp_result = $this->addPegawai($request);
        }
        else if($request->input("kategori") == "keluarga pegawai"){
            $sp_result = $this->addKeluarga($request);
        }
        else $sp_result =  $this->addUmum($request);

        $MR_number = $sp_result[0]->gen_mr;
        /* End of Call SP */

        /* Generate PDF */
        $filename = "membercard_".time().".pdf";
        $output = base_path()."/public/temp/".$filename;

        $input = DB::connection('balkesmas')
                ->table('patient')->select('*')
                ->where('patient_mr_number', $MR_number)
                ->first();


        // echo json_encode($input);
        // exit();

        $generate = (new GeneratePdf())->generateMembercard($input, $output);
        /* End of Generate PDF */
        

        /*Send mail*/
        $send = (new SendMail())->membercard($input, $output);
        /*End of Send mail*/
        

        unlink($output);
        
        return $this->success($MR_number, 'Data berhasil disimpan');

    }

    public function addPegawai($request)
    {
        $input = $request->input();
        $is_vip = $input['is_vip']=="true"?1:0;

        $kode_satker = $input["satkerid"];
        $satkerid = substr($kode_satker, 0, 2);
        
        $result = DB::select('CALL sp_insert_pasien(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 

            array(
                1,      // id_kategori
                null,   // id detail keluarga
                null,   // nip_keluarga
                $input["nama"],  //nama
                $input["id_card"],   // id_card
                $input["ttl"], // ttl
                $input["hp"],   // phone
                $input["email"],    // email 
                $input["jenis_kelamin"],    //gender
                $input["bpjs"], // bpjs
                $input["alamat"],  //alamat
                $input["riwayat_penyakit"], //
                $input["riwayat_turunan"],
                $input["riwayat_alergi"],
                $input["riwayat_vaksinasi"],
                $input["satkerid"],
                $input["mr_lama"],
                $is_vip,
            )
        );

        return $result;
    }

    public function addKeluarga($request)
    {
        $input = $request->input();
        $is_vip = $input['is_vip']=="true"?1:0;

        $kode_satker = $input["satkerid"];
        $satkerid = substr($kode_satker, 0, 2);

        $result = DB::select('CALL sp_insert_pasien(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 

            array(
                2,      // id_kategori
                $input['id_detail_keluarga'],   // id detail keluarga
                $input['id_card'],   // nip_keluarga
                $input["nama"],  //nama
                $input["id_card"],   // id_card
                $input["ttl"], // ttl
                $input["hp"],   // phone
                $input["email"],    // email 
                $input["jenis_kelamin"],    //gender
                $input["bpjs"], // bpjs
                $input["alamat"],  //alamat
                $input["riwayat_penyakit"], //
                $input["riwayat_turunan"],
                $input["riwayat_alergi"],
                $input["riwayat_vaksinasi"],
                $satkerid,
                $input["mr_lama"],
                $is_vip,
            )

            // array(
            //     2,  
            //     "3",
            //     "199503132019012001",
            //     "Rahasia",
            //     "199503132019012001",
            //     "2023-11-10",
            //     "0215253004",
            //     "avita.triutami@kemenkumham.go.id", 
            //     "L",
            //     null,
            //     null,
            //     null, //
            //     null,
            //     null,
            //     null,
            //     "05",
            //     "MR_lama_12313",
            //     0,
            // )
        );

        return $result;
    }

    public function addUmum($request)
    {
        $input = $request->input();
        $is_vip = $input['is_vip']=="true"?1:0;


        $result = DB::select('CALL sp_insert_pasien(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 

            array(
                3,      // id_kategori
                null,   // id detail keluarga
                $input['id_card'],   // nip_keluarga
                $input["nama"],  //nama
                $input["id_card"],   // id_card
                $input["ttl"], // ttl
                $input["hp"],   // phone
                $input["email"],    // email 
                $input["jenis_kelamin"],    //gender
                $input["bpjs"], // bpjs
                $input["alamat"],  //alamat
                $input["riwayat_penyakit"], //
                $input["riwayat_turunan"],
                $input["riwayat_alergi"],
                $input["riwayat_vaksinasi"],
                null,
                $input["mr_lama"],
                $is_vip
            )
        );

        return $result;
    }


    // public function add(Request $request){
    //     $pasien = new Pasien();

    //     $unit = (new Pasien())->unitCode($request);
    //     $patientType = (new Pasien())->patientCode($request);

    //     $lastMR = (new Pasien())->getLastSequence();
    //     $sequence = (int)$lastMR+1;

    //     $digit = strlen((string)$sequence);
    //     if($digit < 6){
    //         $addDigit = 6-$digit;
    //         $addBeginning = str_repeat("0", $addDigit); 
    //         $mr = $addBeginning.$sequence;
    //     }
    //     else $mr = $sequence;

    //     $pasien->patient_mr_number = $unit."-".$mr."-".$patientType;

    //     $pasien->patient_nm = $request->input('nama');
    //     $pasien->id_card = $request->input('id_card');

    //     $pasien->patient_ttl = $request->input('ttl');
    //     $pasien->patient_unit = $request->input('unit');
    //     $pasien->bpjs = $request->input('bpjs');
    //     $pasien->patient_address = $request->input('alamat');
    //     $pasien->patient_phone = $request->input('hp');
    //     $pasien->patient_mail = $request->input('email');
    //     // $pasien->gender = $request->input('jenis_kelamin');

    //     $pasien->diag_diderita_old = $request->input('riwayat_penyakit');
    //     $pasien->diag_turunan = $request->input('penyakit_turunan');
    //     $pasien->alergi_rwy = $request->input('riwayat_alergi');
    //     $pasien->vaksinasi_rwy = $request->input('riwayat_vaksinasi');
    //     $pasien->id_kategori = (new Kategori())->getId($request->input('kategori'));
        
    //     $pasien->save();

    //     /* Generate PDF */
    //     $filename = "test_".time().".pdf";
    //     $output = base_path()."/public/temp/".$filename;
    //     $content = file_get_contents(resource_path('views/template/membercard/index.blade.php'));

    //     $content = str_replace('Shaka Nagano', $pasien->patient_nm, $content);
    //     $content = str_replace('MR_number', $pasien->patient_mr_number, $content);
    //     $content = str_replace('Sekretariat Jenderal', $pasien->patient_unit, $content);
    //     $content = str_replace('Alamat_user', $pasien->patient_address, $content);

    //     $pageOptions = array(
    //                     'orientation'      => 'Landscape',
    //                     'page-size'        => 'A4'
    //                   );


    //     $html2pdf = new HTML2PDF();
    //     $html2pdf->getFromHTML($content, $output);

    //     // // /*Send mail*/
    //     Mail::send('email.membercard', [], function ($message) use ($pasien, $output) {

    //         $message->from('balkesmas.kemenkumham@gmail.com', 'Balkesmas Kemenkumham');

    //         $message->to($pasien["patient_mail"]);
    //         $message->subject('This is your membercard');

    //         /* with attachment */
    //         $message->attach($output);
    //     });

    //     unlink($output);
        

    //     // return redirect('receptionist/patient')->with('status', 'Pasien Berhasil Ditambahkan!');
    //     return $this->success([], 'Data berhasil disimpan');

    // }

    public function MRContent(Request $request)
    {
        $MR = $tipe = $request->input('MR');

        $filename = "MR_".time().".pdf";
        $output = base_path()."/public/temp/".$filename;
        $content = file_get_contents(resource_path('views/template/medicalrecord/index.blade.php'));

        /* Get data by MR */
        $pasien = Pasien::where('patient_mr_number', $MR)->first();

        /**/

        $content = str_replace('replace_nama', $pasien->patient_nm, $content);
        $content = str_replace('replace_mr', $pasien->patient_mr_number, $content);
        $content = str_replace('replace_nip', $pasien->patient_nip, $content);
        $content = str_replace('replace_unit', $pasien->patient_unit, $content);
        $content = str_replace('replace_bpjs', $pasien->bpjs, $content);
        $content = str_replace('replace_email', $pasien->patient_mail, $content);
        $content = str_replace('replace_phone', $pasien->patient_phone, $content);
        $content = str_replace('replace_address', $pasien->patient_address, $content);
        $content = str_replace('replace_penyakit_pernah_diderita', $pasien->diag_diderita_old, $content);
        $content = str_replace('replace_penyakit_turunan', $pasien->diag_turunan, $content);
        $content = str_replace('replace_alergi', $pasien->alergi_rwy, $content);
        $content = str_replace('replace_vaksinasi', $pasien->vaksinasi_rwy, $content);

        $pageOptions = array(
                        'orientation'      => 'Landscape',
                        'page-size'        => 'A4'
                      );


        $html2pdf = new HTML2PDF();
        $html2pdf->getFromHTML($content, $output);

        header('Content-type: application/pdf; Content-disposition: inline; filename='.$filename.'.pdf');
        readfile($output);
        unlink($output);
    }

    public function nipIsExist(Request $request)
    {   
        $nip = $request->input('nip');
        $id_kategori = $request->input('id_kategori');
        $result = Pasien::where('id_card', $nip)
                    ->where('id_kategori', $id_kategori)
                    ->first();

        if($result != null)
            return $this->success($result, "Data Pasien sudah ada");
        return $this->success(null, "Pasien Baru");
    }

    public function getPatient($id)
    {
        // $result = Pasien::where('id', $id)->first();
        $result = DB::select('CALL sp_get_detail_pasien(?)', array($id));
        return $this->success($result[0], "Data berhasil diambil");
    }

    public function editPatient(Request $request)
    {
        $input = $request->input('input');
        $insert = DB::select('CALL sp_update_pasien(?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?)', $input);

        return $this->success([], "Data berhasil disimpan");
    }

    public function periksaIsExist(Request $request)
    {   
        $id_pasien = $request->input('id_pasien');
        $id_poli = $request->input('list_poli');
        $dateNow = date('Y-m-d');

        $result = Tr_periksa::where('id_pasien', $id_pasien)
                    ->where('tgl_periksa', 'like', $dateNow.'%' )
                    ->orderBy('tgl_periksa', 'desc')
                    ->first();

        if($result != null){
            $id_periksa = $result->id;

            $antrian = Tr_antrian_poli::where('id_periksa', $id_periksa)
                    ->where('is_done', 0)
                    ->orderBy('id', 'desc')
                    ->first();

            if($antrian != null){
                $poli = Masterpoli::where('id', $antrian->id_poli)
                    ->first();

                return $this->error($result, "Pasien telah terdaftar di ".$poli->nama.". Selesaikan terlebih dahulu");
            }
            else{
                return $this->success(null, "Registrasi Poli Baru");
            }            
        }
        return $this->success(null, "Registrasi Poli Baru");
    }

    public function registerPoli(Request $request)
    {
        $id_pasien = (int)$request->input('id_pasien');
        $list_poli = (string)$request->input('list_poli');
        $tgl_periksa = date('Y-m-d H:i:s');

        $insert = DB::select('CALL sp_insert_periksa(?, ?, ?)', array($id_pasien, $list_poli, $tgl_periksa));

        if($insert != null){
            $result = $insert[0]->no_urut_perawat;
        }
        else{
            $result = null;
        }

        return $this->success($result, "Berhasil mendaftar ke Poli");
    }

    public function getSimpeg(Request $request)
    {
        $nip = $request->input('nip');

        $content = [
            "nip"   => $nip,
            "page"  => 1
        ];

        $curl       = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => env('API_SIMPEG').'/pegawai',
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => "",
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 30,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "POST",
            CURLOPT_POSTFIELDS      => $content,
            CURLOPT_HTTPHEADER      => array(
                "Authorization: Bearer ".env('TOKEN_SIMPEG'),
                'Cookie: ci_session=nosofehsi6t0f0q6cglahg7e2gq0nool'
            ),
        ));
        $response   = curl_exec($curl);
        $err        = curl_error($curl);
        curl_close($curl);

        if ($err != "") {
            $result = [
                "status"    => false,
                "result"    => $err
            ];

            return [];
        } else {
            $response = json_decode($response);
            $data = (array)$response->data;

            /* we use foreach because this performance is better than array_filter */
            $result=[];
            foreach ($data as $key => $eachdata) {
                if(str_starts_with($eachdata->kode_satker, '05')) $result[] = $eachdata;
                else if(str_starts_with($eachdata->kode_satker, '06')) $result[] = $eachdata;
                elseif(str_starts_with($eachdata->kode_satker, '08')) $result[] = $eachdata;
                else if(str_starts_with($eachdata->kode_satker, '09')) $result[] = $eachdata;
            }
            return $result;
        }
    }

    public function resendMR($id)
    {
        $filename = "membercard_".time().".pdf";
        $output = base_path()."/public/temp/".$filename;

        $input = DB::connection('balkesmas')
                ->table('patient')->select('*')
                ->where('id', $id)
                ->first();


        $generate = (new GeneratePdf())->generateMembercard($input, $output);
        /* End of Generate PDF */
        
        try {
            /*Send mail*/
            $send = (new SendMail())->membercard($input, $output);
            /*End of Send mail*/
            

            unlink($output);
            
            return $this->success([], 'Email Berhasil Terkirim');
        } catch (Exception $ex) {
            return $this->error([], 'Gagal Mengirim Email');
        }
    }

    public function select2FamilyActive(Request $request)
    {
        $nip = $request->input('nip');
        $search = $request->input('search');

        if($search != ''){
            $result = DB::connection('balkesmas')
                ->table('mst_keluarga_pegawai')->select('*')
                ->where('nip', $nip)
                ->where('kddapat', 1)
                ->where('nama', 'like', '%'.$search.'%' )
                ->orderBy('nama', 'asc')
                ->get();    
        }
        else{
            $result = DB::connection('balkesmas')
                ->table('mst_keluarga_pegawai')->select('*')
                ->where('nip', $nip)
                ->where('kddapat', 1)
                ->orderBy('nama', 'asc')
                ->get();
        }
        
        return $this->success($result, 'Berhasil mengambil data');
    }

    public function getFamilyCategory(Request $request)
    {
        $nip = $request->input('nip');
        $tgllhr = $request->input('tgllahir');
        
        $result = DB::connection('balkesmas')
            ->table('mst_keluarga_pegawai')->select('*')
            ->where('nip', $nip)
            ->where('kdkeluarga', 3)
            ->orderBy('tgllhr', 'asc')
            ->get();   

        $id_kategori = 3;
        foreach ($result as $key => $value) {
            if($tgllhr == $value->tgllhr){
                $id_kategori = $key + 3;
                return $this->success($id_kategori, 'Berhasil mengambil data');
            }
            else{
                continue;
            }
        } 
         
        return $this->success([], 'Berhasil mengambil data');
    }

}
