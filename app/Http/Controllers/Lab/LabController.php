<?php

namespace App\Http\Controllers\Lab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use App\Models\Pasien;
use Auth;
use App\Http\Libraries\Html2Pdf;
use Illuminate\Support\Facades\View; 
use Mail;
use Illuminate\Support\Facades\DB;
use PDF;

class LabController extends Controller
{

   public function index(){
        $data['name'] = Auth::user()->name;

        return view('lab/index', $data);
   }

   public function listlab(Request $request){
        $data['name'] = Auth::user()->name;

        //ambil detil lab level 1
        $level1 = DB::select('CALL sp_get_mstlab_1()');
        $length_lv1 = count($level1);

        $data['nlev1'] =  $length_lv1;
        $data['level1']=[];
        for ($i=0; $i<$length_lv1; $i++) {
          $idlistlab = $level1[$i]->id;
          $data['level1'][$i]['id'] = $level1[$i]->id;
          $data['level1'][$i]['nama'] = $level1[$i]->nama;

          $level23 = DB::select('CALL sp_get_mstlab_23(?)', array($idlistlab));
          $length_lv23 = count($level23);
          
          $bagi = floor($length_lv23/4);
          $mod = $length_lv23%4;

          $arrlevel23 = array();
          $x=0;
          while($x < $length_lv23)  {
              for ($j=0; $j<$bagi;$j++) {
                  for ($k=0;$k<4;$k++) {
                    if ($x < $length_lv23) {
                      $arrlevel23[$j][$k]['id'] = $level23[$x]->id;
                      $arrlevel23[$j][$k]['level'] = $level23[$x]->level;
                      $arrlevel23[$j][$k]['nama'] = $level23[$x]->nama;
                      $x++;
                    }
                  }
              }

              if ($mod != 0 ) {
                  for ($l=0;$l<$mod;$l++) {
                    if ($x < $length_lv23) {
                      $arrlevel23[$bagi][$l]['id'] = $level23[$x]->id;
                      $arrlevel23[$bagi][$l]['level'] = $level23[$x]->level;
                      $arrlevel23[$bagi][$l]['nama'] = $level23[$x]->nama;
                      $x++;
                    }
                  }
                  for ($l=$mod;$l<4;$l++) {
                      $arrlevel23[$bagi][$l]['id'] = null;
                      $arrlevel23[$bagi][$l]['level'] = null;
                      $arrlevel23[$bagi][$l]['nama'] = null;
                  }
              }
          }
            
          $data['level23'][$i] =  $arrlevel23;
          $data['nlevel23'][$i] = count($arrlevel23);
          //echo '<script> console.log("masuk",'. json_encode($arrlevel23 ).')</script>';

        }

        echo '<script> console.log("masuk",'. json_encode($data['level23'] ).')</script>';

        return view('lab/listlab', $data);
   }


   public function cekanamnesa($idtr)
    {
        $result = DB::select('CALL sp_cek_dt_anamnesa(?)', array($idtr));
        return $this->success($result, "Berhasil mengambil data");
    }

    public function cek_tr_resep($idtr)
    {
        $result = DB::select('CALL sp_cek_dt_tr_resep(?)', array($idtr));
        return $this->success($result, "Berhasil mengambil data");
    }

    public function cek_tr_det_resep($idtrresep)
    {
        $result = DB::select('CALL sp_cek_tr_det_resep(?)', array($idtrresep));
        return $this->success($result, "Berhasil mengambil data");
    }

   public function getPatient($id)
    {
        $result = Pasien::where('id', $id)->first();

        return $this->success($result, "Data berhasil diambil");
    }


    public function donePatient($idtr)
    {
        $update = DB::table('tr_antrian_poli')
              ->where('id', $idtr)
              ->update(['is_done' => 1]);

        return $this->success($update, "Pasien Telah Selesai Diperiksa!");
    }


   public function simpan_periksa(Request $request){
        $id_tr_poli = (int)$request->input('id_periksa_dokter');
        $id_user_dokter = 4;
        $anamnesa = (string)$request->input('anamnesa');
        $therapy = (string)$request->input('therapy');
        $keterangan = (string)$request->input('keterangan');

        $insert = DB::select('CALL sp_insert_periksadokter(?, ?, ?, ?, ? )', array($id_tr_poli, $id_user_dokter, $anamnesa, $therapy, $keterangan));

        return $this->success($insert, "Berhasil Menyimpan Data Anamnesa!");
   }


   public function update_periksa(Request $request){
        $id_tr_poli = (int)$request->input('id_periksa_dokter');
        $id_user_dokter = 4;
        $anamnesa = (string)$request->input('anamnesa');
        $therapy = (string)$request->input('therapy');
        $keterangan = (string)$request->input('keterangan');

        $update = DB::select('CALL sp_update_periksadokter(?, ?, ?, ?, ? )', array($id_tr_poli, $id_user_dokter, $anamnesa, $therapy, $keterangan));

        return $this->success($update, "Berhasil Memperbarui Data Anamnesa!");
   }


   public function get_list_pasien_dokter(Request $request)
    {
        $doctorid = $request->session()->get('iduser');
        $tgl_periksa = date('Y-m-d H:i:s');
        $result = DB::select('CALL sp_get_antridokter_first(?, ?)', array($tgl_periksa,$doctorid));

        return $this->success($result, "Berhasil mengambil data");
    }


   public function get_history_dokter($idpasien)
    {
        $result = DB::select('CALL sp_get_history_dokter(?)', array($idpasien));
        return $this->success($result, "Berhasil mengambil data");
    }

    public function get_history_resep($idpasien)
    {
        $result = DB::select('CALL sp_get_history_resep(?)', array($idpasien));
        return $this->success($result, "Berhasil mengambil data");
    }

    public function get_history_lab($idpasien)
    {
        $result = DB::select('CALL sp_get_history_lab(?)', array($idpasien));
        return $this->success($result, "Berhasil mengambil data");
    }

    public function get_history_fisio($idpasien)
    {
        $result = DB::select('CALL sp_get_history_fisioterapi(?)', array($idpasien));
        return $this->success($result, "Berhasil mengambil data");
    }

    public function search_obat_bynama()
    {
        $result = DB::select('CALL sp_get_obat_nama()');
        return $this->success($result, "Berhasil mengambil data");
    }

    public function cek_obat_id($obnama)
    {
        $result = DB::select('CALL sp_cek_obat_id(?)', array($obnama));
        return $this->success($result, "Berhasil mengambil data");
    }

    public function cekstock_resep($idobat)
    {
        $result = DB::select('CALL sp_cek_stock_resep_by_idobat(?)', array($idobat));
        return $this->success($result, "Berhasil mengambil data");
    }

    public function del_tr_det_resep($idtrresep)
    {
        $result = DB::select('CALL sp_del_tr_det_resep(?)', array($idtrresep));
        return $this->success($result, "Berhasil mengambil data");
    }

    public function del_tr_resep($idtr)
    {
        $result = DB::select('CALL sp_del_tr_resep(?)', array($idtr));
        return $this->success($result, "Berhasil mengambil data");
    }

    public function simpanresep(Request $request){
        $idperiksadokter_resep = (int)$request->input('idperiksadokter_resep');
        $petugas_farmasi = (int)$request->input('petugas_farmasi');
        $tglresep = (string)$request->input('tglresep');
        $detobat = "";
        $tempdetobat ="";
        //$obid = $request->input('obid')[0];
        $obid = $request->input('obid');
        $n = count($obid);

        for ($i=0;$i<$n;$i++) {
            $obid = $request->input('obid')[$i];
            $obaturan = $request->input('obaturan')[$i];
            $objumlah = $request->input('objumlah')[$i];

            $tempdetobat = $obid."|".$obaturan."|".$objumlah;
            if ($detobat == "") {
                $detobat = $tempdetobat;
            } else {
                $detobat = $detobat."#".$tempdetobat;    
            }
            
        }

        //cek apakah sudah ada resep yang disimpan untuk id tr_poli yg sama
        $cek_tr_resep = DB::select('CALL sp_cek_tr_resep_by_tr_poli(?)', array($idperiksadokter_resep));
        $ncek = count($cek_tr_resep);
        // echo '<script> console.log("id_tr_resep",'.$id_tr_resep.')</script>';

        for ($i=0;$i<$ncek;$i++ ) {
            $id_tr_resep = $cek_tr_resep[$i]->id;;

            //hapus dulu
            DB::table('tr_det_resep')->where('id_tr_resep', '=', $id_tr_resep)->delete();
            DB::table('tr_resep')->where('id', '=', $id_tr_resep)->delete();

            // //update stock obat (ditambahkan lagi)
            //     $update_stock = 
        }
            
        //simpan
        $insert = DB::select('CALL sp_insert_resep(?, ?, ?, ? )', array($idperiksadokter_resep, $petugas_farmasi, $tglresep, $detobat));

        // //update stock obat
        // $update_stock = 

        return $this->success($insert, "Berhasil Menyimpan Data Resep!");
   }

   public function cetak_resep($idperiksa){
       $data['nama'] = Auth::user()->name;
       $data['idperiksa'] = $idperiksa;
 
        $pdf = PDF::loadview('doctor/cetakresep',$data);
        return $pdf->stream('resep.pdf',array('Attachment'=>0));
   }

   public function get_det_pegawai(Request $request) {
      $doctorid = $request->session()->get('iduser');

      $result = DB::select('CALL sp_get_detil_peg_by_userid(?)', array($doctorid));
      return $this->success($result, "Berhasil mengambil data");
    }

    public function simpanlab(Request $request){
        $idtr_lab = (int)$request->input('idtr_lab');
        $tgllab = (string)$request->input('tgllab');
        $cblab = $request->input('cblab');
        $n = count($cblab);

        $detlab = "";
        $templab="";
        for ($i=0;$i<$n;$i++) {
          $templab = $cblab[$i];

            if ($detlab == "") {
                $detlab = $templab;
            } else {
                $detlab = $detlab.";".$templab;    
            }
        }

        echo '<script> console.log("idtr_lab",'. $idtr_lab.')</script>';
        echo '<script> console.log("detlab",'. json_encode($detlab ).')</script>';
        echo '<script> console.log("tgllab",'. $tgllab.')</script>';


        $insert = DB::select('CALL sp_insert_lab(?, ?, ? )', array($idtr_lab, $detlab, $tgllab));
        return $this->success("Berhasil Menyimpan Data Laboratorium!");
   }

   public function simpanfisio(Request $request){
        $idtr_fisio = (int)$request->input('idtr_fisio');
        $tglfisio = (string)$request->input('tglfisio');
        $cbfisio = $request->input('cbfisio');
        $n = count($cbfisio);

        $detfisio = "";
        $tempfisio="";
        for ($i=0;$i<$n;$i++) {
          $tempfisio = $cbfisio[$i];

            if ($detfisio == "") {
                $detfisio = $tempfisio;
            } else {
                $detfisio = $detfisio.";".$tempfisio;    
            }
        }

        //$insert = DB::select('CALL sp_insert_fisio(?, ?, ? )', array($idtr_fisio, $detfisio, $tglfisio));
        return $this->success($insert, "Berhasil Menyimpan Data Fisioterapi!");
   }
}
