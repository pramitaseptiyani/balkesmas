<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Libraries\Html2Pdf;
use Illuminate\Support\Facades\View; 

/**
 * Class PatientController
 */
class PatientController extends Controller
{
    /**
     * View of acknowledgement letter
     *      patient has finished the medical examination
     */
    public function acknowledgement(){
        return view('patient/acknowledgement');
    }


    /**
     * View of patient's signature
     */
    public function finishSignature(){
        return view('patient/signature');
    }

    public function testMR()
    {
        return view('template/medicalrecord/index');
    }

}
