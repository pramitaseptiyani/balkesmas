<?php

namespace App\Http\Controllers\Survei;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\View; 

class ViewController extends Controller
{
	public function index(){
		return view('survei.index');
	}
}