<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function index(){
    	//return "Halo ini adalah method index, dalam controller DosenController. - www.malasngoding.com";
    	$dtuser        = array(
          'nip'                 => '199109092017122001',
          'nama'                => 'Pramita Septiyani',
          'username'            => 'pramitaseptiyani',
          'password'            => 'p@ssw0rd',
          'encryptkey'          => '#$@$@FGE#R#',
          'no_hp'               => '085227544731',
          'email'               => 'mita.septi@gmail.com',
          'id_jenis_user'       => 3,
          'id_jenis_konsultasi' => 0,
          'is_aktif'            => 1
        );

        $dtuser['testya'] = "lalalayeyeye";
    	return view('blog',$dtuser);
    }

    public function dologin(){
    	return view('login.login');
    }

    public function home(){
    	return view('home/home');
    }

}
