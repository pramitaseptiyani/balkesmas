<?php

namespace App\Http\Controllers\Registration;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\View; 
use App\Models\Dokterpoli;
use App\Models\Masterpoli;
use App\Models\Pasien;
use App\Models\Reservasi;

class RegistrationController extends Controller
{
	/**
     * View to add patient with NIP
     */
    public function addPatient(){
        return view('registration/add');
    }

    /**
     * View to add general patient with NIK
     */
    public function addGeneralPatient(){
        return view('registration/addGeneral');
    }

    /**
     * View to add family patient with Reference NIP
     */
    public function addFamilyPatient(){
        return view('registration/addFamily');
    }
}