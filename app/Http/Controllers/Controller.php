<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function success($data, $message ="Berhasil", $http_code=200, $title="Berhasil!")
	{
		$content = (object)[];
		$content->data    	 = $data;
		$content->isSuccess  = true;
        $content->message 	 = $message;
        $content->title 	 = $title;

        return response()
        		->json($content, $http_code)
        		->header('Content-Type', 'application/json');
	}

	public function error($data, $message="Gagal", $http_code=400, $title="Error!")
	{
		$content = (object)[];
		$content->data    	 = $data;
		$content->isSuccess  = false;
        $content->message 	 = $message;
        $content->title 	 = $title;

		return response()
        		->json($content, $http_code)
        		->header('Content-Type', 'application/json');
	}
}
