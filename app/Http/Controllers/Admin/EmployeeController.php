<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View; 

class EmployeeController extends Controller
{
	public function getList(Request $request, $is_active)
	{
		$input = $request->input();

		$orderColumn = $input['order'][0]['column'];
		$orderBy = $input['columns'][$orderColumn]['name'];
		$orderDir = $input['order'][0]['dir'];
		$offset = $input['start'];
		$limit = $input['length'];
		$search = $input['search']['value'];
		if($search == null) $search = "";

		$result = DB::select('CALL sp_get_pegawai_table(?, ?, ?, 		?, ?, ?)', 
					array($orderBy, $orderDir, $offset, $limit, $search, (int) $is_active));

		return $this->success($result, "Berhasil");
	}

	public function detail($id)
	{
		$id = (int) $id;
		$result = DB::select('CALL sp_get_pegawai_byid(?)', array($id));
		return $this->success($result[0], "Berhasil");
	}

	public function select2Satker(Request $request)
	{
		$input = $request->input('search');
		if(isset($input)){	
			$result = DB::connection('balkesmas')
    			->table('satker')
    			->where('satker', 'like', '%'.$input.'%')
    			->where(function ($query) {
    				$query->orWhere('satkerid', 'like', '05%');
    				$query->orWhere('satkerid', 'like', '06%');
    				$query->orWhere('satkerid', 'like', '08%');
    				$query->orWhere('satkerid', 'like', '09%');
    			})
    			->get();
		}
		else{
        	$result = DB::connection('balkesmas')
        			->table('satker')
        			->orderBy('satker', 'asc')
        			->whereNotNull('satker')
        			->where(function ($query) {
	    				$query->orWhere('satkerid', 'like', '05%');
	    				$query->orWhere('satkerid', 'like', '06%');
	    				$query->orWhere('satkerid', 'like', '08%');
	    				$query->orWhere('satkerid', 'like', '09%');
	    			})
        			->limit(10)
        			->get();	
		}

		return $this->success($result, "Berhasil mengambil data");
	}

	public function addPatient(Request $request)
    {
        $input = $request->input();

        $year = substr($input["id_card"], 0, 4);
        $month = substr($input["id_card"], 4, 2);
        $day = substr($input["id_card"], 6, 2);
        $ttl = $year.'-'.$month.'-'.$day;

        $result = DB::select('CALL sp_insert_pasien(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 

            array(
                1,      // id_kategori
                null,   // id detail keluarga
                null,   // nip_keluarga
                $input["nama"],  //nama
                $input["id_card"],   // id_card
                $ttl, // ttl
                $input["hp"],   // phone
                $input["email"],    // email 
                $input["jenis_kelamin"],    //gender
                null, // bpjs
                $input["alamat"],  //alamat
                null, //
                null,
                null,
                null,
                $input['satker']
            )
        );

        return $this->success($result);
    }

}