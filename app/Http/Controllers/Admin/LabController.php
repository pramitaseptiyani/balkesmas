<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\View; 
use Illuminate\Support\Facades\DB;

class LabController extends Controller
{
	public function getList1(Request $request, $is_active)
	{
		$input = (object)$request->input();

		$offset = ($input->page - 1) * $input->limit;

		if($input->search == null) $input->search = "";

		$get = DB::select('CALL sp_get_mstlab(?, ?, ?, ?, ?)', 
			array(
				$offset,
				$input->limit,
				$input->search,
				1,
				0
			));

		if($get == []) $total = 0;
		else $total = $get[0]->jumlah_row;

		$result = (object)[
			"data"	=> $get,
			"total"	=> $total
		];
		return json_encode($result);
	}

	public function getList2(Request $request, $id_parent)
	{
		$input = (object)$request->input();

		$offset = ($input->page - 1) * $input->limit;

		if($input->search == null) $input->search = "";

		$get = DB::select('CALL sp_get_mstlab(?, ?, ?, ?, ?)', 
			array(
				$offset,
				$input->limit,
				$input->search,
				2,
				$id_parent
			));


		if($get == []) $total = 0;
		else $total = $get[0]->jumlah_row;

		$result = (object)[
			"data"	=> $get,
			"total"	=> $total
		];

		return json_encode($result);
	}

	public function getList3(Request $request, $id_parent)
	{

		$result = DB::connection('balkesmas')
        			->table('mst_lab')
        			->where('parent_id', $id_parent)
        			->where('is_aktif', 1)
        			->get();

        $getParent1 = DB::select('CALL sp_get_mstlab_byid(?)', 
            array($id_parent));
        $nama_parent = $getParent1[0]->nama;


        $result->map(function ($lab) use ($nama_parent) {
            $lab->parent = $nama_parent;
            return $lab;
        });

		return $this->success($result, "Berhasil");
	}

	public function inactiveList()
	{

		$result = DB::select('CALL sp_get_mstlab_nonaktif(?)',array(0));
		return $this->success($result, "Berhasil");
	}

	public function detail($id)
	{
		$result = DB::select('CALL sp_get_mstlab_byid(?)', array($id));	
		return $this->success($result[0], "Berhasil");
	}

	public function add(Request $request)
	{
		$input = (object) $request->input();
		
		$input->is_aktif = json_decode($input->is_aktif);
		$input->is_aktif = ($input->is_aktif == true) ? '1':'0';

		if($input->parent_id == 0){
			$input->level = 1;
		}
		else{
			$parent = DB::select('CALL sp_get_mstlab_byid(?)', array($input->parent_id));
			$input->level = ($parent[0]->level)+1;
		}

		$result = DB::select('CALL sp_insert_mstlab(?, ?, ?, ?)',
				 array(
				 	$input->parent_id, 
				 	$input->nama, 
				 	$input->level,  
				 	$input->is_aktif
				 ));
		return $this->success($result, "Berhasil");
	}

	public function edit(Request $request)
	{
		$input = (object) $request->input();
		$id = (int) $input->id;
		
		$input->is_aktif = json_decode($input->is_aktif);
		$input->is_aktif = ($input->is_aktif == true) ? '1':'0';

		if($input->parent_id == 0){
			$input->level = 1;
		}
		else{
			$parent = DB::select('CALL sp_get_mstlab_byid(?)', array($input->parent_id));
			$input->level = ($parent[0]->level)+1;
		}

		$result = DB::select('CALL sp_update_mstlab(?, ?, ?, ?, ?)',
				 array(
				 	$input->id, 
				 	$input->parent_id, 
				 	$input->nama, 
				 	$input->level,  
				 	$input->is_aktif
				 ));
		return $this->success($result, "Berhasil");
	}

	public function select2(Request $request)
	{
		$result = DB::connection('balkesmas')
        			->table('mst_lab')
        			->get();

		return $this->success($result, "Berhasil mengambil data");
	}

	public function archive($id)
	{
		$id = (int) $id;
		$detail = DB::select('CALL sp_get_mstlab_byid(?)', array($id));
		$input = $detail[0];

		$result = DB::select('CALL sp_update_mstlab(?, ?, ?, ?, ?)',
				 array(
				 	$input->id, 
				 	$input->parent_id, 
				 	$input->nama, 
				 	$input->level, 
				 	0,
				 ));

		return $this->success($result, "Berhasil");
	}

	public function activate($id)
	{
		$id = (int) $id;
		$detail = DB::select('CALL sp_get_mstlab_byid(?)', array($id));
		$input = $detail[0];

		$result = DB::select('CALL sp_update_mstlab(?, ?, ?, ?, ?)',
				 array(
				 	$input->id, 
				 	$input->parent_id, 
				 	$input->nama, 
				 	$input->level, 
				 	1,
				 ));

		return $this->success($result, "Berhasil");
	}

}