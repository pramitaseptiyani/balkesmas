<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\View; 

class UserController extends Controller
{
	public function getList($is_active)
	{
		$result = DB::select('CALL sp_get_user_aktif_nonaktif(?)', array($is_active));

		return $this->success($result, "Berhasil");
	}

	public function add(Request $request)
	{
		$password = $request->input('password');
		$hashed = Hash::make($password);

		$input = $request->input();
		$input['password'] = $hashed;
		$input['is_aktif'] = ($input['is_aktif'] == true) ? '1':'0';

		$result = DB::select('CALL sp_insert_user(?, ?, ?, ?, ? )', 
					array($input['nip'], $input['username'], $input['password'], $input['role_id'], $input['is_aktif']));

		return $this->success($result, "Berhasil");
	}

	public function detail($id)
	{
		$id = (int) $id;
		$result = DB::select('CALL sp_get_user(?)', array($id));
		return $this->success($result[0], "Berhasil");
	}

	public function edit(Request $request)
	{
		$input = (object) $request->input();
		$id = (int) $input->id;

		if($input->password != null){
			$password = $request->input('password');
			$hashed = Hash::make($password);

			$input->password = $hashed;
		}
		else{
			$user = DB::connection('balkesmas')
        			->table('users')
        			->where('id', $id)
        			->get('password');
			$input->password = $user[0]->password;
		}
		
		$input->is_aktif = json_decode($input->is_aktif);
		$input->is_aktif = ($input->is_aktif == true) ? '1':'0';
		
		$result = DB::select('CALL sp_update_user(?, ?, ?, ?, ?, ?, ?)',
				 array(
				 	$input->id, 
				 	$input->role_id,
				 	$input->nip,
				 	$input->username,
				 	$input->email,
				 	$input->password,
				 	$input->is_aktif,
				 ));
		return $this->success($result, "Berhasil");
	}

	public function archive($id)
	{
		$id = (int) $id;
		$detail = DB::select('CALL sp_get_user(?)', array($id));

		$user = DB::connection('balkesmas')
        			->table('users')
        			->where('id', $id)
        			->get();
		$input = $user[0];
		$input->role_id = $detail[0]->role_id;

		$result = DB::select('CALL sp_update_user(?, ?, ?, ?, ?, ?, ?)',
				 array(
				 	$input->id, 
				 	$input->role_id,
				 	$input->nip,
				 	$input->username,
				 	$input->email,
				 	$input->password,
				 	'0',
				 ));

		return $this->success($result, "Berhasil");
	}

	public function activate($id)
	{
		$id = (int) $id;
		$detail = DB::select('CALL sp_get_user(?)', array($id));

		$user = DB::connection('balkesmas')
        			->table('users')
        			->where('id', $id)
        			->get();
		$input = $user[0];
		$input->role_id = $detail[0]->role_id;

		$result = DB::select('CALL sp_update_user(?, ?, ?, ?, ?, ?, ?)',
				 array(
				 	$input->id, 
				 	$input->role_id,
				 	$input->nip,
				 	$input->username,
				 	$input->email,
				 	$input->password,
				 	'1',
				 ));

		return $this->success($result, "Berhasil");
	}
	

}