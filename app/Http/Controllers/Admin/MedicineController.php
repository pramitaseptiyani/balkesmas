<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View; 

class MedicineController extends Controller
{
	public function getList(Request $request, $is_active)
	{
		$input = $request->input();

		$orderColumn = $input['order'][0]['column'];
		$orderBy = $input['columns'][$orderColumn]['name'];
		$orderDir = $input['order'][0]['dir'];
		$search = $input['search']['value'];
		
		if($search == null) $search = "";
		$is_active = (int) $is_active;
		if($is_active== 1) $is_ready = 1;
		else $is_ready = 0;

		$records = 0;
		$result = DB::select('CALL sp_get_obat_table(?, ?, ?, ?, ?, ?)',
					array(
						$orderBy,
						$orderDir,
						$input['start'],
						$input['length'],
						$search,
						$is_ready
					)
				);
		if($result != []){
			$records = $result[0]->jumlah_row;
		}

		$datatableResult = (object)[
			"draw"				=> $input["draw"],
			"data"				=> $result,
			"recordsFiltered" 	=> $records,
			"recordsTotal" 		=> $records
		];

		return json_encode($datatableResult);
	}

	public function add(Request $request)
	{
		$input = $request->input();
		$input['is_aktif'] = ($input['is_aktif'] == true) ? '1':'0';

		$result = DB::select('CALL sp_insert_obat(?, ?, ?, ?, ? )', 
					array($input['nama'], $input['kuantitas'], $input['satuan'], $input['tgl_kadaluarsa'], $input['is_aktif']));

		return $this->success($result, "Berhasil");
	}

	public function detail($id)
	{
		$id = (int) $id;
		$result = DB::select('CALL sp_get_obat_byid(?)', array($id));
		return $this->success($result[0], "Berhasil");
	}

	public function edit(Request $request)
	{
		$input = (object) $request->input();
		$id = (int) $input->id;
		
		$input->is_aktif = json_decode($input->is_aktif);
		$input->is_aktif = ($input->is_aktif == true) ? '1':'0';

		$result = DB::select('CALL sp_update_obat(?, ?, ?, ?, ?, ?, ?)',
				 array(
				 	$input->id, 
				 	$input->nama, 
				 	$input->kuantitas, 
				 	$input->satuan, 
				 	$input->tgl_kadaluarsa, 
				 	$input->is_aktif,
				 	$input->komposisi,
				 ));
		return $this->success($result, "Berhasil");
	}

	public function archive($id)
	{
		$id = (int) $id;
		$detail = DB::select('CALL sp_get_obat_byid(?)', array($id));
		$input = $detail[0];

		$result = DB::select('CALL sp_update_obat(?, ?, ?, ?, ?, ?)',
				 array(
				 	$input->id, 
				 	$input->nama, 
				 	$input->kuantitas, 
				 	$input->satuan, 
				 	$input->tgl_kadaluarsa,
				 	0,
				 ));

		return $this->success($result, "Berhasil");
	}

	public function activate($id)
	{
		$id = (int) $id;
		$detail = DB::select('CALL sp_get_obat_byid(?)', array($id));
		$input = $detail[0];

		$result = DB::select('CALL sp_update_obat(?, ?, ?, ?, ?, ?)',
				 array(
				 	$input->id, 
				 	$input->nama, 
				 	$input->kuantitas, 
				 	$input->satuan, 
				 	$input->tgl_kadaluarsa,
				 	1,
				 ));

		return $this->success($result, "Berhasil");
	}

}