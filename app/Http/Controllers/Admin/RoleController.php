<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\View; 

class RoleController extends Controller
{

	public function select2(Request $request)
	{
		$role = $request->input('role');
		if($role == null) $role="";

		$result = DB::select('CALL sp_get_role(?)', array($role));

		return $this->success($result, "Berhasil mengambil data");
	}

	public function archive($id)
	{
		$id = (int) $id;
		$detail = DB::select('CALL sp_get_role_menu_byid(?)', array($id));

		$menus = "";
		for ($i=0; $i < count($detail); $i++) { 
			if($detail[$i]->menu_tipe == "submenu"){
				if($i == 0) $menus = $detail[$i]->menu_id;	
				else $menus = $menus.";".$detail[$i]->menu_id;
			}
		}

		$role = DB::connection('balkesmas')
        			->table('role')
        			->where('id', $id)
        			->get();
		$input = $role[0];

		$result = DB::select('CALL sp_update_role(?, ?, ?, ?)',
				 array(
				 	$input->id, 
				 	$input->role,
				 	$menus,
				 	0
				 ));

		return $this->success($result, "Berhasil");
	}

	public function activate($id)
	{
		$id = (int) $id;
		$detail = DB::select('CALL sp_get_role_menu_byid(?)', array($id));

		$menus = "";
		for ($i=0; $i < count($detail); $i++) { 
			if($detail[$i]->menu_tipe == "submenu"){
				if($i == 0) $menus = $detail[$i]->menu_id;	
				else $menus = $menus.";".$detail[$i]->menu_id;
			}
		}

		$role = DB::connection('balkesmas')
        			->table('role')
        			->where('id', $id)
        			->get();
		$input = $role[0];

		$result = DB::select('CALL sp_update_role(?, ?, ?, ?)',
				 array(
				 	$input->id, 
				 	$input->role,
				 	$menus,
				 	1
				 ));

		return $this->success($result, "Berhasil");
	}
}