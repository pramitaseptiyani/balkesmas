<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\Environment\Console;

/**
 * Class NurseController
 */
class AdminController extends Controller
{
   	public function user(){
        $data['name'] = Auth::user()->name;
        return view('admin/user', $data);
   	}

   	public function permission(){
        $data['name'] = Auth::user()->name;
        return view('admin/permission', $data);
   	}

   	public function medicine(){
        $data['name'] = Auth::user()->name;
        return view('admin/medicine', $data);
   	}

    public function employee(){
        $data['name'] = Auth::user()->name;
        return view('admin/employee', $data);
    }

    public function getlistempl(){
        $data['name'] = Auth::user()->name;
        return view('admin/family', $data);
    }

    public function listFamily($kdsatker, $nip)
    {
        // $data['kdsatker'] = $kdsatker;
        $data['nip'] = $nip;

        $result = DB::select('CALL sp_get_keluarga_pegawai(?, ?)',
            array($kdsatker, $nip));

        return $this->success($result, "Berhasil mengambil data");
    }

    public function labLv1(){
        $data['name'] = Auth::user()->name;
        return view('admin/lab', $data);
    }

    public function labLv2($id_parent){
        $data['name'] = Auth::user()->name;
        $data['id_parent'] = $id_parent;

        /*Get Parent name*/
        $getParent = DB::select('CALL sp_get_mstlab_byid(?)',
            array($id_parent));
        /**/
        $data['parent'] = $getParent[0]->nama;
        return view('admin/lab_level2', $data);
    }

    public function labLv3($id_parent1, $id_parent2){
        $data['name'] = Auth::user()->name;
        $data['id_parent'] = $id_parent1;
        $data['id_parent2'] = $id_parent2;

        /*Get Parent name*/
        $getParent1 = DB::select('CALL sp_get_mstlab_byid(?)',
            array($id_parent1));
        $getParent2 = DB::select('CALL sp_get_mstlab_byid(?)',
            array($id_parent2));
        /**/
        $data['parent1'] = $getParent1[0]->nama;
        $data['parent2'] = $getParent2[0]->nama;
        return view('admin/lab_level3', $data);
    }

    public function getListDokter($is_active)
	{
		$result = DB::select('CALL sp_get_dokter_aktif_nonaktif(?)', array($is_active));

		return $this->success($result, "Berhasil");
	}

    public function dokter(){
        $data['name'] = Auth::user()->name;
        return view('admin/dokter', $data);
   	}

    public function jadwal_dokter($id){
        $data['id'] = $id;
        $data['name'] = Auth::user()->name;
        return view('admin/jadwal_dokter', $data);
   	}

    public function getListJadwalDokter($id){
        $result = DB::select('CALL sp_get_list_jadwal_dokter(?)', array($id));

        return $this->success($result, "Berhasil");
    }

    public function getJadwalDokter($id){

        $result = DB::select('CALL sp_get_jadwal_dokter(?)', array($id));

        return $this->success($result[0], "Berhasil");
    }

    public function selectHari($id_dokter)
	{
		if($id_dokter == null) $id_dokter="";

		$result = DB::select('CALL sp_get_hari(?)', array($id_dokter));

		return $this->success($result, "Berhasil mengambil data");
	}

    public function add(Request $request)
	{
		$input = $request->input();

		$result = DB::select('CALL sp_insert_jadwal_dokter(?, ?, ?, ?)',
                            array(
                                    $input['id_user_dokter'],
                                    $input['id_hari'],
                                    $input['jam_dari'],
                                    $input['jam_sampai'],
                                    ));

		return $this->success($result, "Berhasil");
	}

    public function edit(Request $request)
	{
		$input = (object) $request->input();
		$id = (int) $input->id;

		$result = DB::select('CALL sp_update_jadwal_dokter(?, ?, ?)',
				 array(
                    $id,
                    $input->jam_dari,
                    $input->jam_sampai,
				 ));

		return $this->success($result, "Berhasil");
	}


    public function del_tr_jadwal_dokter($id)
    {
        $result = DB::select('CALL sp_del_tr_jadwal(?)', array($id));
        return $this->success($result, "Data Berhasil Dihapus");
    }


    /* Adding Family Member */

    public function selectkdkeluarga()
	{
		$result = DB::select('CALL sp_get_kdkeluarga()');

		return $this->success($result, "Berhasil mengambil data");
	}

    public function selectkdkerja()
	{
		$result = DB::select('CALL sp_get_kdkerja()');

		return $this->success($result, "Berhasil mengambil data");
	}

    public function addMember(Request $request)
    {
        $input = $request->input();

        if ($input["kddapat"]) {
            $v_kddapat = '1';
        } else {
            $v_kddapat = '2';
        }

        $result = DB::select('CALL sp_insert_family_member(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',

            array(
                $input["kdsatker"],
                $input["nama"],
                $input["tgllhr"],
                $input["nip"],
                $input["kdkeluarga"],
                $input["kdkerja"],
                $input["tglnikah"],
                $v_kddapat,
                $input["no_srt_nikah"],
                $input["tgl_srt_nikah"],
                $input["nmayah"],
                $input["nmibu"],
                $input["keterangan"],
                $input["alamat"]
            )
        );

        return $this->success($result, "Berhasil");
    }


    public function editMember(Request $request)
    {
        $input = $request->input();

        // if ($input["kddapat"]) {
        //     $v_kddapat = '1';
        // } else {
        //     $v_kddapat = '2';
        // }

        $result = DB::select('CALL sp_update_family_member(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',

            array(
                $input["id"],
                $input["kdsatker"],
                $input["nama"],
                $input["tgllhr"],
                $input["nip"],
                $input["kdkeluarga"],
                $input["kdkerja"],
                $input["tglnikah"],
                $input["kddapat"],
                // $v_kddapat,
                $input["no_srt_nikah"],
                $input["tgl_srt_nikah"],
                $input["nmayah"],
                $input["nmibu"],
                $input["keterangan"],
                $input["alamat"]
            )
        );

        return $this->success($result, "Berhasil");
    }

    public function getFamilyMember($id){

        $result = DB::select('CALL sp_get_family_member(?)', array($id));

        return $this->success($result[0], "Berhasil");
    }


    public function removeMember($id)
    {
        $result = DB::select('CALL sp_del_family_member(?)', array($id));
        return $this->success($result, "Data Berhasil Dihapus");
    }

}
