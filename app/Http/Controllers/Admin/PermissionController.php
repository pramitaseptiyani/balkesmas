<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\View; 

class PermissionController extends Controller
{
	public function getList($is_active)
	{
		$result = DB::select('CALL sp_get_role_menu_aktif(?)', array($is_active));

		$menus = [];
		if($result != []){
			$roleNow = $result[0]->role;
			$menu = [];
			foreach ($result as $key => $each) {
				if($roleNow == $each->role){
					if($each->menu_tipe == "submenu"){
						$menu[] = $each->menu_nama;	
					}
					
				}
				else{

					$eachMenu = (object)[
						"id"	=> $result[$key-1]->id,
						"role"	=> $result[$key-1]->role,
						"menu"	=> $menu,
						"updated_at" => $result[$key-1]->updated_at
					];

					$menus[] = $eachMenu;

					$each->menu = $menu;
					$roleNow = $each->role;
					$menu = [$each->menu_nama];
				}
			}

			if($key != 0){
				$eachMenu = (object)[
					"id"	=> $result[$key-1]->id,
					"role"	=> $result[$key-1]->role,
					"menu"	=> $menu,
					"updated_at" => $result[$key-1]->updated_at
				];

				$menus[] = $eachMenu;	
			}
			
		}

		return $this->success($menus, "Berhasil");
	}

	public function add(Request $request)
	{
		$input = $request->input();
		$input['is_aktif'] = ($input['is_aktif'] == true) ? '1':'0';
		$input["menu"] = implode(";", $input['menu']);

		$result = DB::select('CALL sp_insert_role(?, ?, ? )', 
					array($input['role'], $input['menu'], $input['is_aktif']));

		return $this->success($result, "Berhasil");
	}

	public function detail($id)
	{
		$id = (int) $id;
		$result = DB::select('CALL sp_get_role_menu_byid(?)', array($id));

		$menu_ids = [];
		foreach ($result as $key => $menu) {
			if($menu->menu_tipe == "submenu"){
				$menu_ids[] = (string) $menu->menu_id;
				$menus[] = (string) $menu->menu_nama;	
			}
		}
		$result2 = $result[0];
		$result2->menu_ids = $menu_ids;
		$result2->menus = $menus;

		return $this->success($result2, "Berhasil");
	}

	public function edit(Request $request)
	{
		$input = (object) $request->input();
		$input->is_aktif = ($input->is_aktif == true) ? '1':'0';
		$input->menus = implode(";", $input->menus);

		$result = DB::select('CALL sp_update_role(?, ?, ?, ?)',
				 array(
				 	$input->id_role, 
				 	$input->role,
				 	$input->menus,
				 	$input->is_aktif
				 ));

		return $this->success($result, "Berhasil");
	}

	public function menuSelect2(Request $request)
	{
		$result = DB::select('CALL sp_get_menu');

		$menus = [];
		if($result != []){
			$submenu = [];
			foreach ($result as $key => $each) {
				if($each->parent_id == 0){
					$submenu = [];
					$eachMenu = (object)[
						"id"	=> $each->id,
						"name"	=> $each->name,
						"submenu" => [],
						"type"	=> "menu"
					];

					$menus[] = $eachMenu;
				}
				else{
					$submenu[] = (object)[
						"id"	=> $each->id,
						"name"	=> $each->name
					];
					$eachMenu->submenu = $submenu;
				}
			}			
		}

		// $result = [
		// 	(object)[
		// 		"id"	=> 1,
		// 		"name"	=> "PASIEN",
		// 		"type"	=> "menu",
		// 		"submenu"	=> [
		// 			(object)[
		// 				"id"	=> 3,
		// 				"name"	=> "Daftar Pasien"
		// 			],
		// 			(object)[
		// 				"id"	=> 4,
		// 				"name"	=> "Tambah Pasien"
		// 			]
		// 		]
		// 	],
		// 	(object)[
		// 		"id"	=> 10,
		// 		"name"	=> "DOKTOR",
		// 		"type"	=> "menu",
		// 		"submenu"	=> [
		// 			(object)[
		// 				"id"	=> 11,
		// 				"name"	=> "List Pasien"
		// 			]
		// 		]
		// 	]
		// ];

		return $this->success($menus, "Berhasil mengambil data");
	}

}