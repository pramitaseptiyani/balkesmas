<?php

namespace App\Http\Controllers\Reservasi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\View; 
use App\Models\Dokterpoli;
use App\Models\Masterpoli;
use App\Models\Pasien;
use App\Models\Reservasi;
use App\Models\Tr_jadwal_dokter;
use App\Models\Tr_antrian_poli;
use App\Models\Tr_reservasi;
use App\Models\Tr_periksa;

class ReservasiController extends Controller
{
	public function poli_list(Request $request){
		$search = $request->input('search');
            if (!empty($search)) {
            	$result = Masterpoli::where('nama','like','%'.$search.'%')
            			->where('jenis', 'dkt')
                        ->orderBy('nama', 'asc')
            			->get();
            }
            else $result = Masterpoli::where('jenis', 'dkt')->orderBy('nama', 'asc')->get();

		return $this->success($result, "Berhasil mengambil data");;
	}

	public function dokterpoli_list(Request $request){
		$search = $request->input('search');
		$poli_id = $request->input('poli_id');
            if (!empty($search)) {
            	$result = Dokterpoli::with('user')
            			->where('id_poli', $poli_id) 
            			->get();
            }
            else {
            	$result = Dokterpoli::with('user')
            					->where('id_poli', $poli_id)
            					->get();
            }
        
		return $this->success($result, "Berhasil mengambil data");;
	}

    public function jumlah(Request $request)
    {
        $input = $request->input();
        
        $result = DB::select('CALL sp_get_jumlah_reservasi(?, ?)', 
            array(
                $input["tanggal"],
                $input["userid_dokter"],
            )
        );

        return $this->success($result, "Berhasil");
    }

    public function getpatientByDate(Request $request)
    {
        $input = $request->input();
        
        $result = DB::select('CALL sp_get_jumlah_reservasi(?, ?)', 
            array(
                $input["tanggal"],
                $input["userid_dokter"],
            )
        );

        return $this->success($result, "Berhasil");
    }

    public function getDoctorSchedule(Request $request)
    {
        $input = $request->input();
        $id_poli = $input["id_poli"];

        $dokter_poli = Dokterpoli::where("id_poli", $id_poli)
                        ->get()
                        ->pluck("userid_dokter");

        $result = Tr_jadwal_dokter::with('hasHari')
                                ->with('hasUser')
                                ->whereIn("id_user_dokter", $dokter_poli)
                                ->orderBy('id_hari', 'asc')
                                ->get();
        
        // $result = DB::select('CALL sp_get_jadwal_dokter_by_poli(?, ?)', 
        //     array(
        //         $input["id_poli"],
        //         7
        //     )
        // );

        return $this->success($result, "Berhasil");
    }

    // public function getDoctorByDate(Request $request)
    // {
    //     $input = $request->input();
        
    //     $result = DB::select('CALL sp_get_jadwal_dokter_by_poli_tanggal(?, ?)', 
    //         array(
    //             $input["id_poli"],
    //             $input["tanggal"]
    //         )
    //     );

    //     return $this->success($result, "Berhasil");
    // }

    public function getpatientByMR(Request $request)
    {
        $search = $request->input('no_mr');
        
        $result = Pasien::where('patient_mr_number', $search) 
                        ->first();
        if($result != null){
            return $this->success($result, "Berhasil menemukan data");    
        }
        else return $this->error($result, "Anda belum mempunyai MR. Harap isi data Anda", 400, "Data tidak Ditemukan");   
    }

    public function getpatientByPoli(Request $request)
    {
        $input = $request->input();
        
        // $result = DB::select('CALL sp_get_reservasi_byparam(?, ?, ?)', 
        //     array(
        //         $input['tanggal'],
        //         $input['userid_dokter'],
        //         $input['id_poli']
        //     )
        // );

        $result = Reservasi::where('id_poli', $input['id_poli'])
                        ->where('tanggal', $input['tanggal']) 
                        ->with('hasPasien')
                        ->with('hasPoli')
                        ->get();

        return $this->success($result, "Berhasil menemukan data");      
    }

    public function availabilityByDate(Request $request)
    {
        $dates = $request->input('dates');

        /*Poli Gigi*/
        $result = [];
        foreach ($dates as $key => $tanggal) {
            $count = Reservasi::where('id_poli', 2)
                            ->where('tanggal', $tanggal)
                            ->get()->count();
            if($count < 5){
                $result[] = $tanggal;
            }
        }

        return $this->success($result, "Berhasil mengambil data");
    }

    public function insertData(Request $request)
    {
        $input = $request->input();
        
        $result = DB::select('CALL sp_insert_reservasi(?, ?, ?, ?, ?, ?, ?, ?, ?)', 
            array(
                $input["tanggal"], //tanggal
                // $input["userid_dokter"], //userid_dokter
                null, //userid_dokter
                $input["id_patient"], //id_pasien
                $input["nama"], //nama pasien
                $input["no_hp"], //no hp
                $input["id_card"], //id_card
                $input["id_kategori"], //id_kategori
                $input["satkerid"], //satkerid
                $input["id_poli"], //id_poli
            )
        );

        if($input["nama"] == null){
            return $this->success($result, "Berhasil membuat reservasi");
        }
        else return $this->success($result, "No MR Anda adalah ". $result[0]->gen_mr);
    }

    public function getList()
    {
        $tgl_periksa = date('Y-m-d');
        $result = DB::select('CALL sp_get_reservasi_aktif()');
        return $this->success($result, "Berhasil mengambil data");
    }

    public function periksaIsExist(Request $request)
    {   
        $id_pasien = $request->input('id_pasien');
        $id_poli = $request->input('list_poli');
        $tanggal = $request->input('tanggal');

        $result = Tr_reservasi::where('id_pasien', $id_pasien)
                    ->where('tanggal', 'like', $tanggal.'%' )
                    ->orderBy('tanggal', 'desc')
                    ->first();

        if($result != null){
            $poli = Masterpoli::where('id', $result->id_poli)
                    ->first();

            return $this->error($result, "Pasien telah melakukan reservasi di ".$poli->nama);
        }    
        else{
            $periksa = Tr_periksa::where('id_pasien', $id_pasien)
                    ->where('tgl_periksa', 'like', $tanggal.'%' )
                    ->orderBy('tgl_periksa', 'desc')
                    ->first();

            if($result != null){
                $id_periksa = $result->id;

                $antrian = Tr_antrian_poli::where('id_periksa', $id_periksa)
                        ->where('is_done', 0)
                        ->orderBy('id', 'desc')
                        ->first();

                if($antrian != null){
                    $poli = Masterpoli::where('id', $antrian->id_poli)
                        ->first();

                    return $this->error($result, "Pasien telah terdaftar di ".$poli->nama.". Selesaikan terlebih dahulu");
                }
                else{
                    return $this->success(null, "Registrasi Poli Baru");
                }            
            }
            return $this->success(null, "Registrasi Poli Baru");
        }        
    }

}