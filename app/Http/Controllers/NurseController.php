<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

/**
 * Class NurseController
 */
class NurseController extends Controller
{
    /**
     * View to show all patients datatable
     */
    public function patient(){
        $data['name'] = Auth::user()->name;
        return view('process/nurse_queue', $data);
    }

    /**
     * View to show all patients datatable
     */
    // public function queue(){
    //     $data['name'] = Auth::user()->name;
    //     return view('process/nurse_queue', $data);
    // }

    /**
     * View to show all patients datatable
     */
    public function examination(Request $request){
        $data['patient_nm'] = $request->input('patient_nm');
        $data['no_urut_perawat'] = $request->input('no_urut_perawat');
        $data['id_periksa'] = $request->input('id_periksa');
        $data['name'] = Auth::user()->name;
        $data['id_user'] = Auth::user()->id;
      	return view('examination/nurse', $data);
    }

    /**
     * API to show all patients datatable
     */
    public function getList()
    {
        $tgl_periksa = date('Y-m-d');
        $result = DB::select('CALL sp_get_perawat(?)', array($tgl_periksa));
        return $this->success($result, "Berhasil mengambil data");
    }

    /**
     * API to insert data
     */
    public function insertExamination(Request $request)
    {
        $input = array(
            (int) $request->input('id_periksa'),
            (int) $request->input('bb'),
            (string) $request->input('tensimeter'),
            (int) $request->input('id_user_perawat'),
            (int) $request->input('tinggi_badan'),
            (string) $request->input('nadi'),
            (int) $request->input('suhu'),
            (string) $request->input('rr'),
            (string) $request->input('keluhan'),
        );
        $insert = DB::select('CALL sp_insert_perawat(?, ?, ?, ?, ?, ?, ?, ?, ?)', $input);

        return $this->success($insert, "Berhasil menyimpan data");
    }

}
