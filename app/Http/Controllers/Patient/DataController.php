<?php

namespace App\Http\Controllers\Patient;

use Illuminate\Http\Request;
use App\Models\Pegawai;
use App\Models\Pasien;

/**
 * Class ReceptionistController
 */
class DataController extends Controllers
{
    /**
     * add patients datatable
     */
    public function add(Request $request){

        $pasien = new Pasien();
      	$pasien->patient_nm = $request->input('nama');
        $pasien->patient_nip = $request->input('nip');
        $pasien->patient_ttl = $request->input('ttl');
        $pasien->patient_unit = $request->input('unit');
        $pasien->bpjs = $request->input('bpjs');
        $pasien->patient_address = $request->input('alamat');
        $pasien->patient_phone = $request->input('nohp');
        $pasien->patient_mail = $request->input('email');
        $pasien->diag_diderita_old = $request->input('riwayat_penyakit');
        $pasien->diag_turunan = $request->input('penyakit_turunan');
        $pasien->alergi_rwy = $request->input('riwayat_alergi');
        $pasien->vaksinasi_rwy = $request->input('riwayat_vaksinasi');
        $pasien->save();

        return json_encode($pasien);

    }

}
