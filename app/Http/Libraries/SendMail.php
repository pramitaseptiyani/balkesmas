<?php

namespace App\Http\Libraries;

use Mail;

class SendMail
{
	public function membercard($pasien, $output)
	{
		Mail::send('email.membercard', [], function ($message) use ($pasien, $output) {

            $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));

            $message->to($pasien->patient_mail);
            $message->subject('This is your membercard');

            /* with attachment */
            $message->attach($output);
        });
	}

}