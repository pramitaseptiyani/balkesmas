<?php

namespace App\Http\Libraries;

use Knp\Snappy\Pdf;

class Html2Pdf
{

    public function test()
    {
        return "nana";
    }

    /**
     * generate from a url
     *
     * @param string $url
     * @return string
     */
    public function getFromURL($url){
        // $snappy = new Pdf('C://"Program Files"/wkhtmltopdf/bin/wkhtmltopdf');
        // $snappy = new Pdf('/home/wkhtmltopdf');
        $snappy = new Pdf('/usr/bin/xvfb-run /usr/bin/wkhtmltopdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="file.pdf"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        echo $snappy->getOutput($url);
    }

    /**
     *generate from multiple url
     *
     * @param array $urls
     * @return array
     */

    public function getFromURLs(array $urls){
        // $snappy = new Pdf('C://"Program Files"/wkhtmltopdf/bin/wkhtmltopdf');
        // $snappy = new Pdf('/home/wkhtmltopdf');
        $snappy = new Pdf('/usr/bin/xvfb-run /usr/bin/wkhtmltopdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="files.pdf"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        echo $snappy->getOutput($urls);
    }

    /**
     * generate from HTML
     *
     * @param string $html
     * @param string $path_to_save
     * @param array $option
     * @param boolean $overwrite
     * @return string
     */
    public function getFromHTML($html, $path_to_save, $option = [], $overwrite = true){
        // $snappy = new Pdf('C://"Program Files"/wkhtmltopdf/bin/wkhtmltopdf');
        // $snappy = new Pdf('/home/wkhtmltopdf');
        $snappy = new Pdf('/usr/bin/xvfb-run /usr/bin/wkhtmltopdf');
        $snappy->setTimeout(300);
        $snappy->generateFromHtml($html, $path_to_save, $option, $overwrite);
    }

}
