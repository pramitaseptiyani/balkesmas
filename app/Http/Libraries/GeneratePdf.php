<?php

namespace App\Http\Libraries;

use App\Http\Libraries\Html2Pdf;

class GeneratePdf
{
	public function generateMembercard($input, $output)
	{

        $content = file_get_contents(resource_path('views/template/membercard/index.blade.php'));

        $content = str_replace('Shaka Nagano', $input->patient_nm, $content);
        $content = str_replace('MR_number', $input->patient_mr_number, $content);
        // $content = str_replace('Sekretariat Jenderal', $input->patient_unit, $content);
        $content = str_replace('Alamat_user', $input->patient_address, $content);

        $pageOptions = array(
                        'orientation'      => 'Landscape',
                        'page-size'        => 'A4'
                      );


        $html2pdf = new Html2Pdf();
        $html2pdf->getFromHTML($content, $output);
	}
	
}