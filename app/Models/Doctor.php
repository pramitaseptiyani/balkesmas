<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $connection = 'balkesmas';
    protected $table = 'Dokter';

    protected $fillable = [
        'patient_mr_number', 'patient_nm',
        'patient_nip', 'bpjs', 'patient_ttl',
        'patient_unit', 'patient_address',
        'patient_phone', 'patient_mail',
        'diag_derita_old', 'diag_turunan',
        'alergi_rwy', 'vaksinasi_rwy'
    ];

    public function hasKategori()
    {
        return $this->hasOne('App\Models\Kategori','id_kategori','id_kategori');
    }


    public function unitCode($request)
    {
    	switch ($request->input('unit')) {
            case 'SEKRETARIAT JENDERAL':
                $unit = 'SET';
                break;
            case 'DIREKTORAT JENDERAL ADMINISTRASI HUKUM UMUM':
                $unit = 'AHU';
                break;
            case 'INSPEKTORAT JENDERAL':
                $unit = 'ITJ';
                break;
            case 'DIREKTORAT JENDERAL PERATURAN PERUNDANG UNDANGAN':
                $unit = 'PP';
                break;
            case 'UMUM':
                $unit = 'UMU';
                break;
            default:
                $unit = 'UMU';
                break;
        }

        return $unit;
    }

    public function patientCode($request)
    {
    	$tipe = $request->input('tipe');

    	switch (strtolower($tipe)) {
            case 'umum':
                $patientType = '0';
                break;
            case 'pegawai':
                $patientType = '1';
                break;
            case 'pasangan':
                $patientType = '2';
                break;
            case 'anak1':
                $patientType = '3';
                break;
            case 'anak2':
                $patientType = '4';
                break;
            case 'anak3':
                $patientType = '5';
                break;
            default:
                $patientType = '1';
                break;
        }
        return $patientType;
    }

    public function getLastSequence()
    {
    	$last = $this::orderBy('id', 'desc')
    			->first();

        if($last == null){
            $lastSequence = '0000';
        }
        else{
            $MRNumber = $last->patient_mr_number;
            $lastSequence = substr($MRNumber, 4, 6);    
        }

    	return $lastSequence;
    }


}
