<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resep extends Model
{
    protected $connection = 'balkesmas';
    protected $table = 'tr_resep';

    public function getstatusresep($id)
    {
        $kategori = Resep::select('id_status')
            ->where('id_tr_poli', $id)
            ->first();
        return $kategori->id_status;
    }
}
