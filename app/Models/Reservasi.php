<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservasi extends Model
{
    protected $connection = 'balkesmas';
    protected $table = 'tr_reservasi';

    public function hasPasien()
    {
        return $this->hasOne('App\Models\Pasien', 'id','id_pasien');
    }

    public function hasPoli()
    {
        return $this->hasOne('App\Models\Pasien', 'id','id_poli');
    }
}
