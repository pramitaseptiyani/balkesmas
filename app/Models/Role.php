<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
	use SoftDeletes;

    protected $connection = 'balkesmas';
    protected $table = 'role';

    protected $dates =['deleted_at'];

    public function hasPermission()
    {
        return $this->hasMany('App\Models\Permission', 'role_id','id');
    }
}
