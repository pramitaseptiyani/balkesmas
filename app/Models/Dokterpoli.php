<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dokterpoli extends Model
{
    protected $connection = 'balkesmas';
    protected $table = 'dokter_poli';

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id','userid_dokter');
    }
}
