<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Masterpoli extends Model
{
    protected $connection = 'balkesmas';
    protected $table = 'mst_poli';
}
