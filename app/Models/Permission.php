<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\Kementerianlembaga;

class Permission extends Model
{
    use SoftDeletes;

    protected $connection = 'balkesmas';
    protected $table = 'permission';

    public function hasRole()
    {
        return $this->hasOne('App\Models\Role', 'id','role_id');
    }

    public function hasMenu()
    {
        return $this->hasOne('App\Models\Menu', 'id','menu_id');
    }

    public function getMenu($user_id)
    {
        $result = Userrole::where('user_id', $user_id)
                    ->with(['hasRole.hasPermission.hasMenu'
                        => function($menu) {
                        $menu->select('*');
                        $menu->orderBy('parent_id', 'asc');
                        // $menu->orderBy('order_id', 'asc');
                    }
                    ])
                    ->get();
        
        $roles = $result->pluck('role_id');
        $permission= Permission::whereIn('role_id',$roles)
                ->with('hasMenu')
                ->select('menu_id')
                ->groupBy('menu_id')
        ->get();

        if(!empty($permission)){
            $allPermissions = [];
            foreach ($permission as $key => $value) {
                $allPermissions[] = $value->hasMenu;
            }    

            usort($allPermissions, array($this, "compare"));
        }

        return $allPermissions;
    }

    function compare($a, $b)
    {
        // return strcmp($a->parent_id, $b->parent_id);
        return strcmp($a->order_id, $b->order_id);
    }


}