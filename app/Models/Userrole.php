<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Userrole extends Model
{
	use SoftDeletes;

    protected $connection = 'balkesmas';
    protected $table = 'user_role';

    protected $dates =['deleted_at'];

    public function users()
    {
        return $this->hasOne('App\Models\Users','id','user_id');
    }

    public function hasRole()
    {
        return $this->hasOne('App\Models\Role','id','role_id');
    }
}
