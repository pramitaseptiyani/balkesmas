<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\Kementerianlembaga;

class Menu extends Model
{
	use SoftDeletes;

    protected $connection = 'balkesmas';
    protected $table = 'menu';

}