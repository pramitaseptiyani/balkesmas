<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $connection = 'balkesmas';
    protected $table = 'kategori';

    public function getId($kategori_nm)
    {
    	$kategori = Kategori::select('id_kategori')
                ->where('kategori_nm', $kategori_nm)
                ->first();
        return $kategori->id_kategori;
    }
}
