<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tr_jadwal_dokter extends Model
{
    protected $connection = 'balkesmas';
    protected $table = 'tr_jadwal_dokter';

    public function hasHari()
    {
        return $this->hasOne('App\Models\Mst_hari', 'id','id_hari');
    }

    public function hasUser()
    {
        return $this->hasOne('App\Models\User', 'id', 'id_user_dokter');
    }

}