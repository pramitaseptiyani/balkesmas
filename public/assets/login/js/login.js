$( document ).ready(function() {
    "use strict";

    /*==================================================================
    [ Validate ] */
    var input = $('.validate-input .input100');

    /*By Clicking Button*/
    $(".login100-form-btn").on("click", function(e){
        validateLogin(input);
    });

    /*By Pressing Enter*/
    $(".login100-form input").keypress(function (e) {
        if (e.keyCode == 13) {
            validateLogin(input);
        }
     });
    

    /*==================================================================
    [ Focus Contact2 ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

});

function validateLogin(input){
    var check = true;
    for(var i=0; i<input.length; i++) {
        if(validate(input[i]) == false){
            showValidate(input[i]);
            check=false;
        }
    }
    console.log(check);
    if(check == true) sendLogin();           
}


function sendLogin(){
    var formData = {
            'username': $('#username').val(),
            'password': $('#password').val()
        };
    
    /*send to API*/
    // send(APIurl+"login", formData, false, function(response){

    // })
}  

function validate (input) {
    if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
        if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
            return false;
        }
    }
    else {
        if($(input).val().trim() == ''){
            return false;
        }
    }
}

function showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass('alert-validate');
}

function hideValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).removeClass('alert-validate');
} 