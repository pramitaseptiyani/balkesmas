$(document).ready(function() {
    /* Add role*/
    $('.add-employee').click(function(){
        $('#employeeAddModal').modal('show');
    });

    var table = $('#patientDatatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'receptionist/patient/list',
        },
        columns: [        
            { data: 'patient_mr_number', name: 'patient_mr_number' },
            { data: 'id_card', name: 'id_card' },
            { data: 'patient_nm', name: 'patient_nm' },
            { data: 'kategori_nm', name: 'kategori_nm' },
            { data: 'patient_address', name: 'patient_address' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var editButton = ''
                    if(row.id_kategori == 1)
                        var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';

                    return editButton;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 5, "desc" ]]
    });

    $('#patientDatatable').on( 'click', '.menu-edit-button', function () {
        $('#patientEditModal').modal('show');

        var id = $(this).data('id');
        get(base_url+"/receptionist/patient/get/"+id, false, function(response){
            responsedata = response.data;

            $('#patientEditModal #edit-id').val(id);
            $('#patientEditModal #edit-id-kategori').val(responsedata.id_kategori);
            $('#patientEditModal #edit-satker-id').val(responsedata.id_satker);

            fillEditResponse(responsedata)

        })   
    });


    $("#patientEditModal #submit-edit").on("click", function(){
        var id = $(this).data('id');

        console.log("dasda")

        var id_kategori = $('#patientEditModal #edit-id-kategori').val();

        var edit_nama = $('#patientEditModal #edit-nama').val();
        var edit_ttl = $('#patientEditModal #edit-ttl').val();
        var edit_hp = $('#patientEditModal #edit-hp').val();
        var edit_email = $('#patientEditModal #edit-email').val();
        var edit_address = $('#patientEditModal #edit-address').val();
        var edit_satker_id = $('#patientEditModal #edit-satker-id').val();


        if(id_kategori == 1){
            edit_nama = edit_ttl = edit_hp  = null;
            edit_email = edit_address = edit_satker_id = null;
        }


        var gender = $("input[name='optionGender']:checked").val();

        var sendData = {
            "_token"        : $('#patientEditModal #edit-token').val(),
            "input"         : [
                $('#patientEditModal #edit-id').val(), //id
                edit_nama, //nama
                $('#patientEditModal #edit-nip').val(), //id_card
                edit_ttl, //ttl
                edit_hp, // phone
                edit_email, //email
                $('#patientEditModal #edit-bpjs').val(), //bpjs
                edit_address,
                $('#patientEditModal #edit-riwayat-penyakit').val(),
                $('#patientEditModal #edit-penyakit-turunan').val(),
                $('#patientEditModal #edit-riwayat-alergi').val(),
                $('#patientEditModal #edit-riwayat-vaksinasi').val(),
                $('#patientEditModal #edit-MR').val(), //mr
                edit_satker_id, //satker_id
                gender   
            ]
        }

        // console.log(sendData)
        edit(sendData);
    });

    $('#select2-satker-add').select2({
        theme: "classic",
        dropdownParent: $("#employeeAddModal"),
          ajax: {
            dataType: 'json',
            url: base_path+'/admin/api/satker/select2',
            delay: 800,
            data: function(params) {
                return {
                  search: params.term,
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result.data, function(index,item){
                  list.push({
                    id: item.satkerid,
                    text: item.satker,
                  })
                });
                return {
                  results: list
                };
            },
        }
    });
    /* End of select satker */

    /* Add Employee Patient */
    $("#employeeAddModal #submit-add").on("click", function(){
        var sendData = {
            "_token"        : $('meta[name="csrf_token"]').attr('content'),
            "id_card"       : $('#employeeAddModal #add-nip').val(),
            "nama"          : $('#employeeAddModal #add-nama').val(),
            "satker"        : $('#employeeAddModal #select2-satker-add').val(),
            "email"         : $('#employeeAddModal #add-email').val(),
            "hp"            : $('#employeeAddModal #add-hp').val(),
            "alamat"        : $('#employeeAddModal #add-alamat').val(),
            "is_aktif"      : $('#employeeAddModal #add-aktif').is(':checked'),
            "jenis_kelamin" : $('input[type=radio][name=optionGender]:checked').val(),
        }
        add(sendData);
    });
    /* End of Add Employee Patient */


/* ========================================================= */
/* ========================================================= */

    /* Active Permission Datatable */
    // var table = $('#activeEmployeeDatatable').DataTable({
            
    //     ajax: {
    //         type: "GET",
    //         dataType: "json",
    //         url: APIurl+'/admin/api/employee/list/1',
    //     },
    //     columns: [        
    //         { data: 'nip', name: 'nip' },
    //         { data: 'nama', name: 'nama' },
    //         { data: 'unit', name: 'unit' },
    //         { data: 'tgl_lahir', name: 'tgl_lahir' },
    //         { data: 'updated_at', name: 'updated_at' },
    //         { data: 'id', name: 'id', className: 'text-center', 
    //             render: function(data, type, row, meta){
    //                 var seeButton = '<button type="button" title="Lihat" id="seeButton" data-id="'+data+'" class="label label-primary menu-see-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>';
    //                 var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
    //                 var archiveButton = '<button type="button" title="Hapus" id="editButton" data-id="'+data+'" class="label label-danger menu-archive-button style="margin-right:1vw;">&nbsp;<i class="fa fa-times"></i></button>';
    //                 button = seeButton+'&nbsp'+editButton+'&nbsp'+archiveButton;

    //                 return button;
    //             },
    //             searchable: false,
    //             sortable: false
    //         }
    //     ],
    //     order: [[ 0, "asc" ]]
    // });

    // var sendData = {
    //     _token : $('meta[name="csrf_token"]').attr('content')
    // }

    // var table = $('#activeEmployeeDatatable').DataTable({
    //     scrollX: true,
    //     autoWidth: false,
    //     processing: true,
    //     serverSide: true,
    //     bDestroy: true,
    //     ajax: {
    //         type: "POST",
    //         data: sendData,
    //         dataType: "json",
    //         url: APIurl+'/admin/api/employee/list/1',
    //     },
    //     columns: [        
    //         { data: 'nipbaru', name: 'nipbaru' },
    //         { data: 'nama', name: 'nama' },
    //         { data: 'satker', name: 'satker' },
    //         { data: 'tgllahir', name: 'tgllahir',
    //             render: function(data, type, row, meta){
    //                 if(data != null) 
    //                     var tgl_lahir = data.substr(0, 10);
    //                 else tgl_lahir = ''
    //                 return tgl_lahir;
    //             },
    //             searchable: true,
    //             sortable: true
    //         },
    //         // { data: 'updated_at', name: 'updated_at' },
    //         { data: 'nipbaru', name: 'nipbaru', className: 'text-center', 
    //             render: function(data, type, row, meta){
    //                 var seeButton = '<button type="button" title="Lihat" id="seeButton" data-id="'+data+'" class="label label-primary menu-see-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>';
    //                 var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
    //                 var archiveButton = '<button type="button" title="Hapus" id="editButton" data-id="'+data+'" class="label label-danger menu-archive-button style="margin-right:1vw;">&nbsp;<i class="fa fa-times"></i></button>';
    //                 button = seeButton+'&nbsp'+editButton+'&nbsp'+archiveButton;

    //                 return button;
    //             },
    //             searchable: false,
    //             sortable: false
    //         }
    //     ],
    //     order: [[ 1, "asc" ]]
    // });

    // $('#activeEmployeeDatatable').on( 'click', '.menu-see-button', function () {
    //     $('#employeeSeeModal').modal('show');

    //     var id = $(this).data('id');
    //     get(base_url+"/admin/api/employee/get/"+id, false, function(response){
    //         responsedata = response.data;
    //         console.log(responsedata)

    //         $('#employeeSeeModal #see-id').val(id);
    //         $('#employeeSeeModal #see-nip').val(responsedata.nipbaru);
    //         $('#employeeSeeModal #see-nama').val(responsedata.nama);
    //         $('#employeeSeeModal #see-tgllahir').val(responsedata.tgllahir);
    //         $('#employeeSeeModal #see-unit').val(responsedata.satker);
    //         $('#employeeSeeModal #see-email').val(responsedata.email);
    //         $('#employeeSeeModal #see-hp').val(responsedata.hp);
    //         $('#employeeSeeModal #see-alamat').val(responsedata.alamat);
    //         $('#edit-aktif').prop('checked', true).change();

    //         if(responsedata.jeniskelamin == 'P'){
    //             $('#optionGenderP').prop('checked',true);
    //             $('#optionGenderL').prop('checked',false);
    //         }
    //         else{
    //             $('#optionGenderP').prop('checked',false);
    //             $('#optionGenderL').prop('checked',true);
    //         }
    //         $('#multiple-menu-edit').val(["2", "3"]);
    //     })  
    // })

    // /*Click Edit on Active Permission Datatable */
    // $('#activeEmployeeDatatable').on( 'click', '.menu-edit-button', function () {
    //     $('#employeeEditModal').modal('show');

    //     var id = $(this).data('id');
    //     get(base_url+"/employee/patient/get/"+id, false, function(response){
    //         responsedata = response.data;
    //         console.log(responsedata)

    //         $('#roleEditModal #edit-id').val(id);
    //         $('#roleEditModal #edit-role').val('Perawat');
    //         $('#edit-aktif').prop('checked', true).change();
    //         $('#multiple-menu-edit').val(["2", "3"]);
    //     })   
    // });

/* ========================================================= */
/* ========================================================= */

    /* Archive Permission Datatable */
    // var table = $('#archiveEmployeeDatatable').DataTable({
    //     scrollX: true,
    //     autoWidth: false,
    //     processing: true,
    //     serverSide: true,
    //     bDestroy: true,
    //     ajax: {
    //         type: "POST",
    //         data: sendData,
    //         dataType: "json",
    //         url: APIurl+'/admin/api/employee/list/0',
    //     },
    //     columns: [        
    //         { data: 'nipbaru', name: 'nipbaru' },
    //         { data: 'nama', name: 'nama' },
    //         { data: 'satker', name: 'satker' },
    //         { data: 'tgllahir', name: 'tgllahir' },
    //         // { data: 'updated_at', name: 'updated_at' },
    //         { data: 'nipbaru', name: 'nipbaru', className: 'text-center', 
    //             render: function(data, type, row, meta){
    //                 var seeButton = '<button type="button" title="Lihat" id="seeButton" data-id="'+data+'" class="label label-primary menu-see-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>';
    //                 var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
    //                 var activateButton = '<button type="button" title="Aktifkan" id="activateButton" data-id="'+data+'" class="label label-success menu-activate-button style="margin-right:1vw;">&nbsp;<i class="fa fa-check"></i></button>';
    //                 button = seeButton+'&nbsp'+editButton+'&nbsp'+activateButton;

    //                 return button;
    //             },
    //             searchable: false,
    //             sortable: false
    //         }
    //     ],
    //     order: [[ 1, "asc" ]]
    // });

    // /*  */
    // $('#archiveEmployeeDatatable').on( 'click', '.menu-see-button', function () {

    // })

    // /* Click See on Archive Permission Datatable */
    // $('#archiveEmployeeDatatable').on( 'click', '.menu-see-button', function () {
    //     $('#employeeSeeModal').modal('show');

    //     var id = $(this).data('id');
    //     get(base_url+"/receptionist/patient/get/"+id, false, function(response){
    //         responsedata = response.data;

    //         $('#roleEditModal #edit-id').val(id);
    //         $('#roleEditModal #edit-role').val('Perawat');
    //         $('#edit-aktif').prop('checked', false).change();
    //         $('#multiple-menu-edit').val(["2", "3"]);
    //     })   
    // });

    // /* Click Edit on Archive Permission Datatable */
    // $('#archiveEmployeeDatatable').on( 'click', '.menu-edit-button', function () {
    //     $('#employeeEditModal').modal('show');

    //     var id = $(this).data('id');
    //     get(base_url+"/receptionist/patient/get/"+id, false, function(response){
    //         responsedata = response.data;

    //         $('#roleEditModal #edit-id').val(id);
    //         $('#roleEditModal #edit-role').val('Perawat');
    //         $('#edit-aktif').prop('checked', false).change();
    //         $('#multiple-menu-edit').val(["2", "3"]);
    //     })   
    // });

    // /* Submit Edited Values */
    // $("#roleEditModal #submit-edit").on("click", function(){
    //     var id = $(this).data('id');

    //     var sendData = {
    //         "_token"              : $('#roleEditModal #edit-token').val(),
    //         "id"                  : $('#roleEditModal #edit-id').val(),
    //         "role"                : $('#roleEditModal #edit-role').val(),
    //         "is_aktif"            : $('#edit-aktif').is(':checked'),
    //         "menus"               : $('#roleEditModal #multiple-menu-edit').val(),
    //     }

    //     console.log(sendData)
    //     edit(sendData);
    // });

});

function add(data) {
    send(base_url+"/admin/api/employee/addPatient", data, true, function(response){
        if(response.isSuccess){
            $('#employeeAddModal').modal('hide');
            $('#patientDatatable').DataTable().ajax.reload();    
        }
    })
}

function edit(data) {
    send(base_url+"/receptionist/patient/edit", data, true, function(response){
        setTimeout(function(){
            location.reload()
        }, 500);
    })
}


function fillEditResponse(responsedata){
    if(responsedata.gender == "Perempuan")
        $('#optionGenderP').prop('checked',true);
    else $('#optionGenderL').prop('checked',true);

    $('#patientEditModal #edit-MR').val(responsedata.patient_mr_number);
    $('#patientEditModal #edit-nama').val(responsedata.patient_nm);
    $('#patientEditModal #edit-nip').val(responsedata.id_card);
    $('#patientEditModal #edit-bpjs').val(responsedata.bpjs);
    $('#patientEditModal #edit-ttl').val(responsedata.patient_ttl);
    $('#patientEditModal #edit-unit').val(responsedata.patient_unit);
    $('#patientEditModal #edit-email').val(responsedata.patient_mail);
    $('#patientEditModal #edit-hp').val(responsedata.patient_phone);
    $('#patientEditModal #edit-gender').val(responsedata.gender);
    $('#patientEditModal #edit-address').val(responsedata.patient_address);
    $('#patientEditModal #edit-riwayat-penyakit').val(responsedata.diag_derita);
    $('#patientEditModal #edit-penyakit-turunan').val(responsedata.diag_turunan);
    $('#patientEditModal #edit-riwayat-alergi').val(responsedata.alergi_rwy);
    $('#patientEditModal #edit-riwayat-vaksinasi').val(responsedata.vaksinasi_rwy);
}
