$(document).ready(function() {
    /* Add role*/
    $('.add-medicine').click(function(){
        $('#medicineAddModal').modal('show');
    });


/* ========================================================= */
/* ========================================================= */

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
       $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
    });

    var sendData = {
        "_token"    : $('meta[name="csrf_token"]').attr('content')
    }

    var tableActive = $('#activeMedicineDatatable').DataTable({
            scrollX: true,
            autoWidth: true,
            processing: true,
            serverSide: true,
            bDestroy: true,
            ajax: {
                "url": APIurl+'/admin/api/medicine/list/1',
                "type": "POST",
                "data": sendData
            },
            columns: [        
                { data: 'nama', name: 'nama' },
                { data: 'kuantitas', name: 'kuantitas' },
                { data: 'satuan', name: 'satuan' },
                { data: 'tanggal_kadaluarsa_indo', name: 'tanggal_kadaluarsa_indo' },
                { data: 'id', name: 'id', className: 'text-center', 
                    render: function(data, type, row, meta){
                        var button = '<button data-toggle="button" class="btn btn-circle btn-success view-komposisi-button active" data-id="'+data+'" data-komposisi="'+row["komposisi"]+'" aria-pressed="true"><i class="fa fa-search"></i>'+
                                   '</button>';

                        return button;

                    },
                    searchable: false,
                    sortable: false
                },
                { data: 'last_update_indo', name: 'last_update_indo' },
                { data: 'id', name: 'id', className: 'text-center', 
                    render: function(data, type, row, meta){
                        var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
                        var archiveButton = '<button type="button" title="Non Aktifkan" id="editButton" data-id="'+data+'" class="label label-danger menu-archive-button style="margin-right:1vw;">&nbsp;<i class="fa fa-times"></i></button>';
                        button = editButton+'&nbsp'+archiveButton;

                        return button;
                    },
                    searchable: false,
                    sortable: false
                }
            ],
            order: [[ 0, "asc" ]]
        });


    /*Click Edit on Active Permission Datatable */
    $('#activeMedicineDatatable').on( 'click', '.menu-edit-button', function () {
        $('#medicineEditModal').modal('show');

        var id = $(this).data('id');
        get(base_url+"/admin/api/medicine/get/"+id, false, function(response){
            responsedata = response.data;

            console.log(responsedata)

            $('#medicineEditModal #edit-id').val(id);
            $('#medicineEditModal #edit-nama').val(responsedata.nama);
            $('#medicineEditModal #edit-kuantitas').val(responsedata.kuantitas);
            $('#medicineEditModal #edit-satuan').val(responsedata.satuan);
            $('#medicineEditModal #edit-komposisi').val(responsedata.komposisi);
            $('#medicineEditModal #edit-expired').val(responsedata.tgl_kadaluarsa);
            $('#edit-aktif').prop('checked', true).change();
        })   
    });

    $('#activeMedicineDatatable').on( 'click', '.view-komposisi-button', function () {
        var id = $(this).data('id');
        var komposisi = $(this).data('komposisi');

        $('#komposisiViewModal').modal('show');

        $('#komposisi-html').html(komposisi)
    })

    /* Click Archive User */
    $('#activeMedicineDatatable').on( 'click', '.menu-archive-button', function () {
        var title = "Apakah Anda Yakin Menonaktifkan Obat Ini?"
        var id = $(this).data('id')
        showCancelAlert(title, archiveMedicine, id)        
    })
    /**/

/* ========================================================= */
/* ========================================================= */

    /* Archive Permission Datatable */
    var table = $('#archiveMedicineDatatable').DataTable({
            
        scrollX: true,
        autoWidth: true,
        processing: true,
        serverSide: true,
        bDestroy: true,
        ajax: {
            "url": APIurl+'/admin/api/medicine/list/0',
            "type": "POST",
            "data": sendData
        },
        columns: [        
            { data: 'nama', name: 'nama' },
            { data: 'kuantitas', name: 'kuantitas' },
            { data: 'satuan', name: 'satuan' },
            { data: 'tanggal_kadaluarsa_indo', name: 'tanggal_kadaluarsa_indo' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var button = '<button data-toggle="button" class="btn btn-circle btn-success view-komposisi-button active" data-id="'+data+'" data-komposisi="'+row["komposisi"]+'" aria-pressed="true"><i class="fa fa-search"></i>'+
                               '</button>';

                    return button;

                },
                searchable: false,
                sortable: false
            },
            { data: 'last_update_indo', name: 'last_update_indo' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
                    var activateButton = '<button type="button" title="Aktifkan" id="activateButton" data-id="'+data+'" class="label label-success menu-activate-button style="margin-right:1vw;">&nbsp;<i class="fa fa-check"></i></button>';
                    button = editButton+'&nbsp'+activateButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 0, "asc" ]]
    });

    /* Click Edit on Archive Permission Datatable */
    $('#archiveMedicineDatatable').on( 'click', '.menu-edit-button', function () {
        $('#medicineEditModal').modal('show');

        var id = $(this).data('id');
        get(base_url+"/admin/api/medicine/get/"+id, false, function(response){
            responsedata = response.data;

            $('#medicineEditModal #edit-id').val(id);
            $('#medicineEditModal #edit-nama').val(responsedata.nama);
            $('#medicineEditModal #edit-kuantitas').val(responsedata.kuantitas);
            $('#medicineEditModal #edit-satuan').val(responsedata.satuan);
            $('#medicineEditModal #edit-komposisi').val(responsedata.komposisi);
            $('#medicineEditModal #edit-expired').val(responsedata.tgl_kadaluarsa);
            $('#edit-aktif').prop('checked', false).change();
        })   
    });

    /* Submit Edited Values */
    $("#medicineEditModal #submit-edit").on("click", function(){
        var id = $(this).data('id');

        var sendData = {
            "_token"        : $('#medicineEditModal #edit-token').val(),
            "id"            : $('#medicineEditModal #edit-id').val(),
            "nama"          : $('#medicineEditModal #edit-nama').val(),
            "kuantitas"     : $('#medicineEditModal #edit-kuantitas').val(),
            "satuan"        : $('#medicineEditModal #edit-satuan').val(),
            "komposisi"      : $('#medicineEditModal #edit-komposisi').val(),
            "tgl_kadaluarsa" : $('#medicineEditModal #edit-expired').val(),
            "is_aktif"      : $('#edit-aktif').is(':checked'),
        }

        edit(sendData);
    });

    $('#archiveMedicineDatatable').on( 'click', '.view-komposisi-button', function () {
        var id = $(this).data('id');
        var komposisi = $(this).data('komposisi');
        $('#komposisiViewModal').modal('show');
        $('#komposisi-html').html(komposisi)
    })

    /* Click Medicine User */
    $('#archiveMedicineDatatable').on( 'click', '.menu-activate-button', function () {
        var title = "Apakah Anda Yakin Mengaktifkan Obat?"
        var id = $(this).data('id')
        showCancelAlert(title, activateMedicine, id)        
    })
    /**/

    $("#medicineAddModal #submit-add").on("click", function(){
        var sendData = {
            "_token"        : $('#medicineAddModal #add-token').val(),
            "nama"          : $('#medicineAddModal #add-nama').val(),
            "kuantitas"     : $('#medicineAddModal #add-kuantitas').val(),
            "satuan"        : $('#medicineAddModal #add-satuan').val(),
            "komposisi"      : $('#medicineAddModal #add-komposisi').val(),
            "tgl_kadaluarsa" : $('#medicineAddModal #add-expired').val(),
            "is_aktif"      : $('#edit-aktif').is(':checked'),
        }

        console.log(sendData)
        add(sendData);
    });

});

function add(data) {
    send(base_url+"/admin/api/medicine/add", data, true, function(response){
        if(response.isSuccess){
            $('#medicineAddModal').modal('hide');
            $('#archiveMedicineDatatable').DataTable().ajax.reload();   
            $('#activeMedicineDatatable').DataTable().ajax.reload();
        }
    })
}

function edit(data) {
    send(base_url+"/admin/api/medicine/edit", data, true, function(response){
        if(response.isSuccess){
            $('#medicineEditModal').modal('hide');
            $('#archiveMedicineDatatable').DataTable().ajax.reload();   
            $('#activeMedicineDatatable').DataTable().ajax.reload();
        }
    })
}

function archiveMedicine(id) {
    get(base_url+"/admin/api/medicine/archive/"+id, false, function(response){
        if(response.isSuccess){
            swal("Dinonaktifkan!", "Obat Berhasil dinonaktifkan", "success");
            $('#archiveMedicineDatatable').DataTable().ajax.reload();   
            $('#activeMedicineDatatable').DataTable().ajax.reload();
        }
    })
}

function activateMedicine(id) {
    get(base_url+"/admin/api/medicine/activate/"+id, false, function(response){
        if(response.isSuccess){
            swal("Diaktifkan!", "Obat Berhasil diaktifkan", "success");
            $('#archiveMedicineDatatable').DataTable().ajax.reload();   
            $('#activeMedicineDatatable').DataTable().ajax.reload();
        }
        
    })
}