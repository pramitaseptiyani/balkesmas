$(document).ready(function() {
    /* Add role*/
    $('.add-lab').click(function(){
        $('#labAddModal').modal('show');
    });

    /* Select2 */
    $(".select2-lab-add").select2({
        placeholder: "Pilih Lab Parent",
        tags: true,
        dropdownParent: $("#labAddModal")
    });

    $(".select2-lab-edit").select2({
        placeholder: "Pilih Lab Parent",
        tags: true,
        dropdownParent: $("#labEditModal")
    });

    /* Select2 Menu on Add Role */
    get(base_url+"/admin/api/lab/select2/", false, function(response){
        var responsedata = response.data;

        var select2_html = '';

        var add_select2 = '<option value="0">Tidak Ada Parent</option>';
        select2_html += add_select2;

        responsedata.forEach(function (lab, index) {
            var add_select2 = '<option value="'+lab.id+'">'+lab.nama+'</option>';

            select2_html += add_select2;

            $('#select2-lab-add').html(select2_html)
            $('#select2-lab-edit').html(select2_html)
        })
    })
    /**/


/* ========================================================= */
/* ========================================================= */
    reloadLab();
    // var page = 1;
    // var limit = 8;

    // sendData = {
    //     "limit" : limit,
    //     "page"  : page
    // }

    // var search = '';
    // getLabList(sendData, search);

    $('#search').keyup(function(e) {
        search = $('#search').val()
        getLabList(sendData, search)
    });
    


/* ========================================================= */
/* ========================================================= */

    /* Archive Permission Datatable */
    var table = $('#archiveLabDatatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'/admin/api/lab/list/all/0',
        },
        columns: [        
            { data: 'nama', name: 'nama' },
            { data: 'nama_parent', name: 'nama_parent',
                render: function(data, type, row, meta){
                    if(data == null) data = '-'
                    return data;
                },
            },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" data-nama="'+row["nama"]+'" data-parent-id="'+row["parent_id"]+'" data-parent="'+row["parent"]+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
                    var activateButton = '<button type="button" title="Aktifkan" id="activateButton" data-id="'+data+'" class="label label-success menu-activate-button style="margin-right:1vw;">&nbsp;<i class="fa fa-check"></i></button>';
                    button = editButton+'&nbsp'+activateButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 0, "asc" ]]
    });

    /* Click Edit on Archive Permission Datatable */
    $('#archiveLabDatatable').on( 'click', '.menu-edit-button', function () {
        $('#labEditModal').modal('show');

        var id = $(this).data('id');
        var parent_id = $(this).data('parent-id');
        var parent = $(this).data('parent');

        $('#labEditModal #edit-nama').val($(this).data('nama'));
        $('#labEditModal #select2-lab-edit').val($(this).data('parent-id'));

        var $selected = $("<option selected='selected'></option>").val(parent_id).text(parent)
        $("#select2-lab-edit").append($selected).trigger('change');

        $('#edit-aktif').prop('checked', false).change();
    });

    /* Click Edit on Archive Permission Datatable */
    $('#archiveLabDatatable').on( 'click', '.menu-activate-button', function () {
        var title = "Apakah Anda Yakin Mengaktifkan Lab?"
        var id = $(this).data('id')
        showCancelAlert(title, activateLab, id) 
    });       

    /* Submit Edited Values */
    $("#labEditModal #submit-edit").on("click", function(){
        var sendData = {
            "_token"              : $('#labEditModal #edit-token').val(),
            "id"                  : $('#labEditModal #edit-id').val(),
            "nama"                : $('#labEditModal #edit-nama').val(),
            "parent_id"           : $('#labEditModal #select2-lab-edit').val(),
            "is_aktif"            : $('#edit-aktif').is(':checked')
        }

        console.log(sendData)
        edit(sendData);
    });


    /* Submit Edited Values */
    $("#labAddModal #submit-edit").on("click", function(){
        var sendData = {
            "_token"              : $('#labAddModal #add-token').val(),
            "nama"                : $('#labAddModal #add-nama').val(),
            "parent_id"           : $('#labAddModal #select2-lab-add').val(),
            "is_aktif"            : $('#edit-aktif').is(':checked')
        }

        console.log(sendData)
        add(sendData);
    });

});

function add(data) {
    send(base_url+"/admin/api/lab/add", data, true, function(response){
        if(response.isSuccess){
            $('#labAddModal').modal('hide');
            $('#archiveLabDatatable').DataTable().ajax.reload();
            reloadLab();   
        }
    })
}

function edit(data) {
    send(base_url+"/admin/api/lab/edit", data, true, function(response){
        if(response.isSuccess){
            $('#labEditModal').modal('hide');
            $('#archiveLabDatatable').DataTable().ajax.reload();
            reloadLab();   
        }
    })
}

function getLabList(sendData, search) {
    var limit = sendData.limit;
    var page = sendData.page;

    var sendData = {
        "_token": $('meta[name="csrf_token"]').attr('content'),
        "limit" : limit,
        "page"  : page,
        "search": search
    }

    send(base_url+"/admin/api/lab/list/level1/1", sendData,false, function(response){
        responsedata = response.data;

        var total = response.total;
        // var total = 15;
        var pages = Math.ceil(total/sendData.limit);

        var html_levels = '';
        responsedata.forEach(function (level1, index) {
            var html_level1 = '<div class="col-md-3">'+
                        '<div class="card card-topline-aqua" style="height: 250px">'+
                            '<div class="card-body no-padding height-9">'+
                                '<div class="row">'+
                                    '<div class="profile-userpic">'+
                                        '<img src="'+base_url+'/assets/img/lab/icon-lab1.jfif" class="img-responsive" alt=""> '+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="profile-usertitle">'+
                                    '<div class="profile-usertitle-name" style="font-size: 16px;">'+level1.nama+'</div>'+
                                    '<div class="profile-usertitle-job"> </div>'+
                                '</div>'+
                                '<div class="profile-userbuttons">'+
                                    '<a href="'+base_url+'/admin/lab2/'+level1.id+'">'+
                                        '<button type="button" class="btn btn-circle purple btn-xs">Pilih</button>'+
                                    '</a>&nbsp;'+
                                    '<a href="#">'+
                                        '<button type="button" id="edit-lab-'+level1.id+'" data-id="'+level1.id+'" data-nama="'+level1.nama+'" data-parent-id="'+level1.parent_id+'" class="btn btn-circle edit-lab blue btn-xs">Edit</button>'+
                                    '</a>&nbsp;'+
                                    '<a href="#">'+
                                        '<button type="button" data-id="'+level1.id+'" class="btn btn-circle archive-lab red btn-xs">Non Aktifkan</button>'+
                                    '</a>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';

            html_levels +=html_level1

        })

        $('#html-level1').html(html_levels);

        $('#paginate').unbind('page');
        $('#paginate').bootpag({
            total: pages,
            page: sendData.page,
            maxVisible: 3,
            leaps: true,
            firstLastUse: true,
            first: '←',
            last: '→',
            wrapClass: 'pagination',
            activeClass: 'active',
            disabledClass: 'disabled',
            nextClass: 'next',
            prevClass: 'prev',
            lastClass: 'last',
            firstClass: 'first'
        }).on("page", function(event, num){
            sendData.page = num
            getLabList(sendData, search)
        });

        $( ".edit-lab" ).click(function() {
            $('#labEditModal').modal('show');

            $('#labEditModal #edit-id').val($(this).data('id'));
            $('#labEditModal #edit-nama').val($(this).data('nama'));
            $('#labEditModal #select2-lab-edit').val($(this).data('parent-id'));
        })

        $( ".archive-lab" ).click(function() {
            console.log("archive")
            var title = "Apakah Anda Yakin Menonaktifkan Lab Ini?"
            var id = $(this).data('id')
            showCancelAlert(title, archiveLab, id) 
        })
    })

    
}

function activateLab(id) {
    get(base_url+"/admin/api/lab/activate/"+id, false, function(response){
        if(response.isSuccess){
            swal("Diaktifkan!", "Lab Berhasil diaktifkan", "success");
            $('#archiveLabDatatable').DataTable().ajax.reload();   
            reloadLab();
        }
        
    })
}

function archiveLab(id) {
    get(base_url+"/admin/api/lab/archive/"+id, false, function(response){
        if(response.isSuccess){
            swal("Dinonaktifkan!", "Lab Berhasil dinonaktifkan", "success");
            $('#archiveLabDatatable').DataTable().ajax.reload();   
            reloadLab();
        }
        
    })
}

function reloadLab() {
    var page = 1;
    var limit = 8;

    sendData = {
        "limit" : limit,
        "page"  : page
    }

    var search = '';
    getLabList(sendData, search);
}