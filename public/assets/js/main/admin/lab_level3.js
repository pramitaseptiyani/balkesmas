$(document).ready(function() {
    var pageURL = window.location.href;
    var parent_id = pageURL.substr(pageURL.lastIndexOf('/') + 1);

    /* Add role*/
    $('.add-lab').click(function(){
        $('#labAddModal').modal('show');

        get(base_url+"/admin/api/lab/get/"+parent_id, false, function(response){
            var responsedata = response.data;

            var $selected = $("<option selected='selected'></option>").val(responsedata.id).text(responsedata.nama)
            $("#select2-lab-add").append($selected).trigger('change');
        })
        
    });

    $(".select2-lab-edit").select2({
        placeholder: "Pilih Lab Parent",
        tags: true,
        dropdownParent: $("#labEditModal")
    });


/* ========================================================= */
    /* Select2 Menu on Add Role */
    get(base_url+"/admin/api/lab/select2/", false, function(response){
        var responsedata = response.data;

        var select2_html = '';

        var add_select2 = '<option value="0">Tidak Ada Parent</option>';
        select2_html += add_select2;

        responsedata.forEach(function (lab, index) {
            var add_select2 = '<option value="'+lab.id+'">'+lab.nama+'</option>';

            select2_html += add_select2;

            $('#select2-lab-add').html(select2_html)
            $('#select2-lab-edit').html(select2_html)
        })
    })
/* ========================================================= */

    var pageURL = window.location.href;
    var parent_id = pageURL.substr(pageURL.lastIndexOf('/') + 1);

    /* Active Permission Datatable */
    var table = $('#activeLabLevel3Datatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'/admin/api/lab/list/level3/'+parent_id,
        },
        columns: [        
            { data: 'nama', name: 'nama' },
            { data: 'parent', name: 'parent' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" data-nama="'+row["nama"]+'" data-parent-id="'+row["parent_id"]+'" data-parent="'+row["parent"]+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
                    var archiveButton = '<button type="button" title="Non Aktifkan" id="editButton" data-id="'+data+'" class="label label-danger menu-archive-button style="margin-right:1vw;">&nbsp;<i class="fa fa-times"></i></button>';
                    button = editButton+'&nbsp'+archiveButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 0, "asc" ]]
    });


    /*Click Edit on Active Permission Datatable */
    $('#activeLabLevel3Datatable').on( 'click', '.menu-edit-button', function () {
        $('#labEditModal').modal('show');

        var id = $(this).data('id');
        var parent_id = $(this).data('parent-id');
        var parent = $(this).data('parent');

        $('#labEditModal #edit-id').val($(this).data('id'));
        $('#labEditModal #edit-nama').val($(this).data('nama'));
        $('#labEditModal #select2-lab-edit').val($(this).data('parent-id'));

        var $selected = $("<option selected='selected'></option>").val(parent_id).text(parent)
        $("#select2-lab-edit").append($selected).trigger('change');
           
    });

    /* Click Edit on Archive Permission Datatable */
    $('#activeLabLevel3Datatable').on( 'click', '.menu-archive-button', function () {
        var title = "Apakah Anda Yakin Menonaktifkan Lab Ini?"
        var id = $(this).data('id')
        showCancelAlert(title, archiveLab, id) 
    }); 

/* ========================================================= */
/* ========================================================= */

    /* Archive Permission Datatable */
    var table = $('#archiveMedicineDatatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'/admin/api/medicine/list/0',
        },
        columns: [        
            { data: 'nama', name: 'nama' },
            { data: 'kuantitas', name: 'kuantitas' },
            { data: 'satuan', name: 'satuan' },
            { data: 'expired_date', name: 'expired_date' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var button = '<button data-toggle="button" class="btn btn-circle btn-success view-komposisi-button active" data-id="'+data+'" data-komposisi="'+row["komposisi"]+'" aria-pressed="true"><i class="fa fa-search"></i>'+
                               '</button>';

                    return button;

                },
                searchable: false,
                sortable: false
            },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
                    var activateButton = '<button type="button" title="Aktifkan" id="activateButton" data-id="'+data+'" class="label label-success menu-activate-button style="margin-right:1vw;">&nbsp;<i class="fa fa-check"></i></button>';
                    button = editButton+'&nbsp'+activateButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 0, "asc" ]]
    });

    /* Submit Edited Values */
    $("#labAddModal #submit-edit").on("click", function(){
        var sendData = {
            "_token"              : $('#labAddModal #add-token').val(),
            "nama"                : $('#labAddModal #add-nama').val(),
            "parent_id"           : $('#labAddModal #select2-lab-add').val(),
            "is_aktif"            : $('#edit-aktif').is(':checked')
        }

        console.log(sendData)
        add(sendData);
    });

    /* Submit Edited Values */
    $("#labEditModal #submit-edit").on("click", function(){
        var id = $(this).data('id');

        var sendData = {
            "_token"              : $('#labEditModal #edit-token').val(),
            "id"                  : $('#labEditModal #edit-id').val(),
            "nama"                : $('#labEditModal #edit-nama').val(),
            "parent_id"           : $('#labEditModal #select2-lab-edit').val(),
            "is_aktif"            : $('#edit-aktif').is(':checked')
        }


        console.log(sendData)
        edit(sendData);
    });

});

function add(data) {
    send(base_url+"/admin/api/lab/add", data, true, function(response){
        if(response.isSuccess){
            $('#labAddModal').modal('hide');
            $('#activeLabLevel3Datatable').DataTable().ajax.reload();   
        }
    })
}

function edit(data) {
    send(base_url+"/admin/api/lab/edit", data, true, function(response){
        if(response.isSuccess){
            $('#labEditModal').modal('hide');
            $('#activeLabLevel3Datatable').DataTable().ajax.reload();   
        }
    })
}

function archiveLab(id) {
    get(base_url+"/admin/api/lab/archive/"+id, false, function(response){
        if(response.isSuccess){
            swal("Dinonaktifkan!", "Lab Berhasil Dinonaktifkan", "success");
            $('#activeLabLevel3Datatable').DataTable().ajax.reload();   
        }
        
    })
}
