$(document).ready(function() {

    var pageURL = window.location.href;
    var parent_id = pageURL.substr(pageURL.lastIndexOf('/') + 1);

    /* Add role*/
    $('.add-lab').click(function(){
        $('#labAddModal').modal('show');

        get(base_url+"/admin/api/lab/get/"+parent_id, false, function(response){
            var responsedata = response.data;

            var $selected = $("<option selected='selected'></option>").val(responsedata.id).text(responsedata.nama)
            $("#select2-lab-add").append($selected).trigger('change');
        })
        
    });

    /* Select2 */
    $(".select2-lab-add").select2({
        placeholder: "Pilih Lab Parent",
        tags: true,
        dropdownParent: $("#labAddModal")
    });

    $(".select2-lab-edit").select2({
        placeholder: "Pilih Lab Parent",
        tags: true,
        dropdownParent: $("#labEditModal")
    });


/* ========================================================= */
/* ========================================================= */
    
    reloadLab()


    /* ================================================== */

    /* Select2 Menu on Add Role */
    get(base_url+"/admin/api/lab/select2/", false, function(response){
        var responsedata = response.data;

        var select2_html = '';

        var add_select2 = '<option value="0">Tidak Ada Parent</option>';
        select2_html += add_select2;

        responsedata.forEach(function (lab, index) {
            var add_select2 = '<option value="'+lab.id+'">'+lab.nama+'</option>';

            select2_html += add_select2;

            $('#select2-lab-add').html(select2_html)
            $('#select2-lab-edit').html(select2_html)
        })
    })
    /**/
    

/* ========================================================== */


    /* Active Permission Datatable */
    var table = $('#activeMedicineDatatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'/admin/api/medicine/list/1',
        },
        columns: [        
            { data: 'nama', name: 'nama' },
            { data: 'kuantitas', name: 'kuantitas' },
            { data: 'satuan', name: 'satuan' },
            { data: 'expired_date', name: 'expired_date' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var button = '<button data-toggle="button" class="btn btn-circle btn-success view-komposisi-button active" data-id="'+data+'" data-komposisi="'+row["komposisi"]+'" aria-pressed="true"><i class="fa fa-search"></i>'+
                               '</button>';

                    return button;

                },
                searchable: false,
                sortable: false
            },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
                    var archiveButton = '<button type="button" title="Non Aktifkan" id="editButton" data-id="'+data+'" class="label label-danger menu-archive-button style="margin-right:1vw;">&nbsp;<i class="fa fa-times"></i></button>';
                    button = editButton+'&nbsp'+archiveButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 0, "asc" ]]
    });


/* ========================================================= */
/* ========================================================= */


    /* Submit Edited Values */
    $("#labEditModal #submit-edit").on("click", function(){
        var sendData = {
            "_token"              : $('#labEditModal #edit-token').val(),
            "id"                  : $('#labEditModal #edit-id').val(),
            "nama"                : $('#labEditModal #edit-nama').val(),
            "parent_id"           : $('#labEditModal #select2-lab-edit').val(),
            "is_aktif"            : $('#edit-aktif').is(':checked')
        }

        console.log(sendData)
        edit(sendData);
    });

});

function edit(data) {
    // send(base_url+"/receptionist/patient/edit", data, true, function(response){
    //     setTimeout(function(){
    //         location.reload()
    //     }, 500);
    // })
}

function getLabList(sendData, search) {
    var sendData = {
        "_token": $('meta[name="csrf_token"]').attr('content'),
        "parent_id" : 8,
        "limit" : sendData.limit,
        "page"  : sendData.page,
        "search": search
    }

    var pageURL = window.location.href;
    var parent_id = pageURL.substr(pageURL.lastIndexOf('/') + 1);

    send(base_url+"/admin/api/lab/list/level2/"+parent_id, sendData, false, function(response){
        responsedata = response.data;

        var total = response.total;
        // var total = 15;
        var pages = Math.ceil(total/sendData.limit);

        var html_levels = '';
        if(responsedata.length == 0){
            html_levels = 'No data'
        }
        else{
            responsedata.forEach(function (level2, index) {
                var html_level2 = '<div class="col-md-3">'+
                            '<div class="card card-topline-aqua" style="height: 250px">'+
                                '<div class="card-body no-padding height-9">'+
                                    '<div class="row">'+
                                        '<div class="profile-userpic">'+
                                            '<img src="'+base_url+'/assets/img/lab/icon-lab2.png" class="img-responsive" alt=""> '+ 
                                        '</div>'+
                                    '</div>'+
                                    '<div class="profile-usertitle">'+
                                        '<div class="profile-usertitle-name" style="font-size: 16px;">'+level2.nama+'</div>'+
                                        '<div class="profile-usertitle-job"> </div>'+
                                    '</div>'+
                                    '<div class="profile-userbuttons">'+
                                        '<div class="profile-userbuttons">'+
                                        '<a href="'+base_url+'/admin/lab3/'+parent_id+'/'+level2.id+'">'+
                                            '<button type="button" class="btn btn-circle purple btn-xs">Pilih</button>'+
                                        '</a>&nbsp;'+
                                        '<a href="#">'+
                                            '<button type="button" id="edit-lab-'+level2.id+'" data-id="'+level2.id+'" data-nama="'+level2.nama+'" data-parent-id="'+level2.parent_id+'" data-parent-nama="'+level2.nama_parent+'" class="btn btn-circle edit-lab blue btn-xs">Edit</button>'+
                                        '</a>&nbsp;'+
                                        '<a href="#">'+
                                            '<button type="button" data-id="'+level2.id+'" class="btn btn-circle archive-lab red btn-xs">Non Aktifkan</button>'+
                                        '</a>'+
                                    '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';

                html_levels +=html_level2

            })
        }
        

        $('#html-level1').html(html_levels);

        $('#paginate').unbind('page');
        $('#paginate').bootpag({
            total: pages,
            page: sendData.page,
            maxVisible: 3,
            leaps: true,
            firstLastUse: true,
            first: '←',
            last: '→',
            wrapClass: 'pagination',
            activeClass: 'active',
            disabledClass: 'disabled',
            nextClass: 'next',
            prevClass: 'prev',
            lastClass: 'last',
            firstClass: 'first'
        }).on("page", function(event, num){
            sendData.page = num
            getLabList(sendData, search)
        });

        $( ".edit-lab" ).click(function() {
            $('#labEditModal').modal('show');

            $('#labEditModal #edit-id').val($(this).data('id'));
            $('#labEditModal #edit-nama').val($(this).data('nama'));
            $('#labEditModal #select2-lab-edit').val($(this).data('parent-id'));

            var parent_selected = $("<option selected='selected'></option>").val($(this).data('parent-id')).text($(this).data('parent-nama'))
            $("#select2-lab-edit").append(parent_selected).trigger('change');
        })

        $( ".archive-lab" ).click(function() {
            console.log("archive")
            var title = "Apakah Anda Yakin Menonaktifkan Lab Ini?"
            var id = $(this).data('id')
            showCancelAlert(title, archiveLab, id) 
        })
    })

    /* Submit Edited Values */
    $("#labAddModal #submit-edit").on("click", function(){
        var sendData = {
            "_token"              : $('#labAddModal #add-token').val(),
            "nama"                : $('#labAddModal #add-nama').val(),
            "parent_id"           : $('#labAddModal #select2-lab-add').val(),
            "is_aktif"            : $('#edit-aktif').is(':checked')
        }

        add(sendData);
    });

    /* Submit Edited Values */
    $("#labEditModal #submit-edit").on("click", function(){
        var sendData = {
            "_token"              : $('#labEditModal #edit-token').val(),
            "id"                  : $('#labEditModal #edit-id').val(),
            "nama"                : $('#labEditModal #edit-nama').val(),
            "parent_id"           : $('#labEditModal #select2-lab-edit').val(),
            "is_aktif"            : $('#edit-aktif').is(':checked')
        }

        edit(sendData);
    });
}

function add(data) {
    send(base_url+"/admin/api/lab/add", data, true, function(response){
        if(response.isSuccess){
            $('#labAddModal').modal('hide');
            reloadLab();   
        }
    })
}

function edit(data) {
    send(base_url+"/admin/api/lab/edit", data, true, function(response){
        if(response.isSuccess){
            $('#labEditModal').modal('hide');
            reloadLab();   
        }
    })
}

function activateLab(id) {
    get(base_url+"/admin/api/lab/activate/"+id, false, function(response){
        if(response.isSuccess){
            swal("Diaktifkan!", "Lab Berhasil diaktifkan", "success");
            $('#archiveLabDatatable').DataTable().ajax.reload();   
            reloadLab();
        }
        
    })
}

function archiveLab(id) {
    get(base_url+"/admin/api/lab/archive/"+id, false, function(response){
        if(response.isSuccess){
            swal("Dinonaktifkan!", "Lab Berhasil Dinonaktifkan", "success");
            $('#archiveLabDatatable').DataTable().ajax.reload();   
            reloadLab();
        }
        
    })
}

function reloadLab() {
    var page = 1;
    var limit = 8;

    sendData = {
        "limit" : limit,
        "page"  : page
    }

    var search = '';
    getLabList(sendData, search);
}