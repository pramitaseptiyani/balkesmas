$(document).ready(function() { 

    $('#search-nip').select2({
        placeholder: 'Ketikan NIP',
        theme: "classic",
        minimumInputLength: 3,
        dropdownParent: $("#userAddModal"),
        ajax: {
            dataType: 'json',
            url: base_path+'/receptionist/patient/search',
            // delay: 800,
            data: function(params) {
                return {
                    nip: params.term,
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result.data, function(index,item){
                    list.push({
                        id      : item.nip,
                        text    : item.nip +" - "+ item.nama,
                    })
                });
                return {
                    results: list
                };
            },
        }
    });

    $('#edit-nip').select2({
        placeholder: 'Ketikan NIP',
        theme: "classic",
        minimumInputLength: 3,
        dropdownParent: $("#userEditModal"),
        ajax: {
            dataType: 'json',
            url: base_path+'/receptionist/patient/search',
            // delay: 800,
            data: function(params) {
                return {
                    nip: params.term,
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result.data, function(index,item){
                    list.push({
                        id      : item.nip,
                        text    : item.nip +" - "+ item.nama,
                    })
                });
                return {
                    results: list
                };
            },
        }
    });      

    $('#search-nip').on('select2:select', function (e) {
        var data = e.params.data;
    }); 

    /* Select Role */
    $('#edit-role').select2({
        theme: "classic",
        dropdownParent: $("#userEditModal"),
          ajax: {
            dataType: 'json',
            url: base_path+'/admin/api/role/select2',
            // delay: 800,
            data: function(params) {
                return {
                  role: params.term,
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result.data, function(index,item){
                  list.push({
                    id: item.id,
                    text: item.role,
                  })
                });
                return {
                  results: list
                };
            },
        }
    });

    $('#add-role').select2({
        placeholder: 'Ketikan Role',
        theme: "classic",
        dropdownParent: $("#userAddModal"),
          ajax: {
            dataType: 'json',
            url: base_path+'/admin/api/role/select2',
            // delay: 800,
            data: function(params) {
                return {
                  role: params.term,
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result.data, function(index,item){
                  list.push({
                    id: item.id,
                    text: item.role,
                  })
                });
                return {
                  results: list
                };
            },
        }
    });


    /* Active User Datatable */
    var activeTable = $('#activeUserDatatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'admin/api/user/list/1',
        },
        columns: [        
            { data: 'nama', name: 'nama' },
            { data: 'nipbaru', name: 'nipbaru' },
            { data: 'username', name: 'username' },
            { data: 'email', name: 'email' },
            { data: 'role', name: 'role' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
                    var archiveButton = '<button type="button" title="Non Aktifkan" id="editButton" data-id="'+data+'" class="label label-danger menu-archive-button style="margin-right:1vw;">&nbsp;<i class="fa fa-times"></i></button>';
                    button = editButton+'&nbsp'+archiveButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 0, "asc" ]]
    });
    /* */

    /* Click Edit of patient */
    $('#activeUserDatatable').on( 'click', '.menu-edit-button', function () {
        $('#userEditModal').modal('show');

        var id = $(this).data('id');
        get(base_url+"/admin/api/user/get/"+id, false, function(response){
            responsedata = response.data;

            var role_selected = $("<option selected='selected'></option>").val(responsedata.role_id).text(responsedata.role)
            $("#edit-role").append(role_selected).trigger('change');

            var nip_selected = $("<option selected='selected'></option>").val(responsedata.nip).text(responsedata.nip+" - "+responsedata.nama)
            $("#edit-nip").append(nip_selected).trigger('change');

            $('#edit-aktif').prop('checked', true).change()

            $('#userEditModal #edit-id').val(id);
            $('#userEditModal #edit-nip').val(responsedata.nip);
            $('#userEditModal #edit-username').val(responsedata.username);
            $('#userEditModal #edit-email').val(responsedata.email);
        })   
    });

    /* Click Archive User */
    $('#activeUserDatatable').on( 'click', '.menu-archive-button', function () {
        var title = "Apakah Anda Yakin Menonaktifkan User?"
        var id = $(this).data('id')
        showCancelAlert(title, archiveUser, id)        
    })
    /**/

    /* Archive User Datatable */
    var archiveTable = $('#archiveUserDatatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'admin/api/user/list/0',
        },
        columns: [        
            { data: 'nama', name: 'nama' },
            { data: 'nipbaru', name: 'nipbaru' },
            { data: 'username', name: 'username' },
            { data: 'email', name: 'email' },
            { data: 'role', name: 'role' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
                    // var activateButton = '<a href="'+base_path+'/receptionist/poli/add/'+data+'" title="Activate User" id="downloadButton" data-id="'+data+'" class="label label-success download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-check"></i></a>';
                    var activateButton = '<button type="button" title="Aktifkan" id="activateButton" data-id="'+data+'" class="label label-success menu-activate-button style="margin-right:1vw;">&nbsp;<i class="fa fa-check"></i></button>';
                    button = editButton+'&nbsp'+activateButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 0, "asc" ]]
    });

    /* Click Edit detail of patient */
    $('#archiveUserDatatable').on( 'click', '.menu-edit-button', function () {
        $('#userEditModal').modal('show');

        var id = $(this).data('id');
        get(base_url+"/admin/api/user/get/"+id, false, function(response){
            responsedata = response.data;

            var role_selected = $("<option selected='selected'></option>").val(responsedata.role_id).text(responsedata.role)
            $("#edit-role").append(role_selected).trigger('change');

            var nip_selected = $("<option selected='selected'></option>").val(responsedata.nip).text(responsedata.nip+" - "+responsedata.nama)
            $("#edit-nip").append(nip_selected).trigger('change');

            $('#edit-aktif').prop('checked', false).change()

            $('#userEditModal #edit-id').val(id);
            $('#userEditModal #edit-nip').val(responsedata.nip);
            $('#userEditModal #edit-username').val(responsedata.username);
            $('#userEditModal #edit-email').val(responsedata.email);
        })   
    });
    /**/

    /* Click Activate User */
    $('#archiveUserDatatable').on( 'click', '.menu-activate-button', function () {
        var title = "Apakah Anda Yakin Mengaktifkan User?"
        var id = $(this).data('id')
        showCancelAlert(title, activateUser, id)        
    })
    /**/

    /* Click on Save Edit */
    $("#userEditModal #submit-edit").on("click", function(){
        var id = $(this).data('id');

        var sendData = {
            "_token"        : $('#userEditModal #edit-token').val(),
            "role_id"       : $('#userEditModal #edit-role').val(),
            "is_aktif"      : $('#edit-aktif').is(':checked'),
            "id"            : $('#userEditModal #edit-id').val(),
            "nip"           : $('#userEditModal #edit-nip').val(),
            "username"      : $('#userEditModal #edit-username').val(),
            "password"      : $('#userAddModal #add-password').val(),
            "email"         : $('#userEditModal #edit-email').val(),     
        }

        console.log(sendData)
        edit(sendData);
    });

    /* Add user*/
    $('.add-user').click(function(){
        $('#userAddModal').modal('show');
    });

    $("#userAddModal #submit-add").on("click", function(){
        var sendData = {
            "_token"        : $('#userAddModal #add-token').val(),
            "nip"           : $('#userAddModal #search-nip').val(),
            "username"      : $('#userAddModal #add-username').val(),
            "password"      : $('#userAddModal #add-password').val(),
            "role_id"       : $('#userAddModal #add-role').val(),
            "is_aktif"      : $('#add-aktif').is(':checked'),
        }

        console.log(sendData)
        add(sendData);
    });
    /**/

});

function edit(data) {
    send(base_url+"/admin/api/user/edit", data, true, function(response){
        setTimeout(function(){
            location.reload()
        }, 500);
    })
}

function add(data) {
    send(base_url+"/admin/api/user/add", data, true, function(response){
        setTimeout(function(){
            location.reload()
        }, 500);
    })
}

function archiveUser(id) {
    get(base_url+"/admin/api/user/archive/"+id, false, function(response){
        if(response.isSuccess){
            swal("Dinonaktifkan!", "User Berhasil Dinonaktifkan", "success");
            $('#archiveUserDatatable').DataTable().ajax.reload();   
            $('#activeUserDatatable').DataTable().ajax.reload();
        }
    })
}

function activateUser(id) {
    get(base_url+"/admin/api/user/activate/"+id, false, function(response){
        if(response.isSuccess){
            swal("Diaktifkan!", "User Berhasil diaktifkan", "success");
            $('#archiveUserDatatable').DataTable().ajax.reload();   
            $('#activeUserDatatable').DataTable().ajax.reload();
        }
        
    })
}