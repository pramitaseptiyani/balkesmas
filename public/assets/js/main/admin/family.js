$(document).ready(function() {


    /* Select Kode Keluarga */
    $('#select-kdkeluarga').select2({
        theme: "classic",
        dropdownParent: $("#familymemberAddModal"),
          ajax: {
            dataType: 'json',
            url: base_path+'/admin/api/family/selectkdkeluarga',
            // delay: 800,
            data: function(params) {
                return {
                  role: params.term,
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result.data, function(index,item){
                  list.push({
                    id: item.id,
                    text: item.nama,
                  })
                });
                return {
                  results: list
                };
            },
        }
    });

    $('#select-editkdkeluarga').select2({
        theme: "classic",
        dropdownParent: $("#familymemberEditModal"),
          ajax: {
            dataType: 'json',
            url: base_path+'/admin/api/family/selectkdkeluarga',
            // delay: 800,
            data: function(params) {
                return {
                  role: params.term,
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result.data, function(index,item){
                  list.push({
                    id: item.id,
                    text: item.nama,
                  })
                });
                return {
                  results: list
                };
            },
        }
    });

    /* Select Kode Kerja */
    $('#select-kdkerja').select2({
        theme: "classic",
        dropdownParent: $("#familymemberAddModal"),
          ajax: {
            dataType: 'json',
            url: base_path+'/admin/api/family/selectkdkerja',
            // delay: 800,
            data: function(params) {
                return {
                  role: params.term,
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result.data, function(index,item){
                  list.push({
                    id: item.id,
                    text: item.ketkerja,
                  })
                });
                return {
                  results: list
                };
            },
        }
    });

    $('#select-editkdkerja').select2({
        theme: "classic",
        dropdownParent: $("#familymemberEditModal"),
          ajax: {
            dataType: 'json',
            url: base_path+'/admin/api/family/selectkdkerja',
            // delay: 800,
            data: function(params) {
                return {
                  role: params.term,
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result.data, function(index,item){
                  list.push({
                    id: item.id,
                    text: item.ketkerja,
                  })
                });
                return {
                  results: list
                };
            },
        }
    });

    document.getElementById('btn-family-member').style.display = 'none';

    /* Select Pegawai */

    $('#search-nip').select2({
        placeholder: 'Ketikan NIP Pegawai',
        theme: "classic",
        minimumInputLength: 3,
        ajax: {
            dataType: 'json',
            url: base_path+'/simpeg/get_pegawai',
            delay: 800,
            data: function(params) {
                return {
                    nip: params.term
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result, function(index,item){
                    list.push({
                        id      : item.nip,
                        text    : item.nip +" - "+ item.nama_pegawai,
                        nama    : item.nama_pegawai,
                        satker  : item.nama_satker,
                        satkerid  : item.kode_satker,
                        hp      : item.no_hp,
                        email   : item.email_dinas,
                        alamat  : item.alamat,
                        gender  : item.jenis_kelamin,
                        bpjs  : item.askes,
                    })
                });
                return {
                    results: list
                };
            },
        }
    });

    $('#select-kdkeluarga').on('select2:select', function(e){
        var data = e.params.data;
        var kode = data.id;
        if (kode == 1) {
            document.getElementById('data-nikah').style.display = '';
          } else if (kode == 2) {
            document.getElementById('data-nikah').style.display = '';
          } else {
            document.getElementById('data-nikah').style.display = 'none';
          }
    })

    $('#select-editkdkeluarga').on('select2:select', function(e){
        var data = e.params.data;
        var kode = data.id;
        if (kode == 1) {
            document.getElementById('editdata-nikah').style.display = '';
          } else if (kode == 2) {
            document.getElementById('editdata-nikah').style.display = '';
          } else {
            document.getElementById('editdata-nikah').style.display = 'none';
          }
    })

    /* Datatables */
    var table = $('#familyDatatable').DataTable({

        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'admin/api/family/get'+ '/' +satkerid + '/' + nip,
            error: function(req, err){ console.log('my message' + err); }
        },
        columns: [
            { data: 'kdsatker', name: 'kdsatker' },
            { data: 'nama_satker', name: 'nama_satker' },
            { data: 'nama', name: 'nama' },
            { data: 'tgllhr', name: 'tgllhr' },
            { data: 'nip', name: 'nip' },
            { data: 'posisi_keluarga', name: 'posisi_keluarga' },
            { data: 'id', name: 'id', className: 'text-center',
                render: function(data, type, row, meta){
                    var editButton = ''
                    // var editButton = '<a type="button" href="' + APIurl + 'admin/family/member/'+ data + '" title="Detail Anggota Keluarga" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button style="margin-right:1vw;">&nbsp;<i class="fa fa-address-card"></i></a>';
                    var editButton      = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button style="margin-right:1vw;">&nbsp;<i class="fa fa-address-card"></i></button>';
                    var removeButton    = '<button type="button" title="Remove" id="removeButton" data-id="'+data+'" class="label label-danger menu-archive-button style="margin-right:1vw;">&nbsp;<i class="fa fa-times"></i></button>';

                    // button = editButton;
                    button = editButton+'&nbsp'+removeButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        searching: false,
        paging: false,
        order: [[ 3, "asc" ]]
    });


    /* Trigger Datatables Family Member When Employee selected */

    $('#search-nip').on('select2:select', function (e) {
        var data = e.params.data;
        var satker = data.satker
        var unit = satker.substring(0, satker.indexOf("-"));
        var satkerid = data.satkerid;
        var nip = data.id;

        $("#nama").val(data.nama);
        $("#nip").val(data.id);
        $("#unit").val(unit);
        $("#satkerid").val(data.satkerid);

        $("#add-nip").val(data.id);
        $("#add-satkerid").val(data.satkerid);
        $("#add-satkername").val(data.satker);

        var year = data.id.substring(0, 4);
        var month = data.id.substring(4, 6);
        var day = data.id.substring(6, 8);
        var ttl = year+'-'+month+'-'+day;
        $("#date").val(ttl);
        document.getElementById('btn-family-member').style.display = '';
        // console.log(satkerid + ' - ' + nip);
        table.ajax.url('api/family/get/' +satkerid + '/' + nip).load();
    });

    /* Show Modal Add Family */
    $('.add-family-member').click(function(){
        $('#familymemberAddModal').modal('show');
    });

    /* Submit Add Family Member */
    $("#familymemberAddModal #submit-add").on("click", function(){
        var sendData = {
            "_token"        : $('meta[name="csrf_token"]').attr('content'),

            "kdsatker"      : $('#familymemberAddModal #add-satkerid').val(),
            "nama"          : $('#familymemberAddModal #add-nama').val(),
            "tgllhr"        : $('#familymemberAddModal #add-tgllahir').val(),
            "nip"           : $('#familymemberAddModal #add-nip').val(),
            "kdkeluarga"    : $('#familymemberAddModal #select-kdkeluarga').val(),
            "kdkerja"       : $('#familymemberAddModal #select-kdkerja').val(),
            "tglnikah"      : $('#familymemberAddModal #add-tglnikah').val(),
            "kddapat"       : $('#familymemberAddModal #add-tunjangan').is(':checked'),
            "no_srt_nikah"  : $('#familymemberAddModal #add-nosuratnikah').val(),
            "tgl_srt_nikah" : $('#familymemberAddModal #add-tglnikah').val(),
            "nmayah"        : $('#familymemberAddModal #add-namaayah').val(),
            "nmibu"         : $('#familymemberAddModal #add-namaibu').val(),
            "keterangan"    : $('#familymemberAddModal #add-keterangan').val(),
            "alamat"        : $('#familymemberAddModal #add-alamat').val(),
        }

        console.log(sendData)
        add(sendData);
    });

    /* Click Remove Data Family Member */
    $('#familyDatatable').on( 'click', '.menu-archive-button', function () {
        var title = "Apakah Anda Yakin Menghapus Data Anggota Keluarga?"
        var id = $(this).data('id')
        showCancelAlert(title, removeData, id)
    });

    /* Click Edit of Family Member */
    $('#familyDatatable').on( 'click', '.menu-edit-button', function () {
        $('#familymemberEditModal').modal('show');

        var id = $(this).data('id');

        get(base_url+"/admin/api/family/member/get/"+id, false, function(response){
            responsedata = response.data;
            $('#familymemberEditModal #edit-id').val(id);
            $('#familymemberEditModal #edit-nip').val(responsedata.nip);
            $('#familymemberEditModal #edit-satkerid').val(responsedata.kdsatker);
            $('#familymemberEditModal #edit-satkername').val(responsedata.nama_satker);
            $('#familymemberEditModal #edit-nama').val(responsedata.nama);
            $('#familymemberEditModal #edit-tgllahir').val(moment(responsedata.tgllhr).format('YYYY-MM-DD'));

            var kdkeluarga_selected = $("<option selected='selected'></option>").val(responsedata.kdkeluarga).text(responsedata.posisi_keluarga)
            $('#familymemberEditModal #select-editkdkeluarga').append(kdkeluarga_selected).trigger('change');

            if (responsedata.kdkeluarga == 1) {
                document.getElementById('editdata-nikah').style.display = '';
              } else if (responsedata.kdkeluarga == 2) {
                document.getElementById('editdata-nikah').style.display = '';
              } else {
                document.getElementById('editdata-nikah').style.display = 'none';
            }

            $('#familymemberEditModal #edit-tglnikah').val(moment(responsedata.tglnikah).format('YYYY-MM-DD'));

            var kdkerja_selected = $("<option selected='selected'></option>").val(responsedata.kdkerja).text(responsedata.pekerjaan)
            $('#familymemberEditModal #select-editkdkerja').append(kdkerja_selected).trigger('change');

            $('#familymemberEditModal #edit-namaayah').val(responsedata.nmayah);
            $('#familymemberEditModal #edit-namaibu').val(responsedata.nmibu);
            $('#familymemberEditModal #edit-alamat').val(responsedata.alamat);
            $('#familymemberEditModal #edit-keterangan').val(responsedata.keterangan);

            if (responsedata.kddapat==1){
                $('#familymemberEditModal #edit-tunjangan').prop("checked", true).change();
            } else {
                $('#familymemberEditModal #edit-tunjangan').prop("checked", false).change();
            }
        })
    });

    /* Click on Save Edit */
    $("#familymemberEditModal #submit-edit").on("click", function(){

        // console.log($('#familymemberEditModal #edit-id').val());

        let v_kddapat = '';

        if ($('#familymemberEditModal #edit-tunjangan').is(':checked')){
            v_kddapat = '1'
        } else {
            v_kddapat = '2'
        }

        var sendData = {
            "_token"        : $('meta[name="csrf_token"]').attr('content'),
            "id"            : $('#familymemberEditModal #edit-id').val(),
            "kdsatker"      : $('#familymemberEditModal #edit-satkerid').val(),
            "nama"          : $('#familymemberEditModal #edit-nama').val(),
            "tgllhr"        : $('#familymemberEditModal #edit-tgllahir').val(),
            "nip"           : $('#familymemberEditModal #edit-nip').val(),
            "kdkeluarga"    : $('#familymemberEditModal #select-editkdkeluarga').val(),
            "kdkerja"       : $('#familymemberEditModal #select-editkdkerja').val(),
            "tglnikah"      : $('#familymemberEditModal #edit-tglnikah').val(),
            "kddapat"       : v_kddapat,
            "no_srt_nikah"  : $('#familymemberEditModal #edit-nosuratnikah').val(),
            "tgl_srt_nikah" : $('#familymemberEditModal #edit-tglnikah').val(),
            "nmayah"        : $('#familymemberEditModal #edit-namaayah').val(),
            "nmibu"         : $('#familymemberEditModal #edit-namaibu').val(),
            "keterangan"    : $('#familymemberEditModal #edit-keterangan').val(),
            "alamat"        : $('#familymemberEditModal #edit-alamat').val(),
       }
       edit(sendData);
    });

});


function add(data) {
    send(base_url+"/admin/api/family/addMember", data, true, function(response){
        if(response.isSuccess){
            $('#familymemberAddModal').modal('hide');
            $('#familyDatatable').DataTable().ajax.reload();
        }
    })
}


function removeData(id) {
    // console.log(id);
    get(base_url+"/admin/api/family/removeMember/"+id, false, function(response){
        if(response.isSuccess){
            swal("Remove!", "Data Anggota Keluarga Berhasil dihapus", "success");
            $('#familyDatatable').DataTable().ajax.reload();
        }
    })
}


function edit(data) {
    // console.log(data);
    send(base_url+"/admin/api/family/editMember/" + data.id, data, true, function(response){
        if(response.isSuccess){
            $('#familymemberEditModal').modal('hide');
            $('#familyDatatable').DataTable().ajax.reload();
        }
    })
    // console.log(data.id);
}
