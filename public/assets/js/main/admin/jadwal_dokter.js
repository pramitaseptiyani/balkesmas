$(document).ready(function() {

    var id_dokter = document.getElementById('id_dokter').value;
    // console.log(id);

    /* Select Role */
    $('#select-hari').select2({
        theme: "classic",
        dropdownParent: $("#jadwalAddModal"),
          ajax: {
            dataType: 'json',
            url: base_path+'/admin/api/jadwal_dokter/selecthari/'+id_dokter,
            // delay: 800,
            data: function(params) {
                return {
                  role: params.term,
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result.data, function(index,item){
                  list.push({
                    id: item.id,
                    text: item.hari,
                  })
                });
                return {
                  results: list
                };
            },
        }
    });


    /* Add role*/
    $('.add-jadwal').click(function(){
        $('#jadwalAddModal').modal('show');
    });

/* ========================================================= */
/* ========================================================= */

    /* Active Permission Datatable */
    var table = $('#jadwalDokterDatatable').DataTable({
        paging: false,
        searching: false,
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'/admin/api/jadwal_dokter/list/' + id_dokter,
        },
        columns: [
            { data: 'nama', name: 'nama' },
            { data: 'hari', name: 'hari', className: 'text-center' },
            { data: 'jam_dari',  name: 'jam_dari', className: 'text-center' },
            { data: 'jam_sampai',  name: 'jam_sampai', className: 'text-center' },
            { data: 'id', name: 'id', className: 'text-center',
                render: function(data, type, row, meta){
                    var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
                    var archiveButton = '<button type="button" title="Remove" id="removeButton" data-id="'+data+'" class="label label-danger menu-archive-button style="margin-right:1vw;">&nbsp;<i class="fa fa-times"></i></button>';
                    button = editButton+'&nbsp'+archiveButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 0, "asc" ]],
    });

    /* Click Edit of patient */
    $('#jadwalDokterDatatable').on( 'click', '.menu-edit-button', function () {
        $('#jadwalEditModal').modal('show');

        var id = $(this).data('id');

        get(base_url+"/admin/api/jadwal_dokter/get/"+id, false, function(response){
            responsedata = response.data;

            // var hari_selected = $("<option selected='selected'></option>").val(responsedata.id_hari).text(responsedata.hari)
            // $("#edit-hari").append(hari_selected).trigger('change');

            $('#jadwalEditModal #edit-hari').val(responsedata.hari);
            $('#jadwalEditModal #edit-id').val(id);
            $('#jadwalEditModal #edit-jam_dari').val(responsedata.jam_dari);
            $('#jadwalEditModal #edit-jam_sampai').val(responsedata.jam_sampai);
        })
    });

    /* Click Activate User */
    $('#jadwalDokterDatatable').on( 'click', '.menu-archive-button', function () {
        var title = "Apakah Anda Yakin Menghapus Jadwal?"
        var id = $(this).data('id')
        showCancelAlert(title, removeJadwal, id)
    })
    /**/

    $("#jadwalAddModal #submit-add").on("click", function(){
        var sendData = {
            "_token"                : $('#jadwalAddModal #add-token').val(),
            "id_user_dokter"        : $('#jadwalAddModal #add-id').val(),
            "id_hari"               : $('#jadwalAddModal #select-hari').val(),
            "jam_dari"              : $('#jadwalAddModal #add-jam_dari').val(),
            "jam_sampai"            : $('#jadwalAddModal #add-jam_sampai').val(),
        }

        console.log(sendData)
        add(sendData);
    });

    /* Click on Save Edit */
    $("#jadwalEditModal #submit-edit").on("click", function(){

        console.log($('#jadwalEditModal #edit-id').val());

        var sendData = {
            "_token"                : $('#jadwalEditModal #edit-token').val(),
            "id"                    : $('#jadwalEditModal #edit-id').val(),
            "jam_dari"              : $('#jadwalEditModal #edit-jam_dari').val(),
            "jam_sampai"            : $('#jadwalEditModal #edit-jam_sampai').val(),
       }

       console.log(sendData)
       edit(sendData);
    });



});

function add(data) {
    send(base_url+"/admin/api/jadwal_dokter/add", data, true, function(response){
        if(response.isSuccess){
            $('#jadwalAddModal #add-id').val(""),
            $('#jadwalAddModal #select-hari').val(""),
            $('#jadwalAddModal #add-jam_dari').val(""),
            $('#jadwalAddModal #add-jam_sampai').val(""),
            $('#jadwalAddModal').modal('hide');
            $('#jadwalDokterDatatable').DataTable().ajax.reload();
        }
    })
}

function edit(data) {
    send(base_url+"/admin/api/jadwal_dokter/edit/"+data.id, data, true, function(response){
        if(response.isSuccess){
            $('#jadwalEditModal').modal('hide');
            $('#jadwalDokterDatatable').DataTable().ajax.reload();
        }
    })
}


function removeJadwal(id) {
    get(base_url+"/admin/api/jadwal_dokter/remove/"+id, false, function(response){
        if(response.isSuccess){
            swal("Remove!", "Jadwal Berhasil dihapus", "success");
            $('#jadwalDokterDatatable').DataTable().ajax.reload();
        }

    })
}

