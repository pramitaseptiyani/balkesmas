$(document).ready(function() {
    /* Add role*/
    $('.add-role').click(function(){
        $('#roleAddModal').modal('show');
    });

    /* Select2 */
    $(".multiple-menu").select2({
        placeholder: "Pilih Menu"
    });

    /* Select2 Menu on Add Role */
    get(base_url+"/admin/api/menu/select2/", false, function(response){
        var responsedata = response.data;

        var multiple_select2_html = '';
        responsedata.forEach(function (menu, index) {
            if(menu.type == "menu"){
                add_multiple = '<optgroup label="'+menu.name+'">';
                multiple_select2_html += add_multiple;

                if(menu.submenu != []){
                    menu.submenu.forEach(function (submenu, indexSub) {
                        sub_multiple = '<option value='+submenu.id+'>'+submenu.name+'</option>';
                        multiple_select2_html += sub_multiple;
                    })
                }

                var add_multiple = '</optgroup>';
                multiple_select2_html += add_multiple;
            }

            $('#multiple-menu-add').html(multiple_select2_html)
        })
    })

    /* Select2 Menu on Edit Role */
    get(base_url+"/admin/api/menu/select2/", false, function(response){
        var responsedata = response.data;

        var multiple_select2_html = '';
        responsedata.forEach(function (menu, index) {
            if(menu.type == "menu"){
                add_multiple = '<optgroup label="'+menu.name+'">';
                multiple_select2_html += add_multiple;

                if(menu.submenu != []){
                    menu.submenu.forEach(function (submenu, indexSub) {
                        sub_multiple = '<option value='+submenu.id+'>'+submenu.name+'</option>';
                        multiple_select2_html += sub_multiple;
                    })
                }

                var add_multiple = '</optgroup>';
                multiple_select2_html += add_multiple;
            }

            $('#multiple-menu-edit').html(multiple_select2_html)
        })
    })
    /**/

/* ========================================================= */
/* ========================================================= */

    /* Active Permission Datatable */
    var table = $('#activePermissionDatatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'/admin/api/permission/list/1',
        },
        columns: [        
            { data: 'role', name: 'role' },
            { data: 'menu', name: 'menu', className: 'text-center',
                render: function(data, type, row, meta){
                    var html_menus = '';
                    data.forEach(function (value, index) {
                        label_menu = '<button type="button" class="btn btn-xs purple btn-outline m-b-10 btn-circle">'+
                                        value+
                                    '</button>';
                        html_menus = html_menus+label_menu;
                    });

                    return html_menus;
                },
                searchable: true,
                sortable: true
            },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
                    var archiveButton = '<button type="button" title="Non Aktif" id="editButton" data-id="'+data+'" class="label label-danger menu-archive-button style="margin-right:1vw;">&nbsp;<i class="fa fa-times"></i></button>';
                    button = editButton+'&nbsp'+archiveButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 0, "asc" ]]
    });


    /*Click Edit on Active Permission Datatable */
    $('#activePermissionDatatable').on( 'click', '.menu-edit-button', function () {
        $('#roleEditModal').modal('show');

        var id = $(this).data('id');
        get(base_url+"/admin/api/permission/get/"+id, false, function(response){
            responsedata = response.data;
            console.log(responsedata)

            $('#roleEditModal #edit-id').val(responsedata.id);
            $('#roleEditModal #edit-role').val(responsedata.role);
            
            if(responsedata.is_aktif == 1)
                $('#edit-aktif').prop('checked', true).change();
            else $('#edit-aktif').prop('checked', false).change();

            $("#multiple-menu-edit")[0].selectedIndex = -1;
            $('#multiple-menu-edit').val(responsedata.menu_ids).change();   
        })   
    });


/* ========================================================= */
/* ========================================================= */

    /* Archive Permission Datatable */
    var table = $('#archivePermissionDatatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'/admin/api/permission/list/0',
        },
        columns: [        
            { data: 'role', name: 'role' },
            { data: 'menu', name: 'menu', className: 'text-center',
                render: function(data, type, row, meta){
                    var html_menus = '';
                    data.forEach(function (value, index) {
                        label_menu = '<button type="button" class="btn btn-xs purple btn-outline m-b-10 btn-circle">'+
                                        value+
                                    '</button>';
                        html_menus = html_menus+label_menu;
                    });

                    return html_menus;
                },
                searchable: true,
                sortable: true
            },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
                    var activateButton = '<button type="button" title="Aktifkan" id="activateButton" data-id="'+data+'" class="label label-success menu-activate-button style="margin-right:1vw;">&nbsp;<i class="fa fa-check"></i></button>';
                    button = editButton+'&nbsp'+activateButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 0, "asc" ]]
    });

    /* Click Edit on Archive Permission Datatable */
    $('#archivePermissionDatatable').on( 'click', '.menu-edit-button', function () {
        $('#roleEditModal').modal('show');

        var id = $(this).data('id');
        get(base_url+"/admin/api/permission/get/"+id, false, function(response){
            responsedata = response.data;
            console.log(responsedata)

            $('#roleEditModal #edit-id').val(responsedata.id);
            $('#roleEditModal #edit-role').val(responsedata.role);

            if(responsedata.is_aktif == 1)
                $('#edit-aktif').prop('checked', true).change();
            else $('#edit-aktif').prop('checked', false).change();

            $("#multiple-menu-edit")[0].selectedIndex = -1;
            $('#multiple-menu-edit').val(responsedata.menu_ids).change();   
        })   
    });

    /* Click Archive User */
    $('#activePermissionDatatable').on( 'click', '.menu-archive-button', function () {
        var title = "Apakah Anda Yakin Menonaktifkan Role Ini?"
        var id = $(this).data('id')
        showCancelAlert(title, archiveRole, id)        
    })
    /**/

    /* Click Activate User */
    $('#archivePermissionDatatable').on( 'click', '.menu-activate-button', function () {
        var title = "Apakah Anda Yakin Mengaktifkan Role?"
        var id = $(this).data('id')
        showCancelAlert(title, activateRole, id)        
    })
    /**/

    $("#roleAddModal #submit-add").on("click", function(){
        var sendData = {
            "_token"        : $('#roleAddModal #add-token').val(),
            "role"          : $('#roleAddModal #add-role').val(),
            "menu"          : $('#roleAddModal #multiple-menu-add').val(),
            "is_aktif"      : $('#roleAddModal #add-aktif').is(':checked'),
        }
        add(sendData);
    });

    /* Submit Edited Values */
    $("#roleEditModal #submit-edit").on("click", function(){
        var id = $(this).data('id');

        var sendData = {
            "_token"              : $('#roleEditModal #edit-token').val(),
            "id_role"             : $('#roleEditModal #edit-id').val(),
            "role"                : $('#roleEditModal #edit-role').val(),
            "is_aktif"            : $('#edit-aktif').is(':checked'),
            "menus"               : $('#roleEditModal #multiple-menu-edit').val(),
        }
        edit(sendData);
    });

});

function add(data) {
    send(base_url+"/admin/api/permission/add", data, true, function(response){
        if(response.isSuccess){
            $('#roleAddModal').modal('hide');
            $('#archivePermissionDatatable').DataTable().ajax.reload();   
            $('#activePermissionDatatable').DataTable().ajax.reload();    
        }
    })
}

function edit(data) {
    send(base_url+"/admin/api/permission/edit", data, true, function(response){
        if(response.isSuccess){
            $('#roleEditModal').modal('hide');
            $('#archivePermissionDatatable').DataTable().ajax.reload();   
            $('#activePermissionDatatable').DataTable().ajax.reload();    
        }
    })
}

function archiveRole(id) {
    get(base_url+"/admin/api/role/archive/"+id, false, function(response){
        if(response.isSuccess){
            swal("Dinonaktifkan!", "Role Berhasil Dinonaktifkan", "success");
            $('#archivePermissionDatatable').DataTable().ajax.reload();   
            $('#activePermissionDatatable').DataTable().ajax.reload();
        }
    })
}

function activateRole(id) {
    get(base_url+"/admin/api/role/activate/"+id, false, function(response){
        if(response.isSuccess){
            swal("Diaktifkan!", "Role Berhasil diaktifkan", "success");
            $('#archivePermissionDatatable').DataTable().ajax.reload();   
            $('#activePermissionDatatable').DataTable().ajax.reload();
        }
    })
    console.log("Activate "+id)
}