$(document).ready(function() {
	
	$("#simpan").click(function(){
        console.log($('#token').val())

        var formData = {
        	"_token"				         : $('#token').val(),
            'id_periksa_dokter'		 : $('#id_periksa_dokter').val(),
            'sanamnesa'				     : $('#sanamnesa').val(),
            'oanamnesa'				   : $('#oanamnesa').val(),
            'aanamnesa'			     : $('#aanamnesa').val(),
            'panamnesa'          : $('#panamnesa').val(),
          }

          console.log(formData)

          //cek sudah input anamnesa belum
          var narray=0;
          var idtr = $('#id_periksa_dokter').val();
          get(base_url+"/doctor/cekanamnesa/"+idtr, false, function(response){
              responsedata = response.data;
              narray = responsedata.length;
              console.log(idtr, narray);
              console.log(responsedata);

              if (narray != 0) {
                  send(base_url+"/doctor/update_periksa", formData, true, function(response){
                    console.log(response)
                  });
              } else {
                send(base_url+"/doctor/simpan_periksa", formData, true, function(response){
                  console.log(response)
                });
              }
          })
    })

    $("#btndoctorclose").click(function(){
		$('#patientEditModal').modal('hide');
		location.reload();
    })
    
});

