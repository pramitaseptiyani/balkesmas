$(document).ready(function() {

    var table = $('#patientDoctorDatatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'doctor/get_list_pasien_dokter',
        },
        columns: [      
            { data: 'antrian', name: 'antrian' },
            { data: 'nmpoli', name: 'nmpoli' },
            { data: 'nip', name: 'nip' },
            { data: 'nmpasien', name: 'nmpasien' },
            { data: 'tgl_periksa', name: 'tgl_periksa' },
            { data: 'status_periksa', name: 'status_periksa', className: 'text-center', 
                render: function(data, type, row, meta){ ;
                    if (data==0)
                        return '<span class="btn btn-xs btn-circle btn-success">Antri</span>';
                    else return '<span class="btn btn-xs btn-circle btn-warning">Selesai</span>';
                },
                searchable: false,
                sortable: false
            },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var id= row["id"] ;
                    var idtr= row["id_tr_antrian_poli"];
                   //console.log(id, idtr);
                    var seeButton = '<button type="button" title="Lihat" id="downloadButton" data-id="'+id+'" class="btn btn-xs btn-info menu-see-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>';
                    var editButton = '<button type="button" title="Periksa" id="editButton" data-id="'+id+'" data-idtr="'+idtr+'" class="btn btn-xs btn-danger menu-edit-button download-"'+data+' style="margin-right:1vw;">Periksa</button>';
                    button = seeButton+editButton;

                    return button;
                },
                searchable: false,
                sortable: false
            },
            { data: 'id', name: 'done', className: 'text-center', 
                render: function(data, type, row, meta){
                    var id= row["id"] ;
                    var idtr= row["id_tr_antrian_poli"];
                   //console.log(id, idtr);
                    var doneButton = '<button type="button" id="doneButton" data-id="'+id+'" data-idtr="'+idtr+'" data-idrujuk="" class="btn btn-xs btn-warning menu-done-button download-"'+data+' style="margin-right:1vw;">Done</button>';
                    //button = seeButton+'&nbsp;'+editButton;

                    return doneButton;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 2, "asc" ]]
    });


    //patientrujukan tablle
    var table = $('#patientRujukanDatatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'doctor/get_data_antrian_rujukan_poli',
        },
        columns: [      
            { data: 'id_tr_rujukan', name: 'id_tr_rujukan' },
            { data: 'asal_poli', name: 'asal_poli' },
            { data: 'nip', name: 'nip' },
            { data: 'nama', name: 'nama' },
            { data: 'tanggal_rujukan', name: 'tanggal_rujukan' },
            { data: 'is_done', name: 'is_done', className: 'text-center', 
                render: function(data, type, row, meta){ ;
                    if (data==0)
                        return '<span class="btn btn-xs btn-circle btn-success">Antri</span>';
                    else return '<span class="btn btn-xs btn-circle btn-warning">Selesai</span>';
                },
                searchable: false,
                sortable: false
            },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var id= row["id"] ;
                    var idtr= row["id_tr_antrian_poli"];
                   //console.log(id, idtr);
                    var seeButton = '<button type="button" title="Lihat" id="downloadButton" data-id="'+id+'" class="btn btn-xs btn-info menu-see-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>';
                    var editButton = '<button type="button" title="Periksa" id="editButton" data-id="'+id+'" data-idtr="'+idtr+'" class="btn btn-xs btn-danger menu-edit-button download-"'+data+' style="margin-right:1vw;">Periksa</button>';
                    button = seeButton+editButton;

                    return button;
                },
                searchable: false,
                sortable: false
            },
            { data: 'id', name: 'done', className: 'text-center', 
                render: function(data, type, row, meta){
                    var id= row["id"] ;
                    var idtr= row["id_tr_antrian_poli"];
                    var idrujuk= row["id_tr_rujukan"];
                   //console.log(id, idtr);
                    var doneButton = '<button type="button" id="doneButton" data-id="'+id+'" data-idtr="'+idtr+'" data-idrujuk="'+idrujuk+'" class="btn btn-xs btn-warning menu-done-button download-"'+data+' style="margin-right:1vw;">Done</button>';
                    //button = seeButton+'&nbsp;'+editButton;

                    return doneButton;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 2, "asc" ]]
    });

    //end of patientrujukan table



    //Click Done Action of patient
    $('.antriandoctor').on( 'click', '.menu-done-button', function () {
        $("#mi-modal").modal('show');

        var idtr = $(this).data('idtr');
        console.log('idtr', idtr);
        var idrujuk = $(this).data('idrujuk');
          
       var modalConfirm = function(callback){
  
          $("#modal-btn-ya").on("click", function(){
            callback(true);
            $("#mi-modal").modal('hide');
          });
          
          $("#modal-btn-no").on("click", function(){
            callback(false);
            $("#mi-modal").modal('hide');
          });
        };

        modalConfirm(function(confirm){
          if(confirm){
            //aksi button ya
            $("#result").html("<div class='alert alert-success' role='alert'>Pasien Telah Selesai Diperiksa!</div>");

            if(idrujuk != "") {
                get(base_url+"/doctor/donePatientRujuk/"+idrujuk, false, function(response){
                    responsedata = response.data;
                });
            } else {
                get(base_url+"/doctor/donePatient/"+idtr, false, function(response){
                    responsedata = response.data;
                });
            }

            location.reload();   
          }
        });

    });


    //Click See detail of patient
    $('.antriandoctor').on( 'click', '.menu-see-button', function () {
        $('#patientDetailModal').modal('show');

        var id = $(this).data('id');

        get(base_url+"/doctor/getPatient/"+id, false, function(response){
            responsedata = response.data;

            $('#patientDetailModal #detail-MR').val(responsedata.patient_mr_number);
            $('#patientDetailModal #detail-nama').val(responsedata.patient_nm);
            $('#patientDetailModal #detail-nip').val(responsedata.patient_nip);
            $('#patientDetailModal #detail-bpjs').val(responsedata.bpjs);
            $('#patientDetailModal #detail-ttl').val(responsedata.patient_ttl);
            $('#patientDetailModal #detail-unit').val(responsedata.patient_unit);
            $('#patientDetailModal #detail-email').val(responsedata.patient_mail);
            $('#patientDetailModal #detail-hp').val(responsedata.patient_phone);
            $('#patientDetailModal #detail-address').val(responsedata.patient_address);
            $('#patientDetailModal #detail-riwayat-penyakit').val(responsedata.diag_diderita_old);
            $('#patientDetailModal #detail-penyakit-turunan').val(responsedata.diag_turunan);
            $('#patientDetailModal #detail-riwayat-alergi').val(responsedata.alergi_rwy);
            $('#patientDetailModal #detail-riwayat-vaksinasi').val(responsedata.vaksinasi_rwy);
        })   
    });

    //Click Edit detail of patient
    $('.antriandoctor').on( 'click', '.menu-edit-button', function () {
        $('#patientEditModal').modal('show');  
        document.getElementById("editButton").disabled = true; 

        var id = $(this).data('id');
        document.getElementById('id_pasien_antri').value = id;
        var idtr = $(this).data('idtr');

        var btnresep = document.getElementById("btnresep");
        btnresep.setAttribute('data-idtr', idtr);

        var btnlanjutan = document.getElementById("btnlanjutan");
        btnlanjutan.setAttribute('data-id', id);
        btnlanjutan.setAttribute('data-idtr', idtr);
      
        $('#patientEditModal #id_periksa_dokter').val(idtr);
        document.getElementById("id_periksa_dokter").readOnly = true;

        //cek sudah input anamnesa belum
        var narray=0;
        get(base_url+"/doctor/cekanamnesa/"+idtr, false, function(response){
            responsedata = response.data;
            narray = responsedata.length;
            console.log(idtr, narray);
            console.log(responsedata);

            if (narray != 0) {
                $('#patientEditModal #sanamnesa').val(responsedata[0].sanamnesa);
                $('#patientEditModal #oanamnesa').val(responsedata[0].oanamnesa);
                $('#patientEditModal #aanamnesa').val(responsedata[0].aanamnesa);
                $('#patientEditModal #panamnesa').val(responsedata[0].panamnesa);
            }
        })

        get(base_url+"/doctor/getPatient/"+id, false, function(response){
            responsedata = response.data;
            $('#patientEditModal #edit-MR').val(responsedata.patient_mr_number);
            $('#patientEditModal #edit-nama').val(responsedata.patient_nm);
            $('#patientEditModal #edit-nip').val(responsedata.id_card);
            $('#patientEditModal #edit-bpjs').val(responsedata.bpjs);
            $('#patientEditModal #edit-ttl').val(responsedata.patient_ttl);
            $('#patientEditModal #edit-unit').val(responsedata.patient_unit);
            $('#patientEditModal #edit-email').val(responsedata.patient_mail);
            $('#patientEditModal #edit-hp').val(responsedata.patient_phone);
            $('#patientEditModal #edit-address').val(responsedata.patient_address);
        }) 

        var table = $('#riwayatperiksadatatable').DataTable({
            
            ajax: {
                type: "GET",
                dataType: "json",
                url: APIurl+'doctor/get_history_dokter/'+id, 
            },
            columns: [        
                { data: 'tgl_periksa', name: 'tgl_periksa' },
                { data: 'nama_poli', name: 'nama_poli' },
                { data: 'nama_perawat', name: 'nama_perawat' },
                { data: 'keluhan', name: 'keluhan' },
                { data: 'nama_dokter', name: 'nama_dokter' },
                { data: 'sanamnesa', name: 'sanamnesa' },
                { data: 'id', name: 'id', className: 'text-center', 
                    render: function(data, type, row, meta){
                        var seeButton = '<button type="button" title="Lihat" id="downloadButton" data-id="'+data+'" class="btn btn-xs btn-info menu-see-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>';
                        button = seeButton;

                        return button;
                    },
                    searchable: false,
                    sortable: false
                }
            ],
            order: [[ 2, "asc" ]]
        });

        var table = $('#riwayatresepdatatable').DataTable({
            
            ajax: {
                type: "GET",
                dataType: "json",
                url: APIurl+'doctor/get_history_resep/'+id, 
            },
            columns: [        
                { data: 'tgl_resep', name: 'tgl_resep' },
                { data: 'nama_poli', name: 'nama_poli' },
                { data: 'nama', name: 'nama' },
                { data: 'aturan_pakai', name: 'aturan_pakai' },
                { data: 'jumlah_obat', name: 'jumlah_obat' },
                { data: 'satuan', name: 'satuan' },
                { data: 'id', name: 'id', className: 'text-center', 
                    render: function(data, type, row, meta){
                        var seeButton = '<button type="button" title="Lihat" id="downloadButton" data-id="'+data+'" class="btn btn-xs btn-info menu-see-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>';
                        button = seeButton;

                        return button;
                    },
                    searchable: false,
                    sortable: false
                }
            ],
            order: [[ 2, "asc" ]] 
        });

        var table = $('#riwayatlabdatatable').DataTable({
            
            ajax: {
                type: "GET",
                dataType: "json",
                url: APIurl+'doctor/get_history_lab/'+id, 
            },
            columns: [        
                { data: 'id_tr_periksa_lanjut', name: 'id_tr_periksa_lanjut' },
                { data: 'tgl_pemeriksaan', name: 'tgl_pemeriksaan' },
                { data: 'no_lab', name: 'no_lab' },
                { data: 'nama_dokter', name: 'nama_dokter' },
                { data: 'nama1', name: 'nama1'},
                { data: 'hasil', name: 'hasil' },
                { data: 'nilai_rujukan', name: 'nilai_rujukan' },
                { data: 'id', name: 'id', className: 'text-center', 
                    render: function(data, type, row, meta){
                        var seeButton = '<button type="button" title="Lihat" id="downloadButton" data-id="'+data+'" class="btn btn-xs btn-info menu-see-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>';
                        button = seeButton;

                        return button;
                    },
                    searchable: false,
                    sortable: false
                }
            ],
            order: [[ 2, "asc" ]]
        }); 

        var table = $('#riwayatfisiodatatable').DataTable({
            
            ajax: {
                type: "GET",
                dataType: "json",
                url: APIurl+'doctor/get_history_fisio/'+id, 
            },
            columns: [        
                { data: 'id_tr_periksa_lanjut', name: 'id_tr_periksa_lanjut' },
                { data: 'tgl_periksa', name: 'tgl_periksa' },
                { data: 'no_lab', name: 'no_lab' },
                { data: 'nama_dokter', name: 'nama_dokter' },
                { data: 'nama_fisio', name: 'nama_fisio' },
                { data: 'hasil', name: 'hasil' },
                { data: 'nilai_rujukan', name: 'nilai_rujukan' },
                { data: 'id', name: 'id', className: 'text-center', 
                    render: function(data, type, row, meta){
                        var seeButton = '<button type="button" title="Lihat" id="downloadButton" data-id="'+data+'" class="btn btn-xs btn-info menu-see-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>';
                        button = seeButton;

                        return button;
                    },
                    searchable: false,
                    sortable: false
                }
            ],
            order: [[ 2, "asc" ]]
        });


        //insert data resep otomatis
        //cek sudah input data resep belum
        var narray=0;
            get(base_url+"/doctor/cek_tr_resep/"+idtr, false, function(response){
                responsedata = response.data;
                narray = responsedata.length;
                console.log(idtr, narray);
     
                var larray=0;
                for (var i=0; i<narray; i++) {
                    var row = document.getElementById('tta');
                    if (row !== null) {
                      row.parentNode.removeChild(row); 
                    }

                  get(base_url+"/doctor/cek_tr_det_resep/"+responsedata[i].id, false, function(response){
                    responsedata = response.data;
                    console.log(responsedata);
                      larray= responsedata.length;
                      var stock = [];

                      for (var j=0; j<larray; j++) {
                          var no = j+1;
                          var newRow = $("<tbody class=ttb"+j+">");
                          var cols = "";
                          var counter = j+1;

                          console.log('jcol',j, counter);

                        // cols += '<tr style="vertical-align:top;"><td id="noangka" rowspan="3" style="width: 3%;">'+no+'. </td>';
                        cols += '<td style="width: 30%;"><input style="width: 100%;height: 50px;  border-color:#98d9eb;" type="text" name="obnama[]" id="obnama'+counter+'" value="'+responsedata[j].nama+'" class="obnama" data-obnama="'+counter+'"/></td>';
                        cols += '<td style="width: 5%;"><input type="text" style="width: 100%;height: 50px; background-color:#dedad9; border-color:#dedad9;" name="obid[]" id="obid'+counter+'" class="obid" value="'+responsedata[j].id_obat+'" data-obid="'+counter+'" readonly/></td>';
                        cols += '<td style="width: 13%;"><input type="text" style="width: 100%;height: 50px; background-color:#dedad9; border-color:#dedad9;" name="obsat[]" id="obsat'+counter+'" class="obsat" value="'+responsedata[j].satuan+'" data-obsat="'+counter+'" readonly/></td>';
                        cols += '<td style="width: 10%;"><input type="text" style="width: 100%;height: 50px; background-color:#dedad9; border-color:#dedad9;" name="obstock[]" id="obstock'+counter+'" class="obstock" value="" data-obstock="'+counter+'" readonly/></td>';
                        cols += '<td style="width: 6%;"><input type="text" style="width: 100%;height: 50px; border-color:#98d9eb;" name="objumlah[]" value="'+responsedata[j].jumlah_obat+'" id="objumlah'+counter+'" /></td>';
                        cols += '<td style="width: 30%;"><textarea class="form-control" style="width: 100%;height: 50px; border-color:#98d9eb;" id="obaturan'+counter+'" name="obaturan[]">'+responsedata[j].aturan_pakai+'</textarea></td>';
                        cols += '<td rowspan="3"><input size="3" type="button" class="ibtnDel btn btn-sm btn-danger "  value="x" data-ref="'+j+'" onClick="del('+j+')"></td></tr>';

                          newRow.append(cols);
                          $("table.listobat").append(newRow); 
                      }
                            updatestock(responsedata,counter);
                            var counter = larray;
                            document.getElementById("counter").value = counter;

                  })
                }
            })
    });


    $("#btnclosedetail").click(function(){
        $('#patientDetailModal').modal('hide');
        location.reload();
    }) 

    $("#btncloseedit").click(function(){
        $('#patientEditModal').modal('hide');
        location.reload();
    }) 

});


function hideshowlab(row) {
    var x = document.getElementById("lab-card-"+row).style.display;

    if (x == "none") {
        document.getElementById("lab-card-"+row).style.display = "block";
    } else {
        document.getElementById("lab-card-"+row).style.display = "none";
    }
}



function edit(data) {
    send(base_url+"/receptionist/patient/edit", data, true, function(response){
        setTimeout(function(){
            location.reload()
        }, 500);
    })
}

function cekautocomplete(id) {
    // //autocomplete
    var arrnama = [];
    get(base_url+"/doctor/searchobatnama", false, function(response){
        responsedata = response.data;
        var i;
          for (i = 0; i < responsedata.length; i++) {
            arrnama[i] = responsedata[i].nama;
          }
        
        var arrNama = JSON.parse(JSON.stringify(arrnama));
        //var obnamaid = "obnama"+j;
        console.log(id, arrNama);
        autocomplete(document.getElementById("obnama"+id), arrNama);
      })    
}

function updatestock(responsedata,counter) {
    
    var l = responsedata.length;
    console.log("fupdatestock",l, responsedata);

    var d=0;
    for (i=0;i<l;i++) {
        var idobat = responsedata[i].id_obat;
        console.log("idobat i ", i, idobat);
        
        get(base_url+"/doctor/cekstock_resep/"+idobat, false, function(response){
            xxx = response.data;
            var stock = xxx[0].stock;
            d = d+1;
                  console.log("stock counter ",d,stock);
                  $('input[id=obstock'+d+']').val(stock);
        })
    }

   document.getElementById("addobat").removeAttribute("disabled");
}