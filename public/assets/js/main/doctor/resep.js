$(document).ready(function() {

	$("#btnresep").click(function(){
      $('#patientEditModal').modal('hide');
		  $('#resepModal').modal('show');


	    var idtr = $(this).data('idtr');
	  
	    $('#resepModal #idperiksadokter_resep').val(idtr);
	    document.getElementById("idperiksadokter_resep").readOnly = true;

	    $('#resepModal #petugas_farmasi').val("Petugas Lab Farmasi"); //petugasnya di ganti
	    document.getElementById("petugas_farmasi").readOnly = true;

	    //tanggal resep
	    var tgl = new Date();
  		var y = tgl.getFullYear();
  		var mm = String(tgl.getMonth() + 1).padStart(2, '0'); 
  		var dd = String(tgl.getDate()).padStart(2, '0');
  		var h = tgl.getHours();
  		var mi = tgl.getMinutes();
  		$('#resepModal #tglresep').val(y + "-" + mm + "-" + dd + " " + h + ":" + mi);

        
        //simpan resep
        $("#simpanresep").click(function(){
              //cek sudah input data resep belum
              var narray=0;
              get(base_url+"/doctor/cek_tr_resep/"+idtr, false, function(response){
                  responsedata = response.data;
                  narray = responsedata.length;
                  // console.log(idtr, narray);
                  // console.log(responsedata);


                  if (narray != 0) {
                      //delete detil resep dulu
                      for (var i=0; i<narray; i++) {
                        get(base_url+"/doctor/del_tr_det_resep/"+responsedata[i].id, false, function(response){
                          responsedata = response.data;
                          // console.log(responsedata);
                        })
                      }

                      //delete tr resep
                      get(base_url+"/doctor/del_tr_resep/"+idtr, false, function(response){
                        responsedata = response.data;
                        // console.log(responsedata);
                      })
                  }

                  //insert data detil resep
                  var obid = [];
                  var obaturan = [];
                  var objumlah = [];

                  $("input[name='obid[]']").each(function() {
                      obid.push($(this).val());
                  });
                  $("textarea[name='obaturan[]']").each(function() {
                      obaturan.push($(this).val());
                  });
                  $("input[name='objumlah[]']").each(function() {
                      objumlah.push($(this).val());
                  });

                  // console.log($('#token').val())

                  var formData = {
                    "_token"                    : $('#token').val(),
                      'idperiksadokter_resep'   : $('#idperiksadokter_resep').val(),
                      'petugas_farmasi'         : $('#petugas_farmasi').val(),
                      'tglresep'                : $('#tglresep').val(),
                      'obid'                    : obid,
                      'obaturan'                : obaturan,
                      'objumlah'                : objumlah,
                    }

                    console.log("form resep",formData)

                    send(base_url+"/doctor/simpanresep", formData, true, function(response){
                      console.log("respon form resep simpan",response)
                    });

                    // send(base_url+"/doctor/simpanresep", formData, true, function(response){
                    //   console.log(response)
                    // });
                  
              })
        })

        //cetak resep
        $("#cetakresep").click(function(){
              var id = $('#idperiksadokter_resep').val();
              var a = document.createElement('a');
              a.href="/doctor/cetak_resep/"+id;
              a.target = '_blank';
              document.body.appendChild(a);
              a.click();
        })

          $("#btndoctorclose").click(function(){
          $('#patientEditModal').modal('hide');
          location.reload();
          }) 

    })

    $("#closemdlresep").click(function(){
  		$('#resepModal').modal('hide');
  		$('#patientEditModal').modal('show');
    })

});

//start autocomplete resep

function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;

              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
              
              
              var countcek = inp.id;
              // var nameobj = inp.name;
              var cek = document.getElementById(countcek).className;
              var parameter = inp.value;
              var counter = countcek.split(cek);
              autofill(cek, counter[1], parameter, countcek);
    
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target); 
  });
}


function tambahdata() {
    var row = document.getElementById('tta');
    if (row !== null) {
      row.parentNode.removeChild(row); 
    }

    var counter = document.getElementById("counter").value;
    counter++;
   
    var newRow = $("<tbody class=ttb"+counter+">");
    var cols = "";

    // cols += '<tr style="vertical-align:top;"><td id="noangka" rowspan="3" style="width: 3%;">'+counter+'. </td>';
    cols += '<tr style="vertical-align:top;"><td style="width: 30%;"><input style="width: 100%;height: 50px;  border-color:#98d9eb;" type="text" name="obnama[]" id="obnama'+counter+'" placeholder="Ketik Nama Obat.." class="obnama" data-obnama="'+counter+'"/></td>';
    cols += '<td style="width: 5%;"><input type="text" style="width: 100%;height: 50px; background-color:#dedad9; border-color:#dedad9;" name="obid[]" id="obid'+counter+'" class="obid" data-obid="'+counter+'" readonly/></td>';
    cols += '<td style="width: 13%;"><input type="text" style="width: 100%;height: 50px; background-color:#dedad9; border-color:#dedad9;" name="obsat[]" id="obsat'+counter+'" class="obsat" data-obsat="'+counter+'" readonly/></td>';
    cols += '<td style="width: 10%;"><input type="text" style="width: 100%;height: 50px; background-color:#dedad9; border-color:#dedad9;" name="obstock[]" id="obstock'+counter+'" class="obstock" data-obstock="'+counter+'" readonly/></td>';
    cols += '<td style="width: 6%;"><input type="text" style="width: 100%;height: 50px; border-color:#98d9eb;" name="objumlah[]" id="objumlah'+counter+'" /></td>';
    cols += '<td style="width: 30%;"><textarea class="form-control" style="width: 100%;height: 50px; border-color:#98d9eb;" id="obaturan'+counter+'" name="obaturan[]"></textarea></td>';
    cols += '<td rowspan="3"><input size="3" type="button" class="ibtnDel btn btn-sm btn-danger "  value="x" data-ref="'+counter+'" onClick="del('+counter+')"></td></tr>';

    newRow.append(cols);

    $("table.listobat").append(newRow);    
    document.getElementById("counter").value=counter;
    document.getElementById("obid"+counter).readOnly = true;


    //autocomplete
    var arrnama = [];
    get(base_url+"/doctor/searchobatnama", false, function(response){
        responsedata = response.data;
        var i;
          for (i = 0; i < responsedata.length; i++) {
            arrnama[i] = responsedata[i].nama;
          }
        
        var arrNama = JSON.parse(JSON.stringify(arrnama));
        // console.log(arrNama);
        autocomplete(document.getElementById("obnama"+counter), arrNama);
      })  
}

function del(counter) {
    $("tbody.ttb"+counter).remove();
}

function autofill(cek, counter, parameter, id) {

  get(base_url+"/doctor/cekobatid/"+parameter, false, function(response){
    responsedata = response.data;
    // console.log(responsedata);
    var idobat = responsedata[0].id;

    $('input[id=obid'+counter+']').val(responsedata[0].id);
    $('input[id=obsat'+counter+']').val(responsedata[0].satuan);
    
    get(base_url+"/doctor/cekstock_resep/"+idobat, false, function(response){
      responsedata = response.data;
      $('input[id=obstock'+counter+']').val(responsedata[0].stock);
    })

  })   

}
