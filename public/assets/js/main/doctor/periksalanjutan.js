$(document).ready(function() {

  $("#btnlanjutan").click(function(){
      $('#patientEditModal').modal('hide');
      $('#lanjutanModal').modal('show');

      var id = $(this).data('id');
      var idtr = $(this).data('idtr');
      
      //cek data pasien
      get(base_url+"/doctor/getPatient/"+id, false, function(response){
            responsedata = response.data;
            console.log("response",responsedata);
            //tanggal lab
            var tgl = new Date();
            var ttl = new Date(responsedata.patient_ttl);
            var diff = Math.abs(ttl - tgl);
            var diffyear = diff / 31556952000;
            var umur = Math.floor(diffyear)+" TH";

            $('#labModal #idtr_lab').val(idtr);
            $('#labModal #nmpasien_lab').val(responsedata.patient_nm);
            $('#labModal #alamat_lab').val(responsedata.patient_address);
            $('#labModal #email_lab').val(responsedata.patient_mail);
            $('#labModal #umur_lab').val(umur);
            $('#labModal #almtpasien_lab').val(responsedata.patient_address);

            $('#fisioModal #idtr_fisio').val(idtr);
            $('#fisioModal #nmpasien_fisio').val(responsedata.patient_nm);
            $('#fisioModal #alamat_fisio').val(responsedata.patient_address);
            $('#fisioModal #email_fisio').val(responsedata.patient_mail);
            $('#fisioModal #umur_fisio').val(umur);
            $('#fisioModal #almtpasien_fisio').val(responsedata.patient_address);

            $('#rujukanpoliModal #idtr_rujukan').val(idtr);
            $('#rujukanpoliModal #nmpasien_rujukan').val(responsedata.patient_nm);


            if (responsedata.gender == "P") {
                document.getElementById("g_p").checked = true;
                document.getElementById("fg_p").checked = true;
            } else if (responsedata.gender == "L") {
                document.getElementById("g_l").checked = true;
                document.getElementById("fg_l").checked = true;
            }
      })  

      //cek data periksa dokter
      get(base_url+"/doctor/getPeriksarujukan/"+idtr, false, function(response){
          responsedata = response.data;
          console.log("idtr",idtr);
          console.log("resp_rujukan",responsedata);

          $('#rujukanpoliModal #sanamnesa_rujukan').val(responsedata[0].sanamnesa);
          $('#rujukanpoliModal #oanamnesa_rujukan').val(responsedata[0].oanamnesa);
          $('#rujukanpoliModal #aanamnesa_rujukan').val(responsedata[0].aanamnesa);
          $('#rujukanpoliModal #panamnesa_rujukan').val(responsedata[0].panamnesa);

      })

      //cek data dokter
      get(base_url+"/doctor/get_det_pegawai", false, function(response){
            responsedata = response.data;
            console.log("response",responsedata);
           
            $('#labModal #dokter_lab').val(responsedata[0].nama);
            $('#labModal #almt_dokter_lab').val(responsedata[0].alamat);
            $('#labModal #telp_lab').val(responsedata[0].telepon);

            $('#fisioModal #dokter_fisio').val(responsedata[0].nama);
            $('#fisioModal #almt_dokter_fisio').val(responsedata[0].alamat);
            $('#fisioModal #telp_fisio').val(responsedata[0].telepon);
      })   

      //cek data anamnesa
        var narray=0;
        get(base_url+"/doctor/cekanamnesa/"+idtr, false, function(response){
            responsedata = response.data;
            narray = responsedata.length;
            console.log(idtr, narray);
            console.log(responsedata);

            if (narray != 0) {
                $('#labModal #anamnesa_lab').val(responsedata[0].anamnesa);
                $('#fisioModal #anamnesa_fisio').val(responsedata[0].anamnesa);
            }
        })

      $('#labModal #petugas_farmasi').val(1);
      document.getElementById("petugas_farmasi").readOnly = true;

      //tanggal lab
      var tgl = new Date();
      var y = tgl.getFullYear();
      var mm = String(tgl.getMonth() + 1).padStart(2, '0'); 
      var dd = String(tgl.getDate()).padStart(2, '0');
      var h = tgl.getHours();
      var mi = tgl.getMinutes();
      $('#labModal #tgllab').val(y + "-" + mm + "-" + dd);
      $('#fisioModal #tglfisio').val(y + "-" + mm + "-" + dd);
        
        //simpan lab
        $("#simpanlab").click(function(){
              //var cblab = [];
              // var x = $("input[name='cblab[]']").is(":selected");

              // $("input[name='cblab[]']").each(function() {

              //     if ( $(this).is(":selected") ) {
              //       cblab.push($(this).val());    
              //     } 
              // });

              // var elem = document.getElementsByClassName('');
              // var selectid = elem.getAttribute('data-select2-id');

              // const article = document.querySelector('.select2-selection__choice');
              var sclab = [];
              // // const article = document.querySelector('.select2-selection__choice');
              sclab = document.querySelectorAll('.select2-selection__choice');
              // const refs = document.querySelectorAll('[data-js="data-select2-id"]');
             var nsclab = sclab.length;
              // console.log("sclab",sclab);
              // console.log("nsclab",nsclab);

              var cblab = [];
              for (var i=0;i<nsclab;i++) {
                  $(sclab[i].attributes).each(function() {
                    
                    if (this.nodeName == "title") {
                      cblab[i] = this.nodeValue;
                    }
                  });
              }
              console.log("id lab = ", cblab);

              console.log($('#token').val())

              var formData = {
                "_token"        : $('#token').val(),
                  'idtr_lab'   : $('#idtr_lab').val(),
                  'tgllab'       : $('#tgllab').val(),
                  'cblab'      : cblab,
                }

                console.log(formData)

                send(base_url+"/doctor/simpanlab", formData, true, function(response){
                  console.log(response)
                });
          })


        //simpan fisio
        $("#simpanfisio").click(function(){
              var cbfisio = [];
              var x = $("input[name='cbfisio[]']").is(":checked");

              $("input[name='cbfisio[]']").each(function() {
                  if ( $(this).is(":checked") ) {
                    cbfisio.push($(this).val());    
                  } 
              });

              console.log($('#token').val())

              var formData = {
                "_token"        : $('#token').val(),
                  'idtr_fisio'   : $('#idtr_fisio').val(),
                  'tglfisio'       : $('#tglfisio').val(),
                  'cbfisio'      : cbfisio,
                }

                console.log(formData)

                send(base_url+"/doctor/simpanfisio", formData, true, function(response){
                  console.log(response)
                });
          })


        //simpan rujukan
        $("#simpanrujukan").click(function(){
              var cbrujukan = [];
              var x = $("input[name='cbrujukan[]']").is(":checked");

              $("input[name='cbrujukan[]']").each(function() {
                  if ( $(this).is(":checked") ) {
                    cbrujukan.push($(this).val());    
                  } 
              });

              console.log($('#token').val())

              var formData = {
                "_token"        : $('#token').val(),
                  'idtr_poli'   : idtr,
                  'tglrujukan'     : $('#tglrujukan').val(),
                  'cbrujukan'      : cbrujukan,
                }

                console.log(formData)

                send(base_url+"/doctor/simpanrujukan", formData, true, function(response){
                  console.log(response)
                });
          })


        //cetak lab
        $("#cetaklab").click(function(){
              // var id = $('#idperiksadokter_resep').val();
              // var a = document.createElement('a');
              // a.href="/doctor/cetak_resep/"+id;
              // a.target = '_blank';
              // document.body.appendChild(a);
              // a.click();
        })

          $("#btndoctorclose").click(function(){
          $('#patientEditModal').modal('hide');
          location.reload();
          }) 

    })

    $("#closemdllab").click(function(){
      $('#labModal').modal('hide');
      $('#patientEditModal').modal('show');
    })

    $("#closemdllanj").click(function(){
      $('#lanjutanModal').modal('hide');
      $('#patientEditModal').modal('show');
    })

    $("#btlab").click(function(){
      $('#lanjutanModal').modal('hide');
      $('#labModal').modal('show');
    })

    $("#btfisio").click(function(){
      $('#lanjutanModal').modal('hide');
      $('#fisioModal').modal('show');
    })

    $("#closemdlfisio").click(function(){
      $('#fisioModal').modal('hide');
      $('#patientEditModal').modal('show');
    })

    $("#btrujukanpoli").click(function(){
      $('#lanjutanModal').modal('hide');
      $('#rujukanpoliModal').modal('show');
    })
    $("#closemdlrujukan").click(function(){
      $('#rujukanpoliModal').modal('hide');
      $('#patientEditModal').modal('show');
    })

});

