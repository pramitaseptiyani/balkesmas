$(document).ready(function() {
    var id_tr_poli = $("#input-idtrpoli").val();
    $.ajax({
        type: 'GET',
        url: APIurl + "farmasi/resep/" + id_tr_poli,
        dataType: "json",
        success: function(data) {
            var result = data.data;
            for (let i = 0; i < result.length; i++) {
                const element = result[i];
                var nomor = i + 1;
                $("#body-table").append('<tr><td>' + nomor + '<td>' + element.nama_obat + '</td><td>' + element.jumlah_obat + '</td><td>' + element.aturan_pakai + '</td>')
            }

        },
        error: function(request, status, error) {
            alert(request.responseText);

        }
    });

    $.ajax({
        type: 'GET',
        url: APIurl + "farmasi/pasien/" + id_tr_poli,
        dataType: "json",
        success: function(data) {
            var result = data.data;
            $("#nama-pasien").append(result[0].patient_nm)
            $("#nomor-mr").append(result[0].patient_mr_number)
            $("#alergi-pasien").append(result[0].alergi_rwy)
            $("#vaksin-pasien").append(result[0].vaksinasi_rwy)
            $("#edit-MR").val(result[0].patient_mr_number)
            $("#edit-nama").val(result[0].patient_nm)
            $("#edit-nip").val(result[0].id_card)

        },
        error: function(request, status, error) {
            alert(request.responseText);

        }
    });

    resepstatus()

    $("#btn-obat-siap").click(function() {

        var csrf = $("#csrf_token").val();
        $.ajax({
            type: 'POST',
            url: APIurl + "farmasi/resep/siap",
            dataType: "json",
            headers: {
                "X-CSRF-TOKEN": csrf
            },

            data: {
                "id": id_tr_poli,
            },
            success: function(data) {
                swal("Updated!", "Resep Sudah Siap", "success");
                resepstatus()

            },
            error: function(request, status, error) {
                swal("Gagal!", "Resep Belum Siap", "error");
            }
        });
    });

    $("#btn-obat-diambil").click(function() {
        $('#ambilObatModal').modal('show');


    });

    $('.js-signature').jqSignature();
    $("#btn-clear-ttd").click(function() {
        $('.js-signature').jqSignature('clearCanvas');
        digital_sign_first = jQuery('.js-signature').jqSignature('getDataURL');
        $("#signatureJSON").val('');
        $('#submit-ambil').attr('disabled', true);
    })

    $('.js-signature').on('jq.signature.changed', function() {
        digital_sign_first = $('.js-signature').jqSignature('getDataURL');
        $("#signatureJSON").val(digital_sign_first);
        $('#submit-ambil').attr('disabled', false);
    });

    $("#submit-ambil").click(function() {
        ttd = $("#signatureJSON").val();
        csrf = $('#edit-token').val();
        id = id_tr_poli;

        $.ajax({
            type: 'POST',
            url: APIurl + "farmasi/resep/diambil",
            dataType: "json",
            headers: {
                "X-CSRF-TOKEN": csrf
            },

            data: {
                "id": id_tr_poli,
                "ttd": ttd,
            },
            success: function(data) {
                swal("Updated!", "Resep Sudah Diambil", "success");
                $('#ambilObatModal').modal('hide');
                resepstatus()

            },
            error: function(request, status, error) {
                swal("Gagal!", "Resep Belum Diambil", "error");
            }
        });
    })

    function resepstatus() {
        $.ajax({
            type: 'GET',
            url: APIurl + "farmasi/resep/status/" + id_tr_poli,
            dataType: "json",
            success: function(data) {
                var result = data;
                if (result == 0) {
                    document.getElementById('btn-obat-diambil').style.display = 'none';
                    document.getElementById('btn-obat-siap').style.display = 'inline';

                } else if (result == 1) {
                    document.getElementById('btn-obat-diambil').style.display = 'inline';
                    document.getElementById('btn-obat-siap').style.display = 'none';
                } else if (result == 2) {
                    document.getElementById('btn-obat-diambil').style.display = 'none';
                    document.getElementById('btn-obat-siap').style.display = 'none';
                    document.getElementById('text-obat-sudah-diambil').style.display = 'block';
                }

            },
            error: function(request, status, error) {
                alert(request.responseText);

            }
        });
    }
})