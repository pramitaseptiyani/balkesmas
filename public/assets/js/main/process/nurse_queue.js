$(document).ready(function() {

    var table = $('#nurseDatatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'nurse/patient/list',
        },
        columns: [        
            { data: "no_urut_perawat", name : "no_urut_perawat" },
            { data: "patient_mr_number", name : "patient_mr_number" },
            { data: "patient_nm", name : "patient_nm" },
            { data: "tgl_periksa", name : "tgl_periksa" },
            { data: 'periksa_perawat', name: 'periksa_perawat',
                render: function(data, type, row, meta){
                    if (data=='belum')
                        return '<span class="btn btn-xs btn-circle btn-success">Antri</span>&nbsp;&nbsp;';
                    else return '<span class="btn btn-xs btn-circle btn-danger">Selesai</span>&nbsp;&nbsp;';
                }
            },
            { data: 'id_periksa', name: 'id_periksa', className: 'text-center', 
                render: function(data, type, row, meta){

                    if(row['periksa_perawat']=='belum'){
                        var examinationButton = '<a href="'+base_path+'/nurse/patient/examination?id_periksa='+data+'&patient_nm='+row["patient_nm"]+'&no_urut_perawat='+row["no_urut_perawat"]+'" title="Periksa" id="examinationButton" data-id-periksa="'+data+'" class="label label-warning style="margin-right:1vw;">&nbsp;<i class="fa fa-stethoscope"></i></a>';
                        var skipButton = '<a href="#" title="Batal/Lewati" id="skipButton" data-id="'+data+'" class="label label-danger" style="margin-right:1vw;">&nbsp;<i class="fa fa-times"></i></a>';
                    }
                    else{
                        var examinationButton = '<a type="button" title="Periksa" id="examinationButton" class="label label-warning" style="margin-right:1vw;" disabled>&nbsp;<i class="fa fa-stethoscope"></i></a>';
                        var skipButton = '<a type="button" title="Batal/Lewati" id="skipButton" class="label label-danger" style="margin-right:1vw;" disabled>&nbsp;<i class="fa fa-times"></i></a>';
                    }
                    button = examinationButton+'&nbsp;&nbsp;'+skipButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 4, "asc" ], [ 0, "asc" ]]
    });

});