$(document).ready(function() {
    // $('#search-nip').select2({
    //     placeholder: 'Ketikan NIP',
    //     theme: "classic",
    //     minimumInputLength: 3,
    //     ajax: {
    //         dataType: 'json',
    //         url: base_path+'/receptionist/patient/search',
    //         delay: 800,
    //         data: function(params) {
    //             return {
    //                 nip: params.term,
    //             }
    //         },
    //         processResults: function (data, page) {
    //             var list = [];
    //             var result = data;
    //             $.each(result.data, function(index,item){
    //                 list.push({
    //                     id      : item.nip,
    //                     text    : item.nip +" - "+ item.nama,
    //                     nama    : item.nama,
    //                     satker  : item.satker,
    //                     satkerid : item.satkerid,
    //                     hp      : item.telp,
    //                     email   : item.email,
    //                     alamat  : item.alamat,
    //                     gender  : item.jenis_kelamin,
    //                 })
    //             });
    //             return {
    //                 results: list
    //             };
    //         },
    //     }
    // });

    $('#search-nip').select2({
        placeholder: 'Ketikan NIP',
        theme: "classic",
        minimumInputLength: 3,
        ajax: {
            dataType: 'json',
            url: base_path+'/simpeg/get_pegawai',
            delay: 800,
            data: function(params) {
                return {
                    nip: params.term
                }
            },
            processResults: function (data, page) {
                var list = [];
                var result = data;
                $.each(result, function(index,item){
                    list.push({
                        id      : item.nip,
                        text    : item.nip +" - "+ item.nama_pegawai,
                        nama    : item.nama_pegawai,
                        satker  : item.nama_satker,
                        satkerid  : item.kode_satker,
                        hp      : item.no_hp,
                        email   : item.email_dinas,
                        alamat  : item.alamat,
                        gender  : item.jenis_kelamin,
                        bpjs  : item.askes,
                    })
                });
                return {
                    results: list
                };
            },
        }
    });

    $('#search-nip').on('select2:select', function (e) {
        var data = e.params.data;
        var satker = data.satker
        var unit = satker.substring(0, satker.indexOf("-"));         

        $("#nama").val(data.nama);
        $("#nip").val(data.id);
        $("#unit").val(unit);
        $("#satkerid").val(data.satkerid);
        $("#hp").val(data.hp);
        $("#email").val(data.email);
        $("#alamat").val(data.alamat);
        $("#bpjs").val(data.bpjs);

        if(data.bpjs != null) $("#bpjs").val(data.bpjs);

        var year = data.id.substring(0, 4);
        var month = data.id.substring(4, 6);
        var day = data.id.substring(6, 8);
        var ttl = year+'-'+month+'-'+day;
        $("#date").val(ttl);

        if(data.gender == "P")
            $('#optionGenderP').prop('checked',true);
        else $('#optionGenderL').prop('checked',true);
    });

    $("#simpan").click(function(){

        // var hp = $('#hp').val()
        var email = $('#email').val()
        // var nip = $('#nip').val()

        // if(nip=='' || hp =='' || email == ''){
        if(email == ''){

            var alert_html = 
                    '<div class="add-alert alert alert-danger">'+
                      'Harap isi Required Field (bertanda *)'+  
                    '</div>';

            $('.alert-form').html(alert_html);
        }
        else{
            var nip = $('#nip').val();
            get(base_url+"/receptionist/patient/isExistNip?nip="+nip+"&id_kategori=1", false, function(response){

                console.log(response.data)

                if(response.data != null){
                  sweetalert('error', 'Peringatan', response.message)
                }
                else{
                    // var formData = {
                    //     "_token": "{{ csrf_token() }}",
                    //     "input" : {
                    //         'id_kategori'           : 1,
                    //         'id_detail_keluarga'    : null,
                    //         'nip_keluarga'          : null,
                    //         'patient_nm'            : $('#nama').val(),
                    //         'id_card'               : null,

                    //         'patient_ttl'           : null,
                    //         'patient_phone'         : null,
                    //         'patient_mail'          : null,
                    //         'jenis_kelamin'         : null,
                    //         'bpjs'                  : $('#bpjs').val(),
                    //         'patient_address'       : null,
                    //         'riwayat_penyakit'      : $('#riwayat_penyakit').val(),
                    //         'riwayat_turunan'       : $('#riwayat_turunan').val(),
                    //         'riwayat_alergi'        : $('#riwayat_alergi').val(),
                    //         'riwayat_vaksinasi'     : $('#riwayat_vaksinasi').val()
                    //     }
                    // }

                    var formData = {
                        // "_token": "{{ csrf_token() }}",
                        "_token"  : $('#token').val(),
                        'tipe'    : 'pegawai',
                        'kategori': 'pegawai',
                        'nama'    : $('#nama').val(),
                        'id_card' : $('#nip').val(),
                        'unit'    : $('#unit').val(),
                        'ttl'     : $('#date').val(),
                        'hp'      : $('#hp').val(),
                        'email'   : $('#email').val(),
                        'jenis_kelamin' : $('input[type=radio][name=optionGender]:checked').val(),
                        'bpjs'    : $('#bpjs').val(),
                        'alamat'  : $('#alamat').val(),
                        'riwayat_penyakit'  : $('#riwayat_penyakit').val(),
                        'riwayat_turunan'   : $('#riwayat_turunan').val(),
                        'riwayat_alergi'    : $('#riwayat_alergi').val(),
                        'riwayat_vaksinasi' : $('#riwayat_vaksinasi').val(),
                        'satkerid' : $('#satkerid').val(),
                        'mr_lama' : $('#mr_lama').val(),
                        'is_vip' : $('#is-vip').is(':checked'),
                    }

                    console.log(formData)

                    $("#overlay").fadeIn(300);　
                    send(base_url+"/receptionist/patient/tambah", formData, true, function(responseAdd){

                        $("#overlay").fadeOut(300);
                        // setTimeout(function(){ 
                        //     window.location = base_url+'/receptionist/patient';
                        // }, 1000);
                    })

                    // var HttpTimeout=setTimeout(ajaxTimeout, 60000); //10s

                }

            })
        }
    });
});