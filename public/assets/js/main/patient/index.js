$(document).ready(function() {
    var table = $('#patientDatatable').DataTable({
            
        ajax: {
            type: "GET",
            dataType: "json",
            url: APIurl+'receptionist/patient/list',
        },
        columns: [        
            { data: 'patient_mr_number', name: 'patient_mr_number' },
            { data: 'id_card', name: 'id_card' },
            { data: 'patient_nm', name: 'patient_nm' },
            { data: 'kategori_nm', name: 'kategori_nm' },
            { data: 'patient_address', name: 'patient_address' },
            { data: 'updated_at', name: 'updated_at' },
            { data: 'id', name: 'id', className: 'text-center', 
                render: function(data, type, row, meta){
                    var seeButton = '<button type="button" title="Lihat" id="seeButton" data-id="'+data+'" class="label label-primary menu-see-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>';
                    var editButton = '<button type="button" title="Edit" id="editButton" data-id="'+data+'" class="label label-primary menu-edit-button download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-pencil"></i></button>';
                    var sendMailButton = '<button type="button" title="Kirim Ulang Kartu" id="mailButton" data-id="'+data+'" class="label label-primary menu-mail-button mail-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-envelope"></i></button>';
                    var registerPoliButton = '<a href="'+base_path+'/receptionist/poli/add/'+data+'" title="Pendaftaran Poli" id="downloadButton" data-id="'+data+'" class="label label-warning download-"'+data+' style="margin-right:1vw;">&nbsp;<i class="fa fa-stethoscope"></i></a>';
                    
                    button = seeButton+'&nbsp;'+editButton+'&nbsp'+sendMailButton+'&nbsp;'+registerPoliButton;

                    return button;
                },
                searchable: false,
                sortable: false
            }
        ],
        order: [[ 5, "desc" ]]
    });

    //Click See detail of patient
    $('#patientDatatable').on( 'click', '.menu-see-button', function () {
        $('#patientDetailModal').modal('show');

        var id = $(this).data('id');
        get(base_url+"/receptionist/patient/get/"+id, false, function(response){
            responsedata = response.data;
            fillSeeResponse(responsedata)
            
        })   
    });

    //Click Edit detail of patient
    $('#patientDatatable').on( 'click', '.menu-edit-button', function () {
        $('#patientEditModal').modal('show');

        var id = $(this).data('id');
        get(base_url+"/receptionist/patient/get/"+id, false, function(response){
            responsedata = response.data;

            if(responsedata.id_kategori == 1){
                editPegawaiRule();
            }
            else{
                editGeneralRule();
            }

            $('#patientEditModal #edit-id').val(id);
            $('#patientEditModal #edit-id-kategori').val(responsedata.id_kategori);
            $('#patientEditModal #edit-satker-id').val(responsedata.id_satker);

            fillEditResponse(responsedata)

        })   
    });

    $("#patientEditModal #submit-edit").on("click", function(){
        var id = $(this).data('id');

        var id_kategori = $('#patientEditModal #edit-id-kategori').val();

        var edit_nama = $('#patientEditModal #edit-nama').val();
        var edit_ttl = $('#patientEditModal #edit-ttl').val();
        var edit_hp = $('#patientEditModal #edit-hp').val();
        var edit_email = $('#patientEditModal #edit-email').val();
        var edit_address = $('#patientEditModal #edit-address').val();
        var edit_satker_id = $('#patientEditModal #edit-satker-id').val();


        // if(id_kategori == 1){
        //     edit_nama = edit_ttl = edit_hp  = null;
        //     edit_email = edit_address = edit_satker_id = null;
        // }


        var gender = $("input[name='optionGender']:checked").val();

        var sendData = {
            "_token"        : $('#patientEditModal #edit-token').val(),
            "input"         : [
                $('#patientEditModal #edit-id').val(), //id
                edit_nama, //nama
                $('#patientEditModal #edit-nip').val(), //id_card
                edit_ttl, //ttl
                edit_hp, // phone
                edit_email, //email
                $('#patientEditModal #edit-bpjs').val(), //bpjs
                edit_address,
                $('#patientEditModal #edit-riwayat-penyakit').val(),
                $('#patientEditModal #edit-penyakit-turunan').val(),
                $('#patientEditModal #edit-riwayat-alergi').val(),
                $('#patientEditModal #edit-riwayat-vaksinasi').val(),
                $('#patientEditModal #edit-MR').val(), //mr
                edit_satker_id, //satker_id
                gender   
            ]
        }

        console.log(sendData)
        edit(sendData);
    });

});

function edit(data) {
    send(base_url+"/receptionist/patient/edit", data, true, function(response){
        setTimeout(function(){
            location.reload()
        }, 500);
    })
}

function editPegawaiRule(){
    $('#patientEditModal #edit-hp').prop('disabled', true);
    $('#patientEditModal #edit-email').prop('disabled', true);
    $('#patientEditModal #edit-address').prop('disabled', true);
}

function editGeneralRule(){
    $('#patientEditModal #edit-hp').prop('disabled', false);
    $('#patientEditModal #edit-email').prop('disabled', false);
    $('#patientEditModal #edit-address').prop('disabled', false);
}   

function fillSeeResponse(responsedata) {
    $('#patientDetailModal #detail-MR').val(responsedata.patient_mr_number);
    $('#patientDetailModal #detail-nama').val(responsedata.patient_nm);
    $('#patientDetailModal #detail-nip').val(responsedata.id_card);
    $('#patientDetailModal #detail-bpjs').val(responsedata.bpjs);
    $('#patientDetailModal #detail-ttl').val(responsedata.patient_ttl);
    $('#patientDetailModal #detail-unit').val(responsedata.patient_unit);
    $('#patientDetailModal #detail-email').val(responsedata.patient_mail);
    $('#patientDetailModal #detail-hp').val(responsedata.patient_phone);
    $('#patientDetailModal #detail-gender').val(responsedata.gender);
    $('#patientDetailModal #detail-address').val(responsedata.patient_address);
    $('#patientDetailModal #detail-riwayat-penyakit').val(responsedata.diag_derita);
    $('#patientDetailModal #detail-penyakit-turunan').val(responsedata.diag_turunan);
    $('#patientDetailModal #detail-riwayat-alergi').val(responsedata.alergi_rwy);
    $('#patientDetailModal #detail-riwayat-vaksinasi').val(responsedata.vaksinasi_rwy);
}

function fillEditResponse(responsedata){
    if(responsedata.gender == "Perempuan")
        $('#optionGenderP').prop('checked',true);
    else $('#optionGenderL').prop('checked',true);

    $('#patientEditModal #edit-MR').val(responsedata.patient_mr_number);
    $('#patientEditModal #edit-nama').val(responsedata.patient_nm);
    $('#patientEditModal #edit-nip').val(responsedata.id_card);
    $('#patientEditModal #edit-bpjs').val(responsedata.bpjs);
    $('#patientEditModal #edit-ttl').val(responsedata.patient_ttl);
    $('#patientEditModal #edit-unit').val(responsedata.patient_unit);
    $('#patientEditModal #edit-email').val(responsedata.patient_mail);
    $('#patientEditModal #edit-hp').val(responsedata.patient_phone);
    $('#patientEditModal #edit-gender').val(responsedata.gender);
    $('#patientEditModal #edit-address').val(responsedata.patient_address);
    $('#patientEditModal #edit-riwayat-penyakit').val(responsedata.diag_derita);
    $('#patientEditModal #edit-penyakit-turunan').val(responsedata.diag_turunan);
    $('#patientEditModal #edit-riwayat-alergi').val(responsedata.alergi_rwy);
    $('#patientEditModal #edit-riwayat-vaksinasi').val(responsedata.vaksinasi_rwy);
}

$('#patientDatatable').on( 'click', '.menu-mail-button', function () {
        

        var id = $(this).data('id');

        console.log(id)
        $("#overlay").fadeIn(300);　
        get(base_url+"/receptionist/patient/resend_mr/"+id, true, function(response){
            responsedata = response.data;
            $("#overlay").fadeOut(300);
            

        })   
    });

// var file_table_options = {
//     id:"patientDatatable",
//     ajax: function (data, callback, settings) {

//         var results = {
//             "isSuccess":true,
//             "data":
//                 {
//                     "draw":1,
//                     "recordsTotal":2,
//                     "recordsFiltered":2,
//                     "data":[
//                         {
//                             "no_MR":"SET90328392.02",
//                             "name":"Shaka Nagano",
//                             "date_of_birth":"1995/02/28",
//                             "category":"Pegawai",
//                             "NIP/NIK":199502282019012001,
//                             "address":"Simogyo-ku, Kyoto, Kyoto, Japan 600-8216",
//                             "created_at":"2019-08-31 10:25:21",
//                             "updated_at":"2019-08-31 10:25:21"
//                         },
//                         {
//                             "no_MR":"UMU98392902.03",
//                             "name":"James Smith",
//                             "date_of_birth":"1992/02/29",
//                             "category":"Umum",
//                             "NIP/NIK": 3310262902920002,
//                             "address":"Shenton Way 32-08 UIC Building, 068808, Singapore",
//                             "created_at":"2019-09-01 10:25:21",
//                             "updated_at":"2019-09-01 10:25:21"
//                         }
//                     ]
//                 },
//                 "message":"success"
//             };

//         // $.post(APIurl+'data/datatable/'+unit_id,data,function(result){

//             if(results.isSuccess){
//                 result = results.data

//                 console.log(result);

//                 result.data.map(function(x){
//                     var no_MR = '<span class="label label-rounded bg-purple-400 label-field">'+x.no_MR+'</span>&nbsp;&nbsp;';

//                     x.no_MR = no_MR;   
                    
//                     var seeButton = '<button type="button" data-toggle="modal" data-target="#patientDetailModal" title="Lihat" id="downloadButton" data-id="'+x.id+'" class="label label-primary download-"'+x.id+' style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>';
//                     var registerPoliButton = '<a href="'+base_path+'/receptionist/poli/add" title="Pendaftaran Poli" id="downloadButton" data-id="'+x.id+'" class="label label-warning download-"'+x.id+' style="margin-right:1vw;">&nbsp;<i class="fa fa-stethoscope"></i></a>';
//                     x.button = seeButton+'&nbsp;&nbsp'+registerPoliButton;
//                 })
//                 callback(result);
//             }

            
//         // },"json");
//     },
//     columns: [
//         { "data": "no_MR" },
//         { "data": "name" },
//         { "data": "date_of_birth" },
//         { "data": "category" },
//         { "data": "NIP/NIK" },
//         { "data": "address" },
//         { "data": "button" }
//     ],
//     callback:function(){
//     },
//     external:{
//     }
// }

// var patient_table = new Table(file_table_options);