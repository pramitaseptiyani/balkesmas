$(document).ready(function() {

});

var file_table_options = {
    id:"patientDatatable",
    ajax: function (data, callback, settings) {

        var results = {
            "isSuccess":true,
            "data":
                {
                    "draw":1,
                    "recordsTotal":2,
                    "recordsFiltered":2,
                    "data":[
                        {
                            "no_MR":"SET90328392.02",
                            "name":"Shaka Nagano",
                            "poli_category":"Gigi",
                            "date":"2019/08/01",
                            "no_queue_nurse":1,
                            "no_queue_poli" : 1,
                            "status": "Selesai",
                            "created_at":"2019-08-01 10:25:21",
                            "updated_at":"2019-08-01 10:25:21"
                        },
                        {
                            "no_MR":"UMU98392902.03",
                            "name":"James Smith",
                            "poli_category":"Umum",
                            "date":"2019/09/03",
                            "no_queue_nurse":2,
                            "no_queue_poli" : 1,
                            "status": "Belum dilayani",
                            "created_at":"2019-09-03 10:25:21",
                            "updated_at":"2019-09-03 10:25:21"
                        }
                    ]
                },
                "message":"success"
            };

        // $.post(APIurl+'data/datatable/'+unit_id,data,function(result){

            if(results.isSuccess){
                result = results.data

                console.log(result);

                result.data.map(function(x){
                    var no_MR = '<span class="label label-rounded bg-purple-400 label-field">'+x.no_MR+'</span>&nbsp;&nbsp;';

                    x.no_MR = no_MR;   
                    
                    var seeButton = '<button type="button" data-toggle="modal" data-target="#poliDetailModal" title="Lihat" id="downloadButton" data-id="'+x.id+'" class="label label-primary download-"'+x.id+' style="margin-right:1vw;">&nbsp;<i class="fa fa-eye"></i></button>';
                    x.button = seeButton;
                })
                callback(result);
            }

            
        // },"json");
    },
    columns: [
        { "data": "no_MR" },
        { "data": "name" },
        { "data": "poli_category" },
        { "data": "date" },
        { "data": "no_queue_nurse" },
        { "data": "no_queue_poli" },
        { "data": "status" },
        { "data": "button" }
    ],
    callback:function(){
    },
    external:{
    }
}

var patient_table = new Table(file_table_options);