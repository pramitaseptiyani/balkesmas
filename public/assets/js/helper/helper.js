function send(url,jsonData, is_sweetalert=true, callback){
     $.ajax({
        url : url,
        type: "POST",
        data: jsonData,
        dataType: 'json',
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
        dataType: "JSON",
        success: function(response) {
            if(callback!=null){
                callback(response);
            }
            if(is_sweetalert)
                sweetalert('success',response.title,response.message)
        },
         error: function (jqXHR, responseText, error) {
            response = jqXHR.responseJSON
            if(is_sweetalert)
                sweetalert('error', response.title, response.message)
        },
    });
}

function get(url, is_sweetalert=true, callback){
     $.ajax({
        url : url,
        type: "GET",
        dataType: "JSON",
        success: function(response) {
            if(callback!=null){
                callback(response);
            }
            if(is_sweetalert) 
                sweetalert('success',response.title,response.message)
        },
         error: function (jqXHR, responseText, error) {
            response = jqXHR.responseJSON
            if(is_sweetalert)
                sweetalert('error', response.title, response.message)
        },
    });
}

function sweetalert(type,title,text) {
    // Swal.fire({
    //     type: type,
    //     title: title,
    //     text: text,
    // });

    swal({
        type: type,
        title: title,
        text: text,
    });

}

function ajaxTimeout(){
    $("#overlay").fadeOut(300);　
    sweetalert('error', "Request Timed Out", "Failed to process data")
}

function showCancelAlert(title, callback, id) {
    swal({
        title: title,
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya",
        cancelButtonText: "Batal",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            callback(id)
        } else {
            swal("Dibatalkan", "", "error");
        }
    });
}

function actionButton(isEdit,isArchive,isShare){
    var html = "";
    if(isShare) html+= '<a href="#" title="share" class="label bg-green shareButton"><i class="icon-share3" style="font-size: 10px;"></i></a>&nbsp;&nbsp;';
    if(isEdit) html+= '<a href="#" id="editButton" title="edit" class="label bg-teal"><i class="icon-pencil3" style="font-size: 10px;"></i></a>&nbsp;&nbsp;';
    if(isArchive) html+= '<a href="#" id="archiveButton" class="btn btn-sm btn-danger" style="margin-right:1vw;">&nbsp;<i class="fa fa-trash"></i></a>';
      else
          html+='<a href="#" id="activateButton" title="Activate" class="label bg-green"><i class="icon-checkmark4" style="font-size: 10px;"></i></a>';

    return html;
}

function Table(options){

    if(options.external==undefined) options.external = {};   
    if(options.copyBtn == undefined){
        options.copyBtn = 
        {
            extend: 'copyHtml5',
            className: 'btn btn-default',
            exportOptions: {
                columns: ':visible:not(.not-export-col)'
            },
            footer: false
        };
    }
    if(options.excelBtn == undefined){
        options.excelBtn = {
            extend: 'excelHtml5',
            className: 'btn btn-default',
            exportOptions: {
                columns: ':visible:not(.not-export-col)'
            },
            footer: false
        };
    } 
    if(options.pdfBtn == undefined){
        if(options.pdfOrientation == undefined){
            options.pdfOrientation = 'portrait'
        }

        options.pdfBtn = {
            extend: 'pdfHtml5',
            className: 'btn btn-default',
            orientation: options.pdfOrientation,
            pageSize: 'LEGAL',
            background:  '',
            footer: function(currentPage, pageCount) { return currentPage.toString() + ' of ' + pageCount; },
            header: 'simple text',
            exportOptions: {
                columns: ':visible:not(.not-export-col)'
            },
            footer: false
        };
    }
    if(options.customBtn == undefined){
        options.customBtn = {};
    }
    if (options.fixedColumns == undefined) {
        options.fixedColumns = {
            leftColumns: 0,
            rightColumns: 1
        }
    } 

        var basicOptions = {
            "serverSide": true,
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            ajax:options.ajax,
            columns:options.columns,
            buttons: {
                buttons: [
                    options.copyBtn,
                    options.excelBtn,
                    options.pdfBtn,
                    // {
                    //     // extend: 'colvis',
                    //     text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    //     className: 'btn bg-blue btn-icon'
                    // },
                    // options.customBtn
                ]
            },
            columnDefs:options.columnDefs,
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                //$(nRow).children("td").css("overflow", "hidden");
                $(nRow).children("td:last").css("white-space", "nowrap");
                //$(nRow).children("td").css("text-overflow", "ellipsis");
            },
            scrollX:        true,
            fixedColumns:options.fixedColumns,
            drawCallback: function () {
                this.api().columns().every( function(id) {
                    var column = this;
                    var select = null;
                    if($(column.footer()).find(".filter-select").length==0){
                        select = $('<select class="filter-select form-control" data-placeholder="Filter"><option value=""></option></select>')
                            .appendTo($(column.footer()))
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                            $(this).data('value',$(this).val());
                                column
                                    .search( val ? val : '', true, false )
                                    .draw();
                            });
                    }else{
                        select = $(column.footer()).find(".filter-select");
                        select.find('option').not(':first-child').remove();
                    }

                    if(column.ajax.params().columns[id].searchable==false){
                        select.remove();
                    }else{

                        column.data().unique().sort().each( function (d, j) {
                            if(d!=null && d!=undefined){
                                if(d['$date'] && d['$date'] != null){
                                    var date = moment(parseInt(d['$date']['$numberLong'])).format("DD/MM/YYYY");
                                    select.append('<option value="'+parseInt(d['$date']['$numberLong'])+'">'+date+'</option>')
                                }else{
                                    select.append('<option value="'+d+'">'+d+'</option>')
                                }
                            }


                        });
                        select.val(select.data("value"));
                    }
                });
            },
        }
     this.init = $('#'+options.id).DataTable($.extend(basicOptions,options.external))
     this.get = function(){return this.init;}
     this.edit = function(callback){
        $('#'+options.id+' tbody').on( 'click', '#editButton', function (e) {
            var data = $('#'+options.id).DataTable().row( $(this).parents('tr') ).data();
            callback(data);
            e.preventDefault();
        });
     }
     this.test = function(callback){
        $('#'+options.id+' tbody').on( 'click', '#testButton', function (e) {
            var data = $('#'+options.id).DataTable().row( $(this).parents('tr') ).data();
            callback(data);
            e.preventDefault();
        });
     }
     this.copy = function(callback){
        $('#'+options.id+' tbody').on( 'click', '.copyButton', function (e) {
            var data = $('#'+options.id).DataTable().row( $(this).parents('tr') ).data();
            callback(data);
            e.preventDefault();
        });
     }
     this.refresh = function () {
         $('#'+options.id).DataTable().ajax.reload();
     }
     this.delete = function(callback){
        var base = this;
        $('#'+options.id+' tbody').on( 'click', '#delButton', function (e) {
            var data = $('#'+options.id).DataTable().row( $(this).parents('tr') ).data();
            if(options.external.deleteURL != undefined){
                // Alert.confirm(null,null,function(e,notice,val){
                //     if (val){
                    send(options.external.deleteURL+"/"+data._id.$oid,{action:'archive'},function(data){
                        // Alert.success();
                        base.refresh();
                        options.callback();
                    });
                    // }
                // })
            }else{
                callback(data)
            }
            e.preventDefault();
        });

        $('#'+options.id+' tbody').on( 'click', '#activateButton', function (e) {
            var data = $('#'+options.id).DataTable().row( $(this).parents('tr') ).data();
            if(options.external.deleteURL != undefined){
                Alert.confirm(null,null,function(e,notice,val){
                    if (val){
                        send(options.external.deleteURL+"/"+data._id.$oid,{action:'activate'},function(data){
                            Alert.success();
                            $('#'+options.id).DataTable().ajax.reload();
                            options.callback();
                        });
                    }
                })
            }else{
                callback(data)
            }
            e.preventDefault();
        });

     }
     this.click = function(selector,callback){
        $('#'+options.id+' tbody').on( 'click', selector, function (e) {
            var data = $('#'+options.id).DataTable().row( $(this).parents('tr') ).data();
            callback(data);
            e.preventDefault();
        });
     }
     //auto delete
     if(options.external.deleteURL != undefined){
        this.delete();
     }

}