<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'UserController@dologin');


Route::group(['prefix' => 'reservasi'], function(){
	Route::get('/', 'Reservasi\ViewController@index');

	Route::group(['prefix' => 'api'], function(){
		Route::get('/get_pegawai', 'DataController@getSimpeg');

		Route::post('/confirm', 'Reservasi\ReservasiController@insertData');
		Route::post('/jumlah', 'Reservasi\ReservasiController@jumlah');
		Route::post('/patient_by_date', 'Reservasi\ReservasiController@getpatientByDate');
		Route::post('/doktor_working_day', 'Reservasi\ReservasiController@getDoctorByDate');
		Route::post('/doktor_schedule', 'Reservasi\ReservasiController@getDoctorSchedule');
		Route::post('/date_availability', 'Reservasi\ReservasiController@availabilityByDate');

		Route::post('/patient_by_MR', 'Reservasi\ReservasiController@getpatientByMR');
		Route::post('/patient_by_poli', 'Reservasi\ReservasiController@getpatientByPoli');
		Route::post('/isExist_reservasi', 'Reservasi\ReservasiController@periksaIsExist');
	});
});

Route::group(['prefix' => 'registration'], function(){
	Route::get('/patient/add', 'Registration\RegistrationController@addPatient');
	Route::get('/patient/addgeneral','Registration\RegistrationController@addGeneralPatient');
	Route::get('/patient/addfamily','Registration\RegistrationController@addFamilyPatient');
});

Route::group(['prefix' => 'api_public'], function(){
	Route::get('/simpeg/get_pegawai', 'DataController@getSimpeg');
	Route::get('/patient/isExistNip', 'DataController@nipIsExist');
	Route::post('/patient/tambah', 'DataController@addNew');

	Route::get('/family/active/select2', 'DataController@select2FamilyActive');
	Route::post('/family/category', 'DataController@getFamilyCategory');
});

Route::get('/survei', 'Survei\ViewController@index');


Route::get('/api/poli/select2', 'Reservasi\ReservasiController@poli_list');
Route::post('/api/dokterpoli/select2', 'Reservasi\ReservasiController@dokterpoli_list');
Route::get('/api/satker/select2', 'Admin\EmployeeController@select2Satker');


/* Auth Middleware */
Route::group(['middleware' => 'auth'], function(){
	Route::get('/home', 'HomeController@index')->name('home');

	Route::group(['prefix' => 'permission'], function(){
		Route::get('/menu/{user_id}', 'Permission\PermissionController@menu');
	});

	# Admin
		Route::group(['prefix' => 'admin'], function(){
			/* View */
			Route::get('/user', 'Admin\AdminController@user');
			Route::get('/permission', 'Admin\AdminController@permission');
			Route::get('/medicine', 'Admin\AdminController@medicine');
			Route::get('/employee', 'Admin\AdminController@employee');
            Route::get('/family','Admin\AdminController@getListEmpl');
			Route::get('/lab', 'Admin\AdminController@labLv1');
			Route::get('/lab2/{id_parent}', 'Admin\AdminController@labLv2');
			Route::get('/lab3/{id_parent1}/{id_parent2}', 'Admin\AdminController@labLv3');
            Route::get('/dokter','Admin\AdminController@dokter');
            Route::get('/dokter/jadwal_dokter/{id}','Admin\AdminController@jadwal_dokter');

			/* API */
			Route::group(['prefix' => 'api'], function(){
            /* User */
            Route::get('/user/list/{is_active}', 'Admin\UserController@getList');
            Route::post('/user/add', 'Admin\UserController@add');
            Route::get('/user/get/{id}', 'Admin\UserController@detail');
            Route::post('/user/edit', 'Admin\UserController@edit');
            Route::get('/user/archive/{id}', 'Admin\UserController@archive');
            Route::get('/user/activate/{id}', 'Admin\UserController@activate');
            Route::get('/role/select2', 'Admin\RoleController@select2');


            /* Keluarga Pegawai */

            Route::get('/family/get/{kdsatker}/{nip}','Admin\AdminController@listFamily');
            Route::get('/family/selectkdkeluarga', 'Admin\AdminController@selectkdkeluarga');
            Route::get('/family/selectkdkerja', 'Admin\AdminController@selectkdkerja');
            Route::get('/family/member/get/{id}', 'Admin\AdminController@getFamilyMember');
            Route::post('/family/addMember', 'Admin\AdminController@addMember');
            Route::post('/family/editMember/{id}', 'Admin\AdminController@editMember');
            Route::get('/family/removeMember/{id}', 'Admin\AdminController@removeMember');

            /* Permission */
            Route::get('/permission/list/{is_active}', 'Admin\PermissionController@getList');
            Route::post('/permission/add', 'Admin\PermissionController@add');
            Route::post('/permission/edit', 'Admin\PermissionController@edit');
            Route::get('/permission/get/{id}', 'Admin\PermissionController@detail');
            Route::get('/role/archive/{id}', 'Admin\RoleController@archive');
            Route::get('/role/activate/{id}', 'Admin\RoleController@activate');
            Route::get('/menu/select2', 'Admin\PermissionController@menuSelect2');

            /* Medicine */
            Route::post('/medicine/list/{is_active}', 'Admin\MedicineController@getList');
            Route::post('/medicine/add', 'Admin\MedicineController@add');
            Route::get('/medicine/get/{id}', 'Admin\MedicineController@detail');
            Route::post('/medicine/edit', 'Admin\MedicineController@edit');
            Route::get('/medicine/archive/{id}', 'Admin\MedicineController@archive');
            Route::get('/medicine/activate/{id}', 'Admin\MedicineController@activate');

            /* Employee */
            // Route::get('/employee/list/{is_active}', 'Admin\EmployeeController@getList');
            Route::post('/employee/list/{is_active}', 'Admin\EmployeeController@getList');
            Route::get('/employee/get/{id}', 'Admin\EmployeeController@detail');
            Route::get('/satker/select2', 'Admin\EmployeeController@select2Satker');
            Route::post('/employee/addPatient', 'Admin\EmployeeController@addPatient');

            /* Lab */
            Route::post('/lab/list/level1/{is_active}', 'Admin\LabController@getList1');
            Route::post('/lab/list/level2/{id_parent}', 'Admin\LabController@getList2');
            Route::get('/lab/list/level3/{id_parent}', 'Admin\LabController@getList3');
            Route::get('/lab/list/all/0', 'Admin\LabController@inactiveList');
            Route::post('/lab/add', 'Admin\LabController@add');
            Route::get('/lab/get/{id}', 'Admin\LabController@detail');
            Route::post('/lab/edit', 'Admin\LabController@edit');
            Route::get('/lab/archive/{id}', 'Admin\LabController@archive');
            Route::get('/lab/activate/{id}', 'Admin\LabController@activate');

            Route::get('/lab/select2', 'Admin\LabController@select2');

            /* Dokter */
            Route::get('/dokter/list/{is_active}', 'Admin\AdminController@getListDokter');
            /* Jadwal Dokter */
            Route::get('/jadwal_dokter/list/{id}','Admin\AdminController@getListJadwalDokter');
            Route::get('/jadwal_dokter/get/{id}','Admin\AdminController@getJadwalDokter');
            Route::get('/jadwal_dokter/selecthari/{id}', 'Admin\AdminController@selectHari');
            Route::post('/jadwal_dokter/add', 'Admin\AdminController@add');
            Route::post('/jadwal_dokter/edit/{id}', 'Admin\AdminController@edit');
            Route::get('/jadwal_dokter/remove/{id}','Admin\AdminController@del_tr_jadwal_dokter');

			});

		});

		Route::get('user', 'UserController@index');

	# Receptionist (Registration Admin)
		Route::group(['prefix' => 'receptionist/patient'], function(){
			#POST
			// Route::post('/tambah', 'DataController@add');
			Route::post('/tambah', 'DataController@addNew');

			#GET
			Route::get('/', 'ReceptionistController@patient');
			Route::get('/addMenu', 'ReceptionistController@addPatientMenu');


			Route::get('/add', 'ReceptionistController@addPatient');
			Route::get('/add_general','ReceptionistController@addGeneralPatient');
			Route::get('/add_family','ReceptionistController@addFamilyPatient');
			Route::get('/search', 'ReceptionistController@searchNIP');

			Route::get('/isExistNip', 'DataController@nipIsExist');
			Route::get('/get/{id}', 'DataController@getPatient');
			Route::post('/edit', 'DataController@editPatient');

			Route::get('/resend_mr/{id}', 'DataController@resendMR');

			Route::get('/list', 'DataController@list');
			Route::get('/sp', 'DataController@SP');

		});

		Route::group(['prefix' => 'receptionist/family'], function(){
			Route::get('/active/select2', 'DataController@select2FamilyActive');
			Route::post('/category', 'DataController@getFamilyCategory');

		});

		Route::group(['prefix' => 'receptionist/poli'], function(){
			Route::get('/', 'ReceptionistController@poli');
			Route::get('/add/{id}',
				'ReceptionistController@addPoli');

			#API
			Route::post('/isExist', 'DataController@periksaIsExist');
			Route::post('/register', 'DataController@registerPoli');
		});

		Route::group(['prefix' => 'receptionist/reservasi'], function(){
			Route::get('/', 'Reservasi\ViewController@datatable');

			Route::get('/list', 'Reservasi\ReservasiController@getList');

		});

	# Finish Action
		Route::group(['prefix' => 'patient/finish'], function(){
			Route::get('/signature', 'PatientController@finishSignature');
			Route::get('/acknowledgement', 'PatientController@acknowledgement');
		});

	# Nurse
		Route::group(['prefix' => 'nurse'], function(){
			Route::get('/patient', 'NurseController@patient');

			// Route::get('/patient/queue', 'NurseController@queue');
			Route::get('/patient/examination', 'NurseController@examination');

			Route::get('/patient/list', 'NurseController@getList');

			//api
			Route::post('/api/exam/add', 'NurseController@insertExamination');
		});

		Route::group(['prefix' => 'simpeg'], function(){
			Route::get('/get_pegawai', 'DataController@getSimpeg');
		});

	# Doctor
		Route::group(['prefix' => 'doctor'], function(){
			Route::get('/', 'Doctor\DoctorController@index');
			Route::get('/list','Doctor\DoctorController@list');
			Route::get('/listpasien','Doctor\DoctorController@listpasien');
			Route::get('/cekanamnesa/{idtr}','Doctor\DoctorController@cekanamnesa');
			Route::get('/cek_tr_resep/{idtr}','Doctor\DoctorController@cek_tr_resep');
			Route::get('/cek_tr_det_resep/{idtrresep}','Doctor\DoctorController@cek_tr_det_resep');
			Route::get('/del_tr_det_resep/{idtrresep}','Doctor\DoctorController@del_tr_det_resep');
			Route::get('/del_tr_resep/{idtr}','Doctor\DoctorController@del_tr_resep');

			Route::post('/simpan_periksa', 'Doctor\DoctorController@simpan_periksa');
			Route::post('/update_periksa', 'Doctor\DoctorController@update_periksa');
			Route::get('/getPatient/{id}', 'Doctor\DoctorController@getpatient');
			Route::get('/getPeriksarujukan/{idtr}', 'Doctor\DoctorController@getPeriksarujukan');
			Route::get('/donePatient/{id}', 'Doctor\DoctorController@donepatient');
			Route::get('/donePatientRujuk/{idrujuk}', 'Doctor\DoctorController@donePatientRujuk');
			Route::get('/get_list_pasien_dokter', 'Doctor\DoctorController@get_list_pasien_dokter');
			Route::get('/get_data_antrian_rujukan_poli', 'Doctor\DoctorController@get_data_antrian_rujukan_poli');
			Route::get('/get_history_dokter/{id}', 'Doctor\DoctorController@get_history_dokter');
			Route::get('/get_history_resep/{id}', 'Doctor\DoctorController@get_history_resep');
			Route::get('/get_history_lab/{id}', 'Doctor\DoctorController@get_history_lab');
			Route::get('/get_history_fisio/{id}', 'Doctor\DoctorController@get_history_fisio');

			#RESEP
			Route::get('/searchobatnama', 'Doctor\DoctorController@search_obat_bynama');
			Route::get('/cekobatid/{obnama}', 'Doctor\DoctorController@cek_obat_id');
			Route::get('/cekstock_resep/{idobat}', 'Doctor\DoctorController@cekstock_resep');
			Route::post('/simpanresep', 'Doctor\DoctorController@simpanresep');
			Route::get('/cetak_resep/{id}', 'Doctor\DoctorController@cetak_resep');

			#PERIKSA LANJUTAN
			Route::get('/get_det_pegawai', 'Doctor\DoctorController@get_det_pegawai');
			Route::post('/simpanlab', 'Doctor\DoctorController@simpanlab');
			Route::post('/simpanfisio', 'Doctor\DoctorController@simpanfisio');
			Route::post('/simpanrujukan', 'Doctor\DoctorController@simpanrujukan');


		});

	# Lab
		Route::group(['prefix' => 'lab'], function(){
			Route::get('/', 'Lab\LabController@index');
			Route::get('/listlab','Lab\LabController@listlab');

		});

		Route::get('/user/home', 'UserController@home');

	# Farmasi
	Route::group(['prefix' => 'farmasi'], function () {
		Route::get('/', 'Farmasi\FarmasiController@index');
		Route::get('/listpasien', 'Farmasi\FarmasiController@listpasien');
		Route::get('/obatpasien/{id}', 'Farmasi\FarmasiController@obatpasien');
		Route::get('/resep/{id}', 'Farmasi\FarmasiController@getdataresep');
		Route::get('/pasien/{id}', 'Farmasi\FarmasiController@getdatapasien');
		Route::post('/resep/siap', 'Farmasi\FarmasiController@updateresepsiap');
		Route::post('/resep/diambil', 'Farmasi\FarmasiController@updateresepdiambil');
		Route::get('/resep/status/{id}', 'Farmasi\FarmasiController@getresepstatus');
	});

});


