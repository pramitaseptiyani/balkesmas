<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pegawai;
use App\Models\Pasien;
use Auth;

/**
 * Class ReceptionistController
 */
class ReceptionistController extends Controller
{
    /**
     * View to show all patients datatable
     */
    public function patient(){
        $data['name'] = Auth::user()->name;
      	return view('patient/index', $data);
    }

    /**
     * View to add patient with NIP
     */
    public function addPatient(){
        $data['name'] = Auth::user()->name;
        return view('patient/add', $data);
    }

    /**
     * View to add patient with NIP
     */
    public function addPatientMenu(){
        $data['name'] = Auth::user()->name;
        return view('patient/addMenu', $data);
    }

    /**
     * View to add general patient with NIK
     */
    public function addGeneralPatient(){
        $data['name'] = Auth::user()->name;
        return view('patient/addGeneral', $data);
    }

    /**
     * View to add family patient with Reference NIP
     */
    public function addFamilyPatient(){
        $data['name'] = Auth::user()->name;
        return view('patient/addFamily', $data);
    }

    /**
     * View to show all poli datatable
     */
    public function poli(){
        $data['name'] = Auth::user()->name;
        return view('poli/index', $data);
    }

    /**
     * View to add/register to poli
     */
    public function addPoli($id){
        $data['name'] = Auth::user()->name;
        $data['detail'] = Pasien::where('id', $id)->first();
        return view('poli/add', $data);
    }

    public function searchNIP(Request $request){
        $nip = $request->input('nip');
        $pegawai = Pegawai::where('nip', 'LIKE', $nip.'%')
                    ->orWhere('nama', 'LIKE', $nip.'%')
                    ->whereIn('satker', [
                        "SEKRETARIAT JENDERAL",
                        "DIREKTORAT JENDERAL ADMINISTRASI HUKUM UMUM",
                        "INSPEKTORAT JENDERAL",
                        "DIREKTORAT JENDERAL PERATURAN PERUNDANG UNDANGAN"])
                    ->limit(20)
                    ->get();
        return json_encode($pegawai);
    }

}
